<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::post('/register', 'Auth\RegisterController@customRegister')->name('new.register');

Route::get('email-verify/{id}/{vcode}', 'Auth\RegisterController@verifyEmail')->name('email.verify');
Route::post('verify-mobile', 'Auth\RegisterController@verifyMobile')->name('check.mobile.verify');

Route::get('email-verified','Auth\RegisterController@emailVerifiedMsg')->name('user.email.verified.msg');
Route::get('email-verified-failed','Auth\RegisterController@emailVerifiedFailed')->name('user.email.verified.failed');

Route::post('check-email', 'HomeController@checkEmail')->name('check.email');
Route::post('check-mobile', 'HomeController@checkMobile')->name('check.mobile');

Route::post('/login', 'Auth\LoginController@customLogin')->name('user.login');
Route::get('/logout', 'Auth\LoginController@logout')->name('user.logout');
Route::post('/forgot-password', 'Auth\LoginController@forgotPassword')->name('user.forgot.password');
Route::get('/password-reset/{id}/{vcode}', 'Auth\ResetPasswordController@showResetForm')->name('user.reset.password');
Route::post('/user-password-reset', 'Auth\ResetPasswordController@reset')->name('user.password.request');
Route::post('/forgot-password-otp-verify', 'Auth\LoginController@verifyOtp')->name('user.forgot.password.otp.verify');
Route::post('/change-password', 'Auth\LoginController@changePassword')->name('change.password');


Route::get('login/{user_type}/{provider_type}', 'Auth\LoginController@redirectToProvider')->name('login.social');
Route::get('login/{user_type}/{provider_type}/callback', 'Auth\LoginController@handleProviderCallback')->name('login.social.callback');
Route::post('social_login', 'Auth\LoginController@socialRegistation')->name('login.social.register');

Route::get('get-state','HomeController@getState')->name('get.state');
Route::get('get-city','HomeController@getCity')->name('get.city');
Route::get('get-subcategory','HomeController@getSubcategoryUser')->name('get.sub.category');
Route::post('get-locality','HomeController@searchLocality')->name('get.locality');
Route::post('get-available-locality','HomeController@searchLocalityAvailable')->name('get.locality.available');

Route::get('get-sub-category','HomeController@getSubCategory')->name('get.subcategory');

Route::get('/dashboard', 'Modules\User\ProfileController@dashboard')->name('user.dashboard');
Route::get('/profile', 'Modules\User\ProfileController@editProfile')->name('user.profile');
Route::post('/profile-save', 'Modules\User\ProfileController@editProfileSave')->name('user.profile.save');
Route::post('/temp-email-save', 'Modules\User\ProfileController@changeEmail')->name('user.temp.email.save');
Route::post('/temp-mobile-save', 'Modules\User\ProfileController@changeMobile')->name('user.temp.mobile.save');

Route::post('/visit-request-create', 'Modules\User\PropertyVisitController@visitRequestStore')->name('visit.request.create');
Route::post('/visit-request-check', 'Modules\User\PropertyVisitController@visitRequestCheck')->name('visit.request.check');

Route::any('/user-upcoming-property-visit', 'Modules\User\PropertyVisitController@userVisitRequest')->name('user.property.visit.request');
Route::get('/user-upcoming-property-visit-cancel/{id}', 'Modules\User\PropertyVisitController@userVisitRequestCancel')->name('user.property.visit.request.cancel');

/* Agent */
Route::get('/agent/dashboard', 'Modules\Agent\ProfileController@dashboard')->name('agent.dashboard');
Route::get('/agent/profile', 'Modules\Agent\ProfileController@editProfile')->name('agent.profile');
Route::post('/agent/profile-save', 'Modules\Agent\ProfileController@editProfileSave')->name('agent.profile.save');

Route::post('/agent/temp-email-save', 'Modules\Agent\ProfileController@changeEmail')->name('agent.temp.email.save');
Route::post('/agent/temp-mobile-save', 'Modules\Agent\ProfileController@changeMobile')->name('agent.temp.mobile.save');

Route::get('/agent/add-property-details/{id?}', 'Modules\Agent\PropertyController@addProperty')->name('agent.add.property');
Route::post('/agent/add-property-details-save/{id?}', 'Modules\Agent\PropertyController@propertySave')->name('agent.add.property.save');
Route::get('/agent/add-property-image/{id}', 'Modules\Agent\PropertyController@addPropertyImage')->name('agent.add.property.image');
Route::post('/agent/add-property-image-save/{id}', 'Modules\Agent\PropertyController@propertyImageSave')->name('agent.add.property.image.save');
Route::get('/agent/add-property-image-remove/{id}', 'Modules\Agent\PropertyController@propertyImageRemove')->name('agent.add.property.image.remove');
Route::any('/agent/my-property', 'Modules\Agent\PropertyController@myProperty')->name('agent.my.property');
Route::any('/agent/my-property-status-change/{id}', 'Modules\Agent\PropertyController@myPropertyStatusChange')->name('agent.my.property.status.change');
Route::any('/agent/my-property-sold/{id}', 'Modules\Agent\PropertyController@myPropertySold')->name('agent.my.property.sold');
Route::any('/agent/my-property-unsold/{id}', 'Modules\Agent\PropertyController@myPropertyUnSold')->name('agent.my.property.un.sold');
Route::any('/agent/my-property-delete/{id}', 'Modules\Agent\PropertyController@myPropertyDelete')->name('agent.my.property.delete');

Route::get('/agent/manage-availability', 'Modules\Agent\ProfileController@manageAvailability')->name('agent.manage.availability');
Route::any('/agent/manage-availability-save', 'Modules\Agent\ProfileController@manageAvailabilitySave')->name('agent.manage.availability.save');
Route::any('/agent/manage-availability-remove/{id}', 'Modules\Agent\ProfileController@removeAvailability')->name('agent.manage.availability.remove');
Route::post('/agent/reschedule','Modules\Agent\ProfileController@rescheduleAvailability')->name('agent.reschedule.availability');

Route::any('/agent/visit-request', 'Modules\Agent\VisitRequestController@visitRequest')->name('agent.visit.request');
Route::get('/agent/visit-request-cancel/{id}', 'Modules\Agent\VisitRequestController@visitRequestCancel')->name('agent.visit.request.cancel');
/*search-property*/
Route::any('/search-property', 'Modules\Search\PropertySearchController@searchProperty')->name('search.property');
Route::any('/property/{slug}', 'Modules\Search\PropertySearchController@searchPropertyDetails')->name('search.property.details');
// Route::any('/search-property-details/{slug}', 'Modules\Search\PropertySearchController@searchPropertyDetails')->name('search.property.details');

Route::any('/get-time-slot', 'Modules\Search\PropertySearchController@getTimeSlot')->name('get.time.slot');


/* ServiceProvider */
Route::get('/service-provider/dashboard', 'Modules\ServiceProvider\ProfileController@dashboard')->name('service.provider.dashboard');
Route::get('/service-provider/profile', 'Modules\ServiceProvider\ProfileController@editProfile')->name('service.provider.profile');
Route::post('/service-provider/profile-save', 'Modules\ServiceProvider\ProfileController@editProfileSave')->name('service.provider.profile.save');
Route::get('/service-provider/work-images', 'Modules\ServiceProvider\ProfileController@editWorkImage')->name('service.provider.work.image');
Route::get('/service-provider/work-image-remove/{id}', 'Modules\ServiceProvider\ProfileController@removeWorkImage')->name('service.provider.remove.work.image');
Route::post('/service-provider/work-images-save', 'Modules\ServiceProvider\ProfileController@editWorkImageSave')->name('service.provider.work.image.save');

Route::post('/service-provider/temp-email-save', 'Modules\ServiceProvider\ProfileController@changeEmail')->name('service.provider.temp.email.save');
Route::post('/service-provider/temp-mobile-save', 'Modules\ServiceProvider\ProfileController@changeMobile')->name('service.provider.temp.mobile.save');

//search pro

Route::any('/find-service-provider/{slug?}', 'Modules\Search\SearchController@index')->name('search.service.pro');
Route::get('/service-provider-profile/{id}', 'Modules\Search\SearchController@proProfileShow')->name('pro.profile');
Route::get('/agent-public-profile/{slug?}', 'Modules\Search\SearchController@agentProfileShow')->name('agent.public.profile');
//Route::get('/property-details/{slug?}', 'Modules\Search\SearchController@agentPropertyDetails')->name('agent.property.details');
Route::get('/agent-public-profile/{slug}', 'Modules\Search\SearchController@agentProfileShow')->name('agent.public.profile');

Route::get('/hire-a-pro', 'Modules\Search\SearchController@hirePro')->name('hire.pro');
Route::get('/sell', 'Modules\Search\SearchController@sellPage')->name('sell.page');
// Static pages

Route::get('about-us','Modules\Content\ContentController@aboutUs')->name('about.us');
Route::get('contact-us','Modules\Content\ContentController@contactUs')->name('contact.us');
Route::get('faq','Modules\Content\ContentController@faq')->name('faq');
Route::get('terms-and-condition','Modules\Content\ContentController@termsCondition')->name('terms.condition');
Route::get('privacy-policy','Modules\Content\ContentController@privacyPolicy')->name('privacy.policy');


<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Mail;
use App\Mail\EmailVerification;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    /**
     *   Method      : customRegister
     *   Description : registration
     *   Author      : Soumojit
     *   Date        : 2021-OCT-26
     **/

    public function customRegister(Request $request)
    {
        $response = [
            'jsonrpc' => 2.0
        ];
        $params = $request->params;
        $ins=[];
        $checkEmail = User::whereIn('status', ['A', 'I', 'U'])->where('email', $params['email'])->count();
        $checkMobile = User::whereIn('status', ['A', 'I', 'U'])->where('mobile_number', $params['mobile'])->count();
        if($checkEmail){
            $response['error']['message'] = 'This email exist in our platform';
            return response()->json($response);
        }
        if($checkMobile){
            $response['error']['message'] = 'This mobile exist in our platform';
            return response()->json($response);
        }
        $ins['name']=$params['fname'].' '.$params['lname'];
        $ins['email']=$params['email'];
        $ins['password']=Hash::make($params['password']);
        $ins['mobile_number']=$params['mobile'];
        $ins['status']='U';
        $ins['user_type']=$params['userType'];
        $ins['otp_mobile']= mt_rand(100000,999999);
        $ins['vcode_email']=str_random(60);
        $ins['singup_date']=date('Y-m-d H:i:s');
        // return $ins;
        $user= User::create($ins);
        if($user){
            $slug= str_slug(@$user->name, "-");
            $upd['slug'] = $slug. "-".$user->id;
            $userUpdate = User::where('id',$user->id)->update($upd);
            Mail::send(new EmailVerification($user));
            $response['result']['data'] = $user;
            return response()->json($response);
        }
        $response['result']['data'] = $ins;
        return response()->json($response);
    }


    /**
     *   Method      : verifyEmail
     *   Description : email verification
     *   Author      : Soumojit
     *   Date        : 2021-OCT-26
     **/
    public function verifyEmail($id=null,$vcode = null)
    {
        $userData = User::where('vcode_email',$vcode)->where('id',$id)->where('status','!=','D')->first();
        if (@$userData) {
            if($userData->vcode_email==$vcode){
                $upd=[];
                if (@$userData->status == 'U') {
                    $upd['status'] = 'A';
                    if (@$userData->user_type =='U' && @$userData->user_id==null) {
                        $allCustomer= User::where('user_id','!=',null)->where('user_type','U')->get();
                        $code='U';
                        $sum=str_pad($allCustomer->count()+1, 7, '0', STR_PAD_LEFT);
                        $upd['user_id']=$code.$sum;
                    }
                    if (@$userData->user_type =='A' && @$userData->user_id==null) {
                        $allCustomer= User::where('user_id','!=',null)->where('user_type','A')->get();
                        $code='A';
                        $sum=str_pad($allCustomer->count()+1, 7, '0', STR_PAD_LEFT);
                        $upd['user_id']=$code.$sum;
                    }
                    if (@$userData->user_type =='S' && @$userData->user_id==null) {
                        $allCustomer= User::where('user_id','!=',null)->where('user_type','S')->get();
                        $code='PC';
                        $sum=str_pad($allCustomer->count()+1, 6, '0', STR_PAD_LEFT);
                        $upd['user_id']=$code.$sum;
                    }
                }
                $upd['vcode_email'] = null;
                $upd['is_email_verified'] = 'Y';
                User::where('id', $userData->id)->update($upd);
                return redirect()->route('user.email.verified.msg');
            }
            return redirect()->route('user.email.verified.failed');
        }return redirect()->route('user.email.verified.failed');

    }
    /**
     *   Method      :  verifyMobile
     *   Description :  mobile number verification
     *   Author      : Soumojit
     *   Date        : 2021-OCT-26
     **/
    public function verifyMobile(Request $request)
    {
        $response = [
            'jsonrpc' => 2.0
        ];
        $params = $request->params;
        $otp= @$params['codeBox1'] . @$params['codeBox2'] . @$params['codeBox3'] . @$params['codeBox4'] . @$params['codeBox5'] . @$params['codeBox6'];
        $userData = User::where('id', $params['id'])->where('status', '!=', 'D')->first();
        if (@$userData) {
            if (@$userData->is_mobile_verified == 'N') {
                if(@$userData->otp_mobile==$otp){
                    $upd=[];
                    if (@$userData->status == 'U') {
                        $upd['status'] = 'A';
                        if (@$userData->user_type =='U' && @$userData->user_id==null) {
                            $allCustomer= User::where('user_id','!=',null)->where('user_type','U')->get();
                            $code='U';
                            $sum=str_pad($allCustomer->count()+1, 7, '0', STR_PAD_LEFT);
                            $upd['user_id']=$code.$sum;
                        }
                        if (@$userData->user_type =='A' && @$userData->user_id==null) {
                            $allCustomer= User::where('user_id','!=',null)->where('user_type','A')->get();
                            $code='A';
                            $sum=str_pad($allCustomer->count()+1, 7, '0', STR_PAD_LEFT);
                            $upd['user_id']=$code.$sum;
                        }
                        if (@$userData->user_type =='S' && @$userData->user_id==null) {
                            $allCustomer= User::where('user_id','!=',null)->where('user_type','S')->get();
                            $code='PC';
                            $sum=str_pad($allCustomer->count()+1, 6, '0', STR_PAD_LEFT);
                            $upd['user_id']=$code.$sum;
                        }
                    }
                    $upd['otp_mobile'] = null;
                    $upd['is_mobile_verified'] = 'Y';
                    User::where('id', $userData->id)->update($upd);
                    $response['result']['message'] = 'Mobile number verified login your account';
                    $response['result']['data'] = $userData;
                    return response()->json($response);
                }
                $response['error']['message'] = 'Wrong otp Enter';
                return response()->json($response);
            }
            $response['result']['message'] = 'Mobile number verified login your account';
            $response['result']['data'] = $userData;
            return response()->json($response);
        }
        $response['error']['message'] = 'Something went wrong';
        return response()->json($response);

    }


    public function checkEmail(Request $request)
    {
        if ($request->email) {
            $checkEmail = User::whereIn('status', ['A', 'I', 'U'])->where('email', $request->email)->count();
            if ($checkEmail > 0) {
                return response('false');
            } else {
                return response('true');
            }
        }
        return response('no email');
    }
    public function checkMobile(Request $request)
    {
        if ($request->mobile) {
            $checkMobile = User::whereIn('status', ['A', 'I', 'U'])->where('mobile_number', $request->mobile)->count();
            if ($checkMobile > 0) {
                return response('false');
            } else {
                return response('true');
            }
        }
        return response('no mobile');
    }



    public function emailVerifiedMsg()
    {
        return view('auth.email_verified');
    }

    public function emailVerifiedFailed()
    {
        return view('auth.email_failed');
    }
}

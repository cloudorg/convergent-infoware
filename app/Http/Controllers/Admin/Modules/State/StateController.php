<?php

namespace App\Http\Controllers\Admin\Modules\State;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\User;
use App\Models\State;
use App\Country;
class StateController extends Controller
{
    public function __construct(){

        return $this->middleware('admin.auth:admin');
  }

    public function index(Request $request){
        $data['allCountry'] = Country::all();
        if($request->all()){
              $data['state'] = State::with('countryName');
              if($request->state){

                $data['state'] = $data['state']->where(function($q) use($request){

                       $q->where('name','like','%'.$request->state.'%');
                });
              }
              if($request->country){

                $data['state'] = $data['state']->where(function($q) use($request){

                    $q->where('country_id',$request->country);
             });
              }

              $data['state'] = $data['state']->orderBy('id','desc')->paginate(10);
              return view('admin.modules.state.manage_state_list',$data);
           }
           $data['state'] = State::with('countryName')->orderBy('id','desc')->paginate(10);
           return view('admin.modules.state.manage_state_list',$data);
    }

    public function addState(Request $request){
        $data['allCountry'] = Country::all();
        if($request->all()){

            $category['name'] = $request->state;
            $category['country_id'] = $request->country;

            $save = State::create($category);

            if($save){

                 session()->flash('success','State added successfully!');
                 return redirect()->back();
            }else{

               session()->flash('error','Something went wrong');
               return redirect()->back();
            }
       }
       return view('admin.modules.state.add_state')->with($data);
    }

    public function editState(Request $request,$id=null){
        $data['allCountry'] = Country::all();
        if($id!=''){

            if($request->all()){

            
                $state = State::where('id',$id)->first(); 

                  $update['name'] = $request->state;
                  $update['country_id'] = $request->country;

                  $save = State::where('id',$id)->update($update);

                  if($save){

                       session()->flash('success','State update successfully!');
                       return redirect()->back();
                  }else{
                     $data['stateData'] = $state;
                     session()->flash('error','Something went wrong');
                     return redirect()->back()->with($data);  
                       
                  }
            }
            
            $data['stateData'] = State::where('id',$id)->first();

            return view('admin.modules.state.edit_state')->with($data);
     }
    }

    public function deleteState($id=null){

        $check= State::where('id',$id)->first();
        if($check==null){
            session()->flash('error','Something went wrong');
            return redirect()->route('admin.manage.state');
        }
        $user_category_check=User::where('state',$id)->count();
		if(@$user_category_check>0)
		{
			session()->flash('error','State can not be deleted as it is associated with a user.');
                 return redirect()->route('admin.manage.state');
		}

        State::where('id',$id)->delete();
        session()->flash('success','State deleted successfully');
        return redirect()->route('admin.manage.state');
    }
}

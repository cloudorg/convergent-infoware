<?php

namespace App\Http\Controllers\Admin\Modules\City;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\User;
use App\Models\State;
use App\Models\City;
use App\Country;
class CityController extends Controller
{
    public function __construct(){

        return $this->middleware('admin.auth:admin');
  }

  public function index(Request $request){
    $data['allState'] = State::all();     
    if($request->all()){
          $data['city'] = City::with('stateName');
          if($request->city){

            $data['city'] = $data['city']->where(function($q) use($request){

                   $q->where('name','like','%'.$request->city.'%');
            });
          }
          if($request->state){

            $data['city'] = $data['city']->where(function($q) use($request){

                $q->where('state_id',$request->state);
         });
          }

          $data['city'] = $data['city']->orderBy('id','desc')->paginate(10);
          return view('admin.modules.city.manage_city_list',$data);
       }
       $data['city'] = City::with('stateName')->orderBy('id','desc')->paginate(10);
       return view('admin.modules.city.manage_city_list',$data);
}

public function addCity(Request $request){
    $data['allState'] = State::all();
    $data['allCountry'] = Country::all();
    if($request->all()){

        $category['name'] = $request->city;
        $category['state_id'] = $request->state;

        $save = City::create($category);

        if($save){

             session()->flash('success','city added successfully!');
             return redirect()->back();
        }else{

           session()->flash('error','Something went wrong');
           return redirect()->back();
        }
   }
   return view('admin.modules.city.add_city')->with($data);
}

public function stateDropdownResult(Request $request){

     $state = State::where('country_id',$request->countryId)->get();
     return ['success'=>true,'state'=>$state];
}

public function editCity(Request $request,$id=null){
  $data['allState'] = State::all();
  $data['allCountry'] = Country::all();
  if($id!=''){

      if($request->all()){

      
          $city = City::where('id',$id)->first(); 

            $update['name'] = $request->city;
            $update['state_id'] = $request->state;

            $save = City::where('id',$id)->update($update);

            if($save){

                 session()->flash('success','City update successfully!');
                 return redirect()->back();
            }else{
               $data['cityData'] = $city;
               session()->flash('error','Something went wrong');
               return redirect()->back()->with($data);  
                 
            }
      }
      
      $data['cityData'] = City::where('id',$id)->first();

      return view('admin.modules.city.edit_city')->with($data);
}
}

      public function deleteCity($id=null){

      $check= City::where('id',$id)->first();
      if($check==null){
          session()->flash('error','Something went wrong');
          return redirect()->route('admin.manage.city');
      }
      $user_category_check=User::where('city',$id)->count();
      if(@$user_category_check>0)
      {
      session()->flash('error','City can not be deleted as it is associated with a user.');
                return redirect()->route('admin.manage.city');
      }

      City::where('id',$id)->delete();
      session()->flash('success','City deleted successfully');
      return redirect()->route('admin.manage.city');
      }
}

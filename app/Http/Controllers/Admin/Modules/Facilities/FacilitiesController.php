<?php

namespace App\Http\Controllers\Admin\Modules\Facilities;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Http\Controllers\Controller;
use App\Models\FacilitiesAmenities;
use App\Models\PropertyToFacilitiesAmenities;
class FacilitiesController extends Controller
{
    public function __construct(){

        return $this->middleware('admin.auth:admin');
  }

  public function index(Request $request){
           
    if($request->all()){
     $data['facilities'] = FacilitiesAmenities::where('status','A');
       if($request->facilities){

         $data['facilities'] = $data['facilities']->where(function($q) use($request){

                $q->where('name','like','%'.$request->facilities.'%');
         });
       }

       $data['facilities'] = $data['facilities']->orderBy('id','desc')->paginate(10);
       return view('admin.modules.facilities.list_facilities_amenities',$data);
    }
    $data['facilities'] = FacilitiesAmenities::where('status','A')->orderBy('id','desc')->paginate(10);
    return view('admin.modules.facilities.list_facilities_amenities',$data);
}

public function addFacilitiesAmenities(Request $request){

    if($request->all()){

         $category['name'] = $request->facilities;
         $category['status'] = 'A';

         $save = FacilitiesAmenities::create($category);

         if($save){

              session()->flash('success','Facilities added successfully!');
              return redirect()->back();
         }else{

            session()->flash('error','Something went wrong');
            return redirect()->back();
         }
    }
    return view('admin.modules.facilities.add_facilities_amenities');
}

public function checkFacilitiesAmenities(Request $request){

    if($request->facilities){

          $checkSkill = FacilitiesAmenities::where('status','A')->where('name',$request->facilities)->count();

          if($checkSkill>0){

                return response('false');
          }else{

               return response('true');
          }
    }

    return response('no facilities');
}


public function editFacilitiesAmenities(Request $request,$id=null){

    if($id!=''){

           if($request->all()){

            $check = FacilitiesAmenities::where('id','!=',$id)->where('name',$request->facilities)->first();
            $language = FacilitiesAmenities::where('id',$id)->where('status','A')->first(); 
              if($check){
                    
                    session()->flash('error','This facilities name already taken.please enter another facilities');
                    return redirect()->back();
              }

                 $update['name'] = $request->facilities;

                 $save = FacilitiesAmenities::where('id',$id)->update($update);

                 if($save){

                      session()->flash('success','Facilities update successfully!');
                      return redirect()->back();
                 }else{
                    $data['facilities'] = $language;
                    session()->flash('error','Something went wrong');
                    return redirect()->back()->with($data);  
                      
                 }
           }
           
           $data['facilities'] = FacilitiesAmenities::where('id',$id)->where('status','A')->first();

           return view('admin.modules.facilities.edit_facilities_amenities',$data);
    }
}

public function deleteFacilitiesAmenities($id=null){

    $check= FacilitiesAmenities::where('id',$id)->first();
    if($check==null){
        session()->flash('error','Something went wrong');
        return redirect()->route('admin.manage.facilities');
    }
    $user_language_check=PropertyToFacilitiesAmenities::where('facilities_amenities_id',$id)->count();
    if(@$user_language_check>0)
    {
        session()->flash('error','Facilities can not be deleted as it is associated with a property.');
             return redirect()->route('admin.manage.facilities');
    }
    FacilitiesAmenities::where('id',$id)->update(['status'=>'D']);
    session()->flash('success','Facilities deleted successfully');
    return redirect()->route('admin.manage.facilities');
}
  
}

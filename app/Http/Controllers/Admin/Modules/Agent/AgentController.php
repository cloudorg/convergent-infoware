<?php

namespace App\Http\Controllers\Admin\Modules\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Auth;
use App\User;
use App\Models\AgentToAvailability;
use App\Models\PropertyVisitRequest;
use App\Models\Property;
use App\Country;
use Storage;
use App\Models\City;
use App\Models\Language;
use App\Models\State;
use App\Models\ProviderToLanguage;
class AgentController extends Controller
{
    public function __construct(){

        return $this->middleware('admin.auth:admin');
  }

  public function index(Request $request){
    $data['states'] = State::orderBy('name','ASC')->get();
    $data['cites'] = City::orderBy('name','ASC')->get();
    $data['country'] = Country::orderBy('name','ASC')->get();

    if($request->all()){
     $data['agents'] = User::where('user_type','A')->where('status','!=','D');

     if($request->keyword){

            $data['agents'] = $data['agents']->where(function($q) use($request){

                $q->where('name','like','%'.$request->keyword.'%')->orWhere('email','like','%'.$request->keyword.'%')
                ->orWhere('mobile_number','like','%'.$request->keyword.'%')
                ->orWhere('user_id','like','%'.$request->keyword.'%');
            });
     }
     if($request->location){

        $data['agents'] = $data['agents']->where(function($q) use($request){

             $q->where('city','like','%'.$request->location.'%')
             ->orWhere('state','like','%'.$request->location.'%');
        });
     }
     if($request->name){

         $data['agents'] = $data['agents']->where(function($q) use($request){

               $q->where('name','like','%'.$request->name.'%');
         });
     }
     if($request->email){

         $data['agents'] = $data['agents']->where(function($q) use($request){

             $q->where('email','like','%'.$request->email.'%');
       });
     }
     if($request->phone){

         $data['agents'] = $data['agents']->where(function($q) use($request){

             $q->where('mobile_number','like','%'.$request->phone.'%');
       });
     }
     if($request->from_date){

         $data['agents'] = $data['agents']->where(function($q) use($request){

             $q->where(DB::raw("DATE(created_at)") ,'>=',date('Y-m-d',strtotime($request->from_date)));
       });
     }
     if($request->to_date){

         $data['agents'] = $data['agents']->where(function($q) use($request){

             $q->where(DB::raw("DATE(created_at)") ,'<=',date('Y-m-d',strtotime($request->to_date)));
       });

     }
     if($request->signup_from){

        $data['agents'] = $data['agents']->where(function($q) use($request){

            $q->where('signup_from','like','%'.$request->signup_from.'%');
      });
     }
     if($request->status){

         $data['agents'] = $data['agents']->where(function($q) use($request){

             $q->where('status','like','%'.$request->status.'%');
       });
     }
     if($request->approval_status){

        $data['agents'] = $data['agents']->where(function($q) use($request){

            $q->where('email_mob_change','like','%'.$request->approval_status.'%');
      });
    }
    if($request->country!=null){
        $data['agents'] = $data['agents']->where(function($q) use($request){

             $q->where('country',$request->country);
       });
   }
   if($request->state!=null){
        $data['agents'] = $data['agents']->where(function($q) use($request){

             $q->where('state',$request->state);
       });
   }
   if($request->city!=null){
        $data['agents'] = $data['agents']->where(function($q) use($request){

             $q->where('city',$request->city);
       });
   }


     $data['agents'] = $data['agents']->orderBy('id','desc')->paginate(10);
     return view('admin.modules.agent.manage_agent_list',$data);

    }
    $data['agents'] = User::where('user_type','A')->where('status','!=','D')->orderBy('id','desc')->paginate(10);
    return view('admin.modules.agent.manage_agent_list',$data);
}

public function agentStatusUpdate(Request $request){

    $existuser =  User::where('id',$request->id)->whereIn('status',['A','I','U'])->first();
    if($existuser){
         if($existuser->status == 'A'){

             $update['status'] = 'I';
         }
         else if($existuser->status == 'I'){

           $update['status'] = 'A';
         }
         $existuser->update($update);

         $userupdate = User::where('id',$request->id)->where('status', '!=','D')->first();

       return response()->json($userupdate);
    }
}

public function agentDetails($id=null){

    if($id!=''){

          $data['agent'] = User::where('id',$id)->where('status','!=','D')->first();
          $data['country'] = Country::where('id',$data['agent']->country)->value('name');
          return view('admin.modules.agent.view_details',$data);
    }
}

   public function agentEditProfile($id=null){

    $data['allCountry']=Country::get();
    $data['agent'] = User::where('id',$id)->where('status','!=','D')->with(['userCity','userState'])->first();
    $data['states'] = State::where('country_id',$data['agent']->country)->get();
    $data['cites'] = City::where('state_id',$data['agent']->state)->get();
    $data['allLanguage']=Language::get();
    $data['allUserLanguage']=ProviderToLanguage::where('user_id', $data['agent']->id)->get();
    return view('admin.modules.agent.edit_agent')->with($data);
   }

    public function agentEditProfileSave(Request $request){

        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            //'agent_for' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'address' => 'required',
            'about' => 'required',
        ]);
        $name = $request->first_name . ' ' . $request->last_name;
       $existuser = User::where('id',$request->agentId)->where('status','!=','D')->first();
        $upd = [];
        $upd['name'] = @$name;
       // $upd['agent_for'] = @$request->agent_for;
        //$upd['company_name'] = @$request->company_name;
        $upd['city'] = @$request->city;
        $upd['state'] = @$request->state;
        $upd['country'] = @$request->country;
        $upd['address'] = @$request->address;
        $upd['about'] = @$request->about;
        $upd['website'] = @$request->website;
        $upd['whatsapp_no'] = @$request->whatsapp_no;
        $slug = str_slug($name, "-");
        $upd['slug'] = $slug . "-" . $request->agentId;
        if (@$request->profile_picture) {
           // $image = $request->profile_pic;
            // $filename = time() . '-' . rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
            // Storage::putFileAs('public/profile_picture', $image, $filename);
            // $upd['profile_img'] = $filename;
            // @unlink(storage_path('app/public/profile_picture/' . auth()->user()->profile_img));
            @unlink(storage_path('app/public/profile_picture/' . $existuser->profile_pic));
            $destinationPath = "storage/app/public/profile_picture/";
            $img1 = str_replace('data:image/png;base64,', '', @$request->profile_picture);
            $img1 = str_replace(' ', '+', $img1);
            $image_base64 = base64_decode($img1);
            $img = time() . '-' . rand(1000, 9999) . '.png';
            $file = $destinationPath . $img;
            file_put_contents($file, $image_base64);
            chmod($file, 0755);
            $upd['profile_pic'] = $img;
        }
        if (@$request->gov_id) {
            $image = $request->gov_id;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
            Storage::putFileAs('public/gov_id_image', $image, $filename);
            $upd['gov_id_image'] = $filename;
            @unlink(storage_path('app/public/gov_id_image/' . $existuser->gov_id_image));
        }
        if (@$request->license) {
            $image = $request->license;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
            Storage::putFileAs('public/license_image', $image, $filename);
            $upd['license_image'] = $filename;
            @unlink(storage_path('app/public/license_image/' . $existuser->license_image));
        }
        $language= explode(",", @$request->language);
        // return $language;
        foreach ($language as $item1) {
            $insLanguage = [];
            $insLanguage['user_id'] = $existuser->id;
            $insLanguage['language_id'] = $item1;
            if(@$item1){
                $checkAvailable =  ProviderToLanguage::where('user_id', $existuser->id)->where('language_id', $item1)->first();
                if ($checkAvailable == null) {
                    ProviderToLanguage::create($insLanguage);
                }
            }

        }
        ProviderToLanguage::where('user_id', $existuser->id)->whereNotIn('language_id', $language)->delete();

        $user = User::where('id', $request->agentId)->update($upd);
        if( @$user ){
            session()->flash('success', 'Profile updated successfully');
            return redirect()->route('admin.edit.agent.profile',$request->agentId);
        }
        session()->flash('error', 'Profile not updated');
        return redirect()->route('admin.edit.agent.profile',$request->agentId);
    }



    public function agentViewAvailability($id=null){
        $data['userId'] = $id;
        $data['userData'] = User::where('id',$id)->first();
        $data['monday'] = AgentToAvailability::where('user_id',$id)->where('day',1)->get();
        $data['tuesday'] = AgentToAvailability::where('user_id',$id)->where('day',2)->get();
        $data['wednesday'] = AgentToAvailability::where('user_id',$id)->where('day',3)->get();
        $data['thursday'] = AgentToAvailability::where('user_id',$id)->where('day',4)->get();
        $data['friday'] = AgentToAvailability::where('user_id',$id)->where('day',5)->get();
        $data['saturday'] = AgentToAvailability::where('user_id',$id)->where('day',6)->get();
        $data['sunday'] = AgentToAvailability::where('user_id',$id)->where('day',7)->get();
        return view('admin.modules.agent.view_availability_list')->with($data);
    }

    public function agentViewAvailabilitySave(Request $request){

        $request->validate([
            'day' => 'required',
            'fromtime' => 'required',
            'totime' => 'required',
        ]);
        // return $request;
        $check_availability=AgentToAvailability::where('user_id',$request->userId)->where('day',$request->day)
        ->where(function($query) use ($request){
            $query->whereBetween('from_time',[$request->fromtime,$request->totime])
            ->orWhereBetween('to_time',[$request->fromtime,$request->totime]);
        })
        ->count();
        if($check_availability!=null){
            session()->flash('error', 'Availability not added this time slot already added');
            return redirect()->back();
        }
        $ins=[];
        $ins['day']=$request->day;
        $ins['from_time']=$request->fromtime;
        $ins['to_time']=$request->totime;
        $ins['user_id']=$request->userId;
        AgentToAvailability::create($ins);
        session()->flash('success', 'Availability added successfully');
        return redirect()->back();
    }

    public function agentViewAvailabilityRemove($id=null){

        $availability=AgentToAvailability::where('id',$id)->first();
        if($availability==null){
            session()->flash('error', 'Availability not removed');
            return redirect()->back();
        }
        AgentToAvailability::where('id',$id)->delete();
        session()->flash('success', 'Availability removed successfully');
        return redirect()->back();
    }



    public function agentViewProperty(Request $request,$id=null){
        $data['userId'] = $id;
        $data['userData'] = User::where('id',$id)->first();
         if($request->all()){

            $data['propertyDetails'] = Property::with('propertyImageMain')->where('user_id',$id)->where('status','!=','D');

             if($request->property_name){

                $data['propertyDetails'] = $data['propertyDetails']->where(function($q) use($request){

                     $q->where('name','like','%'.$request->property_name.'%')->orWhere('slug','like','%'.str_slug($request->property_name).'%');
                });
             }
             if($request->location){

                $data['propertyDetails'] = $data['propertyDetails']->where(function($q) use($request){

                    $q->where('address','like','%'.$request->location.'%');
               });
             }
             if($request->property_for){

                $data['propertyDetails'] = $data['propertyDetails']->where(function($q) use($request){

                    $q->where('property_for','like','%'.$request->property_for.'%');
               });
             }
             if($request->status){

                $data['propertyDetails'] = $data['propertyDetails']->where(function($q) use($request){

                    $q->where('status','like','%'.$request->status.'%');
               });
             }

             $data['propertyDetails'] = $data['propertyDetails']->orderBy('id','desc')->get();
             return view('admin.modules.agent.view_property')->with($data);

         }

          $data['propertyDetails'] = Property::with('propertyImageMain')->where('user_id',$id)->where('status','!=','D')->orderBy('id','desc')->get();

          return view('admin.modules.agent.view_property')->with($data);
    }

    public function agentPropertyStatusChange($id=null){

        $propertyDetails=Property::where('id',$id)->whereIn('status',['A','B'])->first();
        if($propertyDetails==null){
            session()->flash('error', 'Something went wrong');
            return redirect()->back();
        }
        if($propertyDetails->status=='A'){
            Property::where('id',$id)->update(['status'=>'B']);
            session()->flash('success', 'Property successfully inactive');
            return redirect()->back();
        }
        if($propertyDetails->status=='B'){
            Property::where('id',$id)->update(['status'=>'A']);
            session()->flash('success', 'Property successfully active');
            return redirect()->back();
        }
        session()->flash('error', 'Something went wrong');
        return redirect()->back();
    }

    public function agentApprovalStatusUpdate(Request $request){

        $existuser =  User::where('id',$request->id)->whereIn('status',['A','I','U'])->first();
        if($existuser){
             if($existuser->approval_status == 'N'){

                 $update['approval_status'] = 'Y';
             }
             $existuser->update($update);

             $userupdate = User::where('id',$request->id)->where('status', '!=','D')->first();

           return response()->json($userupdate);
        }
    }
    public function agentVisitRequest(Request $request,$id=null){
        $data['userId'] = $id;
        $data['userData'] = User::where('id',$id)->first();
         if($request->all()){

            $data['visitRequest'] = PropertyVisitRequest::with(['propertyDetails','userDetails'])->where('agent_id',$id)->whereNotIn('status',['CA','C']);
            if($request->property_name){

                $data['visitRequest'] = $data['visitRequest']->whereHas('propertyDetails', function($q) use($request){

                     $q->where('name','like','%'.$request->property_name.'%')->orWhere('slug','like','%'.str_slug($request->property_name).'%');
                });

            }
            if($request->location){

                $data['visitRequest'] = $data['visitRequest']->where(function($q) use($request){

                     $q->where('address','like','%'.$request->location.'%');
                });
            }
            if($request->from_date){

                $data['visitRequest'] = $data['visitRequest']->where(function($q) use($request){

                    $q->where(DB::raw("DATE(visit_date)") ,'>=',date('Y-m-d',strtotime($request->from_date)));
              });
            }
            if($request->to_date){

                $data['visitRequest'] = $data['visitRequest']->where(function($q) use($request){

                    $q->where(DB::raw("DATE(visit_date)") ,'<=',date('Y-m-d',strtotime($request->to_date)));
              });

            }

            $data['visitRequest'] = $data['visitRequest']->orderBy('id','desc')->paginate(10);
            return view('admin.modules.agent.visit_request_list')->with($data);
         }
        $data['visitRequest'] = PropertyVisitRequest::with(['propertyDetails','userDetails'])->where('agent_id',$id)->whereNotIn('status',['CA','C'])->orderBy('id','desc')->paginate(10);
        return view('admin.modules.agent.visit_request_list')->with($data);
  }

  public function agentDeleteProfile($id=null){

    $check= User::where('id',$id)->first();
    if($check==null){
        session()->flash('error','Something went wrong');
        return redirect()->route('admin.manage.agent');
    }

    User::where('id',$id)->update(['status'=>'D']);
    session()->flash('success','User profile deleted successfully');
    return redirect()->route('admin.manage.agent');
  }
  
   public function agentDeleteAll(Request $request){
    if($request->allid){
       
        User::whereIn('id',$request->allid)->update(['status'=>'D']);
       session()->flash('success','User profile deleted successfully');
       return redirect()->route('admin.manage.agent');
    }
       
       session()->flash('error','Something went wrong.please select at least one user.');
       return redirect()->route('admin.manage.agent');

  }

}

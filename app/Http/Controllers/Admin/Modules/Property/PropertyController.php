<?php

namespace App\Http\Controllers\Admin\Modules\Property;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use DB;
use Mail;
use App\Mail\AdminPropertyApproveMail;
use Storage;
use App\Models\Property;
use App\Models\PropertyToFacilitiesAmenities;
use App\Models\FacilitiesAmenities;
use App\Models\PropertyToImage;
use App\Models\PropertyVisitRequest;
use App\Country;
use App\Models\State;
use App\Models\City;
use App\Models\Locality;
class PropertyController extends Controller
{
    public function __construct(){

        return $this->middleware('admin.auth:admin');
  }

   /**
     *   Method      : index
     *   Description : property listing and searching
     *   Author      : sudipta
     *   Date        : 2021-NOV-15
     **/

  public function index(Request $request){
    $data['states'] = State::where('country_id',101)->orderBy('name','ASC')->get();
    $data['cites']  = City::orderBy('name','ASC')->get();
    $data['maxPrice']=Property::where('status','!=','I')->orderby('budget_range_from','desc')->first();
       if($request->all()){

        $data['properties'] = Property::with('propertyUser')->where('status','!=','D');

          if($request->keyword){

               $data['properties'] = $data['properties']->where(function($q) use($request){

                   $q->where('name','like','%'.$request->keyword.'%')->orWhere('slug','like','%'.str_slug($request->keyword).'%');
               })->orWhereHas('propertyUser', function($q) use($request){

                   $q->where('name','like','%'.$request->keyword.'%')->orwhere('slug','like','%'.str_slug($request->keyword).'%');
               });
          }

          if($request->status){

            $data['properties'] = $data['properties']->where(function($q) use($request){

                $q->where('status','like','%'.$request->status.'%');
          });
        }
        if($request->property_type){

            $data['properties'] = $data['properties']->where(function($q) use($request){

                $q->where('property_type','like','%'.$request->property_type.'%');
          });
        }
        if($request->property_for){

            $data['properties'] = $data['properties']->where(function($q) use($request){

                $q->where('property_for','like','%'.$request->property_for.'%');
          });
        }
        if($request->from_date){

            $data['properties'] = $data['properties']->where(function($q) use($request){

                $q->where(DB::raw("DATE(created_at)") ,'>=',date('Y-m-d',strtotime($request->from_date)));
          });
        }
        if($request->to_date){

            $data['properties'] = $data['properties']->where(function($q) use($request){

                $q->where(DB::raw("DATE(created_at)") ,'<=',date('Y-m-d',strtotime($request->to_date)));
          });

        }
        if($request->construction_status){

            $data['properties'] = $data['properties']->where(function($q) use($request){

                $q->where('construction_status','like','%'.$request->construction_status.'%');
          });
        }
        if($request->amount1!=null && $request->amount2!=null ){
            $data['properties']=$data['properties']->whereBetween('budget_range_from',[$request->amount1 , $request->amount2]);
        }
        if(@$request->lat && $request->long){
            $query =  Property::selectRaw("*,(6371 * acos ( cos ( radians('".$request->lat."') ) * cos( radians( address_lat ) ) * cos( radians( address_long ) - radians('".$request->long."') ) + sin ( radians('".$request->lat."') ) * sin( radians( address_lat ) ))) as distance");
            $query = $query->having("distance", "<", $request->distance)->pluck('id')->toArray();
            $data['properties']=$data['properties']->whereIn('id',$query);

        }
        if($request->furnishing){

            $data['properties'] = $data['properties']->where(function($q) use($request){

                $q->where('furnishing','like','%'.$request->furnishing.'%');
          });
        }
        if($request->no_of_bedrooms){

            $data['properties'] = $data['properties']->where(function($q) use($request){

                $q->where('no_of_bedrooms','>=',$request->no_of_bedrooms);
          });
        }
        if($request->bathroom){

            $data['properties'] = $data['properties']->where(function($q) use($request){

                $q->where('bathroom','>=',$request->bathroom);
          });
        }
        if($request->from_area){

            $data['properties'] = $data['properties']->where(function($q) use($request){

                $q->where('area','>=',$request->from_area);
          });

        }
        if($request->to_area){

            $data['properties'] = $data['properties']->where(function($q) use($request){

                $q->where('area','<=',$request->to_area);
          });

        }
        if(@$request->location){
            $data['properties']=$data['properties']->where(function($query) use ($request){
                $query->whereHas('localityName',function($q) use ($request){
                    $q->where('locality_name','LIKE','%'.$request->location.'%');
                });
            });
        }
        if($request->state!=null){
            $data['properties'] = $data['properties']->where(function($q) use($request){
    
                 $q->where('state',$request->state);
           });
       }
       if($request->city!=null){
        $data['properties'] = $data['properties']->where(function($q) use($request){

             $q->where('city',$request->city);
       });
   }

        $data['key'] = $request->all();

          $data['properties'] = $data['properties']->orderBy('id','desc')->paginate(50);
          return view('admin.modules.property.manage_property_list')->with($data);
       }

       $data['properties'] = Property::with('propertyUser')->where('status','!=','D')->orderBy('id','desc')->paginate(50);

        return view('admin.modules.property.manage_property_list')->with($data);
  }

   /**
     *   Method      : propertyStatusUpdate
     *   Description : for property status update
     *   Author      : sudipta
     *   Date        : 2021-NOV-15
     **/

  public function propertyStatusUpdate(Request $request){

    $existproperty =  Property::where('id',$request->id)->where('status','!=','D')->first();
    if($existproperty){
         if($existproperty->status == 'A'){

             $update['status'] = 'B';
         }
         else if($existproperty->status == 'B'){

           $update['status'] = 'A';
         }
         $existproperty->update($update);

         $proupdate = Property::where('id',$request->id)->where('status', '!=','D')->first();

       return response()->json($proupdate);
    }
}

 public function propertyStatusUpdateHome(Request $request){

    $existproperty =  Property::where('id',$request->id)->where('status','!=','D')->first();
    if($existproperty){
         if($existproperty->show_in_home == 'Y'){

             $update['show_in_home'] = 'N';
         }
         else if($existproperty->show_in_home == 'N'){

           $update['show_in_home'] = 'Y';
         }
         $existproperty->update($update);

         $proupdate = Property::where('id',$request->id)->where('status', '!=','D')->first();

       return response()->json($proupdate);
    }
 }

 /**
     *   Method      : propertyDetails
     *   Description : for property details page view
     *   Author      : sudipta
     *   Date        : 2021-NOV-15
     **/

public function propertyDetails($id=null){

       $data['property'] = Property::with(['propertyImageMain','propertyUser','localityName','cityName','stateName'])->where('id',$id)->where('status','!=','D')->first();
       $data['facilities'] = PropertyToFacilitiesAmenities::with('propertyFacilities')->where('property_id',$id)->get();
       $data['country'] = Country::where('id',$data['property']->country)->value('name');
       $data['exteriorimages'] = PropertyToImage::where('property_id',$id)->where('image_for','EP')->get();
       $data['interiorimages'] = PropertyToImage::where('property_id',$id)->where('image_for','IP')->get();
       $data['floorplanimages'] = PropertyToImage::where('property_id',$id)->where('image_for','FP')->get();
       return view('admin.modules.property.view_property_details')->with($data);
}

 /**
     *   Method      : editProperty
     *   Description : for property edit page view
     *   Author      : sudipta
     *   Date        : 2021-NOV-16
     **/

public function editProperty($id=null){
     $data['allCountry'] = Country::all();
     $data['states'] = State::where('country_id',101)->orderBy('name','ASC')->get();
     $data['propertyDetails'] = Property::where('id',$id)->where('status','!=','D')->first();
     $data['cites'] = City::where('state_id',$data['propertyDetails']->state)->orderBy('name','ASC')->get();
     return view('admin.modules.property.edit_property')->with($data);
}

/**
     *   Method      : editPropertySave
     *   Description : edit property save
     *   Author      : sudipta
     *   Date        : 2021-NOV-16
     **/

public function editPropertySave(Request $request){

        $request->validate([
          'property_type' => 'required',
          'property_for' => 'required',
          'property_name' => 'required',
          // 'no_of_bedrooms' => 'required',
          // 'bathroom' => 'required',
          'budget_range_from' => 'required',
          // 'budget_range_to' => 'required',
          // 'area' => 'required',
          // 'super_area' => 'required',
          // 'carpet_area' => 'required',
          'country' => 'required',
          'state' => 'required',
          'city' => 'required',
          'address' => 'required',
          'description' => 'required',
          'locality' => 'required',
          'post_code' => 'required',
          // 'build_year' => 'required',
      ]);
        $propertyDetails = Property::where('id',@$request->propertyId)->where('status','!=','D')->first();
        $upd = [];
        $upd['property_type']= $request->property_type;
        $upd['property_for']= $request->property_for;
        $upd['name']= $request->property_name;
        $upd['no_of_bedrooms']= $request->no_of_bedrooms;
        $upd['bathroom']= $request->bathroom;
        $upd['budget_range_from']= $request->budget_range_from;
        $upd['budget_range_to']= $request->budget_range_to;
        $upd['area']= $request->area;
        $upd['super_area']= $request->super_area;
        $upd['carpet_area']= $request->carpet_area;
        $upd['construction_status']= $request->construction_status;
        $upd['furnishing']= $request->furnishing;
        $upd['country']= $request->country;
        $upd['state']= $request->state;
        $upd['city']= $request->city;
        $upd['address']= $request->address;
        $upd['description']= $request->description;
        $upd['build_year']= $request->build_year;
        $upd['rent_payment']= $request->rent_payment;
        $upd['floor'] = $request->floor;
        // $upd['address_lat']= $request->lat;
        // $upd['address_long']= $request->long;
        $upd['post_code']= $request->post_code;
        $slug= str_slug(@$request->property_name, "-");
        $upd['slug'] = $slug. "-".$propertyDetails->id;
        if(@$request->property_type == 'L'){
                $land_area_unit = @$request->land_area_unit;
                $land_area = @$request->land_area;
                $ins['land_area'] = $land_area;
                $ins['land_area_unit'] = $land_area_unit;
                if($land_area_unit == 'A'){
                    // Conversion of Acre to Sq ft
                        $area = $land_area * 43560;
                        $ins['area'] = @$area;  
                }else{
                    // Conversion of Sq yard to Sq ft
                        $area = $land_area * 9;
                        $ins['area'] = @$area; 
                }
        }
        $checkLocality = Locality::where('locality_name', $request->locality)->first();
        if(@$checkLocality){
            $upd['locality'] = $checkLocality->id;
        }else{
            $loc=[];
            $loc['locality_name']=$request->locality;
            $loc['city_id']=$request->city;
            $com=Locality::create($loc);
            $upd['locality'] = $com->id;
        }
        $updated=Property::where('id',$propertyDetails->id)->update($upd);
        if(@$updated){
            session()->flash('success', 'Property details updated successfully');
            return redirect()->route('admin.edit.property.image',['id'=>$propertyDetails->id]);
        }
        session()->flash('error', 'Property details not updated');
        return redirect()->back();
}

/**
     *   Method      : editPropertyImage
     *   Description : edit property image view page
     *   Author      : sudipta
     *   Date        : 2021-NOV-16
     **/

public function editPropertyImage($id=null){

  $data['allCountry']=Country::get();
  $data['allFacilitiesAmenities']=FacilitiesAmenities::where('status','A')->take(20)->get();
  $data['propertyDetails']=Property::where('id',$id)->first();
  if($data['propertyDetails']==null){
      session()->flash('error', 'Something went wrong');
      return redirect()->route('admin.edit.property',['id'=>$data['propertyDetails']->id]);
  }
  $data['propertyImageExterior']=PropertyToImage::where('property_id',$id)->where('image_for','EP')->get();
  $data['propertyImageInterior']=PropertyToImage::where('property_id',$id)->where('image_for','IP')->get();
  $data['propertyImageFloor']=PropertyToImage::where('property_id',$id)->where('image_for','FP')->get();
  $data['facilitiesAmenities']=PropertyToFacilitiesAmenities::where('property_id',$id)->pluck('facilities_amenities_id')->toArray();

      return view('admin.modules.property.edit_property_image')->with($data);
}

 /**
     *   Method      : editPropertyImageSave
     *   Description : edit property image save
     *   Author      : sudipta
     *   Date        : 2021-NOV-16
     **/

public function editPropertyImageSave(Request $request){

      $data['propertyDetails']=Property::where('id',$request->propertyId)->first();
      if($data['propertyDetails']==null){
          session()->flash('error', 'Something went wrong');
          return redirect()->route('admin.edit.property',['id'=>$data['propertyDetails']->id]);
      }

      $files1 = $request->file('file-1');
      if($request->hasFile('file-1'))
      {
          foreach ($files1 as $file) {
              $filename = time() . '-' . rand(1000, 9999) . '.' . $file->getClientOriginalExtension();
              Storage::putFileAs('public/property_image', $file, $filename);
              $insImage=[];
              $insImage['image']=$filename;
              $insImage['property_id']=$request->propertyId;
              $insImage['image_for']='EP';
              PropertyToImage::create($insImage);
          }
      }
      $files2 = $request->file('file-2');
      if($request->hasFile('file-2'))
      {
          foreach ($files2 as $file) {
              $filename = time() . '-' . rand(1000, 9999) . '.' . $file->getClientOriginalExtension();
              Storage::putFileAs('public/property_image', $file, $filename);
              $insImage=[];
              $insImage['image']=$filename;
              $insImage['property_id']=$request->propertyId;
              $insImage['image_for']='IP';
              PropertyToImage::create($insImage);
          }
      }
      $files3 = $request->file('file-3');
      if($request->hasFile('file-3'))
      {
          foreach ($files3 as $file) {
              $filename = time() . '-' . rand(1000, 9999) . '.' . $file->getClientOriginalExtension();
              Storage::putFileAs('public/property_image', $file, $filename);
              $insImage=[];
              $insImage['image']=$filename;
              $insImage['property_id']=$request->propertyId;
              $insImage['image_for']='FP';
              PropertyToImage::create($insImage);
          }
      }
      foreach($request->facilities_amenities as $facilities_amenities){
          $ins=[];
          $ins['facilities_amenities_id']=$facilities_amenities;
          $ins['property_id']=$request->propertyId;
          $checkAvailable =   PropertyToFacilitiesAmenities::where('property_id',$request->propertyId)->where('facilities_amenities_id', $facilities_amenities)->first();
          if ($checkAvailable == null) {
              PropertyToFacilitiesAmenities::create($ins);
          }
      }
      PropertyToFacilitiesAmenities::where('property_id',$request->propertyId)->whereNotIn('facilities_amenities_id', $request->facilities_amenities)->delete();
      $facilitiesAmenities= PropertyToFacilitiesAmenities::where('property_id',$request->propertyId)->count();
      $image1= PropertyToImage::where('image_for','EP')->where('property_id',$request->propertyId)->count();
      $image2= PropertyToImage::where('image_for','IP')->where('property_id',$request->propertyId)->count();
      $image3= PropertyToImage::where('image_for','FP')->where('property_id',$request->propertyId)->count();
      if($facilitiesAmenities>0 && $image1 >0 && $image2 >0 && $image3 >0 && $data['propertyDetails']->status=='I'){
          $upd=[];
          $upd['status'] = 'A';
          Property::where('id',$request->propertyId)->update($upd);
      }
      session()->flash('success', 'Property image updated successfully');
      return redirect()->route('admin.edit.property.image',['id'=>$data['propertyDetails']->id]);
}

/**
     *   Method      : editPropertyImageRemove
     *   Description : edit property image remove
     *   Author      : sudipta
     *   Date        : 2021-NOV-16
     **/

public function editPropertyImageRemove($id=null){

  $insImage=PropertyToImage::where('id',$id)->first();
  if($insImage==null){
      session()->flash('error', 'Something went wrong image not removed');
      return redirect()->back();
  }

  $propertyDetails=Property::where('id',$insImage->property_id)->first();
  if($propertyDetails==null){
      session()->flash('error', 'Something went wrong image not removed');
      return redirect()->back();
  }
  if($insImage->image_for=='EP'){
      $image= PropertyToImage::where('image_for','EP')->where('property_id',$insImage->property_id)->count();
      if($image==1){
          session()->flash('error', 'Image not removed minimum one exterior image require');
          return redirect()->back();
      }
      @unlink(storage_path('app/public/property_image/' .$insImage->image));
      PropertyToImage::where('id', $insImage->id)->delete();
      session()->flash('success', 'Exterior image removed');
      return redirect()->back();

  }
  if($insImage->image_for=='IP'){
      $image= PropertyToImage::where('image_for','IP')->where('property_id',$insImage->property_id)->count();
      if($image==1){
          session()->flash('error', 'Image not removed minimum one interior image require');
          return redirect()->back();
      }
      @unlink(storage_path('app/public/property_image/' .$insImage->image));
      PropertyToImage::where('id', $insImage->id)->delete();
      session()->flash('success', 'Interior image removed');
      return redirect()->back();
  }
  if($insImage->image_for=='FP'){
      $image= PropertyToImage::where('image_for','FP')->where('property_id',$insImage->property_id)->count();
      if($image==1){
          session()->flash('error', 'Image not removed minimum one floor image require');
          return redirect()->back();
      }
      @unlink(storage_path('app/public/property_image/' .$insImage->image));
      PropertyToImage::where('id', $insImage->id)->delete();
      session()->flash('success', 'Floor image removed');
      return redirect()->back();
  }
  session()->flash('error', 'Something went wrong image not removed ');
  return redirect()->back();
}

/**
     *   Method      : propertyApprovalStatusUpd
     *   Description : for property approval status update
     *   Author      : sudipta
     *   Date        : 2021-NOV-16
     **/

public function propertyApprovalStatusUpd(Request $request){

    $existproperty =  Property::where('id',$request->id)->where('status','!=','D')->first();
    if($existproperty){
         if($existproperty->aprove_by_admin == 'N'){

             $update['aprove_by_admin'] = 'Y';
              
             session()->flash('success','Property approved successfull');
         }
         Property::where('id',$request->id)->update($update);
         $proupdate = Property::where('id',$request->id)->where('status', '!=','D')->first();
         @$propertyname = $proupdate->name;
         $username = User::where('id',@$proupdate->user_id)->where('status','!=','D')->value('name');
         $useremail = User::where('id',@$proupdate->user_id)->where('status','!=','D')->value('email');
          $propertyDetails       =   new \stdClass();
         $propertyDetails->username    = @$username;
         $propertyDetails->property   = @$propertyname;
         $propertyDetails->email   = @$useremail;
         Mail::send(new AdminPropertyApproveMail($propertyDetails));
         
         return response()->json($proupdate);
    }
}

/**
     *   Method      : deleteProperty
     *   Description : for property delete
     *   Author      : sudipta
     *   Date        : 2021-NOV-16
     **/

public function deleteProperty($id=null){

    $check= Property::where('id',$id)->first();
    if($check==null){
        session()->flash('error','Something went wrong');
        return redirect()->route('admin.manage.property');
    }

    Property::where('id',$id)->update(['status'=>'D']);
    session()->flash('success','Property deleted successfully');
    return redirect()->route('admin.manage.property');
}

 public function propertyVisitRequest(Request $request,$id=null){

    $data['propertyId'] = $id;
     if($request->all()){

        $data['visitRequest'] = PropertyVisitRequest::with(['propertyDetails','userDetails'])->where('Property_id',$id)->whereNotIn('status',['CA','C']);
        if($request->client_name){

            $data['visitRequest'] = $data['visitRequest']->whereHas('userDetails', function($q) use($request){

                 $q->where('name','like','%'.$request->client_name.'%')->orWhere('slug','like','%'.str_slug($request->client_name).'%');
            });

        }
        if($request->location){

            $data['visitRequest'] = $data['visitRequest']->where(function($q) use($request){

                 $q->where('address','like','%'.$request->location.'%');
            });
        }
        if($request->from_date){

            $data['visitRequest'] = $data['visitRequest']->where(function($q) use($request){

                $q->where(DB::raw("DATE(visit_date)") ,'>=',date('Y-m-d',strtotime($request->from_date)));
          });
        }
        if($request->to_date){

            $data['visitRequest'] = $data['visitRequest']->where(function($q) use($request){

                $q->where(DB::raw("DATE(visit_date)") ,'<=',date('Y-m-d',strtotime($request->to_date)));
          });

        }
        $data['key'] = $request->all();
        $data['visitRequest'] = $data['visitRequest']->orderBy('id','desc')->paginate(10);
        return view('admin.modules.property.property_visit_request')->with($data);
     }
    $data['visitRequest'] = PropertyVisitRequest::with(['propertyDetails','userDetails'])->where('property_id',$id)->whereNotIn('status',['CA','C'])->orderBy('id','desc')->paginate(10);
    return view('admin.modules.property.property_visit_request')->with($data);
 }

 public function propertytDeleteAll(Request $request){

    if($request->status && $request->allid){

        Property::whereIn('id',$request->allid)->update(['status'=>'B']);
        session()->flash('success','Property Block successfully');
        return redirect()->route('admin.manage.property');
    }
    else if($request->status_home && $request->allid){

         
        Property::whereIn('id',$request->allid)->update(['show_in_home'=>'Y']);
        session()->flash('success','Property show in home successfully');
        return redirect()->route('admin.manage.property');
    }
    else{

        Property::whereIn('id',$request->allid)->update(['status'=>'D']);
        session()->flash('success','Property deleted successfully');
        return redirect()->route('admin.manage.property');
    }
       
       session()->flash('error','Something went wrong.please select at least one property.');
       return redirect()->route('admin.manage.property');
 }


}

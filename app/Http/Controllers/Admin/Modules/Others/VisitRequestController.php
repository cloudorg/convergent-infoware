<?php

namespace App\Http\Controllers\Admin\Modules\Others;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Models\PropertyVisitRequest;
class VisitRequestController extends Controller
{
    public function __construct(){

        return $this->middleware('admin.auth:admin');
  }

    public function index(Request $request){

    if($request->all()){

       $data['visitRequest'] = PropertyVisitRequest::with(['propertyDetails','userDetails','agentDetails']);
        
       if($request->keyword){

        $data['visitRequest'] = $data['visitRequest']->whereHas('userDetails',function($q) use($request){

             $q->where('name','like','%'.$request->keyword.'%')->orWhere('slug','like','%'.str_slug($request->keyword).'%');
        })->orWhereHas('agentDetails',function($q) use($request){

             $q->where('name','like','%'.$request->keyword.'%')->orWhere('slug','like','%'.str_slug($request->keyword).'%');
        });
       }

       if($request->property_name){
         
           $data['visitRequest'] = $data['visitRequest']->whereHas('propertyDetails', function($q) use($request){

                $q->where('name','like','%'.$request->property_name.'%')->orWhere('slug','like','%'.str_slug($request->property_name).'%');
           });
            
       }
       if($request->location){

           $data['visitRequest'] = $data['visitRequest']->where(function($q) use($request){

                $q->where('address','like','%'.$request->location.'%');
           });
       }
       if($request->from_date){

           $data['visitRequest'] = $data['visitRequest']->where(function($q) use($request){
  
               $q->where(DB::raw("DATE(visit_date)") ,'>=',date('Y-m-d',strtotime($request->from_date)));
         });
       }
       if($request->to_date){
            
           $data['visitRequest'] = $data['visitRequest']->where(function($q) use($request){
  
               $q->where(DB::raw("DATE(visit_date)") ,'<=',date('Y-m-d',strtotime($request->to_date)));
         });
            
       }
       $data['key'] = $request->all();
       $data['visitRequest'] = $data['visitRequest']->orderBy('id','desc')->paginate(10);
       return view('admin.modules.visit.visit_request_list')->with($data); 
    }
   $data['visitRequest'] = PropertyVisitRequest::with(['propertyDetails','userDetails','agentDetails'])->orderBy('id','desc')->paginate(10);
   return view('admin.modules.visit.visit_request_list')->with($data);
    }
}

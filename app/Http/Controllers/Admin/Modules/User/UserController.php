<?php

namespace App\Http\Controllers\Admin\Modules\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Auth;
use App\User;
use App\Country;
use App\Models\City;
use App\Models\State;
class UserController extends Controller
{
    public function __construct(){

          return $this->middleware('admin.auth:admin');
    }

     /**
     *   Method      : index
     *   Description : listing and searching user
     *   Author      : sudipta
     *   Date        : 2021-OCT-27
     **/

    public function index(Request $request){

        $data['states'] = State::orderBy('name','ASC')->get();
        $data['cites'] = City::orderBy('name','ASC')->get();
        $data['country'] = Country::orderBy('name','ASC')->get();
        if($request->all()){
            $data['users'] = User::where('user_type','U')->where('status','!=','D');

            if($request->keyword){

                   $data['users'] = $data['users']->where(function($q) use($request){

                       $q->where('name','like','%'.$request->keyword.'%')->orWhere('email','like','%'.$request->keyword.'%')
                       ->orWhere('mobile_number','like','%'.$request->keyword.'%')
                       ->orWhere('user_id','like','%'.$request->keyword.'%');
                   });
            }
            if($request->location){

              $data['users'] = $data['users']->where(function($q) use($request){

                   $q->where('city','like','%'.$request->location.'%')
                   ->orWhere('state','like','%'.$request->location.'%');
              });
           }
            if($request->name){

                $data['users'] = $data['users']->where(function($q) use($request){

                      $q->where('name','like','%'.$request->name.'%');
                });
            }
            if($request->email){

                $data['users'] = $data['users']->where(function($q) use($request){

                    $q->where('email','like','%'.$request->email.'%');
              });
            }
            if($request->phone){

                $data['users'] = $data['users']->where(function($q) use($request){

                    $q->where('mobile_number','like','%'.$request->phone.'%');
              });
            }
            if($request->from_date){

                $data['users'] = $data['users']->where(function($q) use($request){

                    $q->where(DB::raw("DATE(created_at)") ,'>=',date('Y-m-d',strtotime($request->from_date)));
              });
            }
            if($request->to_date){

                $data['users'] = $data['users']->where(function($q) use($request){

                    $q->where(DB::raw("DATE(created_at)") ,'<=',date('Y-m-d',strtotime($request->to_date)));
              });

            }
            if($request->signup_from){

              $data['users'] = $data['users']->where(function($q) use($request){

                  $q->where('signup_from','like','%'.$request->signup_from.'%');
            });

          }
            if($request->status){

                $data['users'] = $data['users']->where(function($q) use($request){

                    $q->where('status','like','%'.$request->status.'%');
              });
            }
            if($request->approval_status){

              $data['users'] = $data['users']->where(function($q) use($request){

                  $q->where('email_mob_change','like','%'.$request->approval_status.'%');
            });
          }
          if($request->country!=null){
            $data['users'] = $data['users']->where(function($q) use($request){

                 $q->where('country',$request->country);
           });
       }
       if($request->state!=null){
            $data['users'] = $data['users']->where(function($q) use($request){

                 $q->where('state',$request->state);
           });
       }
       if($request->city!=null){
            $data['users'] = $data['users']->where(function($q) use($request){

                 $q->where('city',$request->city);
           });
       }
            $data['users'] = $data['users']->orderBy('id','desc')->paginate(10);
            return view('admin.modules.user.manage_user_list',$data);

        }
        $data['users'] = User::where('user_type','U')->where('status','!=','D')->orderBy('id','desc')->paginate(10);
        return view('admin.modules.user.manage_user_list',$data);
    }

     /**
     *   Method      : userStatusUpdate
     *   Description : user status update
     *   Author      : sudipta
     *   Date        : 2021-OCT-27
     **/

    public function userStatusUpdate(Request $request){

        $existuser =  User::where('id',$request->id)->whereIn('status',['A','I','U'])->first();
        if($existuser){
             if($existuser->status == 'A'){

                 $update['status'] = 'I';
             }
             else if($existuser->status == 'I'){

               $update['status'] = 'A';
             }
             $existuser->update($update);

             $userupdate = User::where('id',$request->id)->where('status', '!=','D')->first();

           return response()->json($userupdate);
        }
  }

   /**
     *   Method      : userDetails
     *   Description : user Details show
     *   Author      : sudipta
     *   Date        : 2021-OCT-27
     **/

   public function userDetails($id=null){
       if($id!=''){
            $data['user'] = User::where('id',$id)->where('status','!=','D')->with(['userCity','userState'])->first();
            $data['country'] = Country::where('id',$data['user']->country)->value('name');
            return view('admin.modules.user.view_details',$data);
        }
   }

    /**
     *   Method      : userEditSave
     *   Description : user profile edit
     *   Author      : sudipta
     *   Date        : 2021-OCT-27
     **/
    public function userEditSave(Request $request,$id=null){
        $data['country'] = Country::get();
        if($id!=''){
            if($request->all()){
                $request->validate([
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'country' => 'required',
                    'state' => 'required',
                    'city' => 'required',
                ]);
                $existuser = User::where('id',$id)->where('status','!=','D')->first();
                $name = $request->first_name . ' ' . $request->last_name;
                $update['name'] = $name;
                $update['slug'] = str_slug($name.'-'.$id);
                $update['country'] = $request->country;
                $update['state'] = $request->state;
                $update['city'] = $request->city;
                // $update['address'] = $request->address;
                if($request->profile_picture){
                    // $image = $request->profile_pic;
                    // $filename = time() . '-' . rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
                    // Storage::putFileAs('public/profile_picture', $image, $filename);
                    // $upd['profile_img'] = $filename;
                    // @unlink(storage_path('app/public/profile_picture/' . auth()->user()->profile_img));
                    @unlink(storage_path('app/public/profile_picture/' . $existuser->profile_pic));
                    $destinationPath = "storage/app/public/profile_picture/";
                    $img1 = str_replace('data:image/png;base64,', '', @$request->profile_picture);
                    $img1 = str_replace(' ', '+', $img1);
                    $image_base64 = base64_decode($img1);
                    $img = time() . '-' . rand(1000, 9999) . '.png';
                    $file = $destinationPath . $img;
                    file_put_contents($file, $image_base64);
                    chmod($file, 0755);
                    $update['profile_pic'] = $img;
                }
                $save = User::where('id',$id)->update($update);
                if($save){
                    session()->flash('success','user update successfully!');
                    return redirect()->back();
                }else{
                    $data['user'] = $existuser;
                    session()->flash('error','Something went wrong');
                    return redirect()->back()->with($data);
                }
            }
            $data['user'] = User::where('id',$id)->where('status','!=','D')->first();
            $data['states'] = State::where('country_id',$data['user']->country)->get();
            $data['cites'] = City::where('state_id',$data['user']->state)->get();
            return view('admin.modules.user.edit_user',$data);
        }
   }

    public function userDeleteProfile($id=null){

    $check= User::where('id',$id)->first();
    if($check==null){
        session()->flash('error','Something went wrong');
        return redirect()->route('admin.manage.user');
    }

    User::where('id',$id)->update(['status'=>'D']);
    session()->flash('success','User profile deleted successfully');
    return redirect()->route('admin.manage.user');
    }

    public function usertDeleteAll(Request $request){

        if($request->allid){

            User::whereIn('id',$request->allid)->update(['status'=>'D']);
           session()->flash('success','User profile deleted successfully');
           return redirect()->route('admin.manage.user');
        }

           session()->flash('error','Something went wrong.please select at least one user.');
           return redirect()->route('admin.manage.user');
    }
}

<?php

namespace App\Http\Controllers\Admin\Modules\Skill;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\Models\Skill;
use App\Models\ProviderToSkills;
class SkillController extends Controller
{
    public function __construct(){

          return $this->middleware('admin.auth:admin');
    }

     /**
     *   Method      : index
     *   Description : listing and searching skill
     *   Author      : sudipta
     *   Date        : 2021-OCT-27
     **/

    public function index(Request $request){

        if($request->all()){
            $data['skill'] = Skill::where('status','A');
              if($request->skill){
       
                $data['skill'] = $data['skill']->where(function($q) use($request){
       
                       $q->where('skill_name','like','%'.$request->skill.'%');
                });
              }
       
              $data['skill'] = $data['skill']->orderBy('id','desc')->paginate(10);
              return view('admin.modules.skill.list_skill',$data);
           }
           $data['skill'] = Skill::where('status','A')->orderBy('id','desc')->paginate(10);
           return view('admin.modules.skill.list_skill',$data);
    }

     /**
     *   Method      : addSkill
     *   Description : add skill
     *   Author      : sudipta
     *   Date        : 2021-OCT-27
     **/

    public function addSkill(Request $request){

        if($request->all()){
    
             $category['skill_name'] = $request->skill;
             $category['status'] = 'A';
    
             $save = Skill::create($category);
    
             if($save){
    
                  session()->flash('success','Skill added successfully!');
                  return redirect()->back();
             }else{
    
                session()->flash('error','Something went wrong');
                return redirect()->back();
             }
        }
        return view('admin.modules.skill.add_skill');
    }

     /**
     *   Method      : checkSkill
     *   Description : duplicate checking
     *   Author      : sudipta
     *   Date        : 2021-OCT-27
     **/
    
    public function checkSkill(Request $request){

           if($request->skill){

                 $checkSkill = Skill::where('status','A')->where('skill_name',$request->skill)->count();

                 if($checkSkill>0){

                       return response('false');
                 }else{

                      return response('true');
                 }
           }

           return response('no skill');
    }

     /**
     *   Method      : editSkill
     *   Description : edit skill
     *   Author      : sudipta
     *   Date        : 2021-OCT-27
     **/

    public function editSkill(Request $request,$id=null){

        if($id!=''){
    
               if($request->all()){
    
                $check = Skill::where('id','!=',$id)->where('skill_name',$request->skill)->where('status','A')->first();
                $skill = Skill::where('id',$id)->where('status','A')->first(); 
                  if($check){
                        
                        session()->flash('error','This skill name already taken.please enter another skill');
                        return redirect()->back();
                  }
    
                     $update['skill_name'] = $request->skill;
    
                     $save = Skill::where('id',$id)->update($update);
    
                     if($save){
    
                          session()->flash('success','Skill update successfully!');
                          return redirect()->back();
                     }else{
                        $data['skill'] = $skill;
                        session()->flash('error','Something went wrong');
                        return redirect()->back()->with($data);  
                          
                     }
               }
               
               $data['skill'] = Skill::where('id',$id)->where('status','A')->first();
    
               return view('admin.modules.skill.edit_skill',$data);
        }
    }

     /**
     *   Method      : deleteSkill
     *   Description : delete a skill
     *   Author      : sudipta
     *   Date        : 2021-OCT-27
     **/

    public function deleteSkill($id=null){

        $check= Skill::where('id',$id)->first();
        if($check==null){
            session()->flash('error','Something went wrong');
            return redirect()->route('admin.manage.skill');
        }
        $user_skill_check=ProviderToSkills::where('skills_id',$id)->count();
        if(@$user_skill_check>0)
        {
            session()->flash('error','Skill can not be deleted as it is associated with a user.');
                 return redirect()->route('admin.manage.skill');
        } 

        Skill::where('id',$id)->update(['status'=>'D']);
        session()->flash('success','Skill deleted successfully');
        return redirect()->route('admin.manage.skill');
    }
}

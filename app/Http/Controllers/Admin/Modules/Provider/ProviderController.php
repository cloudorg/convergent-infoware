<?php

namespace App\Http\Controllers\Admin\Modules\Provider;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Auth;
use App\User;
use App\Country;
use Storage;
use App\Models\Category;
use App\Models\Language;
use App\Models\Skill;
use App\Models\ProviderToLanguage;
use App\Models\ProviderToSkills;
use App\Models\ProviderToImage;
use App\Models\ProToCategory;
use App\Models\SubCategory;
use App\Models\City;
use App\Models\State;
class ProviderController extends Controller
{
    public function __construct(){

        return $this->middleware('admin.auth:admin');
  }

  public function index(Request $request){
    $data['category'] = Category::where('status','A')->get();
    $data['skill'] = Skill::where('status','A')->get();
    $data['states'] = State::orderBy('name','ASC')->get();
    $data['cites'] = City::orderBy('name','ASC')->get();
    $data['country'] = Country::orderBy('name','ASC')->get();
    if($request->all()){
     $data['providers'] = User::with(['proToCategory.categoryName','providerSkill.userSkill'])->where('user_type','S')->where('status','!=','D');

     if($request->keyword){

            $data['providers'] = $data['providers']->where(function($q) use($request){

                $q->where('name','like','%'.$request->keyword.'%')->orWhere('email','like','%'.$request->keyword.'%')
                ->orWhere('mobile_number','like','%'.$request->keyword.'%')
                ->orWhere('user_id','like','%'.$request->keyword.'%');
            });
     }
     if($request->location){

        $data['providers'] = $data['providers']->where(function($q) use($request){

             $q->where('city','like','%'.$request->location.'%')
             ->orWhere('state','like','%'.$request->location.'%');
        });
     }
     if($request->name){

         $data['providers'] = $data['providers']->where(function($q) use($request){

               $q->where('name','like','%'.$request->name.'%');
         });
     }
     if($request->category){

        $data['providers'] = $data['providers']->whereHas('proToCategory',function($q) use($request){

              $q->where('category_id',$request->category);
        });
    }
    if($request->skill){

        $data['providers'] = $data['providers']->whereHas('providerSkill',function($q) use($request){

              $q->where('skills_id',$request->skill);
        });
    }
     if($request->email){

         $data['providers'] = $data['providers']->where(function($q) use($request){

             $q->where('email','like','%'.$request->email.'%');
       });
     }
     if($request->phone){

         $data['providers'] = $data['providers']->where(function($q) use($request){

             $q->where('mobile_number','like','%'.$request->phone.'%');
       });
     }
     if($request->from_date){

         $data['providers'] = $data['providers']->where(function($q) use($request){

             $q->where(DB::raw("DATE(created_at)") ,'>=',date('Y-m-d',strtotime($request->from_date)));
       });
     }
     if($request->to_date){

         $data['providers'] = $data['providers']->where(function($q) use($request){

             $q->where(DB::raw("DATE(created_at)") ,'<=',date('Y-m-d',strtotime($request->to_date)));
       });

     }
     if($request->signup_from){

        $data['providers'] = $data['providers']->where(function($q) use($request){

            $q->where('signup_from','like','%'.$request->signup_from.'%');
      });
     }

     if($request->status){

         $data['providers'] = $data['providers']->where(function($q) use($request){

             $q->where('status','like','%'.$request->status.'%');
       });
     }
     if($request->approval_status){

        $data['providers'] = $data['providers']->where(function($q) use($request){

            $q->where('email_mob_change','like','%'.$request->approval_status.'%');
      });
    }
    if($request->country!=null){
         $data['providers'] = $data['providers']->where(function($q) use($request){

              $q->where('country',$request->country);
        });
    }
    if($request->state!=null){
         $data['providers'] = $data['providers']->where(function($q) use($request){

              $q->where('state',$request->state);
        });
    }
    if($request->city!=null){
         $data['providers'] = $data['providers']->where(function($q) use($request){

              $q->where('city',$request->city);
        });
    }

     $data['providers'] = $data['providers']->orderBy('id','desc')->paginate(10);
     return view('admin.modules.provider.manage_provider_list',$data);

    }
    $data['providers'] = User::with(['proToCategory.categoryName','providerSkill.userSkill'])->where('user_type','S')->where('status','!=','D')->orderBy('id','desc')->paginate(10);
    return view('admin.modules.provider.manage_provider_list',$data);
}

public function providerStatusUpdate(Request $request){

    $existuser =  User::where('id',$request->id)->whereIn('status',['A','I','U'])->first();
    if($existuser){
         if($existuser->status == 'A'){

             $update['status'] = 'I';
         }
         else if($existuser->status == 'I'){

           $update['status'] = 'A';
         }
         $existuser->update($update);

         $userupdate = User::where('id',$request->id)->where('status', '!=','D')->first();

       return response()->json($userupdate);
    }
}

public function providerApprovalStatusUpdate(Request $request){

    $existuser =  User::where('id',$request->id)->whereIn('status',['A','I','U'])->first();
    if($existuser){
         if($existuser->approval_status == 'N'){

             $update['approval_status'] = 'Y';
         }
         $existuser->update($update);

         $userupdate = User::where('id',$request->id)->where('status', '!=','D')->first();

       return response()->json($userupdate);
    }

}

public function providerDetails($id=null){

    if($id!=''){

          $data['provider'] = User::where('id',$id)->where('status','!=','D')->with(['proToCategory.categoryName','userCity','userState'])->first();
          $data['country'] = Country::where('id',$data['provider']->country)->value('name');
          $data['language'] = ProviderToLanguage::where('user_id',$id)->get();
          $data['skills'] = ProviderToSkills::where('user_id',$id)->get();
          $data['mySubCategory'] = ProToCategory::where('user_id',$id)->where('master_id','!=', 0)->get();
          $data['images'] = ProviderToImage::where('user_id',$id)->get();
          return view('admin.modules.provider.view_details',$data);
    }
}

  public function providerEditProfile($id=null){
    $data['provider'] = User::where('id',$id)->where('status','!=','D')->first();
    $data['allCountry']=Country::get();
    $data['allCategory']=Category::get();
    $data['allLanguage']=Language::get();
    $data['allSkill']=Skill::get();
    $data['allUserLanguage']=ProviderToLanguage::where('user_id', $id)->get();
    $data['allUserSkill']=ProviderToSkills::where('user_id', $id)->get();
    $data['allWorkImage']=ProviderToImage::where('user_id', $id)->get();
    $data['states'] = State::where('country_id',$data['provider']->country)->get();
    $data['cites'] = City::where('state_id',$data['provider']->state)->get();

    $data['myCategory']=ProToCategory::where('user_id', $id)->where('master_id','=', 0)->first();
    $data['allSubCategory']=[];
    $data['mySubCategory']=ProToCategory::where('user_id',$id)->where('master_id','!=', 0)->get();
    if($data['myCategory']!==null){
        $data['allSubCategory'] = SubCategory::where('category_id',$data['myCategory']->category_id)->get();
    }
    // return $data['allUserSkill'];

    return view('admin.modules.provider.edit_provider')->with($data);
  }

   public function providerEditProfileSave(Request $request){

    $request->validate([
        'first_name' => 'required',
        'last_name' => 'required',
        'country' => 'required',
        'state' => 'required',
        'city' => 'required',
        'address' => 'required',
        'about' => 'required',
        'category' => 'required',
        'budget' => 'required',
        'budget_type' => 'required',
        'experience' => 'required',
    ]);
    $existuser = User::where('id',$request->proId)->where('status','!=','D')->first();
    $name = $request->first_name . ' ' . $request->last_name;
    $upd = [];
    $upd['name'] = @$name;
    $upd['city'] = @$request->city;
    $upd['state'] = @$request->state;
    $upd['country'] = @$request->country;
    $upd['address'] = @$request->address;
    $upd['about'] = @$request->about;
    $upd['website'] = @$request->website;
    $upd['whatsapp_no'] = @$request->whatsapp_no;
    //$upd['category_id'] = @$request->category;
    $upd['budget'] = @$request->budget;
    $upd['budget_type'] = @$request->budget_type;
    $upd['experience'] = @$request->experience;
    $slug = str_slug($name, "-");
    $upd['slug'] = $slug . "-" . @$request->proId;
    if (@$request->profile_picture) {
        // $image = $request->profile_pic;
            // $filename = time() . '-' . rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
            // Storage::putFileAs('public/profile_picture', $image, $filename);
            // $upd['profile_img'] = $filename;
            // @unlink(storage_path('app/public/profile_picture/' . auth()->user()->profile_img));
            @unlink(storage_path('app/public/profile_picture/' . $existuser->profile_pic));
            $destinationPath = "storage/app/public/profile_picture/";
            $img1 = str_replace('data:image/png;base64,', '', @$request->profile_picture);
            $img1 = str_replace(' ', '+', $img1);
            $image_base64 = base64_decode($img1);
            $img = time() . '-' . rand(1000, 9999) . '.png';
            $file = $destinationPath . $img;
            file_put_contents($file, $image_base64);
            chmod($file, 0755);
            $upd['profile_pic'] = $img;
    }
    if (@$request->gov_id) {
        $image = $request->gov_id;
        $filename = time() . '-' . rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
        Storage::putFileAs('public/gov_id_image', $image, $filename);
        $upd['gov_id_image'] = $filename;
        @unlink(storage_path('app/public/gov_id_image/' . $existuser->gov_id_image));
    }
    if (@$request->certificate) {
        $image = $request->certificate;
        $filename = time() . '-' . rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
        Storage::putFileAs('public/certificate_image', $image, $filename);
        $upd['certificate_image'] = $filename;
        @unlink(storage_path('app/public/certificate_image/' . $existuser->certificate_image));
    }


    $skill = explode(",", @$request->skill);
    // return $skill;
    foreach ($skill as $item) {
        $insSkill = [];
        $insSkill['user_id'] = $request->proId;
        $insSkill['skills_id'] = $item;
        if(@$item){
            $checkAvailable =  ProviderToSkills::where('user_id', $request->proId)->where('skills_id', $item)->first();
            if ($checkAvailable == null) {
                ProviderToSkills::create($insSkill);
            }
        }

    }
    ProviderToSkills::where('user_id', $request->proId)->whereNotIn('skills_id', $skill)->delete();

    $language= explode(",", @$request->language);
    // return $language;
    foreach ($language as $item1) {
        $insLanguage = [];
        $insLanguage['user_id'] = $request->proId;
        $insLanguage['language_id'] = $item1;
        if(@$item1){
            $checkAvailable =  ProviderToLanguage::where('user_id', $request->proId)->where('language_id', $item1)->first();
            if ($checkAvailable == null) {
                ProviderToLanguage::create($insLanguage);
            }
        }

    }
    ProviderToLanguage::where('user_id', $request->proId)->whereNotIn('language_id', $language)->delete();

    $insCat=[];
    $insCat['category_id']=$request->category;
    $insCat['master_id'] = 0;
    $insCat['user_id'] = $request->proId;
    ProToCategory::where('user_id', $request->proId)->where('master_id', 0)->delete();
    ProToCategory::create($insCat);

    $sub_category = explode(",", @$request->sub_category);
    // return $sub_category;
    foreach ($sub_category as $item2) {
        $insSubCategory = [];
        $insSubCategory['user_id'] = $request->proId;
        $insSubCategory['category_id'] = $item2;
        $insSubCategory['master_id'] = $request->category;
        if(@$item1){
            $checkAvailable =  ProToCategory::where('user_id', $request->proId)->where('category_id', $item2)->where('master_id','!=', 0)->first();
            if ($checkAvailable == null) {
                ProToCategory::create($insSubCategory);
            }
        }

    }

    ProToCategory::where('user_id', $request->proId)->where('master_id','!=', 0)->whereNotIn('category_id', $sub_category)->delete();



    $user = User::where('id', $request->proId)->update($upd);
    if( @$user ){
        session()->flash('success', 'Profile updated successfully');
        return redirect()->route('admin.edit.provider.profile',$request->proId);
    }
    session()->flash('error', 'Profile not updated');
    return redirect()->route('admin.edit.provider.profile',$request->proId);
   }


   public function providerWorkImageSave(Request $request,$id=null){

    $ins=[];
    if (@$request->work_image) {
        // @unlink(storage_path('app/public/work_image/' . auth()->user()->profile_pic));
        $destinationPath = "storage/app/public/work_image/";
        $img1 = str_replace('data:image/png;base64,', '', @$request->work_image);
        $img1 = str_replace(' ', '+', $img1);
        $image_base64 = base64_decode($img1);
        $img = time() . '-' . rand(1000, 9999) . '.png';
        $file = $destinationPath . $img;
        file_put_contents($file, $image_base64);
        chmod($file, 0755);
        $ins['image'] = $img;
        $ins['user_id'] = $id;
    }
    $create = ProviderToImage::create($ins);
    if( @$create ){
        session()->flash('success', ' Work image upload successfully');
        return redirect()->route('admin.edit.provider.profile',$id);
    }
    session()->flash('error', 'Work image not upload');
    return redirect()->route('admin.edit.provider.profile',$id);
   }


     public function providerWorkImageRemove($id=null,$proid=null){

         $checkimage = ProviderToImage::where('user_id',$proid)->count();
         if($checkimage == 1){

             session()->flash('error', 'Image not removed');
             return redirect()->route('admin.edit.provider.profile',$proid);

         }else{

            $data= ProviderToImage::where('id',$id)->first();
            if(@$data){
                @unlink(storage_path('app/public/work_image/' .$data->image));
                ProviderToImage::where('id', $data->id)->delete();
                session()->flash('success', ' Image removed');
                return redirect()->route('admin.edit.provider.profile',$proid);
            }
         }

        session()->flash('error', 'Image not removed');
        return redirect()->route('admin.edit.provider.profile',$proid);
     }

     public function proDeleteProfile($id=null){


    $check= User::where('id',$id)->first();
    if($check==null){
        session()->flash('error','Something went wrong');
        return redirect()->route('admin.manage.provider');
    }

    User::where('id',$id)->update(['status'=>'D']);
    session()->flash('success','User profile deleted successfully');
    return redirect()->route('admin.manage.provider');
     }

     public function protDeleteAll(Request $request){

        if($request->allid){
       
            User::whereIn('id',$request->allid)->update(['status'=>'D']);
           session()->flash('success','User profile deleted successfully');
           return redirect()->route('admin.manage.provider');
        }
           
           session()->flash('error','Something went wrong.please select at least one user.');
           return redirect()->route('admin.manage.provider');
     }
}

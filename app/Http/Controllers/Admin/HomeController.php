<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;

class HomeController extends Controller
{

    protected $redirectTo = '/admin/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    /**
     * Show the Admin dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        
        $data['totalAgent'] = User::where('user_type','A')->count();
        $data['totalUser'] = User::where('user_type','U')->count();
        $data['totalProvider'] = User::where('user_type','S')->count();
        $data['totalNewAgent'] = User::where('user_type','A')->where('singup_date','like','%'.date('Y-m-d').'%')->count();
        $data['totalNewUser'] = User::where('user_type','U')->where('singup_date','like','%'.date('Y-m-d').'%')->count();
        $data['totalNewProvider'] = User::where('user_type','S')->where('singup_date','like','%'.date('Y-m-d').'%')->count();
        return view('admin.home')->with($data);
    }

}

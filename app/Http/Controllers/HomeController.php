<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Property;
use App\Models\State;
use App\Models\City;
use App\Models\Locality;
use App\Models\SubCategory;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['propertyDetails'] = Property::with('propertyImageMain')->where('status','A')->where('show_in_home','Y')->get();
        return view('home');
    }

    public function checkEmail(Request $request)
    {
        if ($request->email) {
            $checkEmail = User::whereIn('status', ['A', 'I', 'U'])->where('email', $request->email)->count();
            if ($checkEmail > 0) {
                return response('false');
            } else {
                return response('true');
            }
        }
        return response('no email');
    }

    public function checkMobile(Request $request)
    {
        if ($request->mobile) {
            $checkMobile = User::whereIn('status', ['A', 'I', 'U'])->where('mobile_number', $request->mobile)->count();
            if ($checkMobile > 0) {
                return response('false');
            } else {
                return response('true');
            }
        }
        return response('no mobile');
    }


    public function getState(Request $request)
    {
        $data = State::where('country_id',$request->country)->get();
        $response=array();
        $result="<option value=''>Select State</option>";
        if(@$data->isNotEmpty())
        {
            foreach($data as $rows)
            {
                // if(@$request->id==$rows->id)
                // {
                //     $result.="<option value='".$rows->id."' selected >".$rows->name."</option>";
                // }

                // else
                // {
                    $result.="<option value='".$rows->id."' >".$rows->name."</option>";
                // }

            }
        }
        $response['state']=$result;
        return response()->json($response);
    }

    public function getCity(Request $request)
    {
        $data = City::where('state_id',$request->state)->get();
        $response=array();
        $result="<option value='' >Select City</option>";
        if(@$data->isNotEmpty())
        {
            foreach($data as $rows)
            {
                // if(@$request->id==$rows->id)
                // {
                //     $result.="<option value='".$rows->id."' selected >".$rows->name."</option>";
                // }

                // else
                // {
                    $result.="<option value='".$rows->id."' >".$rows->name."</option>";
                // }

            }
        }
        $response['city']=$result;
        return response()->json($response);
    }


    /**
     *   Method      : searchLocality
     *   Description : Locality name Search
     *   Author      : Soumojit
     *   Date        : 2021-DEC-07
     **/

    public function searchLocality(Request $request)
    {
        if ($request->name) {
            $html = '';
            $allLocality= Locality::where('locality_name', 'like', $request->name . '%')->get();
            if ($allLocality) {
                foreach ($allLocality as $key => $value) {
                    $html .= '<li onclick="add_searchbar(this)" ><a href="javascript:void(0)">' . $value->locality_name . '</a></li>';
                }
            }
            return $html;
        }
    }
    /**
     *   Method      : searchLocalityAvailable
     *   Description : Locality name Search available
     *   Author      : Soumojit
     *   Date        : 2021-DEC-07
     **/

    public function searchLocalityAvailable(Request $request)
    {
        if ($request->name) {
            $html = '';
            $allLocality= Locality::where('locality_name', 'like', $request->name . '%')->get();
            $allCity= City::where('name', 'like', $request->name . '%')->get();
            if(@$allCity){
                foreach ($allCity as $key1 => $value1) {
                    $check=Property::where('city',$value1->id)->first();
                    if($check){
                        $html .= '<li onclick="add_searchbar(this)" ><a href="javascript:void(0)">' . $value1->name . '</a></li>';
                    }
                }
            }
            if ($allLocality) {
                foreach ($allLocality as $key => $value) {
                    $check=Property::where('locality',$value->id)->first();
                    if($check){
                        $html .= '<li onclick="add_searchbar(this)" ><a href="javascript:void(0)">' . $value->locality_name . '</a></li>';
                    }
                }
            }
            return $html;
        }
    }

    public function getSubCategory(Request $request){

        $data = SubCategory::where('category_id',$request->category)->get();
        $response=array();
        $result="<option value='' >Select Sub Category</option>";
        if(@$data->isNotEmpty())
        {
            foreach($data as $rows)
            {
                // if(@$request->id==$rows->id)
                // {
                //     $result.="<option value='".$rows->id."' selected >".$rows->name."</option>";
                // }

                // else
                // {
                    $result.="<option value='".$rows->id."' >".$rows->name."</option>";
                // }

            }
        }
        $response['subcategory']=$result;
        return response()->json($response);
    }

    public function getSubcategoryUser(Request $request){
        $data = SubCategory::where('category_id',$request->category)->get();
        $response=array();
        $result=$data ;
        $response['sub_category']=$result;
        return response()->json($response);
    }
}

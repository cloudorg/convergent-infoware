<?php

namespace App\Http\Controllers\Modules\Search;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Mail;
use App\Country;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Storage;
use App\Models\FacilitiesAmenities;
use App\Models\Property;
use App\Models\PropertyToImage;
use App\Models\PropertyToFacilitiesAmenities;
use App\Models\AgentToAvailability;
use App\Models\PropertyVisitRequest;



class PropertySearchController extends Controller
{
     public function __construct()
    {
        // $this->middleware('auth');
    }


    /**
     *   Method      : searchProperty
     *   Description : for search Property
     *   Author      : Soumojit
     *   Date        : 2021-NOv-13
     **/

    public function searchProperty(Request $request){
        $myAllProperty= Property::with(['propertyImageMain','propertyUser'])->where('status','=','A')->where('aprove_by_admin','Y');
        $data['maxPrice']=Property::where('status','!=','I')->orderby('budget_range_from','desc')->first();
        // $allDetails= Property::select('status','bathroom','no_of_bedrooms')->where('status','=','A')->pluck('bathroom','no_of_bedrooms');
        // return $allDetails;
        if(@$request->all()){
            // return $request;
            if(@$request->property_type){
                $myAllProperty=$myAllProperty->whereIn('property_type',$request->property_type);
            }
            if(@$request->property_for){
                $myAllProperty=$myAllProperty->whereIn('property_for',$request->property_for);
            }
            if(@$request->no_of_bedrooms){
                $myAllProperty=$myAllProperty->where('no_of_bedrooms','>=',$request->no_of_bedrooms);
            }
            if(@$request->bathroom){
                $myAllProperty=$myAllProperty->where('bathroom','>=',$request->bathroom);
            }
            if(@$request->furnishing){
                $myAllProperty=$myAllProperty->where('furnishing',$request->furnishing);
            }
            if(@$request->construction_status){
                $myAllProperty=$myAllProperty->where('construction_status',$request->construction_status);
            }
            if($request->amount1!=null && $request->amount2!=null ){
                $myAllProperty=$myAllProperty->whereBetween('budget_range_from',[$request->amount1 , $request->amount2]);
            }
            if(@$request->keyword){
                $myAllProperty=$myAllProperty->where(function($query) use ($request){
                    $query->where('name','LIKE','%'.$request->keyword.'%')
                    ->orWhere('state','LIKE','%'.$request->keyword.'%')
                    ->orWhere('city','LIKE','%'.$request->keyword.'%')
                    ->orWhereHas('countryName',function($q) use ($request){
                        $q->where('name','LIKE','%'.$request->keyword.'%');
                    });
                });
        			// ->orWhere('profile_headline','LIKE','%'.request('keyword').'%')
        			// 	  ->orWhereHas('companyName',function($q){
        			// 		$q->where('name','LIKE','%'.request('keyword').'%');
        			// 	});
                // ->where('name',[$property_type]);
            }
            // if(@$request->lat && $request->long){
            //     $query =  Property::selectRaw("*,(6371 * acos ( cos ( radians('".$request->lat."') ) * cos( radians( address_lat ) ) * cos( radians( address_long ) - radians('".$request->long."') ) + sin ( radians('".$request->lat."') ) * sin( radians( address_lat ) ))) as distance");
            //     $query = $query->having("distance", "<", $request->distance)->pluck('id')->toArray();
            //     $myAllProperty=$myAllProperty->whereIn('id',$query);

            // }
            if(@$request->location){
                $myAllProperty=$myAllProperty->where(function($query) use ($request){
                    $query->whereHas('localityName',function($q) use ($request){
                        $q->where('locality_name','LIKE','%'.$request->location.'%');
                    })->orWhereHas('cityName',function($q) use ($request){
                        $q->where('name','LIKE','%'.$request->location.'%');
                    });
                });
            }
            if($request->sqFtFrom!=null && $request->sqFtTo!=null ){
                $myAllProperty=$myAllProperty->whereBetween('area',[$request->sqFtFrom , $request->sqFtTo]);
            }

            if(@$request->short_by_value==0 ){
                $myAllProperty=$myAllProperty->orderBy('id','desc');
            }
            if(@$request->short_by_value==1 ){
                $myAllProperty=$myAllProperty->orderBy('budget_range_from','asc');
            }
            if(@$request->short_by_value==2 ){
                $myAllProperty=$myAllProperty->orderBy('budget_range_from','desc');
            }
            $data['key']=$request->all();
        }
        if($request->short_by_value==null){
            $myAllProperty=$myAllProperty->orderBy('id','desc');
        }
        $data['totalProperty']=$myAllProperty->count();
        $myAllProperty=$myAllProperty->paginate(9);


        $data['allProperty']=$myAllProperty;
        // return $data['allProperty'];
        return view('modules.search.search_property')->with($data);
    }

    /**
     *   Method      : searchPropertyDetails
     *   Description : for Property Details
     *   Author      : Soumojit
     *   Date        : 2021-NOv-15
     **/
    public function searchPropertyDetails($slug){
        $propertyDetails=Property::with(['propertyImageMain','propertyUser.userCountry','localityName','cityName','stateName'])->where('status','!=','I')->where('slug',$slug)->first();
        if($propertyDetails==null){
            session()->flash('error', 'Something went wrong');
            return redirect()->route('search.property');
        }
        $data['propertyDetails']=$propertyDetails;
        $data['propertyFacilitiesAmenities']=PropertyToFacilitiesAmenities::with(['facilitiesAmenitiesName'])->where('property_id',$propertyDetails->id)->get();
        $data['propertyImageExterior']=PropertyToImage::where('image_for','EP')->where('property_id',$propertyDetails->id)->get();
        $data['propertyImageInterior']=PropertyToImage::where('image_for','IP')->where('property_id',$propertyDetails->id)->get();
        $data['propertyImageFloor']=PropertyToImage::where('image_for','FP')->where('property_id',$propertyDetails->id)->get();
        $data['similarProperty']=Property::with(['propertyImageMain','propertyUser'])->where('status','A')->get();

        $dayAvailability= AgentToAvailability::where('user_id',$propertyDetails->user_id)->pluck('day')->toArray();
        $dayAvailability=array_unique($dayAvailability);

        $replacements = array(
            '7' => '0',
        );
        foreach ($dayAvailability as $key => $value) {
            if (isset($replacements[$value])) {
                $dayAvailability[$key] = $replacements[$value];
            }
        }
        // $dayAvailability=array_unique($dayAvailability);
        $data['dayAvailability']= $dayAvailability;
        return view('modules.search.search_property_details')->with($data);

    }

     /**
     *   Method      : getTimeSlot
     *   Description : for get Time Slot
     *   Author      : Soumojit
     *   Date        : 2021-Nov-23
     **/
    public function getTimeSlot(Request $request){

        $response = [
            'jsonrpc' => 2.0
        ];
        $params = $request->params;
        $day = date('N',strtotime($params['date']));
        $allTimeSlot=AgentToAvailability::where('user_id',$params['user_id'])->where('day',$day)->get();
        $new=[];
        $html='';
        $html.='<option value="" selected>Select Time Slot</option>';
        foreach($allTimeSlot as $friday){
            $timeDifference=(strtotime($friday->to_time)-strtotime($friday->from_time))/60;
            if($timeDifference>30){
                $i=0;
                $num=($timeDifference/30);
                for($i=0; $i<$num ; $i++){
                    $timeAdd=30*$i;
                    $from_time=date('H:i:s',strtotime($timeAdd." minutes",strtotime($friday->from_time)));
                    $to_time=date('H:i:s',strtotime("30 minutes",strtotime($from_time)));
                    $check=PropertyVisitRequest::whereBetween('visit_date',[date('Y-m-d H:i:s',strtotime($params['date'].$from_time)),date('Y-m-d H:i:s',strtotime("-1 seconds",strtotime($params['date'].$to_time)))])
                    ->where('agent_id',$params['user_id'])->count();
                    if($check==0){
                        $new[]=[
                            'day'=>$friday->day,
                            'from_time'=>$from_time,
                            'to_time'=>$to_time,
                        ];
                        $html.='<option value='.date('H:i',strtotime($from_time)).'>'.date('H:i',strtotime($from_time)).'</option>';
                    }
                }
            }else{
                $new[]=[
                    'day'=>$friday->day,
                    'from_time'=>$friday->from_time,
                    'to_time'=>$friday->to_time,
                ];
                $html.='<option value='.date('H:i',strtotime($friday->from_time)).'>'.date('H:i',strtotime($friday->from_time)).'</option>';
            }
        }
        if($allTimeSlot->isEmpty()){
            $html.='<option value="" selected>No Time Slot Available</option>';
        }
        $response['result']['data'] = $new;
        $response['result']['html'] = $html;
        return response()->json($response);
    }

}

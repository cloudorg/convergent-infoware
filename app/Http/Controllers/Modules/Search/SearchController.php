<?php

namespace App\Http\Controllers\Modules\Search;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Category;
use App\Models\Property;
use App\Models\PropertyToImage;
use App\Models\PropertyToFacilitiesAmenities;
use App\Models\ProviderToImage;
use App\Models\State;
use App\Models\City;
use App\Models\SubCategory;
class SearchController extends Controller
{
    /**
    *   Method      : index
    *   Description : service provider search listing and filtering
    *   Author      : sudipta
    *   Date        : 2021-NOV-12
    **/
    public function index(Request $request,$slug=null){
        $data['category'] = Category::where('status','A')->get();
        $data['states'] = State::where('country_id',101)->orderBy('name','ASC')->get();
        $data['subcategory'] = [];
        $data['cites'] =[];
        $data['maxPrice']=User::where('user_type','S')->orderby('budget','desc')->first();
        // $experience = User::where('user_type','S')->where('approval_status','Y')->pluck('experience')->toArray();
        $allProviders = User::with(['proToCategory.categoryName','providerToLanguage.userLanguage','proToSubCategory'])->where('user_type','S')->where('status','A')->where('approval_status','Y');
        if(@$slug != null){
            $subcat = [];
           $subcat = SubCategory::where('slug',$slug)->first();
            
            $allProviders = $allProviders->whereHas('proToSubCategory',function($q) use($subcat){
                $q->where('category_id', @$subcat->id);
            });
            $data['subcategory'] = SubCategory::orderBy('name','ASC')->get();
            $data['catId'] = SubCategory::where('slug',$slug)->first();
            $data['subcatId'] = SubCategory::where('slug',$slug)->first();  
        }
        if($request->all()){
            if($request->category){
                $data['subcategory'] = SubCategory::orderBy('name','ASC')->get();
                $allProviders = $allProviders->whereHas('proToCategory', function($q) use($request){
                    $q->where('category_id',$request->category);
                });
            }
            if($request->sub_category){
                $data['subcategory'] = SubCategory::orderBy('name','ASC')->get();
                $data['catId'] = SubCategory::where('id',$request->sub_category)->first();
                $allProviders = $allProviders->whereHas('proToSubCategory', function($q) use($request){
                    $q->where('category_id',$request->sub_category);
                });
            }
            if($request->state){

                $data['cites'] = City::orderBy('name','ASC')->get();
                $allProviders=$allProviders->where('state',$request->state);
            }
            if($request->city){

                $allProviders=$allProviders->where('city',$request->city);
            }
            if($request->amount1!=null && $request->amount2!=null ){
                $allProviders=$allProviders->whereBetween('budget',[$request->amount1 , $request->amount2]);
            }
            if(@$request->rating){

                foreach(@$request->rating as $key=>$rate){
                    if($rate == 3){

                        $allProviders = $allProviders->where('avg_review','>=',2.75);

                    }
                   if($rate == 4){

                        $allProviders = $allProviders->where('avg_review','>=',3.75);

                    }
                    if($rate == 2){

                        $allProviders = $allProviders->where('avg_review','>=',2.75);

                    }

                }

            }
            if(@$request->year){

                    foreach(@$request->year as $yr){
                        if(@$yr == 2){

                            $allProviders = $allProviders->whereBetween('experience',[0,2]);

                        }
                        if(@$yr == 5){

                            $allProviders = $allProviders->whereBetween('experience',[3,5]);

                        }
                        if(@$yr == 10){

                        $allProviders = $allProviders->whereBetween('experience',[6,10]);

                        }
                       if(@$yr == 11){

                            $allProviders = $allProviders->where('experience','>=',11);

                        }


            }
        }
            if(@$request->sort_by_value){
                if(@$request->sort_by_value == 'H'){
                    $allProviders = $allProviders->orderBy('budget','desc');
                }
                else if(@$request->sort_by_value == 'L'){
                    $allProviders = $allProviders->orderBy('budget','asc');
                }
                else if(@$request->sort_by_value == 'RH'){
                    $allProviders = $allProviders->orderBy('tot_review','desc');
                }
                else if(@$request->sort_by_value == 'RL'){
                    $allProviders = $allProviders->orderBy('tot_review','asc');
                }
            }
            $data['request'] = $request->all();
        }
        if(empty(@$request->sort_by_value)){
            $allProviders = $allProviders->orderBy('id','desc');
        }
        $allProviders = $allProviders->paginate(10);
        $data['providers'] = $allProviders;
        return view('modules.search.find_service_pro')->with($data);
    }

     /**
     *   Method      : proProfileShow
     *   Description : service provider profile show
     *   Author      : sudipta
     *   Date        : 2021-NOV-12
     **/

    public function proProfileShow($slug=null){
        $data['provider'] = User::with('proToCategory.categoryName')->where('slug',$slug)->where('approval_status','Y')->first();
        $workImages = ProviderToImage::where('user_id',$data['provider']->id)->get();
        $new=[];
        $i=0;
        foreach($workImages as $key=>$item){
            $new[$i][]=$item;
            if(($key+1)%2==0){
                $i=$i+1;
            }
        }
        $data['workImages']=$new;
        return view('modules.search.pro_profile')->with($data);
    }

     /**
     *   Method      : agentProfileShow
     *   Description : agent profile show
     *   Author      : sudipta
     *   Date        : 2021-NOV-12
     **/

    public function agentProfileShow($slug=null){

     $data['agent'] = User::where('slug',$slug)->where('approval_status','Y')->first();
     if($data['agent']==null){
          return redirect()->route('search.property');
      }
     $data['property'] = Property::with('propertyImageMain')->where('user_id',$data['agent']->id)->where('status','A')->where('aprove_by_admin','Y')->get();
     return view('modules.search.agent_profile')->with($data);
}

 public function hirePro(){
       $data['category'] = Category::where('status','A')->get();
       $data['states'] = State::where('country_id',101)->orderBy('name','ASC')->get();
       $data['cites'] = [];
       $data['categories'] = Category::with('subCategoryDetails')->where('status','A')->get();
      return view('modules.search.hire_pro')->with($data);
 }
 
  public function sellPage(){

      return view('modules.search.sell');
 }

}

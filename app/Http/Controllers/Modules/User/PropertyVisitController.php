<?php

namespace App\Http\Controllers\Modules\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Mail;
use App\Country;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\AgentToAvailability;
use App\Models\PropertyVisitRequest;
use App\Models\Property;


class PropertyVisitController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *   Method      : visitRequestStore
     *   Description : for user visit Request Store
     *   Author      : Soumojit
     *   Date        : 2021-Nov-24
     **/
    public function visitRequestStore(Request $request){

        $request->validate([
            'property' => 'required',
            'date' => 'required',
            'time' => 'required',
            // 'address' => 'required',
            'phonenumber' => 'required',
        ]);

        $property=Property::where('id',$request->property)->first();
        if($property==null){
            session()->flash('error', 'Something went wrong');
            return redirect()->back();
        }
        $ins=[];
        $ins['user_id']=Auth::user()->id;
        $ins['property_id']=$request->property;
        $ins['visit_date']=date('Y-m-d H:i:s',strtotime($request->date.$request->time));
        $ins['address']=$request->address;
        $ins['phone']=$request->phonenumber;
        $ins['agent_id']=$property->user_id;
        $create =PropertyVisitRequest::create($ins);
        if( @$create){
            session()->flash('success', 'Visiting request send to agent');
            return redirect()->back();
        }
        session()->flash('error', 'Something went wrong');
        return redirect()->back();

    }

    /**
     *   Method      : visitRequestCheck
     *   Description : for user visit Request Check
     *   Author      : Soumojit
     *   Date        : 2021-Nov-25
     **/

    public function visitRequestCheck(Request $request){

        $from_time=date('Y-m-d H:i:s',strtotime($request->date.$request->time));
        $to_time=date('Y-m-d H:i:s',strtotime("-1 seconds",strtotime($from_time)));
        $to_time=date('Y-m-d H:i:s',strtotime("30 minutes",strtotime($to_time)));
        $check=PropertyVisitRequest::whereBetween('visit_date',[$from_time,$to_time])->where('agent_id',$request->user_id)->count();
        if($check>0){
            return response('false');
        }else{
            return response('true');
        }
        return response('no Slot');
    }


    public function userVisitRequest(Request $request){

        $allVisitRequest = PropertyVisitRequest::with(['propertyDetails','agentDetails'])->where('user_id',Auth::user()->id)->whereNotIn('status',['CA','C']);
        if($request->all()){

             if($request->property_name){

               $allVisitRequest = $allVisitRequest->whereHas('propertyDetails', function($q) use($request){

                     $q->where('name','like','%'.$request->property_name.'%')
                     ->orWhere('slug','like','%'.str_slug($request->property_name).'%');
               });
           }
               if($request->start_date){

                   $allVisitRequest = $allVisitRequest->where(DB::raw("DATE(visit_date)"),'=',date('Y-m-d',strtotime($request->start_date)));
               }
               if($request->time){

                   $allVisitRequest = $allVisitRequest->where(DB::raw("DATE(visit_date)"),'=',date('h:i',strtotime($request->time)));
               }

            $data['key'] = $request->all();

        }
         $allVisitRequest = $allVisitRequest->orderBy('id','desc')->paginate(10);
         $data['allVisitRequest'] = $allVisitRequest;
          return view('modules.user.property_visit_request_list')->with($data);
    }

    public function userVisitRequestCancel($id=null){


    $check= PropertyVisitRequest::where('id',$id)->first();
    if($check==null){
        session()->flash('error','Something went wrong');
        return redirect()->route('user.property.visit.request');
    }

    PropertyVisitRequest::where('id',$id)->update(['status'=>'CA']);
    session()->flash('success','Property visit request cancel successfully');
    return redirect()->route('user.property.visit.request');
    }

}

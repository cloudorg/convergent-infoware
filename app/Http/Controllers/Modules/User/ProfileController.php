<?php

namespace App\Http\Controllers\Modules\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Mail;
use App\Country;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\City;
use App\Models\State;
use App\Models\PropertyVisitRequest;
use App\Models\Property;

class ProfileController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *   Method      : dashboard
     *   Description : for user Dashboard
     *   Author      : Soumojit
     *   Date        : 2021-OCT-28
     **/

    public function dashboard(){
        
       $data['allVisitRequest'] = PropertyVisitRequest::with(['propertyDetails','agentDetails'])->where('user_id',Auth::user()->id)->whereNotIn('status',['CA','C'])->orderBy('id','desc')->get();
        return view('modules.user.dashboard')->with($data);
    }
    /**
     *   Method      : editProfile
     *   Description : for user editProfile view page
     *   Author      : Soumojit
     *   Date        : 2021-OCT-28
     **/

    public function editProfile(){
        $data['allCountry']=Country::get();
        $data['states'] = State::where('country_id',auth()->user()->country)->get();
        $data['cites'] = City::where('state_id',auth()->user()->state)->get();

        return view('modules.user.edit_profile')->with($data);
    }
     /**
     *   Method      : editProfileSave
     *   Description : for user edit Profile Save
     *   Author      : Soumojit
     *   Date        : 2021-OCT-28
     **/

    public function editProfileSave(Request $request){

        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
        ]);
        $name = $request->first_name . ' ' . $request->last_name;
        $upd = [];
        $upd['name'] = $name;
        $upd['gender'] = $request->gender;
        $upd['city'] = $request->city;
        $upd['state'] = $request->state;
        $upd['country'] = $request->country;
        $slug = str_slug($name, "-");
        $upd['slug'] = $slug . "-" . Auth::user()->id;
        if (@$request->profile_picture) {
            // $image = $request->profile_pic;
            // $filename = time() . '-' . rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
            // Storage::putFileAs('public/profile_picture', $image, $filename);
            // $upd['profile_img'] = $filename;
            // @unlink(storage_path('app/public/profile_picture/' . auth()->user()->profile_img));
            @unlink(storage_path('app/public/profile_picture/' . auth()->user()->profile_pic));
            $destinationPath = "storage/app/public/profile_picture/";
            $img1 = str_replace('data:image/png;base64,', '', @$request->profile_picture);
            $img1 = str_replace(' ', '+', $img1);
            $image_base64 = base64_decode($img1);
            $img = time() . '-' . rand(1000, 9999) . '.png';
            $file = $destinationPath . $img;
            file_put_contents($file, $image_base64);
            chmod($file, 0755);
            $upd['profile_pic'] = $img;
        }
        if (@$request->old_password || @$request->new_password || @$request->new_password) {
            if(auth()->user()->password!=null){
                $request->validate([
                    'old_password' => 'required',
                    'new_password' => 'required|confirmed',
                    'new_password_confirmation' => 'required',
                ]);
                $check_user = User::where('id', auth()->user()->id)->first();
                if (!Hash::check($request->old_password, $check_user->password)) {
                    session()->flash('error', 'Old password wrong');
                    return redirect()->back();
                } else {
                    $upd['password'] = \Hash::make($request->new_password);
                }
            }
            $request->validate([
                'new_password' => 'required|confirmed',
                'new_password_confirmation' => 'required',
            ]);
            $upd['password'] = \Hash::make($request->new_password);
        }
        $user = User::where('id', Auth::user()->id)->update($upd);
        if( @$user ){
            session()->flash('success', 'Profile updated successfully');
            return redirect()->route('user.profile');
        }
        session()->flash('error', 'Profile not updated');
        return redirect()->route('user.profile');

    }

    /**
     *   Method      : changeEmail
     *   Description : for user Email change
     *   Author      : Soumojit
     *   Date        : 2021-NOV-10
     **/

    public function changeEmail(Request $request){
        $request->validate([
            'email' => 'required',
        ]);

        $upd = [];
        $upd['temp_email'] = @$request->email;
        if(auth()->user()->email_mob_change==null){
            $upd['email_mob_change']='E';
        }
        if(auth()->user()->email_mob_change=='M'){
            $upd['email_mob_change']='B';
        }
        if(auth()->user()->email_mob_change=='E'){
            $upd['email_mob_change']='E';
        }
        if(auth()->user()->email_mob_change=='B'){
            $upd['email_mob_change']='B';
        }
        User::where('id', Auth::user()->id)->update($upd);
        session()->flash('success', 'Email saved waiting for admin approval');
        return redirect()->route('user.profile');
    }
    /**
     *   Method      : changeMobile
     *   Description : for user Mobile change
     *   Author      : Soumojit
     *   Date        : 2021-NOV-10
     **/

    public function changeMobile(Request $request){
        $request->validate([
            'mobile' => 'required',
        ]);

        $upd = [];
        $upd['temp_mobile'] = @$request->mobile;
        if(auth()->user()->email_mob_change==null){
            $upd['email_mob_change']='M';
        }
        if(auth()->user()->email_mob_change=='E'){
            $upd['email_mob_change']='B';
        }
        if(auth()->user()->email_mob_change=='M'){
            $upd['email_mob_change']='M';
        }
        if(auth()->user()->email_mob_change=='B'){
            $upd['email_mob_change']='B';
        }
        User::where('id', Auth::user()->id)->update($upd);
        session()->flash('success', 'Mobile no saved waiting for admin approval');
        return redirect()->route('user.profile');
    }
}

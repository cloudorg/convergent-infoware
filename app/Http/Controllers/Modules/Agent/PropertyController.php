<?php

namespace App\Http\Controllers\Modules\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Mail;
use App\Country;
use App\Models\City;
use App\Models\State;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Storage;
use App\Models\FacilitiesAmenities;
use App\Models\Property;
use App\Models\PropertyToImage;
use App\Models\PropertyToFacilitiesAmenities;
use App\Models\Locality;



class PropertyController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     *   Method      : editProfile
     *   Description : for agent editProfile view page
     *   Author      : Soumojit
     *   Date        : 2021-OCT-28
     **/

    public function addProperty($id=null){
       // $data['allCountry']=Country::get();
        $data['states'] = State::where('country_id',101)->orderBy('name','ASC')->get();
        $data['cites'] = [];
        if($id==null){
            return view('modules.agent.add_property_details')->with($data);
        }
        $data['propertyDetails']=Property::with('localityName')->where('id',$id)->where('user_id',auth()->user()->id)->first();
        $data['cites'] = City::where('state_id',$data['propertyDetails']->state)->orderBy('name','ASC')->get();
        if($data['propertyDetails']==null){
            session()->flash('error', 'Something went wrong');
            return redirect()->route('agent.add.property');
        }
        return view('modules.agent.add_property_details')->with($data);
    }
     /**
     *   Method      : editProfileSave
     *   Description : for agent edit Profile Save
     *   Author      : Soumojit
     *   Date        : 2021-OCT-28
     **/

    public function propertySave(Request $request,$id=null){
        // return $request;

        $request->validate([
            'property_type' => 'required',
            'property_for' => 'required',
            'property_name' => 'required',
            // 'no_of_bedrooms' => 'required',
            // 'bathroom' => 'required',
            'budget_range_from' => 'required',
            // 'budget_range_to' => 'required',
            // 'area' => 'required',
            // 'super_area' => 'required',
            // 'carpet_area' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'address' => 'required',
            'description' => 'required',
            'locality' => 'required',
            'post_code' => 'required',
            // 'construction_status' => 'required',
            // 'build_year' => 'required',
        ]);

        // if($request->property_type=='O'){
        //     $request->validate([
        //         'office_class' => 'required',
        //     ]);
        // }else{
        //     $request->validate([
        //         'no_of_bedrooms' => 'required',
        //         'bathroom' => 'required',
        //     ]);
        // }
        // if($request->property_for=='R'){
        //     $request->validate([
        //         'rent_payment' => 'required',
        //     ]);
        // }
        if($id==null){
            $ins = [];
            $ins['property_type']= $request->property_type;
            $ins['property_for']= $request->property_for;
            $ins['name']= $request->property_name;
            $ins['no_of_bedrooms']=  @$request->no_of_bedrooms;
            $ins['bathroom']=  @$request->bathroom;
            $ins['budget_range_from']= $request->budget_range_from;
            $ins['budget_range_to']= $request->budget_range_to;
            $ins['area']= @$request->area;
            $ins['super_area']= $request->super_area;
            $ins['carpet_area']= $request->carpet_area;
            $ins['construction_status']= @$request->construction_status;
            $ins['furnishing']= @$request->furnishing;
            $ins['country']= $request->country;
            $ins['state']= $request->state;
            $ins['city']= $request->city;
            $ins['address']= $request->address;
            $ins['description']= $request->description;
            $ins['post_code']= $request->post_code;
            $ins['build_year']= $request->build_year;
            $ins['status']= 'I';
            $ins['office_class']= $request->property_type=='O'?$request->office_class:null;
            $ins['rent_payment']= $request->property_for=='R'?$request->rent_payment:null;
            $ins['preference']= $request->property_for=='R'?$request->preference:null;
            $ins['available_from']= $request->property_for=='R'?date('Y-m-d',strtotime($request->available_from)):null;
            $ins['deposit_amount']= $request->property_for=='R'?$request->deposit_amount:null;
            $ins['maintenance_charge']= $request->property_for=='R'?$request->maintenance_charge:null;
            $ins['floor']= @$request->floor;
            // $ins['address_lat']= $request->lat;
            // $ins['address_long']= $request->long;
            $ins['user_id']= auth()->user()->id;
            if(@$request->property_type == 'L'){
                $land_area_unit = @$request->land_area_unit;
                $land_area = @$request->land_area;
                $ins['land_area'] = $land_area;
                $ins['land_area_unit'] = $land_area_unit;
                if($land_area_unit == 'A'){
                    // Conversion of Acre to Sq ft
                        $area = $land_area * 43560;
                        $ins['area'] = @$area;  
                }else{
                    // Conversion of Sq yard to Sq ft
                        $area = $land_area * 9;
                        $ins['area'] = @$area; 
                }
            }
            $checkLocality = Locality::where('locality_name', $request->locality)->first();
            if(@$checkLocality){
                $ins['locality'] = $checkLocality->id;
            }else{
                $loc=[];
                $loc['locality_name']=$request->locality;
                $loc['city_id']=$request->city;
                $com=Locality::create($loc);
                $ins['locality'] = $com->id;
            }
            $create=Property::create($ins);
            if(@$create){
                $upd=[];
                $slug= str_slug(@$request->property_name, "-");
                $upd['slug'] = $slug. "-".$create->id;
                Property::where('id',$create->id)->update($upd);
                session()->flash('success', 'Property details added successfully');
                return redirect()->route('agent.add.property.image',['id'=>$create->id]);
            }
            session()->flash('error', 'Property details not added');
            return redirect()->route('agent.add.property');
        }
        $propertyDetails=Property::where('id',$id)->where('user_id',auth()->user()->id)->first();
        if($propertyDetails==null){
            session()->flash('error', 'Something went wrong');
            return redirect()->back();
        }
        $upd = [];
        $upd['property_type']= $request->property_type;
        $upd['property_for']= $request->property_for;
        $upd['name']= $request->property_name;
        $upd['no_of_bedrooms']=  @$request->no_of_bedrooms;
        $upd['bathroom']=  @$request->bathroom;
        $upd['budget_range_from']= $request->budget_range_from;
        $upd['budget_range_to']= $request->budget_range_to;
        $upd['area']= $request->area;
        $upd['super_area']= $request->super_area;
        $upd['carpet_area']= $request->carpet_area;
        $upd['construction_status']= @$request->construction_status;
        $upd['furnishing']= @$request->furnishing;
        $upd['country']= $request->country;
        $upd['state']= $request->state;
        $upd['city']= $request->city;
        $upd['address']= $request->address;
        $upd['description']= $request->description;
        $upd['post_code']= $request->post_code;
        $upd['build_year']= $request->build_year;
        $upd['office_class']=  $request->property_type=='O'?$request->office_class:null;
        $upd['rent_payment']=  $request->property_for=='R'?$request->rent_payment:null;
        $upd['preference']=  $request->property_for=='R'?$request->preference:null;
        $upd['available_from']=  $request->property_for=='R'?date('Y-m-d',strtotime($request->available_from)):null;
        $upd['deposit_amount']=  $request->property_for=='R'?$request->deposit_amount:null;
        $upd['maintenance_charge']=  $request->property_for=='R'?$request->maintenance_charge:null;
        $upd['floor']=  @$request->floor;
        // $upd['address_lat']= $request->lat;
        // $upd['address_long']= $request->long;
        $slug= str_slug(@$request->property_name, "-");
        $upd['slug'] = $slug. "-".$propertyDetails->id;
        $checkLocality = Locality::where('locality_name', $request->locality)->first();
        if(@$checkLocality){
            $upd['locality'] = $checkLocality->id;
        }else{
            $loc=[];
            $loc['locality_name']=$request->locality;
            $loc['city_id']=$request->city;
            $com=Locality::create($loc);
            $upd['locality'] = $com->id;
        }
        $updated=Property::where('id',$propertyDetails->id)->update($upd);
        if(@$updated){
            session()->flash('success', 'Property details updated successfully');
            return redirect()->route('agent.add.property.image',['id'=>$propertyDetails->id]);
        }
        session()->flash('error', 'Property details not updated');
        return redirect()->back();


    }
    /**
     *   Method      : addPropertyImage
     *   Description : for add Property Image view page
     *   Author      : Soumojit
     *   Date        : 2021-NOV-11
     **/

    public function addPropertyImage($id=null){
        $data['allCountry']=Country::get();
        $data['allFacilitiesAmenities']=FacilitiesAmenities::where('status','A')->take(20)->get();
        $data['propertyDetails']=Property::where('id',$id)->where('user_id',auth()->user()->id)->first();
        if($data['propertyDetails']==null){
            session()->flash('error', 'Something went wrong');
            return redirect()->route('agent.add.property');
        }
        $data['propertyImageExterior']=PropertyToImage::where('property_id',$id)->where('image_for','EP')->get();
        $data['propertyImageInterior']=PropertyToImage::where('property_id',$id)->where('image_for','IP')->get();
        $data['propertyImageFloor']=PropertyToImage::where('property_id',$id)->where('image_for','FP')->get();
        $data['facilitiesAmenities']=PropertyToFacilitiesAmenities::where('property_id',$id)->pluck('facilities_amenities_id')->toArray();

        return view('modules.agent.add_property_image')->with($data);
    }


    /**
     *   Method      : propertyImageSave
     *   Description : for property Image Save
     *   Author      : Soumojit
     *   Date        : 2021-NOV-11
     **/

    public function propertyImageSave(Request $request,$id=null){


        $data['propertyDetails']=Property::where('id',$id)->where('user_id',auth()->user()->id)->first();
        if($data['propertyDetails']==null){
            session()->flash('error', 'Something went wrong');
            return redirect()->route('agent.add.property');
        }

        $files1 = $request->file('file-1');
        if($request->hasFile('file-1'))
        {
            foreach ($files1 as $file) {
                $filename = time() . '-' . rand(1000, 9999) . '.' . $file->getClientOriginalExtension();
                Storage::putFileAs('public/property_image', $file, $filename);
                $insImage=[];
                $insImage['image']=$filename;
                $insImage['property_id']=$id;
                $insImage['image_for']='EP';
                PropertyToImage::create($insImage);
            }
        }
        $files2 = $request->file('file-2');
        if($request->hasFile('file-2'))
        {
            foreach ($files2 as $file) {
                $filename = time() . '-' . rand(1000, 9999) . '.' . $file->getClientOriginalExtension();
                Storage::putFileAs('public/property_image', $file, $filename);
                $insImage=[];
                $insImage['image']=$filename;
                $insImage['property_id']=$id;
                $insImage['image_for']='IP';
                PropertyToImage::create($insImage);
            }
        }
        $files3 = $request->file('file-3');
        if($request->hasFile('file-3'))
        {
            foreach ($files3 as $file) {
                $filename = time() . '-' . rand(1000, 9999) . '.' . $file->getClientOriginalExtension();
                Storage::putFileAs('public/property_image', $file, $filename);
                $insImage=[];
                $insImage['image']=$filename;
                $insImage['property_id']=$id;
                $insImage['image_for']='FP';
                PropertyToImage::create($insImage);
            }
        }
        if(@$request->facilities_amenities){
        foreach(@$request->facilities_amenities as $facilities_amenities){
            $ins=[];
            $ins['facilities_amenities_id']=$facilities_amenities;
            $ins['property_id']=$id;
            $checkAvailable =   PropertyToFacilitiesAmenities::where('property_id',$id)->where('facilities_amenities_id', $facilities_amenities)->first();
            if ($checkAvailable == null) {
                PropertyToFacilitiesAmenities::create($ins);
            }
        }
        PropertyToFacilitiesAmenities::where('property_id',$id)->whereNotIn('facilities_amenities_id', $request->facilities_amenities)->delete();
    }
        $facilitiesAmenities= PropertyToFacilitiesAmenities::where('property_id',$id)->count();
        $image1= PropertyToImage::where('image_for','EP')->where('property_id',$id)->count();
        $image2= PropertyToImage::where('image_for','IP')->where('property_id',$id)->count();
        $image3= PropertyToImage::where('image_for','FP')->where('property_id',$id)->count();
        if($image1 >0 && $image2 >0 && $image3 >0 && $data['propertyDetails']->status=='I'){
            $allProperty= Property::where('property_id','!=',null)->where('status','!=','I')->get();
            $code='P';
            $sum=str_pad($allProperty->count()+1, 7, '0', STR_PAD_LEFT);
            $upd=[];
            $upd['status'] = 'A';
            if($data['propertyDetails']->property_id==null){
                $upd['property_id'] =$code.$sum;
            }
            Property::where('id',$id)->update($upd);
        }
        if($data['propertyDetails']->status=='I'){
            session()->flash('success', 'Property image added successfully');
            return redirect()->route('agent.my.property');
        }
        session()->flash('success', 'Property image updated successfully');
        return redirect()->route('agent.my.property');
    }
    /**
     *   Method      : propertyImageRemove
     *   Description : for property Image remove
     *   Author      : Soumojit
     *   Date        : 2021-NOV-12
     **/
    public function propertyImageRemove($id=null){

        $insImage=PropertyToImage::where('id',$id)->first();
        if($insImage==null){
            session()->flash('error', 'Something went wrong image not removed');
            return redirect()->back();
        }

        $propertyDetails=Property::where('id',$insImage->property_id)->where('user_id',auth()->user()->id)->first();
        if($propertyDetails==null){
            session()->flash('error', 'Something went wrong image not removed');
            return redirect()->back();
        }
        if($insImage->image_for=='EP'){
            $image= PropertyToImage::where('image_for','EP')->where('property_id',$insImage->property_id)->count();
            if($image==1){
                session()->flash('error', 'Image not removed minimum one exterior image require');
                return redirect()->back();
            }
            @unlink(storage_path('app/public/property_image/' .$insImage->image));
            PropertyToImage::where('id', $insImage->id)->delete();
            session()->flash('success', 'Exterior mage removed');
            return redirect()->back();

        }
        if($insImage->image_for=='IP'){
            $image= PropertyToImage::where('image_for','IP')->where('property_id',$insImage->property_id)->count();
            if($image==1){
                session()->flash('error', 'Image not removed minimum one interior image require');
                return redirect()->back();
            }
            @unlink(storage_path('app/public/property_image/' .$insImage->image));
            PropertyToImage::where('id', $insImage->id)->delete();
            session()->flash('success', 'Interior image removed');
            return redirect()->back();
        }
        if($insImage->image_for=='FP'){
            $image= PropertyToImage::where('image_for','FP')->where('property_id',$insImage->property_id)->count();
            if($image==1){
                session()->flash('error', 'Image not removed minimum one floor image require');
                return redirect()->back();
            }
            @unlink(storage_path('app/public/property_image/' .$insImage->image));
            PropertyToImage::where('id', $insImage->id)->delete();
            session()->flash('success', 'Floor image removed');
            return redirect()->back();
        }
        session()->flash('error', 'Something went wrong image not removed ');
        return redirect()->back();
    }
    /**
     *   Method      : myProperty
     *   Description : for view my Property
     *   Author      : Soumojit
     *   Date        : 2021-NOV-13
     **/
    public function myProperty(Request $request){
        $data['states'] = State::where('country_id',101)->orderBy('name','ASC')->get();
        $data['cites'] =[];
        $myAllProperty= Property::where('user_id',auth()->user()->id)->with('propertyImageMain')->whereNotIn('status',['I','D']);
        if(@$request->all()){
            // return $request;
            if(@$request->state){
                $data['cites'] = City::orderBy('name','ASC')->get();
                $myAllProperty=$myAllProperty->where('state',$request->state);
            }
            if(@$request->city){
                $myAllProperty=$myAllProperty->where('city',$request->city);
            }
            if(@$request->property_for){
                $myAllProperty=$myAllProperty->where('property_for',$request->property_for);
            }
            if(@$request->status){
                if($request->status=='S'){
                    $myAllProperty=$myAllProperty->where('is_sold','Y');
                }else{
                    $myAllProperty=$myAllProperty->where('status',$request->status);
                }

            }
            if(@$request->name){
                $myAllProperty=$myAllProperty->where('name','LIKE','%'.$request->name.'%');
            }
            $data['key']=$request->all();
        }

        $myAllProperty=$myAllProperty->orderBy('id','desc')->paginate(12);
        $data['allProperty']=$myAllProperty;

        // return 10%2;
        // $alluser=User::get();
        // $da=[];
        // $key1=0;
        // foreach($alluser as $key=>$user){
        //     $da[$key1][]=$user;
        //     if(($key+1)%2==0){
        //         $key1=$key1+1;
        //     }

        // }
        // $data['all']=$da;

        return view('modules.agent.my_property')->with($data);

    }

    /**
     *   Method      : myPropertyStatusChange
     *   Description : for agent property status change
     *   Author      : Soumojit
     *   Date        : 2021-Nov-13
     **/

    public function myPropertyStatusChange($id=null){
        $propertyDetails=Property::where('id',$id)->where('user_id',auth()->user()->id)->whereIn('status',['A','B'])->first();
        if($propertyDetails==null){
            session()->flash('error', 'Something went wrong');
            return redirect()->route('agent.my.property');
        }
        if($propertyDetails->status=='A'){
            Property::where('id',$id)->where('user_id',auth()->user()->id)->update(['status'=>'B']);
            session()->flash('success', 'Property successfully inactive');
            return redirect()->route('agent.my.property');
        }
        if($propertyDetails->status=='B'){
            Property::where('id',$id)->where('user_id',auth()->user()->id)->update(['status'=>'A']);
            session()->flash('success', 'Property successfully active');
            return redirect()->route('agent.my.property');
        }
        session()->flash('error', 'Something went wrong');
        return redirect()->route('agent.my.property');
    }
    /**
     *   Method      : myPropertySold
     *   Description : for agent property status change
     *   Author      : Soumojit
     *   Date        : 2021-Nov-13
     **/

    public function myPropertySold($id=null){
        $propertyDetails=Property::where('id',$id)->where('user_id',auth()->user()->id)->whereIn('status',['A','B'])->first();
        if($propertyDetails==null){
            session()->flash('error', 'Something went wrong');
            return redirect()->route('agent.my.property');
        }
        Property::where('id',$id)->where('user_id',auth()->user()->id)->update(['is_sold'=>'Y']);
        session()->flash('success', 'Property Mark as sold');
        return redirect()->route('agent.my.property');
    }
    /**
     *   Method      : myPropertyUnSold
     *   Description : for agent property UnSold
     *   Author      : Soumojit
     *   Date        : 2021-Nov-13
     **/

    public function myPropertyUnSold($id=null){
        $propertyDetails=Property::where('id',$id)->where('user_id',auth()->user()->id)->whereIn('status',['A','B'])->first();
        if($propertyDetails==null){
            session()->flash('error', 'Something went wrong');
            return redirect()->route('agent.my.property');
        }
        Property::where('id',$id)->where('user_id',auth()->user()->id)->update(['is_sold'=>'N']);
        session()->flash('success', 'Property Mark as unsold');
        return redirect()->route('agent.my.property');
    }
    /**
     *   Method      : myPropertyUnSold
     *   Description : for agent property UnSold
     *   Author      : Soumojit
     *   Date        : 2021-Nov-13
     **/

    public function myPropertyDelete($id=null){
        $propertyDetails=Property::where('id',$id)->where('user_id',auth()->user()->id)->where('status','!=','D')->first();
        if($propertyDetails==null){
            session()->flash('error', 'Something went wrong');
            return redirect()->route('agent.my.property');
        }
        Property::where('id',$id)->where('user_id',auth()->user()->id)->update(['status'=>'D']);
        session()->flash('success', 'Property Delete successful');
        return redirect()->route('agent.my.property');
    }


}

<?php

namespace App\Http\Controllers\Modules\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Mail;
use App\Country;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Storage;
use App\Models\AgentToAvailability;
use App\Models\Language;
use App\Models\ProviderToLanguage;
use App\Models\City;
use App\Models\State;
use App\Models\PropertyVisitRequest;
use App\Mail\AgentRescheduleMail;

class ProfileController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *   Method      : dashboard
     *   Description : for agent Dashboard
     *   Author      : Soumojit
     *   Date        : 2021-OCT-28
     **/

    public function dashboard(){
        $today = date('Y-m-d H:i:s');
        $data['visit_req'] = PropertyVisitRequest::with('propertyDetails','userDetails','agentDetails')->where('agent_id',Auth::user()->id)->where('visit_date','>=',$today)->whereIn('status',['A','AA'])->get();
        return view('modules.agent.dashboard')->with($data);
    }
    /**
     *   Method      : editProfile
     *   Description : for agent editProfile view page
     *   Author      : Soumojit
     *   Date        : 2021-OCT-28
     **/

    public function editProfile(){
        $data['allCountry']=Country::get();
        $data['allLanguage']=Language::get();
        $data['allUserLanguage']=ProviderToLanguage::where('user_id', Auth::user()->id)->get();
        $data['states'] = State::where('country_id',auth()->user()->country)->get();
        $data['cites'] = City::where('state_id',auth()->user()->state)->get();
        return view('modules.agent.edit_profile')->with($data);
    }
     /**
     *   Method      : editProfileSave
     *   Description : for agent edit Profile Save
     *   Author      : Soumojit
     *   Date        : 2021-OCT-28
     **/

    public function editProfileSave(Request $request){

        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'agent_for' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'address' => 'required',
            'about' => 'required',
            'whatsapp_no' => 'required',
        ]);
        $name = $request->first_name . ' ' . $request->last_name;
        $upd = [];
        $upd['name'] = @$name;
        $upd['agent_for'] = @$request->agent_for;
        $upd['company_name'] = @$request->company_name;
        $upd['city'] = @$request->city;
        $upd['state'] = @$request->state;
        $upd['country'] = @$request->country;
        $upd['address'] = @$request->address;
        $upd['about'] = @$request->about;
        $upd['website'] = @$request->website;
        $upd['whatsapp_no'] = @$request->whatsapp_no;
        $slug = str_slug($name, "-");
        $upd['slug'] = $slug . "-" . Auth::user()->id;
        if (@$request->profile_picture) {
            // $image = $request->profile_pic;
            // $filename = time() . '-' . rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
            // Storage::putFileAs('public/profile_picture', $image, $filename);
            // $upd['profile_img'] = $filename;
            // @unlink(storage_path('app/public/profile_picture/' . auth()->user()->profile_img));
            @unlink(storage_path('app/public/profile_picture/' . auth()->user()->profile_pic));
            $destinationPath = "storage/app/public/profile_picture/";
            $img1 = str_replace('data:image/png;base64,', '', @$request->profile_picture);
            $img1 = str_replace(' ', '+', $img1);
            $image_base64 = base64_decode($img1);
            $img = time() . '-' . rand(1000, 9999) . '.png';
            $file = $destinationPath . $img;
            file_put_contents($file, $image_base64);
            chmod($file, 0755);
            $upd['profile_pic'] = $img;
        }
        if (@$request->gov_id) {
            $image = $request->gov_id;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
            Storage::putFileAs('public/gov_id_image', $image, $filename);
            $upd['gov_id_image'] = $filename;
            @unlink(storage_path('app/public/gov_id_image/' . auth()->user()->gov_id_image));
        }
        if (@$request->license) {
            $image = $request->license;
            $filename = time() . '-' . rand(1000, 9999) . '.' . $image->getClientOriginalExtension();
            Storage::putFileAs('public/license_image', $image, $filename);
            $upd['license_image'] = $filename;
            @unlink(storage_path('app/public/license_image/' . auth()->user()->license_image));
        }
        if (@$request->old_password || @$request->new_password || @$request->new_password) {
            $request->validate([
                'old_password' => 'required',
                'new_password' => 'required|confirmed',
                'new_password_confirmation' => 'required',
            ]);
            $check_user = User::where('id', auth()->user()->id)->first();
            if (!Hash::check($request->old_password, $check_user->password)) {
                session()->flash('error', 'Old password wrong');
                return redirect()->back();
            } else {
                $upd['password'] = \Hash::make($request->new_password);
            }
        }
        $user = User::where('id', Auth::user()->id)->update($upd);
        $userData =User::where('id', Auth::user()->id)->first();
        $language= explode(",", @$request->language);
        // return $language;
        foreach ($language as $item1) {
            $insLanguage = [];
            $insLanguage['user_id'] = Auth::user()->id;
            $insLanguage['language_id'] = $item1;
            if(@$item1){
                $checkAvailable =  ProviderToLanguage::where('user_id', Auth::user()->id)->where('language_id', $item1)->first();
                if ($checkAvailable == null) {
                    ProviderToLanguage::create($insLanguage);
                }
            }

        }
        ProviderToLanguage::where('user_id', Auth::user()->id)->whereNotIn('language_id', $language)->delete();
        if($userData->approval_status==null){
            if($userData->profile_pic!=null){
                User::where('id', Auth::user()->id)->update(['approval_status'=>'N']);
            }
        }
        if( @$user ){
            session()->flash('success', 'Profile updated successfully');
            return redirect()->route('agent.profile');
        }
        session()->flash('error', 'Profile not updated');
        return redirect()->route('agent.profile');

    }

    /**
     *   Method      : changeEmail
     *   Description : for agent Email change
     *   Author      : Soumojit
     *   Date        : 2021-NOV-10
     **/

    public function changeEmail(Request $request){
        $request->validate([
            'email' => 'required',
        ]);

        $upd = [];
        $upd['temp_email'] = @$request->email;
        if(auth()->user()->email_mob_change==null){
            $upd['email_mob_change']='E';
        }
        if(auth()->user()->email_mob_change=='M'){
            $upd['email_mob_change']='B';
        }
        if(auth()->user()->email_mob_change=='E'){
            $upd['email_mob_change']='E';
        }
        if(auth()->user()->email_mob_change=='B'){
            $upd['email_mob_change']='B';
        }
        User::where('id', Auth::user()->id)->update($upd);
        session()->flash('success', 'Email saved waiting for admin approval');
        return redirect()->route('agent.profile');
    }
    /**
     *   Method      : changeMobile
     *   Description : for agent mobile change
     *   Author      : Soumojit
     *   Date        : 2021-NOV-10
     **/

    public function changeMobile(Request $request){
        $request->validate([
            'mobile' => 'required',
        ]);

        $upd = [];
        $upd['temp_mobile'] = @$request->mobile;
        if(auth()->user()->email_mob_change==null){
            $upd['email_mob_change']='M';
        }
        if(auth()->user()->email_mob_change=='E'){
            $upd['email_mob_change']='B';
        }
        if(auth()->user()->email_mob_change=='M'){
            $upd['email_mob_change']='M';
        }
        if(auth()->user()->email_mob_change=='B'){
            $upd['email_mob_change']='B';
        }
        User::where('id', Auth::user()->id)->update($upd);
        session()->flash('success', 'Mobile no saved waiting for admin approval');
        return redirect()->route('agent.profile');
    }

    /**
     *   Method      : manageAvailability
     *   Description : for manage availability page
     *   Author      : Soumojit
     *   Date        : 2021-NOV-18
     **/

    public function manageAvailability(){
        $data['monday']=AgentToAvailability::where('user_id',auth()->user()->id)->where('day',1)->get();
        $data['tuesday']=AgentToAvailability::where('user_id',auth()->user()->id)->where('day',2)->get();
        $data['wednesday']=AgentToAvailability::where('user_id',auth()->user()->id)->where('day',3)->get();
        $data['thursday']=AgentToAvailability::where('user_id',auth()->user()->id)->where('day',4)->get();
        $data['friday']=AgentToAvailability::where('user_id',auth()->user()->id)->where('day',5)->get();
        $data['saturday']=AgentToAvailability::where('user_id',auth()->user()->id)->where('day',6)->get();
        $data['sunday']=AgentToAvailability::where('user_id',auth()->user()->id)->where('day',7)->get();
        // $new=[];
        // foreach($data['friday'] as $friday){
        //     $timeDifference=(strtotime($friday->to_time)-strtotime($friday->from_time))/60;
        //     if($timeDifference>30){
        //         $i=0;
        //         $num=($timeDifference/30);
        //         for($i=0; $i<$num ; $i++){
        //             $timeAdd=30*$i;
        //             $from_time=date('H:i:s',strtotime($timeAdd." minutes",strtotime($friday->from_time)));
        //             $to_time=date('H:i:s',strtotime("30 minutes",strtotime($from_time)));
        //             $new[]=[
        //                 'day'=>$friday->day,
        //                 'from_time'=>$from_time,
        //                 'to_time'=>$to_time
        //             ];
        //         }

        //     }else{
        //         $new[]=[
        //             'day'=>$friday->day,
        //             'from_time'=>$friday->from_time,
        //             'to_time'=>$friday->to_time,

        //         ];
        //     }
        // }
        // return $new;






        return view('modules.agent.manage_availability')->with($data);
    }
    /**
     *   Method      : manageAvailabilitySave
     *   Description : for manage availability data save
     *   Author      : Soumojit
     *   Date        : 2021-NOV-18
     **/
    public function manageAvailabilitySave(Request $request){
        $request->validate([
            'day' => 'required',
            'fromtime' => 'required',
            'totime' => 'required',
        ]);
        // return $request;
        $check_availability=AgentToAvailability::where('user_id',auth()->user()->id)->where('day',$request->day)
        ->where(function($query) use ($request){
            $query->whereBetween('from_time',[$request->fromtime,$request->totime])
            ->orWhereBetween('to_time',[$request->fromtime,$request->totime]);
        })
        ->count();
        if($check_availability!=null){
            session()->flash('error', 'Availability not added this time slot already added');
            return redirect()->back();
        }
        $ins=[];
        $ins['day']=$request->day;
        $ins['from_time']=$request->fromtime;
        $ins['to_time']=$request->totime;
        $ins['user_id']=auth()->user()->id;
        AgentToAvailability::create($ins);
        session()->flash('success', 'Availability added successfully');
        return redirect()->route('agent.manage.availability');

    }
    /**
     *   Method      :  removeAvailability
     *   Description : for  remove availability
     *   Author      : Soumojit
     *   Date        : 2021-NOV-19
     **/

    public function removeAvailability($id=null){
        $availability=AgentToAvailability::where('user_id',auth()->user()->id)->where('id',$id)->first();
        if($availability==null){
            session()->flash('error', 'Availability not removed');
            return redirect()->back();
        }
        AgentToAvailability::where('user_id',auth()->user()->id)->where('id',$id)->delete();
        session()->flash('success', 'Availability removed successfully');
        return redirect()->back();
    }

    /**
     *   Method      : rescheduleAvailability
     *   Description : for reschedule availability data save
     *   Author      : Puja
     *   Date        : 2021-DEC-13
     **/

    public function rescheduleAvailability(Request $request){
        
        
        $ins['visit_date'] = date('Y-m-d H:i:s',strtotime(@$request->date.@$request->time));
        $ins['reschedule'] = 'Y';
        // dd($data);
        $result = PropertyVisitRequest::where('id',@$request->id)->update($ins);
        $request = PropertyVisitRequest::with('propertyDetails','userDetails','agentDetails')->where('id',@$request->id)->first();
        $data['name'] = $request->userDetails->name;
        $data['agent_name'] = $request->agentDetails->name;
        $data['email'] = $request->userDetails->email;
        $data['property_name'] = $request->propertyDetails->name;
        $data['visit_date'] = $request->visit_date;
            if(@$result){
                Mail::send(new AgentRescheduleMail($data));
                session()->flash('success', 'Reschedule Property Visits updated successfully');
                return redirect()->back();
            }else{
                session()->flash('error', 'Reschedule Property Visits not updated');
                return redirect()->back();   
            }
        
    }
}

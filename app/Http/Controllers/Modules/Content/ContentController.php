<?php

namespace App\Http\Controllers\Modules\Content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Mail;
use App\Country;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Storage;
use App\Models\Category;
use App\Models\Language;
use App\Models\Skill;
use App\Models\ProviderToLanguage;
use App\Models\ProviderToSkills;
use App\Models\ProviderToImage;
use App\Models\City;
use App\Models\State;
use App\Models\SubCategory;
use App\Models\ProToCategory;
use App\Models\PostJob;
use App\Models\Quotes;




class ContentController extends Controller
{
     public function __construct()
    {
        // $this->middleware('auth');
    }

    

    public function aboutUs(){
        return view('modules.contant.about_us');
    }

    public function contactUs(){
        return view('modules.contant.contact_us');
    }
    public function faq(){
        return view('modules.contant.faq');
    }
    public function termsCondition(){
        return view('modules.contant.terms_condition');
    }
    public function privacyPolicy(){
        return view('modules.contant.privacy_policy');
    }

}

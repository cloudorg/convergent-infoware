<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FacilitiesAmenities extends Model
{
    protected $table = 'facilities_amenities';
    protected $guarded = [];
}

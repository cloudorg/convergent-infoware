<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyToFacilitiesAmenities extends Model
{
    protected $table = 'properties_to_facilities_amenities';
    protected $guarded = [];

    public function propertyFacilities(){

          return $this->hasOne('App\Models\FacilitiesAmenities','id','facilities_amenities_id');
    }
    public function facilitiesAmenitiesName(){

        return $this->hasOne('App\Models\FacilitiesAmenities','id','facilities_amenities_id');
      }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentToAvailability extends Model
{
    protected $table = 'agent_to_availability';
    protected $guarded = [];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProviderToLanguage extends Model
{
    protected $table = 'provider_to_language';
    protected $guarded = [];


    public function userLanguage(){

          return $this->hasOne('App\Models\Language','id','language_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProviderToSkills extends Model
{
    protected $table = 'provider_to_skills';
    protected $guarded = [];

    public function userSkill(){

          return $this->hasOne('App\Models\Skill','id','skills_id');
    }
}

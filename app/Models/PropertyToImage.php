<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyToImage extends Model
{
    protected $table = 'properties_to_image';
    protected $guarded = [];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyVisitRequest extends Model
{
    protected $table = 'property_visit_requests';
    protected $guarded = [];

    public function propertyDetails(){

          return $this->hasOne('App\Models\Property','id','property_id');
    } 
    public function userDetails(){

        return $this->hasOne('App\User','id','user_id');
  }
  public function agentDetails(){
         
      return $this->hasOne('App\User','id','agent_id');
  }
}

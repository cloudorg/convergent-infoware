<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'states';
    protected $guarded = [];
    
     public function countryName(){

          return $this->hasOne('App\Country','id','country_id');
    }
}

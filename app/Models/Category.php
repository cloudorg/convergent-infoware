<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $guarded = [];

    public function subCategoryDetails(){

        return $this->hasMany('App\Models\SubCategory','category_id','id')->where('status','A');
    }
}

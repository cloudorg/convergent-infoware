@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Reset Password</title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<!----header--->
<div class="haeder-padding"></div>


<div class="property_manag_ban">
	<div class="container">
		<div class="property_manag_inn">
			<div class="property_manag_left">
				{{-- <h4>Rivirtual Manages Clients’ property and collect information using this box </h4>
				<ul>
					<li><em><i class="fa fa-check"></i></em>There will be timely and live updates provided to the owners on the website. </li>
					<li><em><i class="fa fa-check"></i></em>One may hire a property manager for all or any one of the services mentioned.</li>
					<li><em><i class="fa fa-check"></i></em>The property managers will also provide for lands and vacant properties. </li>
				</ul> --}}
			</div>
			<div class="property_manag_rig">
				<div class="property_manag_from">
					<h4> To Manage your Account</h4>
					<p>You Can reset your password </p>
                    @include('includes.message')
					<form action="{{route('user.password.request')}}" method="POST" id="social_from">
                        @csrf
                        <input type="hidden" name="id" value="{{ @$id }}">
                        {{--<input type="hidden" value="{{@$user->email}}" name="social_user_email">
                        <input type="hidden" value="{{@$user->name}}" name="social_user_name">
                        <input type="hidden" value="{{@$user->id}}" name="social_user_id">
                        <input type="hidden" value="{{@$user_type}}" name="user_type">
                        <input type="hidden" value="{{@$provider_type}}" name="provider_type">--}}
						{{-- <div class="das_input">
							<input type="text" placeholder="Name" value="">
						</div> --}}
						<div class="das_input password-input">
							<input type="password" placeholder="New password" class="required" name="password" id="password">
                            <span toggle="#password" class="fa fa-fw field-icon toggle-password password-show-icon fa-eye-slash"></span>
						</div>
                        <div class="das_input password-input">
							<input type="password" placeholder="Confirm password" class="required" name="confirm_password" id="confirm_password">
                            <span toggle="#confirm_password" class="fa fa-fw field-icon toggle-password password-show-icon fa-eye-slash"></span>
						</div>
						{{-- <div class="das_input">
							<input type="text" placeholder="Email" class="required">
						</div> --}}
						<div class="manage_sub">
							<input type="submit" value="Save" class="see_cc">
						</div>
						{{-- <div class="manage_tx">
							<span>Call or WhatsApp us <a href="tel:918793456789">+918793456789 </a></span>
						</div> --}}
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
<script>
     $("#social_from").validate({
            rules: {
                password:{
                    required: true,
                    minlength: 8,
                },
                confirm_password: {

                    required: true,
                    minlength: 8,
                    equalTo : "#password"
                },
            },
        });
</script>
@endsection

<script src="{{ URL::to('public/frontend/js/jquerymin.js')}}"></script>
<script src="{{ URL::to('public/frontend/js/bootstrap.js')}}"></script>
<script src="{{ URL::to('public/frontend/js/carouselscript.js')}}"></script>
<script src="{{ URL::to('public/frontend/js/owl.carousel.js')}}"></script>
 <script src="{{ URL::to('public/frontend/js/custom-file-input.js')}}"></script>
<script src="{{ URL::to('public/frontend/js/calendar1.js')}}"></script>

<script src="{{ URL::to('public/frontend/js/bundle.min.js')}}"></script>
<script src="{{ URL::to('public/frontend/js/jquery-ui.js')}}"></script>
<script src="{{ URL::to('public/frontend/js/pinterest_grid.js')}}"></script>
<script src="{{ URL::to('public/frontend/js/ninja-slider.js')}}"></script>
<script src="{{ URL::to('public/frontend/js/thumbnail-slider.js')}}"></script>
{{-- <script src="{{ URL::to('public/frontend/js/calendar.js')}}"></script> --}}
<script src="{{ URL::to('public/frontend/js/custom.js')}}"></script>
{{-- jquery-validator --}}
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>




<script>
    $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>
{{-- Model Open Script --}}
<script>
$(document).ready(function(){
    $(".opensignin").click(function(){
        $('#modalsignin2').modal('hide');
        $('#forgotpass').modal('hide');
        $('#otpopup').modal('hide');
        $('#modalagentsignin').modal('hide');
        $('#modalproignin').modal('hide');
        $('#modalagentsignup').modal('hide');
        $('#modalproignup').modal('hide');
        $('#forgototpopup').modal('hide');
        $('#modalchangepassword').modal('hide');
        $('#modalsignin').modal('show');
        $('#userloginbtn').show();
        $('#userlogina').hide();
        $('#modalsignin .alert-success').css('display','none');
        $('#modalsignin .alert-danger').css('display','none');
    });
    $(".opensignup").click(function(){
        $('#modalsignin').modal('hide');
        $('#forgotpass').modal('hide');
        $('#otpopup').modal('hide');
        $('#modalagentsignin').modal('hide');
        $('#modalproignin').modal('hide');
        $('#modalagentsignup').modal('hide');
         $('#modalproignup').modal('hide');
         $('#forgototpopup').modal('hide');
         $('#modalchangepassword').modal('hide');
        $('#modalsignin2').modal('show');
        $('#userbtn').show();
        $('#usera').hide();
        $('#modalsignin2 .alert-success').css('display','none');
        ('#modalsignin2 .alert-danger').css('display','none');
    });
    $(".openpass").click(function(){
        $('#modalsignin').modal('hide');
        $('#otpopup').modal('hide');
        $('#modalsignin2').modal('hide');
        $('#modalagentsignin').modal('hide');
        $('#modalproignin').modal('hide');
        $('#modalagentsignup').modal('hide');
        $('#modalproignup').modal('hide');
        $('#forgototpopup').modal('hide');
        $('#modalchangepassword').modal('hide');
        $('#forgotpass').modal('show');
        $('#forgotpass .alert-success').css('display','none');
        $('#forgotpass .alert-danger').css('display','none');
    });
    $(".openotp").click(function(){
        $('#modalsignin').modal('hide');
        $('#modalsignin2').modal('hide');
        $('#forgotpass').modal('hide');
        $('#modalagentsignin').modal('hide');
        $('#modalproignin').modal('hide');
        $('#modalagentsignup').modal('hide');
         $('#modalproignup').modal('hide');
         $('#forgototpopup').modal('hide');
         $('#modalchangepassword').modal('hide');
        $('#otpopup').modal('show');
    });
    $(".agentlogin").click(function(){
        $('#modalsignin').modal('hide');
        $('#modalsignin2').modal('hide');
        $('#forgotpass').modal('hide');
        $('#otpopup').modal('hide');
        $('#modalproignin').modal('hide');
        $('#modalagentsignup').modal('hide');
        $('#modalproignup').modal('hide');
        $('#forgototpopup').modal('hide');
        $('#modalchangepassword').modal('hide');
        $('#modalagentsignin').modal('show');
        $('#agentloginbtn').show();
        $('#agentlogina').hide();
        $('#modalagentsignin .alert-success').css('display','none');
        $('#modalagentsignin .alert-danger').css('display','none');
    });
    $(".prologin").click(function(){
        $('#modalsignin').modal('hide');
        $('#modalsignin2').modal('hide');
        $('#forgotpass').modal('hide');
        $('#otpopup').modal('hide');
        $('#modalagentsignin').modal('hide');
        $('#modalagentsignup').modal('hide');
        $('#modalproignup').modal('hide');
        $('#forgototpopup').modal('hide');
        $('#modalchangepassword').modal('hide');
        $('#modalproignin').modal('show');
        $('#serviceloginbtn').show();
        $('#servicelogina').hide();
        $('#modalproignin .alert-success').css('display','none');
        $('#modalproignin .alert-danger').css('display','none');
    });
    $(".openagentsignup").click(function(){
        $('#modalsignin').modal('hide');
        $('#modalsignin2').modal('hide');
        $('#forgotpass').modal('hide');
        $('#otpopup').modal('hide');
        $('#modalagentsignin').modal('hide');
        $('#modalproignin').modal('hide');
        $('#modalproignup').modal('hide');
        $('#modalagentsignup').modal('show');
        $('#forgototpopup').modal('hide');
        $('#modalchangepassword').modal('hide');
        $('#agentbtn').show();
        $('#agenta').hide();
        $('#modalagentsignup .alert-success').css('display','none');
        $('#modalagentsignup .alert-danger').css('display','none');
    });
     $(".openprosignup").click(function(){
        $('#modalsignin').modal('hide');
        $('#modalsignin2').modal('hide');
        $('#forgotpass').modal('hide');
        $('#otpopup').modal('hide');
        $('#modalagentsignin').modal('hide');
        $('#modalproignin').modal('hide');
        $('#modalagentsignup').modal('hide');
        $('#modalproignup').modal('show');
        $('#forgototpopup').modal('hide');
        $('#modalchangepassword').modal('hide');
        $('#modalchangepassword').modal('hide');
        $('#servicebtn').show();
        $('#servicea').hide();
        $('#modalproignup .alert-success').css('display','none');
        $('#modalproignup .alert-danger').css('display','none');
    });
    $(".forgototp").click(function(){
        $('#modalsignin').modal('hide');
        $('#modalsignin2').modal('hide');
        $('#forgotpass').modal('hide');
        $('#modalagentsignin').modal('hide');
        $('#modalproignin').modal('hide');
        $('#modalagentsignup').modal('hide');
        $('#modalproignup').modal('hide');
        $('#otpopup').modal('hide');
        $('#modalchangepassword').modal('hide');
        $('#forgototpopup').modal('show');
        $('#forgototpopup .alert-success').css('display','none');
        $('#forgototpopup .alert-danger').css('display','none');
    });
    $(".changepasswordclick").click(function(){
        $('#modalsignin').modal('hide');
        $('#modalsignin2').modal('hide');
        $('#forgotpass').modal('hide');
        $('#modalagentsignin').modal('hide');
        $('#modalproignin').modal('hide');
        $('#modalagentsignup').modal('hide');
        $('#modalproignup').modal('hide');
        $('#otpopup').modal('hide');
        $('#forgototpopup').modal('hide');
        $('#modalchangepassword').modal('show');
        $('#modalchangepassword .alert-success').css('display','none');
        $('#modalchangepassword .alert-danger').css('display','none');
    });

    // $('.user_google').click(function(){
    //     var url= '{{route('home')}}'+'/login/user/google';
    //     window.location = url;
    // })
    // $('.user_facebook').click(function(){
    //     var url= '{{route('home')}}'+'/login/user/facebook';
    //     window.location = url;
    // })
});
</script>
{{-- Model Open Script --}}

{{-- Singup Script --}}
<script>
    $(document).ready(function(){
        jQuery.validator.addMethod("validate_word", function(value, element) {
        if (/^([a-zA-Z ])+$/.test(value)) {
            return true;
        } else {
            return false;
        }
    }, "Not allow special characters or numbers");

        jQuery.validator.addMethod("validate_email", function(value, element) {
            // var testEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z]{2,4})+$/;
            var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,5}$/i;
            if (testEmail.test(value)) {
                return true;
            } else {
                return false;
            }
        }, "Oops...! check your Email again ");

        $("#usersingupform").validate({
            rules: {
                userfname:{
                    validate_word:true,
                },
                userlname:{
                    validate_word:true,
                },
                useremail: {
                    required: true,
                    // email: true,
                    validate_email:true,
                    remote: {
                        url: '{{ route("check.email") }}',
                        dataType: 'json',
                        type:'post',
                        data: {
                            email: function() {
                                return $('#useremail').val();
                            },
                            _token: '{{ csrf_token() }}'
                        }
                    },
                },
                userpassword:{
                    required: true,
                    minlength: 8
                },
                usercpassword:{
                    required: true,
                    minlength: 8,
                    equalTo : "#userpassword"
                },
                usermob:{
                    required: true,
                    digits: true,
                    minlength: 10,
                    maxlength: 10,
                    remote: {
                        url: '{{ route("check.mobile") }}',
                        dataType: 'json',
                        type:'post',
                        data: {
                            mobile: function() {
                                return $('#usermob').val();
                            },
                            _token: '{{ csrf_token() }}'
                        }
                    },
                },
                accept_user_terms:{
                    required:true,
                },
            },
            messages: {
                useremail:{
                    remote:'Email already in Use ',
                    email:'Oops...! check your Email again '
                },
                usermob:{
                    remote:'Mobile Number already in Use ',
                    digits:"Please enter a valid number",
                },
                usercpassword:{
                    equalTo : "Confirm password not match "
                },
                accept_user_terms:{
                    required:'Please accept terms and conditions',
                },
            },
            submitHandler: function (form) {
                if($('#accept_user_terms').is(":checked")){
                    console.log('test');
                    $('#userbtn').hide();
                    $('#usera').show();
                    $.post("{{ route('new.register') }}",{
                        jsonrpc: 2.0,
                        _token: "{{ csrf_token() }}",
                        params: {
                            fname: $('#userfname').val(),
                            lname: $('#userlname').val(),
                            email: $('#useremail').val(),
                            mobile: $('#usermob').val(),
                            password: $('#userpassword').val(),
                            cpassword: $('#usercpassword').val(),
                            userType: 'U'
                        }
                    })
                    .done(response => {
                        console.log(response);
                        if (response.error && response.error.message) {
                            $('#userbtn').show();
                            $('#usera').hide();
                            $('#modalsignin2 .alert-success').css('display','none');
                            $('#modalsignin2 .alert-danger').css('display','block');
                            $('#modalsignin2 .alert-danger .error-message').text(response.error.message);
                        }else{
                            $('#otptext').text('Enter the 6 digit code sent to you at +91' +response.result.data.mobile_number);
                            $('#otp').text(response.result.data.otp_mobile);
                            $('#user_id').val(response.result.data.id);
                            $('#otpopup .alert-danger').css('display','none');
                            $('#otpopup .alert-success').css('display','block');
                            $('#otpopup .alert-success .success-message ').text('A verification link has been sent to your email account. Please verify.');
                            $('.openotp').click();
                        }
                    })
                    .fail(error => {

                    })

                }else{
                    $('#accept_user_terms-error').html('Please accept terms and conditions');
                    $('#accept_user_terms-error').css('display', 'block');
                    return false;
                }

            }
        });

        $("#agentingupform").validate({
            rules: {
                agentfname:{
                    validate_word:true,
                },
                agentlname:{
                    validate_word:true,
                },
                agentemail: {
                    required: true,
                    email: true,
                    validate_email:true,
                    remote: {
                        url: '{{ route("check.email") }}',
                        dataType: 'json',
                        type:'post',
                        data: {
                            email: function() {
                                return $('#agentemail').val();
                            },
                            _token: '{{ csrf_token() }}'
                        }
                    },
                },
                agentpassword:{
                    required: true,
                    minlength: 8
                },
                agentcpassword:{
                    required: true,
                    minlength: 8,
                    equalTo : "#agentpassword"
                },
                agentmob:{
                    required: true,
                    digits: true ,
                    minlength: 10,
                    maxlength: 10,
                    remote: {
                        url: '{{ route("check.mobile") }}',
                        dataType: 'json',
                        type:'post',
                        data: {
                            mobile: function() {
                                return $('#agentmob').val();
                            },
                            _token: '{{ csrf_token() }}'
                        }
                    },
                }
            },
            messages: {
                agentemail:{
                    remote:'Email already in Use ',
                    email:'Oops...! check your Email again '
                },
                agentmob:{
                    remote:'Mobile Number already in Use ',
                    digits:"Please enter a valid number",
                },
                agentcpassword:{
                    equalTo : "Confirm password not match "
                },
            },
            submitHandler: function (form) {
                if($('#accept_agent_terms').is(":checked")){
                    console.log('test');
                    $('#agentbtn').hide();
                    $('#agenta').show();
                    $.post("{{ route('new.register') }}",{
                        jsonrpc: 2.0,
                        _token: "{{ csrf_token() }}",
                        params: {
                            fname: $('#agentfname').val(),
                            lname: $('#agentlname').val(),
                            email: $('#agentemail').val(),
                            mobile: $('#agentmob').val(),
                            password: $('#agentpassword').val(),
                            cpassword: $('#agentcpassword').val(),
                            userType: 'A'
                        }
                    })
                    .done(response => {
                        console.log(response);
                        if (response.error && response.error.message) {
                            $('#agentbtn').show();
                            $('#agenta').hide();
                            $('#modalagentsignup .alert-success').css('display','none');
                            $('#modalagentsignup .alert-danger').css('display','block');
                            $('#modalagentsignup .alert-danger .error-message').text(response.error.message);
                        }else{
                            $('#otptext').text('Enter the 6 digit code sent to you at +91' +response.result.data.mobile_number);
                            $('#otp').text(response.result.data.otp_mobile);
                            $('#user_id').val(response.result.data.id);
                            $('.openotp').click();
                        }
                    })
                    .fail(error => {

                    })
                }else{
                    $('#accept_agent_terms-error').html('Please accept terms and conditions');
                    $('#accept_agent_terms-error').css('display', 'block');
                    return false;
                }

            }
        });


        $("#serviceingupform").validate({
            rules: {
                servicefname:{
                    validate_word:true,
                },
                servicelname:{
                    validate_word:true,
                },
                serviceemail: {
                    required: true,
                    email: true,
                    validate_email:true,
                    remote: {
                        url: '{{ route("check.email") }}',
                        dataType: 'json',
                        type:'post',
                        data: {
                            email: function() {
                                return $('#serviceemail').val();
                            },
                            _token: '{{ csrf_token() }}'
                        }
                    },
                },
                servicepassword:{
                    required: true,
                    minlength: 8
                },
                servicecpassword:{
                    required: true,
                    minlength: 8,
                    equalTo : "#servicepassword"
                },
                servicemob:{
                    required: true,
                    digits: true ,
                    minlength: 10,
                    maxlength: 10,
                    remote: {
                        url: '{{ route("check.mobile") }}',
                        dataType: 'json',
                        type:'post',
                        data: {
                            mobile: function() {
                                return $('#servicemob').val();
                            },
                            _token: '{{ csrf_token() }}'
                        }
                    },
                }
            },
            messages: {
                serviceemail:{
                    remote:'Email already in Use ',
                    email:'Oops...! check your Email again '
                },
                servicemob:{
                    remote:'Mobile Number already in Use ',
                    digits:"Please enter a valid number",
                },
                servicecpassword:{
                    equalTo : "Confirm password not match "
                },
            },
            submitHandler: function (form) {
                if($('#accept_service_terms').is(":checked")){
                    console.log('test');
                    $('#servicebtn').hide();
                    $('#servicea').show();
                    $.post("{{ route('new.register') }}",{
                        jsonrpc: 2.0,
                        _token: "{{ csrf_token() }}",
                        params: {
                            fname: $('#servicefname').val(),
                            lname: $('#servicelname').val(),
                            email: $('#serviceemail').val(),
                            mobile: $('#servicemob').val(),
                            password: $('#servicepassword').val(),
                            cpassword: $('#servicecpassword').val(),
                            userType: 'S'
                        }
                    })
                    .done(response => {
                        console.log(response);
                        if (response.error && response.error.message) {
                            $('#servicebtn').show();
                            $('#servicea').hide();
                            $('#modalproignup .alert-success').css('display','none');
                            $('#modalproignup .alert-danger').css('display','block');
                            $('#modalproignup .alert-danger .error-message').text(response.error.message);
                        }else{
                            $('#otptext').text('Enter the 6 digit code sent to you at +91' +response.result.data.mobile_number);
                            $('#otp').text(response.result.data.otp_mobile);
                            $('#user_id').val(response.result.data.id);
                            $('.openotp').click();
                        }
                    })
                    .fail(error => {

                    })
                }else{
                    $('#accept_service_terms-error').html('Please accept terms and conditions');
                    $('#accept_service_terms-error').css('display', 'block');
                    return false;
                }

            }
        });


        $("#otp_form").validate({
            rules: {
                codeBox6:{
                    required: true,
                },
            },
            messages: {
                codeBox6:{
                    required: 'OTP Required',
                },
            },
            submitHandler: function (form) {
                console.log('test');
                $.post("{{ route('check.mobile.verify') }}",{
                jsonrpc: 2.0,
                _token: "{{ csrf_token() }}",
                params: {
                    codeBox1: $('#codeBox1').val(),
                    codeBox2: $('#codeBox2').val(),
                    codeBox3: $('#codeBox3').val(),
                    codeBox4: $('#codeBox4').val(),
                    codeBox5: $('#codeBox5').val(),
                    codeBox6: $('#codeBox6').val(),
                    id: $('#user_id').val()
                }
            })
            .done(response => {
                console.log(response);
                if (response.error && response.error.message) {
                    $('#otpopup .alert-success').css('display','none');
                    $('#otpopup .alert-danger').css('display','block');
                    $('#otpopup .alert-danger .error-message').text(response.error.message);

                }else{
                    if(response.result.data.user_type=='U'){
                        $('#modalsignin .alert-danger').css('display','none');
                        $('#modalsignin .alert-success').css('display','block');
                        $('#modalsignin .alert-success .success-message ').text(response.result.message);
                        $(".opensignin").click();
                    }
                    else if (response.result.data.user_type=='A'){
                        $('#modalagentsignin .alert-danger').css('display','none');
                        $('#modalagentsignin .alert-success').css('display','block');
                        $('#modalagentsignin .alert-success .success-message ').text(response.result.message);
                        $(".agentlogin").click();

                    }else{
                        $('#modalproignin .alert-danger').css('display','none');
                        $('#modalproignin .alert-success').css('display','block');
                        $('#modalproignin .alert-success .success-message ').text(response.result.message);
                        $(".prologin").click();
                    }
                }
            })
            .fail(error => {
            })
            }
        });

        $('#modalproignup').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });
        $('#modalagentsignup').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });
        $('#modalsignin2').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });
        $('#otpopup').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });
        $('#modalsignin').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });
        $('#modalagentsignin').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });
        $('#modalproignin').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });
        $('#forgotpass').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });
        $('#forgototpopup').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });
        $('#modalchangepassword').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });


        $("#usersininform").validate({
            rules: {
                username:{
                    validate_email: function(){
                        var userName = $('#username').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            false;
                        } else {
                            return true;
                        }
                    },
                    minlength: function(){
                        var userName = $('#username').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            return 10;
                        } else {
                            false;
                        }
                    },
                    maxlength: function(){
                        var userName = $('#username').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            return 10;
                        } else {
                            false;
                        }
                    },
                    digits: function(){
                        var userName = $('#username').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            return true;
                        } else {
                            false;
                        }
                    },
                },
            },
            messages: {
                username:{
                    required: 'Enter Email or mobile',
                    minlength: 'Mobile number exactly only 10 digits without country code',
                    maxlength: 'Mobile number exactly only 10 digits without country code',
                    digits: 'Please enter a valid number ',
                }
            },
            submitHandler: function (form) {
                console.log('test');
                $('#userloginbtn').hide();
                $('#userlogina').show();
                $.post("{{ route('user.login') }}",{
                jsonrpc: 2.0,
                _token: "{{ csrf_token() }}",
                params: {
                    username: $('#username').val(),
                    password: $('#userloginpassword').val(),
                    userType: 'U'
                }
            })
            .done(response => {
                console.log(response);
                if (response.error && response.error.message) {
                    if(response.error.otp){
                        $('#otptext').text('Enter the 6 digit code sent to you at +91' +response.error.data.mobile_number);
                        $('#otp').text(response.error.otp);
                        $('#user_id').val(response.error.data.id);
                        $('#otpopup .alert-success').css('display','none');
                        $('#otpopup .alert-danger').css('display','block');
                        $('#modalsignin .alert-success').css('display','none');
                        $('#modalsignin .alert-danger').css('display','none');
                        $('#otpopup .alert-danger .error-message').text(response.error.message);
                        $('.openotp').click();
                    }else{
                        $('#userloginbtn').show();
                        $('#userlogina').hide();
                        $('#modalsignin .alert-success').css('display','none');
                        $('#modalsignin .alert-danger').css('display','block');
                        $('#modalsignin .alert-danger .error-message').text(response.error.message);
                    }
                }else{
                    // $('#otptext').text('Enter the 6 digit code sent to you at +91' +response.result.data.mobile_number);
                    // $('#otp').text(response.result.data.otp_mobile);
                    // $('#user_id').val(response.result.data.id);
                    // $('.openotp').click();
                    // window.location.href = "{{route('user.dashboard')}}";
                    location.reload();
                }
            })
            .fail(error => {
            })
            }
        });

        $("#agentsininform").validate({
            rules: {
                agentusername:{
                    validate_email: function(){
                        var userName = $('#agentusername').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            false;
                        } else {
                            return true;
                        }
                    },
                    minlength: function(){
                        var userName = $('#agentusername').val();
                        if (/^([0-9])+$/.test(userName)) {
                            return 10;
                        } else {
                            false;
                        }
                    },
                    maxlength: function(){
                        var userName = $('#agentusername').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            return 10;
                        } else {
                            false;
                        }
                    },
                    digits: function(){
                        var userName = $('#agentusername').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            return true;
                        } else {
                            false;
                        }
                    },
                },
            },
            messages: {
                agentusername:{
                    required: 'Enter Email or mobile',
                    minlength: 'Mobile number exactly only 10 digits without country code',
                    maxlength: 'Mobile number exactly only 10 digits without country code',
                    digits: 'Please enter a valid number ',
                }
            },
            submitHandler: function (form) {
                console.log('test');
                $('#agentloginbtn').hide();
                $('#agentlogina').show();
                $.post("{{ route('user.login') }}",{
                jsonrpc: 2.0,
                _token: "{{ csrf_token() }}",
                params: {
                    username: $('#agentusername').val(),
                    password: $('#agentloginpassword').val(),
                    userType: 'A'
                }
            })
            .done(response => {
                console.log(response);
                if (response.error && response.error.message) {
                    if(response.error.otp){
                        $('#otptext').text('Enter the 6 digit code sent to you at +91' +response.error.data.mobile_number);
                        $('#otp').text(response.error.otp);
                        $('#user_id').val(response.error.data.id);
                        $('#otpopup .alert-success').css('display','none');
                        $('#otpopup .alert-danger').css('display','block');
                        $('#modalsignin .alert-success').css('display','none');
                        $('#modalsignin .alert-danger').css('display','none');
                        $('#otpopup .alert-danger .error-message').text(response.error.message);
                        $('.openotp').click();
                    }else{
                        $('#agentloginbtn').show();
                        $('#agentlogina').hide();
                        $('#modalagentsignin .alert-success').css('display','none');
                        $('#modalagentsignin .alert-danger').css('display','block');
                        $('#modalagentsignin .alert-danger .error-message').text(response.error.message);
                    }
                }else{
                    // $('#otptext').text('Enter the 6 digit code sent to you at +91' +response.result.data.mobile_number);
                    // $('#otp').text(response.result.data.otp_mobile);
                    // $('#user_id').val(response.result.data.id);
                    // $('.openotp').click();
                    window.location.href = "{{route('agent.dashboard')}}";
                    // location.reload();
                }
            })
            .fail(error => {
            })
            }
        });

        $("#servicesininform").validate({
            rules: {
                serviceusername:{
                    validate_email: function(){
                        var userName = $('#serviceusername').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            false;
                        } else {
                            return true;
                        }
                    },
                    minlength: function(){
                        var userName = $('#serviceusername').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            return 10;
                        } else {
                            false;
                        }
                    },
                    maxlength: function(){
                        var userName = $('#serviceusername').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            return 10;
                        } else {
                            false;
                        }
                    },
                    digits: function(){
                        var userName = $('#serviceusername').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            return true;
                        } else {
                            false;
                        }
                    },
                },
            },
            messages: {
                serviceusername:{
                    required: 'Enter Email or mobile',
                    minlength: 'Mobile number exactly only 10 digits without country code',
                    maxlength: 'Mobile number exactly only 10 digits without country code',
                    digits: 'Please enter a valid number ',
                }
            },
            submitHandler: function (form) {
                console.log('test');
                $('#serviceloginbtn').hide();
                $('#servicelogina').show();
                $.post("{{ route('user.login') }}",{
                jsonrpc: 2.0,
                _token: "{{ csrf_token() }}",
                params: {
                    username: $('#serviceusername').val(),
                    password: $('#serviceloginpassword').val(),
                    userType: 'S'
                }
            })
            .done(response => {
                console.log(response);
                if (response.error && response.error.message) {
                    if(response.error.otp){
                        $('#otptext').text('Enter the 6 digit code sent to you at +91' +response.error.data.mobile_number);
                        $('#otp').text(response.error.otp);
                        $('#user_id').val(response.error.data.id);
                        $('#otpopup .alert-success').css('display','none');
                        $('#otpopup .alert-danger').css('display','block');
                        $('#modalproignin .alert-success').css('display','none');
                        $('#modalproignin .alert-danger').css('display','none');
                        $('#otpopup .alert-danger .error-message').text(response.error.message);
                        $('.openotp').click();
                    }else{
                        $('#serviceloginbtn').show();
                        $('#servicelogina').hide();
                        $('#modalproignin .alert-success').css('display','none');
                        $('#modalproignin .alert-danger').css('display','block');
                        $('#modalproignin .alert-danger .error-message').text(response.error.message);
                    }
                }else{
                    // $('#otptext').text('Enter the 6 digit code sent to you at +91' +response.result.data.mobile_number);
                    // $('#otp').text(response.result.data.otp_mobile);
                    // $('#user_id').val(response.result.data.id);
                    // $('.openotp').click();
                    // location.reload();
                    window.location.href = "{{route('service.provider.dashboard')}}";
                }
            })
            .fail(error => {
            })
            }
        });

        $("#forgotpasswordform").validate({
            rules: {
                forgotusername:{
                    validate_email: function(){
                        var userName = $('#forgotusername').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            false;
                        } else {
                            return true;
                        }
                    },
                    minlength: function(){
                        var userName = $('#forgotusername').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            return 10;
                        } else {
                            false;
                        }
                    },
                    maxlength: function(){
                        var userName = $('#forgotusername').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            return 10;
                        } else {
                            false;
                        }
                    },
                    digits: function(){
                        var userName = $('#forgotusername').val();
                        if (/^([0-9.])+$/.test(userName)) {
                            return true;
                        } else {
                            false;
                        }
                    },
                },
            },
            messages: {
                forgotusername:{
                    required: 'Enter Email or mobile',
                    minlength: 'Mobile number exactly only 10 digits without country code',
                    maxlength: 'Mobile number exactly only 10 digits without country code',
                    digits: 'Please enter a valid number ',
                }
            },
            submitHandler: function (form) {
                console.log('test');
                $('#forgotbtn').hide();
                $('#forgota').show();
                $.post("{{ route('user.forgot.password') }}",{
                jsonrpc: 2.0,
                _token: "{{ csrf_token() }}",
                params: {
                    username: $('#forgotusername').val(),
                }
            })
            .done(response => {
                console.log(response);
                if (response.error && response.error.message) {
                    $('#forgotbtn').show();
                    $('#forgota').hide();
                    $('#forgotpass .alert-success').css('display','none');
                    $('#forgotpass .alert-danger').css('display','block');
                    $('#forgotpass .alert-danger .error-message').text(response.error.message);

                }else{
                    if(response.result.otp){
                        $('#forgototptext').text('Enter the 6 digit code sent to you at +91' +response.result.data.mobile_number);
                        $('#forgototp').text(response.result.otp);
                        $('#forgot_user_id').val(response.result.data.id);
                        $('.forgototp').click();

                    }else{
                        $('#forgotbtn').show();
                        $('#forgota').hide();
                        $('#forgotpass .alert-success').css('display','block');
                        $('#forgotpass .alert-danger').css('display','none');
                        $('#forgotpass .alert-success .success-message').text(response.result.message);
                    }
                }
            })
            .fail(error => {
            })
            }
        });


        $("#changepasswordform").validate({
            rules: {
                newpassword:{
                    required: true,
                    minlength: 8
                },
                newcpassword:{
                    required: true,
                    minlength: 8,
                    equalTo : "#newpassword"
                },
            },
            messages: {
                codeBoxs6:{
                    required: 'OTP Required',
                },
            },
            submitHandler: function (form) {
                console.log('test');
                $.post("{{ route('change.password') }}",{
                jsonrpc: 2.0,
                _token: "{{ csrf_token() }}",
                params: {
                    newpassword: $('#newpassword').val(),
                    newcpassword: $('#newcpassword').val(),
                    id: $('#change_user_id').val()
                }
            })
            .done(response => {
                console.log(response);
                if (response.error && response.error.message) {
                    $('#modalchangepassword .alert-success').css('display','none');
                    $('#modalchangepassword .alert-danger').css('display','block');
                    $('#modalchangepassword .alert-danger .error-message').text(response.error.message);

                }else{
                    if(response.result.data.user_type=='U'){
                        $(".opensignin").click();
                        $('#modalsignin .alert-danger').css('display','none');
                        $('#modalsignin .alert-success').css('display','block');
                        $('#modalsignin .alert-success .success-message ').text(response.result.message);
                    }
                    else if (response.result.data.user_type=='A'){
                        $(".agentlogin").click();
                        $('#modalagentsignin .alert-danger').css('display','none');
                        $('#modalagentsignin .alert-success').css('display','block');
                        $('#modalagentsignin .alert-success .success-message ').text(response.result.message);

                    }else{
                        $(".prologin").click();
                        $('#modalproignin .alert-danger').css('display','none');
                        $('#modalproignin .alert-success').css('display','block');
                        $('#modalproignin .alert-success .success-message ').text(response.result.message);
                    }
                }
            })
            .fail(error => {
            })
            }
        });

        $("#forgototp_form").validate({
            rules: {
                codeBoxs6:{
                    required: true,
                },
            },
            messages: {
                codeBoxs6:{
                    required: 'OTP Required',
                },
            },
            submitHandler: function (form) {
                console.log('test');
                $.post("{{ route('user.forgot.password.otp.verify') }}",{
                jsonrpc: 2.0,
                _token: "{{ csrf_token() }}",
                params: {
                    codeBox1: $('#codeBoxs1').val(),
                    codeBox2: $('#codeBoxs2').val(),
                    codeBox3: $('#codeBoxs3').val(),
                    codeBox4: $('#codeBoxs4').val(),
                    codeBox5: $('#codeBoxs5').val(),
                    codeBox6: $('#codeBoxs6').val(),
                    id: $('#forgot_user_id').val()
                }
            })
            .done(response => {
                console.log(response);
                if (response.error && response.error.message) {
                    $('#forgototpopup.alert-success').css('display','none');
                    $('#forgototpopup .alert-danger').css('display','block');
                    $('#forgototpopup .alert-danger .error-message').text(response.error.message);

                }else{
                    $(".changepasswordclick").click();
                    $('#modalproignin .alert-danger').css('display','none');
                    $('#modalproignin .alert-success').css('display','block');
                    $('#change_user_id').val(response.result.data.id)
                    $('#modalproignin .alert-success .success-message ').text(response.result.message);
                }
            })
            .fail(error => {
            })
            }
        });



    });

</script>
{{-- Singup Script --}}

{{-- OTP Script --}}
<script>
    function getCodeBoxElement(index) {
        return document.getElementById('codeBox' + index);
    }
    function getCodeBoxElement1(index) {
        return document.getElementById('codeBoxs' + index);
    }
      function onKeyUpEvent(index, event) {
        const eventCode = event.which || event.keyCode;
        if (getCodeBoxElement(index).value.length === 1) {
          if (index !== 6) {
            getCodeBoxElement(index+ 1).focus();
          } else {
            getCodeBoxElement(index).blur();
            // Submit code
            console.log('submit code ');
          }
        }
        if (eventCode === 8 && index !== 1) {
          getCodeBoxElement(index - 1).focus();
        }
      }
      function onKeyUpEvent1(index, event) {
        const eventCode = event.which || event.keyCode;
        if (getCodeBoxElement1(index).value.length === 1) {
          if (index !== 6) {
            getCodeBoxElement1(index+ 1).focus();
          } else {
            getCodeBoxElement1(index).blur();
            // Submit code
            console.log('submit code ');
          }
        }
        if (eventCode === 8 && index !== 1) {
          getCodeBoxElement1(index - 1).focus();
        }
      }
      function onFocusEvent(index) {
        for (item = 1; item < index; item++) {
          const currentElement = getCodeBoxElement(item);
          if (!currentElement.value) {
              currentElement.focus();
              break;
          }
        }
      }
      function onFocusEvent1(index) {
        for (item = 1; item < index; item++) {
          const currentElement = getCodeBoxElement1(item);
          if (!currentElement.value) {
              currentElement.focus();
              break;
          }
        }
      }
</script>
{{-- OTP Script --}}
<script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=6195ee4013b8f60014cbfb4f&product=image-share-buttons" async="async"></script>

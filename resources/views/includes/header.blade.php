<!----header--->
@if(!Auth::user() || Auth::user()->user_type=='U')
<header>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="{{route('home')}}"> <img src="{{ URL::to('public/frontend/images/logo.png')}}" alt="logo"> </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav" aria-expanded="false" aria-label="Toggle navigation"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <div class="collapse navbar-collapse" id="main_nav">
                <ul class="navbar-nav">
                    <li class="nav-item"> <a class="nav-link" href="{{route('search.property',['property_for[]' => 'B'])}}">Buy </a> </li>
                    <li class="nav-item"><a class="nav-link" href="{{route('search.property',['property_for[]' => 'R'])}}"> Rent </a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('sell.page') }}"> Sell </a></li>
                    <li class="nav-item"><a class="nav-link" href="javascript:;"> Loans </a></li>
                    <li class="nav-item"><a class="nav-link" href="#"> Agents </a></li>
                </ul>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a class="nav-link" href="javascript:;"> Property management </a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('hire.pro') }}"> Hire a Pro </a></li>
                    <li class="nav-item"><a class="nav-link" href="javascript:;"> Help </a></li>
                </ul>
            </div>
            @if(!Auth::user())
            <div class="btn-sign-in"><a href="javascript:void(0);" class="haderbtn opensignin">Sign in</a></div>
            @endif
            @if(@Auth::user()->user_type =='U')
            <div class="af_log_dv">
                <em href="#url" id="profidrop" >
                    <b>
                        @if(auth()->user()->profile_pic != null)
                        <img src="{{ URL::to('storage/app/public/profile_picture')}}/{{auth()->user()->profile_pic}}" alt="">
                        @else
                        <img src="{{ URL::to('public/frontend/images/uplod_pc.png')}}" alt="">
                        @endif
                        {{-- <img src="{{ URL::to('public/frontend/images/uplod_pc.png')}}" alt=""> --}}
                    </b><span><img src="{{ URL::to('public/frontend/images/caret.png')}}" alt=""></span>
                </em>
                <div class="profidropdid" id="profidropdid">
                  <ul>
                        <li><a href="{{route('user.dashboard')}}">Dashboard</a></li>
                        <li><a href="{{route('user.profile')}}">Edit profile</a></li>
                        <li><a href="javascript:;">Find a Provider</a></li>
                        <li><a href="javascript:;">Post Service Requirements</a></li>
                        <li><a href="javascript:;">My Service Requirements </a></li>
                        <li><a href="{{ route('user.property.visit.request') }}">Upcoming Property Visits</a></li>
                        <li><a href="javascript:;">Saved Searches</a></li>
                        <li><a href="javascript:;">Chat History</a></li>
                        <li><a href="{{route('user.logout')}}">Logout</a></li>
                    </ul>
              </div>
            </div>
            @endif
        </nav>
    </div>
</header>
@endif
<!----header--->

@if(@Auth::user()->user_type=='A')

<header class="header_inr header_af_log">
    <div class="container">
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="{{route('home')}}"> <img src="{{ URL::to('public/frontend/images/logo.png')}}" alt="logo"> </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav" aria-expanded="false" aria-label="Toggle navigation"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <div class="collapse navbar-collapse" id="main_nav">

                <ul class="navbar-nav ml-auto">

                    <li class="nav-item"><a class="nav-link" href="#"> Help </a></li>
                </ul>
            </div>
            <div class="af_log_dv">
                <em href="#url" id="profidrop" >
                    <b>
                        @php
                        $name1 =explode(" ",auth()->user()->name);
                        $fname1 =$name1[0];
                        @endphp

                        @if(auth()->user()->profile_pic != null)
                        <img src="{{ URL::to('storage/app/public/profile_picture')}}/{{auth()->user()->profile_pic}}" alt="">{{$fname1}}
                        @else
                        <img src="{{ URL::to('public/frontend/images/agent-users.png')}}" alt="">{{$fname1}}

                        @endif
                        {{-- <img src="{{ URL::to('public/frontend/images/uplod_pc.png')}}" alt=""> Avishek --}}
                    </b><span><img src="{{ URL::to('public/frontend/images/dr.png')}}" alt=""></span>
                </em>
                <div class="profidropdid" id="profidropdid">
                  <ul>
                      <li><a href="{{route('agent.dashboard')}}">Dashboard</a></li>
                        <li><a href="{{route('agent.profile')}}">Edit profile</a></li>
                        <li><a href="javascript:;">Subscription</a></li>
                        <li><a href="{{route('agent.my.property')}}">My Property</a></li>
                        <li><a href="javascript:;">Manage Visit Availability</a></li>
                        <li><a href="{{ route('agent.visit.request') }}">View Visit Requests </a></li>
                        <li><a href="javascript:;">Chat History </a></li>
                        <li><a href="javascript:;">View Reviews & Rating</a></li>
                        <li><a href="{{route('user.logout')}}">Logout</a></li>
                  </ul>
              </div>
            </div>
        </nav>
    </div>
</header>

@endif
@if(@Auth::user()->user_type=='S')

<header class="header_inr header_af_log">
    <div class="container">
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="{{route('home')}}"> <img src="{{ URL::to('public/frontend/images/logo.png')}}" alt="logo"> </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav" aria-expanded="false" aria-label="Toggle navigation"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <div class="collapse navbar-collapse" id="main_nav">

                <ul class="navbar-nav ml-auto">

                    <li class="nav-item"><a class="nav-link" href="#"> Help </a></li>
                </ul>
            </div>
            <div class="af_log_dv">
                <em href="#url" id="profidrop" >
                    <b>
                        @php
                        $name1 =explode(" ",auth()->user()->name);
                        $fname1 =$name1[0];
                        @endphp

                        @if(auth()->user()->profile_pic != null)
                        <img src="{{ URL::to('storage/app/public/profile_picture')}}/{{auth()->user()->profile_pic}}" alt="">{{$fname1}}
                        @else
                        <img src="{{ URL::to('public/frontend/images/proimg.png')}}" alt="">{{$fname1}}

                        @endif
                        {{-- <img src="{{ URL::to('public/frontend/images/uplod_pc.png')}}" alt=""> Avishek --}}
                     </b>
                    <span><img src="{{ URL::to('public/frontend/images/dr.png')}}" alt=""></span>
                </em>
                <div class="profidropdid" id="profidropdid">
                  <ul>
                      <li><a href="{{route('service.provider.dashboard')}}">Dashboard</a></li>
                        <li><a href="{{route('service.provider.profile')}}">Edit profile</a></li>
                        <li><a href="javascript:;">Find a Job</a></li>
                        {{-- <li><a href="{{ route('search.job') }}">Find a Job</a></li> --}}
                        <li><a href="javascript:;">My Quotes </a></li>
                        <li><a href="javascript:;">View Reviews & Rating </a></li>
                        <li><a href="javascript:;">Memberships </a></li>
                        <li><a href="javascript:;">Payments  </a></li>
                        <li><a href="javascript:;">Chat History </a></li>
                        <li><a href="{{route('user.logout')}}">Logout</a></li>
                  </ul>
              </div>
            </div>
        </nav>
    </div>
</header>

@endif

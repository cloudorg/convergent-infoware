
<!-- otp -->
<div class="modal sign_popup forgot_pass" id="otpopup">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <div class="sign_popup_body">
                <h4>Let's get started</h4>
                @include('includes.modal_message')
                <p id="otptext">Enter the 6 digit code sent to you at +91 9876543210</p>
                <p id="otp"></p>
                <form action="javascript:;" method="POST" id="otp_form">
                    <div class="sign_popup_pane">
                        <input type="hidden" name="user_id" id="user_id">
                        <div class="input_otp">
                            <ul>
                              <li><input id="codeBox1" name="codeBox1" type="number" maxlength="1" onkeyup="onKeyUpEvent(1, event)" onfocus="onFocusEvent(1)" onKeyPress="if(this.value.length==1) return false;" oninput="this.value = this.value.replace(/[^0-9.]/g,'')"></li>
                              <li><input id="codeBox2" name="codeBox2" type="number" maxlength="1" onkeyup="onKeyUpEvent(2, event)" onfocus="onFocusEvent(2)" onKeyPress="if(this.value.length==1) return false;" oninput="this.value = this.value.replace(/[^0-9.]/g,'')"></li>
                              <li><input id="codeBox3" name="codeBox3" type="number" maxlength="1" onkeyup="onKeyUpEvent(3, event)" onfocus="onFocusEvent(3)" onKeyPress="if(this.value.length==1) return false;" oninput="this.value = this.value.replace(/[^0-9.]/g,'')"></li>
                              <li><input id="codeBox4" name="codeBox4" type="number" maxlength="1" onkeyup="onKeyUpEvent(4, event)" onfocus="onFocusEvent(4)" onKeyPress="if(this.value.length==1) return false;" oninput="this.value = this.value.replace(/[^0-9.]/g,'')"></li>
                              <li><input id="codeBox5" name="codeBox5" type="number" maxlength="1" onkeyup="onKeyUpEvent(5, event)" onfocus="onFocusEvent(5)" onKeyPress="if(this.value.length==1) return false;" oninput="this.value = this.value.replace(/[^0-9.]/g,'')"></li>
                              <li><input id="codeBox6" name="codeBox6" type="number" maxlength="1" onkeyup="onKeyUpEvent(6, event)" onfocus="onFocusEvent(6)" onKeyPress="if(this.value.length==1) return false;" oninput="this.value = this.value.replace(/[^0-9.]/g,'')"></li>
                                {{-- <li><input type="text" name=""></li>
                                <li><input type="text" name=""></li>
                                <li><input type="text" name=""></li>
                                <li><input type="text" name=""></li>
                                <li><input type="text" name=""></li>
                                <li><input type="text" name=""></li> --}}

                            </ul>
                        </div>
                        <label id="codeBox6-error" class="error" for="codeBox6"></label>
                       <div class="sing_button">
                           <button class="see_cc">Submit</button>
                       </div>
                        <div class="sign_popup_div">
                            <p>Didn’t receive verification code yet?</p>
                           <a href="#url" class="a_popup">Resend Code</a>
                       </div>
                    </div>
                </form>
            </div>
        </div>
      </div>
    </div>
</div>
<!-- otp -->


{{-- user singup model--}}
<div class="modal sign_popup" id="modalsignin2">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <div class="sign_popup_body">
                <h4>Sign Up</h4>
                @include('includes.modal_message')
                <form action="javascript:;" id="usersingupform" method="POST">
                    <div class="sign_popup_pane">
                        <div class="input-field">
                          <input type="text" class="required" id="userfname" name="userfname">
                          <label for="userfname">First Name</label>
                       </div>
                       <div class="input-field">
                          <input type="text" class="required" name="userlname" id="userlname">
                          <label for="userlname">Last Name</label>
                       </div>
                       <div class="input-field">
                          <input type="email" class="required" name="useremail" id="useremail" >
                          <label for="useremail">Email Id</label>
                       </div>
                       <div class="input-field">
                          <input type="tel" class="required" name="usermob" id="usermob" maxlength="10">
                          <label for="usermob">Phone Number</label>
                       </div>
                       <div class="input-field">
                           <div style="position: relative">
                            <input type="password"  class="required" id="userpassword" name="userpassword">
                            <label for="userpassword">Password</label>
                            <span toggle="#userpassword" class="fa fa-fw field-icon toggle-password fa-eye-slash"></span>
                        </div>
                        <label id="userpassword-error" class="error" for="userpassword" style="display: none"></label>

                       </div>
                       <div class="input-field">
                           <div style="position: relative">
                            <input type="password"  class="required" id="usercpassword" name="usercpassword">
                            <label for="usercpassword">Confirm Password</label>
                            <span toggle="#usercpassword" class="fa fa-fw field-icon toggle-password fa-eye-slash"></span>
                        </div>
                        <label id="usercpassword-error" class="error" for="usercpassword" style="display: none"></label>
                       </div>
                       <div class="chec_bx">
                           <div class="radiobx">
                               <input type="checkbox" id="accept_user_terms" name="accept_user_terms">
                               <label for="accept_user_terms">By creating an account, you agree that you have read and accepted our <a href="#url">Terms of Service</a>  & <a href="#url">Privacy Policy</a></label>
                               <label id="accept_user_terms-error" class="error" for="accept_user_terms" style="display: none">Please accept terms and conditions</label>
                          </div>
                       </div>
                       <div class="sing_button">
                           <button class="see_cc" id="userbtn">Sign Up</button>
                           <a href="javascript:void();" class="see_cc" id="usera" style="display: none">Please Wait</a>
                       </div>
                      <div class="or_div">
                           <strong><span>or</span></strong>
                      </div>
                       <div class="google_div">
                            {{-- <button class="goo_btn"><i class="fa fa-facebook-official"></i>Continue with Facebook</button>
                            <button class="goo_btn goo_btn1"><i class="fa fa-google-plus"></i>Continue with Google</button> --}}
                            <a class="goo_btn social_btn" href="{{route('login.social',['user_type'=>'user','provider_type'=>'facebook'])}}"><i class="fa fa-facebook-official"></i>Continue with Facebook</a>
                            <a class="goo_btn goo_btn1 social_btn" href="{{route('login.social',['user_type'=>'user','provider_type'=>'google'])}}"><i class="fa fa-google-plus"></i>Continue with Google</a>
                       </div>
                       <div class="sign_popup_div">
                            <p>Already have an account on RiVirtual?</p>
                           <a href="javascript:void();" class="a_popup opensignin" id="sin_mod">Sign In Now</a>
                       </div>
                       <div class="estate_agent_div">
                           <ul>
                               <li><a href="javascript:void(0);"  id="clocv" class="a_popup openagentsignup">Real Estate Agent Signup</a></li>
                               <li><a href="javascript:void(0);" id="clocv" class="a_popup openprosignup">Pro Signup</a></li>
                           </ul>
                       </div>
                    </div>
                </form>
            </div>
        </div>
      </div>
    </div>
</div>
{{-- user singup model--}}


{{-- agent singup model--}}
<div class="modal sign_popup" id="modalagentsignup">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="sign_popup_body">
                    <h4>Real Estate Agent Signup</h4>
                    @include('includes.modal_message')
                    <form action="javascript:;" id="agentingupform" method="POST">
                        <div class="sign_popup_pane">
                            <div class="input-field">
                                <input type="text" class="required" id="agentfname" name="agentfname">
                                <label for="agentfname">First Name</label>
                            </div>
                            <div class="input-field">
                                <input type="text" class="required" name="agentlname" id="agentlname">
                                <label for="agentlname">Last Name</label>
                            </div>
                            <div class="input-field">
                                <input type="email" class="required" name="agentemail" id="agentemail" >
                                <label for="agentemail">Email Id</label>
                            </div>
                            <div class="input-field">
                                <input type="tel" class="required" name="agentmob" id="agentmob" maxlength="10">
                                <label for="agentmob">Phone Number</label>
                            </div>
                             <div class="input-field">
                                <div style="position: relative">
                                <input type="password"  class="required" id="agentpassword" name="agentpassword">
                                <label for="agentpasswor">Password</label>
                                 <span toggle="#agentpassword" class="fa fa-fw field-icon toggle-password fa-eye-slash"></span>
                                </div>
                                <label id="agentpassword-error" class="error" for="agentpassword" style="display: none"></label>
                            </div>
                             <div class="input-field">
                                <div style="position: relative">
                                <input type="password"  class="required" id="agentcpassword" name="agentcpassword">
                                <label for="agentcpassword">Confirm Password</label>
                                 <span toggle="#agentcpassword" class="fa fa-fw field-icon toggle-password fa-eye-slash"></span>
                                </div>
                                <label id="agentcpassword-error" class="error" for="agentcpassword" style="display: none"></label>

                            </div>
                            <div class="chec_bx">
                                <div class="radiobx">
                                    <input type="checkbox" id="accept_agent_terms" name="accept_agent_terms">
                                    <label for="accept_agent_terms">By creating an account, you agree that you have read and accepted our <a href="#url">Terms of Service</a>  & <a href="#url">Privacy Policy</a></label>
                                    <label id="accept_agent_terms-error" class="error" for="accept_agent_terms" style="display: none">Please accept terms and conditions</label>
                               </div>
                            </div>
                            <div class="sing_button">
                                <button class="see_cc" id="agentbtn">Sign Up</button>
                                <a href="javascript:void();" class="see_cc" id="agenta" style="display: none">Please Wait</a>
                            </div>
                            <div class="or_div">
                                <strong><span>or</span></strong>
                           </div>
                           <div class="google_div">
                               {{-- <button class="goo_btn"><i class="fa fa-facebook-official"></i>Continue with Facebook</button>
                               <button class="goo_btn goo_btn1"><i class="fa fa-google-plus"></i>Continue with Google</button> --}}
                               <a class="goo_btn social_btn" href="{{route('login.social',['user_type'=>'agent','provider_type'=>'facebook'])}}"><i class="fa fa-facebook-official"></i>Continue with Facebook</a>
                            <a class="goo_btn goo_btn1 social_btn" href="{{route('login.social',['user_type'=>'agent','provider_type'=>'google'])}}"><i class="fa fa-google-plus"></i>Continue with Google</a>
                            </div>
                            <div class="sign_popup_div">
                                <p>Already have an account on RiVirtual?</p>
                               <a href="javascript:void();" class="a_popup agentlogin" id="sin_mod">Sign In Now</a>
                           </div>
                           <div class="estate_agent_div">
                            <ul>
                                <li><a href="javascript:void(0);" class="a_popup opensignup" id="clocv">User Signup</a></li>
                                <li><a href="javascript:void(0);" id="clocv" class="a_popup openprosignup">Pro Signup</a></li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      </div>
    </div>
</div>
{{-- agent singup model--}}

{{-- Service Provider singup model--}}
<div class="modal sign_popup" id="modalproignup">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <div class="sign_popup_body">
                <h4>Pro Signup</h4>
                @include('includes.modal_message')
                <form action="javascript:;" id="serviceingupform" method="POST">
                    <div class="sign_popup_pane">
                        <div class="input-field">
                            <input type="text" class="required" id="servicefname" name="servicefname">
                            <label for="servicefname">First Name</label>
                         </div>
                         <div class="input-field">
                            <input type="text" class="required" name="servicelname" id="servicelname">
                            <label for="servicelname">Last Name</label>
                         </div>
                         <div class="input-field">
                            <input type="email" class="required" name="serviceemail" id="serviceemail" >
                            <label for="serviceemail">Email Id</label>
                         </div>
                         <div class="input-field">
                            <input type="tel" class="required" name="servicemob" id="servicemob" maxlength="10">
                            <label for="servicemob">Phone Number</label>
                         </div>
                         <div class="input-field">
                            <div style="position: relative">
                            <input type="password"  class="required" id="servicepassword" name="servicepassword">
                            <label for="servicepassword">Password</label>
                             <span toggle="#servicepassword" class="fa fa-fw field-icon toggle-password fa-eye-slash"></span>
                            </div>
                            <label id="servicepassword-error" class="error" for="servicepassword" style="display: none"></label>
                         </div>
                         <div class="input-field">
                            <div style="position: relative">
                            <input type="password"  class="required" id="servicecpassword" name="servicecpassword">
                            <label for="servicecpassword">Confirm Password</label>
                             <span toggle="#servicecpassword" class="fa fa-fw field-icon toggle-password fa-eye-slash"></span>
                            </div>
                             <label id="servicecpassword-error" class="error" for="servicecpassword" style="display: none"></label>
                         </div>
                       <div class="chec_bx">
                           <div class="radiobx">
                            <input type="checkbox" id="accept_service_terms" name="accept_service_terms">
                            <label for="accept_service_terms">By creating an account, you agree that you have read and accepted our <a href="#url">Terms of Service</a>  & <a href="#url">Privacy Policy</a></label>
                            <label id="accept_service_terms-error" class="error" for="accept_service_terms" style="display: none">Please accept terms and conditions</label>
                          </div>
                       </div>
                       <div class="sing_button">
                           <button class="see_cc" id="servicebtn">Sign Up</button>
                           <a href="javascript:void();" class="see_cc" id="servicea" style="display: none">Please Wait</a>
                       </div>
                      <div class="or_div">
                           <strong><span>or</span></strong>
                      </div>
                       <div class="google_div">
                            {{-- <button class="goo_btn"><i class="fa fa-facebook-official"></i>Continue with Facebook</button>
                            <button class="goo_btn goo_btn1"><i class="fa fa-google-plus"></i>Continue with Google</button> --}}
                            <a class="goo_btn social_btn" href="{{route('login.social',['user_type'=>'service','provider_type'=>'facebook'])}}"><i class="fa fa-facebook-official"></i>Continue with Facebook</a>
                            <a class="goo_btn goo_btn1 social_btn" href="{{route('login.social',['user_type'=>'service','provider_type'=>'google'])}}"><i class="fa fa-google-plus"></i>Continue with Google</a>
                       </div>
                       <div class="sign_popup_div">
                            <p>Already have an account on RiVirtual?</p>
                           <a href="javascript:void();" class="a_popup prologin" id="sin_mod">Sign In Now</a>
                       </div>
                       <div class="estate_agent_div">
                           <ul>
                               <li><a href="javascript:void(0);" class="a_popup opensignup" id="clocv">User Signup</a></li>
                               <li><a href="javascript:void(0);"  id="clocv" class="a_popup openagentsignup">Real Estate Agent Signup</a></li>
                           </ul>
                       </div>
                    </div>
                </form>
            </div>
        </div>
      </div>
    </div>
</div>
{{-- Service Provider singup model--}}

<button class="see_cc openotp" style="display: none">otp</button>
<button class="see_cc forgototp" style="display: none">forgototp</button>
<button class="see_cc changepasswordclick" style="display: none">modalchangepassword</button>



{{-- login user --}}

<div class="modal sign_popup" id="modalsignin">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <div class="sign_popup_body">
                <h4>Sign in</h4>
                @include('includes.modal_message')
                <form action="javascript:;" method="POST" id="usersininform">
                    <div class="sign_popup_pane">
                        <div class="input-field">
                          <input type="text" class="required" name="username" id="username">
                          <label for="username" >Email/Mobile Number </label>
                       </div>
                       <div class="input-field">
                           <div style="position: relative">
                          <input type="password" class="required" name="userloginpassword" id="userloginpassword">
                          <label for="userloginpassword">Password</label>
                           <span toggle="#userloginpassword" class="fa fa-fw field-icon toggle-password fa-eye-slash"></span>
                           </div>
                           <label id="userloginpassword-error" class="error" for="userloginpassword" style="display: none"></label>
                       </div>
                       <div class="for_pas">
                           <a href="javascript:void(0);" class="a_popup openpass" id="forgot_pass">Forgot Password?</a>
                       </div>
                       <div class="sing_button">
                           <button class="see_cc" id="userloginbtn">Sign In</button>
                           <a href="javascript:;" class="see_cc" style="display: none" id="userlogina">Please Wait</a>
                       </div>
                      <div class="or_div">
                           <strong><span>or</span></strong>
                      </div>
                       <div class="google_div">
                            {{-- <button class="goo_btn user_facebook"><i class="fa fa-facebook-official"></i>Continue with Facebook</button>
                            <button class="goo_btn goo_btn1 user_google"><i class="fa fa-google-plus"></i>Continue with Google</button> --}}
                            <a class="goo_btn social_btn" href="{{route('login.social',['user_type'=>'user','provider_type'=>'facebook'])}}"><i class="fa fa-facebook-official"></i>Continue with Facebook</a>
                            <a class="goo_btn goo_btn1 social_btn" href="{{route('login.social',['user_type'=>'user','provider_type'=>'google'])}}"><i class="fa fa-google-plus"></i>Continue with Google</a>
                       </div>
                       <div class="sign_popup_div">
                            <p>Don’t have an account on RiVirtual?</p>
                           <a href="javascript:void(0);"  id="clocv" class="a_popup opensignup">Sign Up Now</a>
                       </div>
                       <div class="estate_agent_div">
                           <ul>
                               <a href="javascript:void(0);"  id="clocv" class="a_popup agentlogin">Real Estate Agent Login</a></li>
                               <li><a href="javascript:void(0);" id="clocv" class="a_popup prologin">Pro Login</a></li>
                           </ul>
                       </div>
                    </div>
                </form>
            </div>
        </div>
      </div>
    </div>
</div>
{{-- login user --}}

{{-- login agent --}}
<div class="modal sign_popup" id="modalagentsignin">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <div class="sign_popup_body">
                <h4>Real Estate Agent Signin</h4>
                @include('includes.modal_message')
                <form action="javascript:;" method="POST" id="agentsininform">
                    <div class="sign_popup_pane">
                        <div class="input-field">
                          <input type="text" class="required"  name="agentusername" id="agentusername">
                          <label for="agentusername">Email/Mobile Number </label>
                       </div>
                       <div class="input-field">
                        <div style="position: relative">
                          <input type="password" class="required" name="agentloginpassword" id="agentloginpassword">
                          <label for="agentloginpassword">Password</label>
                           <span toggle="#agentloginpassword" class="fa fa-fw field-icon toggle-password fa-eye-slash"></span>
                        </div>
                           <label id="agentloginpassword-error" class="error" for="agentloginpassword" style="display: none"></label>
                       </div>
                       <div class="for_pas">
                           <a href="javascript:void(0);" class="a_popup openpass" id="forgot_pass">Forgot Password?</a>
                       </div>
                       <div class="sing_button">
                           <button class="see_cc" id="agentloginbtn">Sign In</button>
                           <a href="javascript:;" class="see_cc" style="display: none" id="agentlogina">Please Wait</a>
                       </div>
                      <div class="or_div">
                           <strong><span>or</span></strong>
                      </div>
                       <div class="google_div">
                            {{-- <button class="goo_btn"><i class="fa fa-facebook-official"></i>Continue with Facebook</button>
                            <button class="goo_btn goo_btn1"><i class="fa fa-google-plus"></i>Continue with Google</button> --}}
                            <a class="goo_btn social_btn" href="{{route('login.social',['user_type'=>'agent','provider_type'=>'facebook'])}}"><i class="fa fa-facebook-official"></i>Continue with Facebook</a>
                            <a class="goo_btn goo_btn1 social_btn" href="{{route('login.social',['user_type'=>'agent','provider_type'=>'google'])}}"><i class="fa fa-google-plus"></i>Continue with Google</a>
                       </div>
                       <div class="sign_popup_div">
                            <p>Don’t have an account on RiVirtual?</p>
                           <a href="javascript:void(0);"  id="clocv" class="a_popup openagentsignup">Sign Up Now</a>
                       </div>
                       <div class="estate_agent_div">
                           <ul>
                               <li><a href="javascript:void(0);" class="a_popup opensignin" id="clocv">User Login</a></li>
                               <li><a href="javascript:void(0);" id="clocv" class="a_popup prologin">Pro Login</a></li>
                           </ul>
                       </div>
                    </div>
                </form>
            </div>
        </div>
      </div>
    </div>
</div>
{{-- login agent --}}

{{-- login service --}}
<div class="modal sign_popup" id="modalproignin">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <div class="sign_popup_body">
                <h4>Pro Signin</h4>
                @include('includes.modal_message')
                <form action="javascript:;" method="POST" id="servicesininform">
                    <div class="sign_popup_pane">
                        <div class="input-field">
                          <input type="text" class="required" name="serviceusername" id="serviceusername">
                          <label for="serviceusername">Email/Mobile Number </label>
                       </div>
                       <div class="input-field">
                        <div style="position: relative">
                          <input type="password"  class="required" name="serviceloginpassword" id="serviceloginpassword">
                          <label for="serviceloginpassword">Password</label>
                           <span toggle="#serviceloginpassword" class="fa fa-fw field-icon toggle-password fa-eye-slash"></span>
                        </div>
                        <label id="serviceloginpassword-error" class="error" for="serviceloginpassword" style="display: none"></label>
                       </div>
                       <div class="for_pas">
                           <a href="javascript:void(0);" class="a_popup openpass" id="forgot_pass">Forgot Password?</a>
                       </div>
                       <div class="sing_button">
                        <button class="see_cc" id="serviceloginbtn">Sign In</button>
                        <a href="javascript:;" class="see_cc" style="display: none" id="servicelogina">Please Wait</a>
                       </div>
                      <div class="or_div">
                           <strong><span>or</span></strong>
                      </div>
                       <div class="google_div">
                            {{-- <button class="goo_btn"><i class="fa fa-facebook-official"></i>Continue with Facebook</button>
                            <button class="goo_btn goo_btn1"><i class="fa fa-google-plus"></i>Continue with Google</button> --}}
                            <a class="goo_btn social_btn" href="{{route('login.social',['user_type'=>'service','provider_type'=>'facebook'])}}"><i class="fa fa-facebook-official"></i>Continue with Facebook</a>
                            <a class="goo_btn goo_btn1 social_btn" href="{{route('login.social',['user_type'=>'service','provider_type'=>'google'])}}"><i class="fa fa-google-plus"></i>Continue with Google</a>
                       </div>
                       <div class="sign_popup_div">
                            <p>Don’t have an account on RiVirtual?</p>
                           <a href="javascript:void(0);"  id="clocv" class="a_popup openprosignup">Sign Up Now</a>
                       </div>
                       <div class="estate_agent_div">
                           <ul>
                               <li><a href="javascript:void(0);" class="a_popup opensignin" id="clocv">User Login</a></li>
                               <li><a href="javascript:void(0);"  id="clocv" class="a_popup agentlogin">Real Estate Agent Login</a></li>
                           </ul>
                       </div>
                    </div>
                </form>
            </div>
        </div>
      </div>
    </div>
</div>

{{-- login service --}}

<!-- Forgot Password -->

<div class="modal sign_popup forgot_pass" id="forgotpass">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="sign_popup_body">
          	<h4>Forgot Password?</h4>
              @include('includes.modal_message')
          	<p>Enter your registered email address/mobile number to receive the link/OTP to reset your password </p>
          	<form action="" method="post" id="forgotpasswordform">
          		<div class="sign_popup_pane">
          			<div class="input-field">
					    <input type="text" id="forgotusername" name="forgotusername"required="">
					    <label for="forgotusername">Email/Mobile Number </label>
					 </div>
					 <div class="sing_button">
                        <button class="see_cc" id="forgotbtn">Continue</button>
                        <a href="javascript:;" class="see_cc" style="display: none" id="forgota">Please Wait</a>
					 </div>
					 <div class="sign_popup_div">
					 	 <p>Having Problem? Ask for <a href="#url" class="a_popup">Help</a></p>

					 </div>
          		</div>
          	</form>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- Forgot Password -->


<!-- Forgot otp -->
<div class="modal sign_popup forgot_pass" id="forgototpopup">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <div class="sign_popup_body">
                <h4>Let's get started</h4>
                @include('includes.modal_message')
                <p id="forgototptext">Enter the 6 digit code sent to you at +91 9876543210</p>
                <p id="forgototp"></p>
                <form action="javascript:;" method="POST" id="forgototp_form">
                    <div class="sign_popup_pane">
                        <input type="hidden" name="forgot_user_id" id="forgot_user_id">
                        <div class="input_otp">
                            <ul>
                              <li><input id="codeBoxs1" name="codeBoxs1" type="number" maxlength="1" onkeyup="onKeyUpEvent1(1, event)" onfocus="onFocusEvent1(1)" onKeyPress="if(this.value.length==1) return false;" oninput="this.value = this.value.replace(/[^0-9.]/g,'')"></li>
                              <li><input id="codeBoxs2" name="codeBoxs2" type="number" maxlength="1" onkeyup="onKeyUpEvent1(2, event)" onfocus="onFocusEvent1(2)" onKeyPress="if(this.value.length==1) return false;" oninput="this.value = this.value.replace(/[^0-9.]/g,'')"></li>
                              <li><input id="codeBoxs3" name="codeBoxs3" type="number" maxlength="1" onkeyup="onKeyUpEvent1(3, event)" onfocus="onFocusEvent1(3)" onKeyPress="if(this.value.length==1) return false;" oninput="this.value = this.value.replace(/[^0-9.]/g,'')"></li>
                              <li><input id="codeBoxs4" name="codeBoxs4" type="number" maxlength="1" onkeyup="onKeyUpEvent1(4, event)" onfocus="onFocusEvent1(4)" onKeyPress="if(this.value.length==1) return false;" oninput="this.value = this.value.replace(/[^0-9.]/g,'')"></li>
                              <li><input id="codeBoxs5" name="codeBoxs5" type="number" maxlength="1" onkeyup="onKeyUpEvent1(5, event)" onfocus="onFocusEvent1(5)" onKeyPress="if(this.value.length==1) return false;" oninput="this.value = this.value.replace(/[^0-9.]/g,'')"></li>
                              <li><input id="codeBoxs6" name="codeBoxs6" type="number" maxlength="1" onkeyup="onKeyUpEvent1(6, event)" onfocus="onFocusEvent1(6)" onKeyPress="if(this.value.length==1) return false;" oninput="this.value = this.value.replace(/[^0-9.]/g,'')"></li>
                                {{-- <li><input type="text" name=""></li>
                                <li><input type="text" name=""></li>
                                <li><input type="text" name=""></li>
                                <li><input type="text" name=""></li>
                                <li><input type="text" name=""></li>
                                <li><input type="text" name=""></li> --}}

                            </ul>
                        </div>
                        <label id="codeBoxs6-error" class="error" for="codeBoxs6"></label>
                       <div class="sing_button">
                           <button class="see_cc">Submit</button>
                       </div>
                        <div class="sign_popup_div">
                            <p>Didn’t receive verification code yet?</p>
                           <a href="#url" class="a_popup">Resend Code</a>
                       </div>
                    </div>
                </form>
            </div>
        </div>
      </div>
    </div>
</div>
<!-- Forgot otp -->



{{-- Chenge Password --}}
<div class="modal sign_popup" id="modalchangepassword">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <div class="sign_popup_body">
                <h4>Change Password</h4>
                @include('includes.modal_message')
                <form action="javascript:;" method="POST" id="changepasswordform">
                    <input type="hidden" name="change_user_id" id="change_user_id" value="">
                    <div class="sign_popup_pane">
                       <div class="input-field">
                        <div style="position: relative">
                          <input type="password"  class="required" name="newpassword" id="newpassword">
                          <label for="newpassword">New Password</label>
                          <span toggle="#newpassword" class="fa fa-fw field-icon toggle-password fa-eye-slash"></span>
                        </div>
                        <label id="newpassword-error" class="error" for="newpassword" style="display: none"></label>
                       </div>
                       <div class="input-field">
                        <div style="position: relative">
                           <input type="password"  class="required" id="newcpassword" name="newcpassword">
                           <label for="newcpassword">Confirm Password</label>
                           <span toggle="#newcpassword" class="fa fa-fw field-icon toggle-password fa-eye-slash"></span>
                        </div>
                        <label id="newcpassword" class="error" for="newcpassword" style="display: none"></label>
                        </div>
                       <div class="sing_button">
                        <button class="see_cc" id="serviceloginbtn">Change Password</button>
                        <a href="javascript:;" class="see_cc" style="display: none" id="servicelogina">Please Wait</a>
                       </div>
                       <div class="sign_popup_div">
                           <p>Having Problem? Ask for <a href="#url" class="a_popup">Help</a></p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      </div>
    </div>
</div>

{{-- Chenge Password --}}

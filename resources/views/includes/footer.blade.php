<footer class="foot_sec">
	<div class="foottop_sec">
		<div class="container">
			<div class="foottop_Iner">
				<div class="row">
					<div class="col-md-4 col-lg-5">
						<div class="footbx text-ffot">
							<a href="{{route('home')}}" class="foot_logo"><img src="{{ URL::to('public/frontend/images/ft-logo.png')}}" alt=""></a>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit Etiam vestibulum mollis justo,eu lacinia lorem ipsum dolor sit amet,consectetur adipiscing elit. Etiam vestibulum mollis justo, eulacinia lorem ipsum dolor sit amet justo, eu lacinia lorem ipsum dolor sit amet ipsum dolor sit amet, consectetur adipiscing elit Etiam vestibulum mollis justo,</p>
							<a href="#" class="redmor">Read More <img src="{{ URL::to('public/frontend/images/foot-arr.png')}}" class="hovern">
								<img src="{{ URL::to('public/frontend/images/arrwgr.png')}}" class="hoverb"></a>
						</div>
					</div>
					<div class="col-md-5 col-sm-8">
						<div class="footbx pl65" >
							<h5>Quick links</h5>
							<div class="quick-li">
							<ul>
								<li><a href="{{route('home')}}">Home</a></li>
								<li><a href="{{route('about.us')}}">About </a></li>
								<li><a href="{{route('contact.us')}}">Contact </a></li>
								<li><a href="{{route('faq')}}">FAQ</a></li>
								<li><a href="{{route('terms.condition')}}">Terms & conditions</a></li>
								<li><a href="{{route('privacy.policy')}}">Privacy policy </a></li>
							</ul>
							<ul>
								<li><a href="{{route('search.property',['property_for[]' => 'B'])}}">Buy</a></li>
								<li><a href="{{route('search.property',['property_for[]' => 'R'])}}">Rent</a></li>
								<li><a href="{{ route('sell.page') }}">Sell</a></li>
								<li><a href="javascript:;">Loans</a></li>
								<li><a href="javascript:;">Agents</a></li>
							</ul>

							<ul>
                                @if(!Auth::user())
								<li><a href="javascript:void(0);" class="opensignin">Login</a></li>
								<li><a href="javascript:void(0);" class="opensignup">User Sign Up</a></li>
								<li><a href="javascript:void(0);" class="openagentsignup">Agent Sign Up</a></li>
								<li><a href="javascript:void(0);" class="openprosignup" >Pro Sign Up</a></li>
                                @endif
							</ul>
							</div>
						</div>
					</div>

					<div class="col-md-3 col-lg-2 col-sm-4">
						<div class="footbx pl25" >
							<h5>By property type</h5>
							<ul>
								<li><a href="{{route('search.property',['property_type[]' => 'F'])}}"> Flat</a></li>
								<li><a href="{{route('search.property',['property_type[]' => 'H'])}}">House </a></li>
								<li><a href="{{route('search.property',['property_type[]' => 'L'])}}"> Land</a></li>
								<li><a href="{{route('search.property',['property_type[]' => 'C'])}}">Commercial </a></li>
								<li><a href="{{route('search.property',['property_type[]' => 'O'])}}"> Office</a></li>
							</ul>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="foot_botm">
        <div class="container">
            <div class="foot_botm_inr">
                <div class="foot_botm_left">
                    <ul>
                        <li>Copyright © 2021  <a href="#">rivirtual.com</a>   <span>|</span> all rights reserved </li>
                    </ul>
                </div>
                <div class="foot_botm_right">
                	<p>Connect with social</p>
                    <ul>
                        <li><a href="#" target="_blank"><img src="{{ URL::to('public/frontend/images/facebbok.png')}}" alt="facebook"></a></li>
                        <li><a href="#" target="_blank"><img src="{{ URL::to('public/frontend/images/twitter.png')}}" alt="twitter"></a></li>
                        <li><a href="#" target="_blank"><img src="{{ URL::to('public/frontend/images/linkin.png')}}" alt="linkin"></a></li>
                        <li><a href="#" target="_blank"><img src="{{ URL::to('public/frontend/images/insta.png')}}" alt="instagram"></a></li>
                        <li><a href="#" target="_blank"><img src="{{ URL::to('public/frontend/images/youtube.png')}}" alt="youtube"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</footer>


<div class="foot_arw fixed">
	<a href="javascript:" id="return-to-top"><img src="{{ URL::to('public/frontend/images/top.png')}}" alt=""></a>
</div>
<div class="modal res-modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Reschedule Property Visits</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <form>
                                        <div class="appome_input das_input">
                                            <label>Alternative Date:</label>
                                            <div class="dash-d">
                                            <input type="text" placeholder="Select Date" id="datepicker3">
                                            <span class="over_llp1"><img src="{{ url('public/frontend/images/cala.png') }}"></span>
                                            </div>
                                        </div>
                                        <div class="appome_input das_input">
                                            <label>Select Time</label>
                                            <div class="dash-d">
                                            <input type="text" placeholder="Select Time" name="time">
                                            <span class="over_llp1"><img src="{{ url('public/frontend/images/clock.png') }}"></span>
                                            </div>
                                        </div>
                                        
                                    </form>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Save</button>
        </div>
        
      </div>
    </div>
</div>
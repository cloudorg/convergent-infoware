@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Home </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<!----header--->
<div class="haeder-padding"></div>
<!----banner--->
<section class="banner-section">
	<div class="banner-image">
		<img src="{{ URL::to('public/frontend/images/banner-img.jpg')}}" alt="banner-image">
	</div>
	<div class="banner-text">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="text-center">
						<h1> This is a simply dummy <span class="d-fblock"> caption here. </span></h1>



						<form class="banner-form" action="{{route('search.property')}}" method="POST">
                            @csrf
						  	 <ul class="nav nav-pills" role="tablist">
							    <li class="nav-item">
                                    <a class="nav-link active" data-toggle="pill" href="javascript:;" id="buy_link">
                                      <img src="{{ URL::to('public/frontend/images/tab1h.png')}}" alt="buy" class="hovern">
                                      <img src="{{ URL::to('public/frontend/images/tab1.png')}}" alt="buy" class="hoverb">
                                      BUY
                                    </a>
                                    <div class="radiobx">
                                        <input type="radio" id="buy" name="property_for[]" value="B"  checked>
                                        {{-- <label for="buy">For Buy</label> --}}
                                    </div>
							    </li>
							    <li class="nav-item">
                                    <a class="nav-link" data-toggle="pill" href="javascript:;" id="rent_link">
                                        <img src="{{ URL::to('public/frontend/images/tab2h.png')}}" alt="rent" class="hovern">
                                        <img src="{{ URL::to('public/frontend/images/tab2.png')}}" alt="rent" class="hoverb">
                                        Rent
                                    </a>
                                    <div class="radiobx">
                                        <input type="radio" id="rent" name="property_for[]" value="R" >
                                        {{-- <label for="rent">For Rent</label> --}}

                                    </div>
							    </li>

							  </ul>
							  <div class="tab-content">
                                <div id="home" class="tab-pane active">
                                    <div class="from-sec-banner">
                                        <div class="input-from">
                                            <div class="form-group borders">
                                                <div class="input-with-icon">
                                                  <input type="text" class="form-control" placeholder="Location" name="location" id="location" onkeyup="search_result_check(this);" autocomplete="off">
                                                  <img src="{{ URL::to('public/frontend/images/map1.png')}}">
                                                </div>
                                                <ul id="serch_result" class="search_sajasation" style="display: none"></ul>
                                            </div>
                                            <div class="form-group ">
                                                <div class="input-with-icon">
                                                    <a class="user_llllk"> <span>Property Type</span> <img src="https://phpwebdevelopmentservices.com/development/rivirtual_code/public/frontend/images/caret.png" class="caret-img"> </a>
                                                  <img src="{{ URL::to('public/frontend/images/house1.png')}}">


                                                  <div class="show03 filter-category" style="display: none;">
                                        <div class="diiferent-sec">
                                            <ul class="category-ul">
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate1" name="property_type[]" value="F" >
                                                        <label for="rate1">Flat</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate2" name="property_type[]" value="H" >
                                                        <label for="rate2">House</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate3" name="property_type[]" value="L" >
                                                        <label for="rate3">Land</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate4" name="property_type[]" value="R" >
                                                        <label for="rate4">Residential</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate5" name="property_type[]" value="O" >
                                                        <label for="rate5">Office</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                              </div>


                                            </div>
                                        </div>
                                        <div class="search-box">
                                            <div class="sub2-lap">
                                                <img src="{{ URL::to('public/frontend/images/search.png')}}" alt="search">
                                            </div>
                                            <input type="submit" value="Search">
                                        </div>
                                    </div>
                                </div>
                                {{-- <div id="menu1" class=" tab-pane fade">
                                    <div class="from-sec-banner">
                                        <div class="input-from">
                                            <div class="form-group borders">
                                                <div class="input-with-icon">
                                                  <input type="text" class="form-control" placeholder="Location">
                                                  <img src="{{ URL::to('public/frontend/images/map1.png')}}">
                                              </div>
                                            </div>
                                            <div class="form-group ">
                                                <div class="input-with-icon">
                                                  <input type="text" class="form-control" placeholder="Property Type">
                                                  <img src="{{ URL::to('public/frontend/images/house1.png')}}">
                                              </div>
                                            </div>
                                        </div>
                                        <div class="search-box">
                                            <div class="sub2-lap">
                                                <img src="{{ URL::to('public/frontend/images/search.png')}}" alt="search">
                                            </div>
                                            <input type="submit" name="" value="Search">
                                        </div>
                                    </div>
                                </div> --}}

                            </div>

						</form>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!----banner--->

<!----service--->
<section class="service-section">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 col-xl-9 col-md-11 mx-auto">
				<div class="section-heading text-center">
					<h2>Our main focus</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem sapien, auctor in justo id, dignissim.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-md-4">
				<div class="service-white buy">
					<img src="{{ URL::to('public/frontend/images/serv1.png')}}" class="hovern">
					<img src="{{ URL::to('public/frontend/images/serv1h.png')}}" class="hoverb">
					<h5>Buy a property</h5>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry lorem Ipsum has been dummy caption</p>
					<a href="#">Learn more</a>
				</div>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="service-white rent">
					<img src="{{ URL::to('public/frontend/images/serv3h.png')}}" class="hovern">
					<img src="{{ URL::to('public/frontend/images/serv3.png')}}" class="hoverb">
					<h5>Rent a property</h5>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry lorem Ipsum has been dummy caption</p>
					<a href="#">Learn more</a>
				</div>
			</div>
			<div class="col-sm-6 col-md-4">
				<div class="service-white hire">
					<img src="{{ URL::to('public/frontend/images/serv2.png')}}"  class="hovern">
					<img src="{{ URL::to('public/frontend/images/serv2h.png')}}"  class="hoverb">
					<h5>Hire a pro</h5>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry lorem Ipsum has been dummy caption</p>
					<a href="#">Learn more</a>
				</div>
			</div>
		</div>
	</div>
</section>

<!----Properties--->
<section class="property-sec py-50">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 col-xl-9 col-md-11 mx-auto">
				<div class="section-heading2 text-center">
					<h2>Explore Properties on RiVirtual</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed lorem sapien, auctor in justo id, dignissim dipiscing elit sed lorem sapien</p>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="properties-carousel">
			<div class="propertiesinr">
			<div class="owl-carousel">
				<div class="item">
					<div class="propertiesbx">
						<span>
							<img src="{{ URL::to('public/frontend/images/propertiespic1.png')}}" alt="">
						</span>

						<a href="javascript:;" class="probtn">View Home <i class="fa fa-angle-right"></i></a>
                        <div class="propertiesbx-link">
                            <strong>Scottsdale, AZ</strong>
                            <p>Simply dummy caption show here</p>
                        </div>
					</div>
					<div class="propertiesbx">
						<span>
                        <img src="{{ URL::to('public/frontend/images/propertiespic2.png')}}" alt="">
                    </span>
						<a href="javascript:;" class="probtn">View Home <i class="fa fa-angle-right"></i></a>
                        <div class="propertiesbx-link">
                            <strong>Scottsdale, AZ</strong>
                            <p>Simply dummy caption show here</p>
                        </div>
					</div>
				</div>
				<div class="item">
					<div class="propertiesbx">
						<span>
	                        <img src="{{ URL::to('public/frontend/images/propertiespic3.png')}}" alt="">
	                    </span>
						<a href="javascript:;" class="probtn">View Home <i class="fa fa-angle-right"></i></a>
                        <div class="propertiesbx-link">
                            <strong>Scottsdale, AZ</strong>
                            <p>Simply dummy caption show here</p>
                        </div>
					</div>
					<div class="propertiesbx">
						<span>
                        <img src="{{ URL::to('public/frontend/images/propertiespic4.png')}}" alt="">
                    </span>
						<a href="javascript:;" class="probtn">View Home <i class="fa fa-angle-right"></i></a>
                        <div class="propertiesbx-link">
                            <strong>Scottsdale, AZ</strong>
                            <p>Simply dummy caption show here</p>
                        </div>
					</div>
				</div>
				<div class="item">
					<div class="propertiesbx">
						<span>
                        <img src="{{ URL::to('public/frontend/images/propertiespic5.png')}}" alt="">
                    </span>
						<a href="javascript:;" class="probtn">View Home <i class="fa fa-angle-right"></i></a>
                        <div class="propertiesbx-link">
                            <strong>Scottsdale, AZ</strong>
                            <p>Simply dummy caption show here</p>
                        </div>
					</div>
					<div class="propertiesbx">
						<span>
                        <img src="{{ URL::to('public/frontend/images/propertiespic6.png')}}" alt="">
                    </span>
						<a href="javascript:;" class="probtn">View Home <i class="fa fa-angle-right"></i></a>
                        <div class="propertiesbx-link">
                            <strong>Scottsdale, AZ</strong>
                            <p>Simply dummy caption show here</p>
                        </div>
					</div>
				</div>
				<div class="item">
					<div class="propertiesbx">
						<span>
                        <img src="{{ URL::to('public/frontend/images/propertiespic7.png')}}" alt="">
                    </span>
						<a href="javascript:;" class="probtn">View Home <i class="fa fa-angle-right"></i></a>
                        <div class="propertiesbx-link">
                            <strong>Scottsdale, AZ</strong>
                            <p>Simply dummy caption show here</p>
                        </div>
					</div>
					<div class="propertiesbx">
						<span>
                        <img src="{{ URL::to('public/frontend/images/propertiespic8.png')}}" alt="">
                    </span>
						<a href="javascript:;" class="probtn">View Home <i class="fa fa-angle-right"></i></a>
                        <div class="propertiesbx-link">
                            <strong>Scottsdale, AZ</strong>
                            <p>Simply dummy caption show here</p>
                        </div>
					</div>
				</div>
				<div class="item">
					<div class="propertiesbx">
						<span>
                        <img src="{{ URL::to('public/frontend/images/propertiespic9.png')}}" alt="">
                    </span>
						<a href="javascript:;" class="probtn">View Home <i class="fa fa-angle-right"></i></a>
                        <div class="propertiesbx-link">
                            <strong>Scottsdale, AZ</strong>
                            <p>Simply dummy caption show here</p>
                        </div>
					</div>
					<div class="propertiesbx">
						<span>
                        <img src="{{ URL::to('public/frontend/images/propertiespic10.png')}}" alt="">
                    </span>
						<a href="javascript:;" class="probtn">View Home <i class="fa fa-angle-right"></i></a>
                        <div class="propertiesbx-link">
                            <strong>Scottsdale, AZ</strong>
                            <p>Simply dummy caption show here</p>
                        </div>
					</div>
				</div>
				<div class="item">
					<div class="propertiesbx">
						<span>
                        <img src="{{ URL::to('public/frontend/images/propertiespic1.png')}}" alt="">
                    </span>
						<a href="javascript:;" class="probtn">View Home <i class="fa fa-angle-right"></i></a>
                        <div class="propertiesbx-link">
                            <strong>Scottsdale, AZ</strong>
                            <p>Simply dummy caption show here</p>
                        </div>
					</div>
					<div class="propertiesbx">
						<span>
                        <img src="{{ URL::to('public/frontend/images/propertiespic2.png')}}" alt="">
                    </span>
						<a href="javascript:;" class="probtn">View Home <i class="fa fa-angle-right"></i></a>
                        <div class="propertiesbx-link">
                            <strong>Scottsdale, AZ</strong>
                            <p>Simply dummy caption show here</p>
                        </div>
					</div>
				</div>
				<div class="item">
					<div class="propertiesbx">
						<span>
                        <img src="{{ URL::to('public/frontend/images/propertiespic3.png')}}" alt="">
                    </span>
						<a href="javascript:;" class="probtn">View Home <i class="fa fa-angle-right"></i></a>
                        <div class="propertiesbx-link">
                            <strong>Scottsdale, AZ</strong>
                            <p>Simply dummy caption show here</p>
                        </div>
					</div>
					<div class="propertiesbx">
						<span>
                        <img src="{{ URL::to('public/frontend/images/propertiespic4.png')}}" alt="">
                    </span>
						<a href="javascript:;" class="probtn">View Home <i class="fa fa-angle-right"></i></a>
                        <div class="propertiesbx-link">
                            <strong>Scottsdale, AZ</strong>
                            <p>Simply dummy caption show here</p>
                        </div>
					</div>
				</div>
				<div class="item">
					<div class="propertiesbx">
						<span>
                        <img src="{{ URL::to('public/frontend/images/propertiespic5.png')}}" alt="">
                    </span>
						<a href="javascript:;" class="probtn">View Home <i class="fa fa-angle-right"></i></a>
                        <div class="propertiesbx-link">
                            <strong>Scottsdale, AZ</strong>
                            <p>Simply dummy caption show here</p>
                        </div>
					</div>
					<div class="propertiesbx">
						<span>
                        <img src="{{ URL::to('public/frontend/images/propertiespic6.png')}}" alt="">
                    </span>
						<a href="javascript:;" class="probtn">View Home <i class="fa fa-angle-right"></i></a>
                        <div class="propertiesbx-link">
                            <strong>Scottsdale, AZ</strong>
                            <p>Simply dummy caption show here</p>
                        </div>
					</div>
				</div>
				<div class="item">
					<div class="propertiesbx">
						<span>
                        <img src="{{ URL::to('public/frontend/images/propertiespic7.png')}}" alt="">
                    </span>
						<a href="javascript:;" class="probtn">View Home <i class="fa fa-angle-right"></i></a>
                        <div class="propertiesbx-link">
                            <strong>Scottsdale, AZ</strong>
                            <p>Simply dummy caption show here</p>
                        </div>
					</div>
					<div class="propertiesbx">
						<span>
                        <img src="{{ URL::to('public/frontend/images/propertiespic8.png')}}" alt="">
                    </span>
						<a href="javascript:;" class="probtn">View Home <i class="fa fa-angle-right"></i></a>
                        <div class="propertiesbx-link">
                            <strong>Scottsdale, AZ</strong>
                            <p>Simply dummy caption show here</p>
                        </div>
					</div>
				</div>
				<div class="item">
					<div class="propertiesbx">
						<span>
                        <img src="{{ URL::to('public/frontend/images/propertiespic9.png')}}" alt="">
                    </span>
						<a href="javascript:;" class="probtn">View Home <i class="fa fa-angle-right"></i></a>
                        <div class="propertiesbx-link">
                            <strong>Scottsdale, AZ</strong>
                            <p>Simply dummy caption show here</p>
                        </div>
					</div>
					<div class="propertiesbx">
						<span>
                        <img src="{{ URL::to('public/frontend/images/propertiespic10.png')}}" alt="">
                    </span>
						<a href="javascript:;" class="probtn">View Home <i class="fa fa-angle-right"></i></a>
                        <div class="propertiesbx-link">
                            <strong>Scottsdale, AZ</strong>
                            <p>Simply dummy caption show here</p>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>


</section>

@if(!Auth::user())
<!--------register----->
<section class="register-section">
	<div class="container">
		<div class="row no-gutters">
			<div class="col-md-6">
				<div class="rester-info">
					<div class="image_div"><img src="{{ URL::to('public/frontend/images/agent.png')}}"></div>
					<div class="register-text">
						<h5>Agents - Add Your Properties. </h5>
						<p>Lorem Ipsum is simply dummy text of the printing and men book.Lorem leap into electronic typesetting,</p>
						<a href="javascript:;" class="openagentsignup">Register Now <img src="{{ URL::to('public/frontend/images/arrwg.png')}}" class="hovern">  <img src="{{ URL::to('public/frontend/images/arrwy.png')}}"  class="hoverb"></a>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="rester-info pro-secr">
					<div class="image_div"><img src="{{ URL::to('public/frontend/images/pro.png')}}"></div>
					<div class="register-text">
						<h5>Pro - Showcase Your Profile  </h5>
						<p>Lorem Ipsum is simply dummy text of the printing and men book.Lorem leap into electronic typesetting,</p>
						<a href="javascript:;" class="openprosignup">Sign Up Now <img src="{{ URL::to('public/frontend/images/arrww.png')}}"  class="hovern">  <img src="{{ URL::to('public/frontend/images/arrwy.png')}}"  class="hoverb"></a>
					</div>
				</div>
			</div>
			<div class="center_tag_wrapper">
                <p>OR</p>
            </div>
		</div>
	</div>
</section>
<!--------register----->
@endif
@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
@include('includes.toaster')
<script>
    $(document).ready(function(){
        $('#rent_link').click(function(){
            $('#rent').click();
        })
        $('#buy_link').click(function(){
            $('#buy').click();
        })
    })
</script>
<script>
    function search_result_check(that) {
        var $this = that;
        var name = $($this).val();
        console.log($($this).val());
        $.ajax({
            type: "POST",
            url:"{{route('get.locality.available')}}",
            data: {
                'name': name,
                '_token':'{{@csrf_token()}}'
            },
            success: function(data) {
              if(data){
                console.log(data);
                $('#serch_result').show();
                $('#serch_result').fadeIn();
                $('#serch_result').last().html(data);

              }
              else
              {
                $('#serch_result').children().remove();
              }
            }
        });
    }
    function add_searchbar(that) {
        var $this = that;
        var name = $($this).text();
        console.log(name);
        $('#location').val(name);
        $($this).remove();
        $('#serch_result').children().remove();
        $('#serch_result').hide();
    }
</script>
@endsection

@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Dashbord </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<!----header--->
<div class="haeder-padding"></div>

<div class="dashboard-provider">
	<div class="container">
		<div class="row">
			@include('includes.sidebar')
            <div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
				   <h2 class="pro-heading-das">Dashboard</h2>
				</div>
                @if(auth()->user()->approval_status==null)
                <center><p class="alert alert-info">Complete Your Profile  <a href="{{route('service.provider.profile')}}"> click here</a></p></center>
                @endif
				<div class="prod-dash-right">
                    <div class="agent-right-body">
                        <div class="prov-statis">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="stats-box">
                                        <div>
                                            <p>Membership Expiry on </p>
                                            <h5>29.11.2021</h5>
                                        </div>
                                        <img src="{{ URL::to('public/frontend/images/mems.png')}}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="stats-box">
                                        <div>
                                            <p>Incoming Escrow </p>
                                            <h5><span>₹</span>15000</h5>
                                        </div>
                                        <img src="{{ URL::to('public/frontend/images/memsa.png')}}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="stats-box">
                                        <div>
                                            <p>Released Escrow </p>
                                            <h5><span>₹</span>9800</h5>
                                        </div>
                                        <img src="{{ URL::to('public/frontend/images/memsb.png')}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div>
                                <div class="user_statis mt-4">
                                    <em>
                                        <i class="icofont-briefcase-1"></i>
                                    </em>
                                    <p> Jobs In Progress : <span> 3 </span></p>
                                </div>
                                <div class="custom-table ">
                                    <div class="new-table-mr">
                                        <div class="table">
                                            <div class="one_row1 hidden-sm-down only_shawo">
                                                <div class="cell1 tab_head_sheet">Job Title	</div>
                                                <div class="cell1 tab_head_sheet">Client's Name </div>
                                                <div class="cell1 tab_head_sheet">Category </div>

                                                <div class="cell1 tab_head_sheet">Date </div>
                                                <div class="cell1 tab_head_sheet">Location </div>
                                                <div class="cell1 tab_head_sheet">Budget</div>

                                                <div class="cell1 tab_head_sheet">Action </div>
                                            </div>
                                            <!--row 1-->
                                            <div class="one_row1 small_screen31">
                                                <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Job Title	</span>
                                                    <p class="add_ttrr"> Dummy title heresed do mod a type... </p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Client's Name</span>
                                                    <p class="add_ttrr">Avisharika Das</p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
                                                    <p class="add_ttrr">Carpenter</p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
                                                    <p class="add_ttrr">10.10.2021</p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Location</span>
                                                    <p class="add_ttrr">Kolkata</p>
                                                </div>

                                                <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
                                                    <p class="add_ttrr">₹150/hr. </p>
                                                </div>

                                                <div class="cell1 tab_head_sheet_1 "> <span class="W55_1">Action</span>
                                                    <div class="add_ttrr actions-main text-center">
                                                        <a href="javascript:void(0);" class="action-dots" id="action11"><img src="{{ URL::to('public/frontend/images/action-dots.png')}}" alt=""></a>
                                                        <div class="show-actions" id="show-action11" style="display: none;"> <span class="angle"><img src="{{ URL::to('public/frontend/images/angle.png')}}" alt=""></span>
                                                            <ul class="text-left">


                                                                <li><a href="#">Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--row 1-->
                                            <!--row 1-->
                                            <div class="one_row1 small_screen31">
                                                <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Job Title	</span>
                                                    <p class="add_ttrr"> Dummy title heresed do mod a type... </p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Client's Name</span>
                                                    <p class="add_ttrr">Rahul Das</p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
                                                    <p class="add_ttrr">Carpenter</p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
                                                    <p class="add_ttrr">25.09.2021</p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Location</span>
                                                    <p class="add_ttrr">Mumbai</p>
                                                </div>

                                                <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
                                                    <p class="add_ttrr">₹150/hr. </p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
                                                    <div class="add_ttrr actions-main text-center">
                                                        <a href="javascript:void(0);" class="action-dots" id="action15"><img src="{{ URL::to('public/frontend/images/action-dots.png')}}" alt=""></a>
                                                        <div class="show-actions" id="show-action15" style="display: none;"> <span class="angle"><img src="{{ URL::to('public/frontend/images/angle.png')}}" alt=""></span>
                                                            <ul class="text-left">


                                                                <li><a href="#">Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="one_row1 small_screen31">
                                                <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Job Title	</span>
                                                    <p class="add_ttrr"> Dummy title heresed do mod a type... </p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Client's Name</span>
                                                    <p class="add_ttrr">Avishek Bera</p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
                                                    <p class="add_ttrr">Carpenter</p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
                                                    <p class="add_ttrr">10.10.2021</p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Location</span>
                                                    <p class="add_ttrr">Delhi</p>
                                                </div>

                                                <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
                                                    <p class="add_ttrr">₹150/hr. </p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
                                                    <div class="add_ttrr actions-main text-center">
                                                        <a href="javascript:void(0);" class="action-dots" id="action14"><img src="{{ URL::to('public/frontend/images/action-dots.png')}}" alt=""></a>
                                                        <div class="show-actions" id="show-action14" style="display: none;"> <span class="angle"><img src="{{ URL::to('public/frontend/images/angle.png')}}" alt=""></span>
                                                            <ul class="text-left">


                                                                <li><a href="#">Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="one_row1 small_screen31">
                                                <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Job Title	</span>
                                                    <p class="add_ttrr"> Dummy title heresed do mod a type... </p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Client's Name</span>
                                                    <p class="add_ttrr">Avishek Bera</p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
                                                    <p class="add_ttrr">Carpenter</p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
                                                    <p class="add_ttrr">10.10.2021</p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Location</span>
                                                    <p class="add_ttrr">Delhi</p>
                                                </div>

                                                <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
                                                    <p class="add_ttrr">₹150/hr. </p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
                                                    <div class="add_ttrr actions-main text-center">
                                                        <a href="javascript:void(0);" class="action-dots" id="action5"><img src="{{ URL::to('public/frontend/images/action-dots.png')}}" alt=""></a>
                                                        <div class="show-actions" id="show-action5" style="display: none;"> <span class="angle"><img src="{{ URL::to('public/frontend/images/angle.png')}}" alt=""></span>
                                                            <ul class="text-left">


                                                                <li><a href="#">Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="one_row1 small_screen31">
                                                <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Job Title	</span>
                                                    <p class="add_ttrr"> Dummy title heresed do mod a type... </p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Client's Name</span>
                                                    <p class="add_ttrr">Avishek Bera</p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Category</span>
                                                    <p class="add_ttrr">Carpenter</p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
                                                    <p class="add_ttrr">10.10.2021</p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Location</span>
                                                    <p class="add_ttrr">Delhi</p>
                                                </div>

                                                <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Budget</span>
                                                    <p class="add_ttrr">₹150/hr. </p>
                                                </div>
                                                <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
                                                    <div class="add_ttrr actions-main text-center">
                                                        <a href="javascript:void(0);" class="action-dots" id="action4"><img src="{{ URL::to('public/frontend/images/action-dots.png')}}" alt=""></a>
                                                        <div class="show-actions" id="show-action4" style="display: none;"> <span class="angle"><img src="{{ URL::to('public/frontend/images/angle.png')}}" alt=""></span>
                                                            <ul class="text-left">


                                                                <li><a href="#">Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
@endsection

@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
<link href="{{ URL::asset('public/frontend/croppie/croppie.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('public/frontend/croppie/croppie.min.css') }}" rel="stylesheet" />
@endsection

@section('title')
<title> RiVirtual | Edit Profile </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<!----header--->
<div class="haeder-padding"></div>

<div class="dashboard-provider">
	<div class="container">
		<div class="row">
			@include('includes.sidebar')
            <div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
				   <h2 class="pro-heading-das">Edit Profile</h2>
				</div>
				<div class="prod-dash-right">
                    @if(auth()->user()->approval_status==null && $allWorkImageCount<1)
                    <center><p class="alert alert-info">Complete Your Profile upload at lest one work image</p></center>
                    @elseif(auth()->user()->approval_status==null)
                    <center><p class="alert alert-info">Complete Your Profile  <a href="{{route('service.provider.profile')}}"> click here</a></p></center>
                    @endif
                    @include('includes.message')
					<div class="agent-das-tab">
						<ul>
							<li ><a href="{{route('service.provider.profile')}}"> <img src="{{ URL::asset('public/frontend/images/geninfo.png')}}" class="hovern"> <img src="{{ URL::asset('public/frontend/images/geninfoh.png')}}" class="hoverb">  General Info</a></li>
							<li class="active"><a href="{{route('service.provider.work.image')}}"> <img src="{{ URL::asset('public/frontend/images/wr.png')}}" class="hovern"> <img src="{{ URL::asset('public/frontend/images/wr2.png')}}" class="hoverb"> Work Images</a></li>
						</ul>
					</div>

                    <div class="agent-right-body">
                        <form action="{{route('service.provider.work.image.save')}}" method="POST" id="work_image_galary">
                            <div class="up-work-img">
                                <div class="uplodimgfil">
                                    <input type="file" name="file" id="file" class="inputfile inputfile-1">

                                    <label for="file"><img src="{{ URL::asset('public/frontend/images/grbb.png')}}" alt="" class="hovern"> <img src="{{ URL::asset('public/frontend/images/clickhe.png')}}" alt="" class="hoverb"> Upload Images </label>
                                </div>
                            </div>
                            <input type="hidden" id="work_image" name="work_image">
                            @csrf
                        </form>
                        <div class="dash-listnew">
                            <ul class="edit-gallerynew">
                                @foreach ($allWorkImage as $image)
                                <li>
                                    <div class="upimg">
                                        {{-- <img src="{{ URL::asset('public/frontend/images/w1.png')}}"> --}}
                                        <img src="{{ URL::to('storage/app/public/work_image')}}/{{$image->image}}" alt="">
                                        <a href="{{route('service.provider.remove.work.image',['id'=>@$image->id])}}" onclick="return confirm('Are you sure remove image?')"><img src="{{ URL::asset('public/frontend/images/w-cross.png')}}"></a>
                                    </div>
                                </li>
                                @endforeach
                                {{-- <li>
                                    <div class="upimg">
                                        <img src="{{ URL::asset('public/frontend/images/w1.png')}}">
                                        <a href="#"><img src="{{ URL::asset('public/frontend/images/w-cross.png')}}"></a>
                                    </div>
                                </li>
                                <li>
                                    <div class="upimg">
                                        <img src="{{ URL::asset('public/frontend/images/w2.png')}}">
                                        <a href="#"><img src="{{ URL::asset('public/frontend/images/w-cross.png')}}"></a>
                                    </div>
                                </li>
                                <li>
                                    <div class="upimg">
                                        <img src="{{ URL::asset('public/frontend/images/w3.png')}}">
                                        <a href="#"><img src="{{ URL::asset('public/frontend/images/w-cross.png')}}"></a>
                                    </div>
                                </li>
                                <li>
                                    <div class="upimg">
                                        <img src="{{ URL::asset('public/frontend/images/w4.png')}}">
                                        <a href="#"><img src="{{ URL::asset('public/frontend/images/w-cross.png')}}"></a>
                                    </div>
                                </li>
                                <li>
                                    <div class="upimg">
                                        <img src="{{ URL::asset('public/frontend/images/w5.png')}}">
                                        <a href="#"><img src="{{ URL::asset('public/frontend/images/w-cross.png')}}"></a>
                                    </div>
                                </li> --}}
                                <div class="clearfix"></div>
                            </ul>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="bodar"></div>
                            </div>
                        </div>
						<div class="row">
                           <div class="col-md-12">
                               <div class="usei_sub uppro">
                                   {{-- <input type="submit" value="Save &amp; Complete " class="see_cc"> --}}
                                   <a href="{{route('service.provider.profile')}}" class="see_cc">Save &amp; Complete</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


		</div>

	</div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="croppie-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Crop Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="croppie-div" style="width: 100%;"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="crop-img">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
<script src="{{ URL::asset('public/frontend/croppie/croppie.js') }}"></script>
<script>
    function dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]),
        n = bstr.length,
        u8arr = new Uint8Array(n);
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, {type:mime});
    }
    var uploadCrop;
    $(document).ready(function(){
        $('#croppie-modal').on('hidden.bs.modal', function() {
            uploadCrop.croppie('destroy');
        });

        $('#crop-img').click(function() {
            uploadCrop.croppie('result', {
                type: 'base64',
                format: 'png'
            }).then(function(base64Str) {
                $("#croppie-modal").modal("hide");
               //  $('.lds-spinner').show();
               let file = dataURLtoFile('data:text/plain;'+base64Str+',aGVsbG8gd29ybGQ=','hello.png');
                  console.log(file.mozFullPath);
                  console.log(base64Str);
                  $('#work_image').val(base64Str);
                  $('#work_image_galary').submit();
               // $.each(file, function(i, f) {
            //         var reader = new FileReader();
            //         reader.onload = function(e){
            //             $('.uplodimgfilimg').append('<em><img  src="' + e.target.result + '"><em>');
            //         };
            //         reader.readAsDataURL(file);

            //    //  });
            //     $('.uplodimgfilimg').show();

            });
        });
    });
    $("#file").change(function () {
            $('.uplodimgfilimg').html('');
            let files = this.files;
            console.log(files);
            let img  = new Image();
            if (files.length > 0) {
                let exts = ['image/jpeg', 'image/png', 'image/gif'];
                let valid = true;
                $.each(files, function(i, f) {
                    if (exts.indexOf(f.type) <= -1) {
                        valid = false;
                        return false;
                    }
                });
                if (! valid) {
                    alert('Please choose valid image files (jpeg, png, gif) only.');
                    $("#file").val('');
                    return false;
                }
                // img.src = window.URL.createObjectURL(event.target.files[0])
                // img.onload = function () {
                //     if(this.width > 250 || this.height >160) {
                //         flag=0;
                //         alert('Please upload proper image size less then : 250px x 160px');
                //         $("#file").val('');
                //         $('.uploadImg').hide();
                //         return false;
                //     }
                // };
                $("#croppie-modal").modal("show");
                uploadCrop = $('.croppie-div').croppie({
                    viewport: { width: 398, height: 222, type: 'square' },
                    boundary: { width: $(".croppie-div").width(), height: 400 }
                });
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.upload-demo').addClass('ready');
                    // console.log(e.target.result)
                    uploadCrop.croppie('bind', {
                        url: e.target.result
                    }).then(function(){
                        console.log('jQuery bind complete');
                    });
                }
                reader.readAsDataURL(this.files[0]);
               //  $('.uploadImg').append('<img width="100"  src="' + reader.readAsDataURL(this.files[0]) + '">');
               //  $.each(files, function(i, f) {
               //      var reader = new FileReader();
               //      reader.onload = function(e){
               //          $('.uploadImg').append('<img width="100"  src="' + e.target.result + '">');
               //      };
               //      reader.readAsDataURL(f);
               //  });
               //  $('.uploadImg').show();
            }

        });
</script>
<script>
    $(document).ready(function(){
        $('#editform').validate({
            rules: {
                whatsapp_no:{
                    number: true ,
                    minlength: 10,
                    maxlength: 10,
                },
                budget:{
                    required:true,
                    number: true ,
                    min:1
                },
                experience:{
                    number: true ,
                    min:0
                },
            },
            messages: {
                budget:{
                    min:'Minimum amount 1 '
                },
                experienc:{
                    min:'Minimum experience 0 '
                },
            },
        });
    });
</script>

@endsection

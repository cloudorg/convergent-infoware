@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
        margin-top: 10px;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
<link href="{{ URL::asset('public/frontend/croppie/croppie.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('public/frontend/croppie/croppie.min.css') }}" rel="stylesheet" />
@endsection

@section('title')
<title> RiVirtual | Visit Request </title>
@endsection




@section('header')
@include('includes.header')
@endsection

@section('content')


<div class="haeder-padding"></div>

<div class="dashboard-agent">
	<div class="container">
		<div class="row">
            @include('includes.sidebar')
			<div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
				   <h2>View Visit Requests</h2>
				</div>
				<div class="agent-dash-right">

					<div class="agent-right-body">
                        <div class="agent-visits">
                                @include('includes.message')
                                <form action="{{ route('agent.visit.request') }}" method="post">
                                     @csrf
                                    <div class="user_dashboard_form">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-5">
                                                         <div class="das_input">
                                                            <label>Property Name</label>
                                                            <input type="text" placeholder="Enter here.." name="property_name" value="{{ @$key['property_name'] }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-7">
                                                        <div class="row">
                                                            <div class="col-md-6 col-sm-6">
                                                                <div class="das_input">
                                                                    <label>Date</label>
                                                                    <div class="dash-d">
                                                                        <input type="text" name="start_date" placeholder="Enter Date" id="datepicker" value="{{ @$key['start_date'] }}"> <span class="over_llp1"><img src="{{ url('public/frontend/images/cala.png') }}" alt=""></span> </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6">
                                                                <div class="das_input">
                                                                    <label>Time</label>
                                                                    <div class="dash-d">
                                                                        <input type="text" placeholder="Enter here.. " class="position-relative" name="time" value="{{ @$key['time'] }}"> <span class="over_llp1"><img src="{{ url('public/frontend/images/clock.png') }}" alt=""></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-4">
                                                <button type="submit" class="dash-btn"> <img src="{{ url('public/frontend/images/ser.png') }}"> Search </button>
                                            </div>

                                        </div>
                                    </div>
                                </form>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="bodar"></div>
                                    </div>
                                </div>
                           </div>

                           <div class="main-tabels">

                            <div class="custom-table">
                                        <div class="new-table-mr">
                                            <div class="table">
                                                <div class="one_row1 hidden-sm-down only_shawo">
                                                    <div class="cell1 tab_head_sheet">Property Name </div>
                                                    <div class="cell1 tab_head_sheet">Client’s Name</div>
                                                    <div class="cell1 tab_head_sheet">Date </div>
                                                    <div class="cell1 tab_head_sheet">Time </div>
                                                    <div class="cell1 tab_head_sheet">Location </div>

                                                    <div class="cell1 tab_head_sheet">Action </div>
                                                </div>
                                                <!--row 1-->
                                                @if(count(@$allVisitRequest)>0)
                                                 @foreach($allVisitRequest as $vr)

                                                <div class="one_row1 small_screen31" @if(@$vr->reschedule == 'Y') style="background-color:#D0F0C0 !important" @endif>
                                                    <div class="cell1 tab_head_sheet_1" > <span class="W55_1">Property Name  </span>
                                                        <p class="add_ttrr" > 
                                                            <a href="javascript:;" >
                                                             @if(strlen(@$vr->propertyDetails->name)>20)
                                                              {!! substr(@$vr->propertyDetails->name,0,20) . '...'!!}
                                                              @else
                                                            {!! @$vr->propertyDetails->name !!}
                                                            @endif
                                                          </a>
                                                         </p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Client’s Name</span>
                                                        <p class="add_ttrr"><a href="#">{{ @$vr->userDetails->name }}</a></p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
                                                        <p class="add_ttrr">{{@$vr->visit_date?date('d.m.Y',strtotime(@$vr->visit_date)) : '' }}</p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Time</span>
                                                        <p class="add_ttrr">{{@$vr->visit_date?date('h:i A',strtotime(@$vr->visit_date)) : '' }}</p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
                                                        <p class="add_ttrr">{{ @$vr->address }}</p>
                                                    </div>


                                                    <div class="cell1 tab_head_sheet_1 "> <span class="W55_1">Action</span>
                                                        <div class="add_ttrr actions-main text-center">
                                                            <a href="javascript:void(0);" class="action-dots" id="action{{ @$vr->id }}" data-id="{{ @$vr->id }}"><img src="{{ url('public/frontend/./images/action-dots.png') }}" alt=""></a>
                                                            <div class="show-actions" id="show-action{{ @$vr->id }}" style="display: none;"> <span class="angle"><img src="{{ url('public/frontend/./images/angle.png') }}" alt=""></span>
                                                                <ul class="text-left">
                                                                    <li><a data-toggle="modal" data-target="#myModal1" data-id="{{@$vr->id}}" class="rsch">Reschedule </a></li>
                                                                    <!-- <li><a href="javascript:;">Cancel</a></li> -->
                                                                    <li><a href="{{ route('agent.visit.request.cancel',['id'=>@$vr->id]) }}" onclick="return confirm('Are you want to cancel this visit request?')">Cancel</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @else
                                                    @if(@$allVisitRequest->isEmpty())
                                                   <div class="col-sm-12 col-md-12" align="center">
                                                   <center style="padding: 10px"><p>No Data Found</p></center>
                                                   </div>
                                                   @endif
                                                @endif
                                                <!--row 1-->
                                                <!--row 1-->
                                                {{--<div class="one_row1 small_screen31">
                                                    <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Property Name  </span>
                                                        <p class="add_ttrr"> <a href="property-details.html">Mission District Area</a></p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Client’s Name</span>
                                                        <p class="add_ttrr"><a href="#">Rosalina D. William</a></p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
                                                        <p class="add_ttrr">04.10.2021</p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Time</span>
                                                        <p class="add_ttrr">12:30pm</p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
                                                        <p class="add_ttrr">New Delhi</p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
                                                        <div class="add_ttrr actions-main text-center">
                                                            <a href="javascript:void(0);" class="action-dots" id="action12"><img src="./images/action-dots.png" alt=""></a>
                                                            <div class="show-actions" id="show-action12" style="display: none;"> <span class="angle"><img src="./images/angle.png" alt=""></span>
                                                                <ul class="text-left">
                                                                    <li><a data-toggle="modal" data-target="#myModal">Reschedule </a></li>
                                                                    <li><a href="#">Cancel</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--row 1-->
                                                <!--row 1-->
                                                <div class="one_row1 small_screen31">
                                                    <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Property Name  </span>
                                                        <p class="add_ttrr"> <a href="property-details.html">Pacific Heights Area</a></p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Client’s Name</span>
                                                        <p class="add_ttrr"><a href="#">Aiden Benjamin</a></p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
                                                        <p class="add_ttrr">01.10.2021</p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Time</span>
                                                        <p class="add_ttrr">02:15pm</p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
                                                        <p class="add_ttrr">Hyderabad</p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
                                                        <div class="add_ttrr actions-main text-center">
                                                            <a href="javascript:void(0);" class="action-dots" id="action13"><img src="./images/action-dots.png" alt=""></a>
                                                            <div class="show-actions" id="show-action13" style="display: none;"> <span class="angle"><img src="./images/angle.png" alt=""></span>
                                                                <ul class="text-left">
                                                                    <li><a data-toggle="modal" data-target="#myModal">Reschedule </a></li>
                                                                    <li><a href="#">Cancel</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--row 1-->
                                                <div class="one_row1 small_screen31">
                                                    <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Property Name  </span>
                                                        <p class="add_ttrr"> <a href="property-details.html">Noe Valley Zones</a></p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Client’s Name</span>
                                                        <p class="add_ttrr"><a href="#">Avishek Das</a></p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
                                                        <p class="add_ttrr">25.09.2021</p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Time</span>
                                                        <p class="add_ttrr">12:30pm</p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
                                                        <p class="add_ttrr">Mumbai</p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
                                                        <div class="add_ttrr actions-main text-center">
                                                            <a href="javascript:void(0);" class="action-dots" id="action15"><img src="./images/action-dots.png" alt=""></a>
                                                            <div class="show-actions" id="show-action15" style="display: none;"> <span class="angle"><img src="./images/angle.png" alt=""></span>
                                                                <ul class="text-left">
                                                                    <li><a data-toggle="modal" data-target="#myModal">Reschedule </a></li>
                                                                    <li><a href="#">Cancel</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="one_row1 small_screen31">
                                                    <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Property Name  </span>
                                                        <p class="add_ttrr"> <a href="property-details.html">Pacific Heights Area</a></p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Client’s Name</span>
                                                        <p class="add_ttrr"><a href="#">Rahul Manna</a></p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Date</span>
                                                        <p class="add_ttrr">22.09.2021</p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1 half-boxes"> <span class="W55_1">Time</span>
                                                        <p class="add_ttrr">02:15pm</p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Location </span>
                                                        <p class="add_ttrr">Kolkata </p>
                                                    </div>
                                                    <div class="cell1 tab_head_sheet_1"> <span class="W55_1">Action</span>
                                                        <div class="add_ttrr actions-main text-center">
                                                            <a href="javascript:void(0);" class="action-dots" id="action14"><img src="./images/action-dots.png" alt=""></a>
                                                            <div class="show-actions" id="show-action14" style="display: none;"> <span class="angle"><img src="./images/angle.png" alt=""></span>
                                                                <ul class="text-left">
                                                                    <li><a data-toggle="modal" data-target="#myModal">Reschedule </a></li>
                                                                    <li><a href="#">Cancel</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>--}}
                                            </div>
                                        </div>
                                        <nav aria-label="...">
                                            <ul class="pagination new-pagination">
                                            {{@$allVisitRequest->appends(request()->except(['page', '_token']))->links()}}
                                                {{--<li class="page-item"> <span class="page-link">Prev </span> </li>
                                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                                <li class="page-item" aria-current="page"> <span class="page-link">
                                                2
                                                <span class="sr-only">(current)</span> </span>
                                                </li>
                                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                <li class="page-item"><a class="page-link" href="#">4</a></li>
                                                <li class="page-item"> <a class="page-link act" href="#">Next </a> </li>--}}
                                            </ul>
                                        </nav>
                            </div>
                            <div class="clearfix"></div>

                        </div>

                    </div>

					</div>
				</div>

			</div>
		</div>

	</div>

    <div class="modal res-modal" id="myModal1">
        <div class="modal-dialog">
          <div class="modal-content">
          
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">Reschedule Property Visits</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body">
                <form action="{{route('agent.reschedule.availability')}}" method="POST" id="myForm">
                    @csrf
                    <input type="hidden" name="id" id="id">
                    <div class="appome_input das_input">
                        <label>Alternative Date:</label>
                            <div class="dash-d">
                                <input type="text" placeholder="Select Date" id="datepicker3" name="date">
                                <span class="over_llp1"><img src="{{ url('public/frontend/images/cala.png') }}"></span>
                            </div>
                    </div>
                    <div class="appome_input das_input">
                        <label>Select Time</label>
                            <div class="dash-d">
                                <input type="text" placeholder="Select Time" name="time">
                                <span class="over_llp1"><img src="{{ url('public/frontend/images/clock.png') }}"></span>
                            </div>
                    </div>
                                            
                
            </div>
            
            <!-- Modal footer -->
            <div class="modal-footer">
                <input type="submit" name="" value="Save" class="btn btn-danger">
              <!-- <button type="submit" class="btn btn-danger" data-dismiss="modal">Save</button> -->
            </div>
            </form>
          </div>
        </div>
    </div>
    @endsection



@section('footer')
@include('includes.footer')
@endsection



@section('script')
@include('includes.script')
<link rel='stylesheet' href='css/jquery-clockpicker.min.css'>
<script src='https://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.js'></script>
<script>
    $('.rsch').click(function(){
        var id = $(this).data('id');
        $('#id').val(id);
    });
</script>
<script>
    $('#myForm').validate({
        rules: {
                date:{
                    required:true,
                },
                time:{
                    required:true,
                }
                
            },
    })
</script>
<script>
        $(function() {
            $("#datepicker").datepicker();
        });
        $(function() {
            $("#datepicker2").datepicker();
        });
        $(function() {
            $("#datepicker3").datepicker();
        });
        </script>

<script>
    $("input[name=time]").clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        default: 'now',
        donetext: "Select",
        init: function() {
            console.log("colorpicker initiated");
        },
        beforeShow: function() {
            console.log("before show");
        },
        afterShow: function() {
            console.log("after show");
        },
        beforeHide: function() {
            console.log("before hide");
        },
        afterHide: function() {
            console.log("after hide");
        },
        beforeHourSelect: function() {
            console.log("before hour selected");
        },
        afterHourSelect: function() {
            console.log("after hour selected");
        },
        beforeDone: function() {
            console.log("before done");
        },
        afterDone: function() {
            console.log("after done");
        }
    });
    </script>
    <script>
         $(document).on('click', function () {
		@foreach(@$allVisitRequest as $value)
		var $target = $(event.target);
		if (!$target.closest('#action{{@$value->id}}').length && $('#show-action{{@$value->id}}').is(":visible")) {
			$('#show-action{{@$value->id}}').slideUp();
		}
		@endforeach
	});

	$(document).ready(function() {
		@foreach(@$allVisitRequest as $value)
			$("#action{{@$value->id}}").click(function() {
				$("#show-action{{@$value->id}}").slideToggle();
			});
		@endforeach
    });
    </script>

@endsection

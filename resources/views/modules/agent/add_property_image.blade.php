@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
        margin-top: 10px;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
<link href="{{ URL::asset('public/frontend/croppie/croppie.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('public/frontend/croppie/croppie.min.css') }}" rel="stylesheet" />
@endsection

@section('title')
<title> RiVirtual | Add Property Image </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<!----header--->
<div class="haeder-padding"></div>

<div class="dashboard-agent">
	<div class="container">
		<div class="row">
			@include('includes.sidebar')
            <div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
				   <h2>Add Property</h2>
				</div>
				<div class="agent-dash-right">
					<div class="agent-das-tab">
						<ul>
							<li ><a href="{{route('agent.add.property',['id'=>$propertyDetails->id])}}" >Property Details</a></li>
							<li class="active"><a @if($propertyDetails->status=='I') href="{{route('agent.add.property.image',['id'=>$propertyDetails->id])}}" @else href="{{route('agent.add.property.image',['id'=>$propertyDetails->id])}}" @endif>Property Images</a></li>
						</ul>
					</div>
                    @include('includes.message')
                    <form action="{{route('agent.add.property.image.save',['id'=>$propertyDetails->id])}}" id="addPropertyForm" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="agent-right-body">
                            <div class="row">
                                @if($propertyDetails->property_type!='L')
                                <div class="col-md-12">
                                    <div class="redio_bx prop">
                                        <span>Facilities / Amenities  :</span>
                                        <div class="diiferent-sec">
                                            <ul class="category-ul new-aget-pro">
                                                @foreach ($allFacilitiesAmenities as $data)
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate{{@$data->id}}" name="facilities_amenities[]" value="{{@$data->id}}" {{@in_array(@$data->id, @$facilitiesAmenities) ?'checked':''}}>
                                                        <label for="rate{{@$data->id}}">{{@$data->name}}</label>
                                                    </div>
                                                </li>
                                                @endforeach
                                                {{-- <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate2" name="radios" value="all">
                                                        <label for="rate2">Amenities/Facilities1</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate3" name="radios" value="all">
                                                        <label for="rate3">Available Car Parking Area</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate4" name="radios" value="all">
                                                        <label for="rate4">Open balcony</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate5" name="radios" value="all">
                                                        <label for="rate5">Air Conditioning</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate6" name="radios" value="all">
                                                        <label for="rate6">Car Parking Area</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate7" name="radios" value="all">
                                                        <label for="rate7">Air Conditioning</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate8" name="radios" value="all">
                                                        <label for="rate8">Gym Center</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate9" name="radios" value="all">
                                                        <label for="rate9">Alarm</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate10" name="radios" value="all">
                                                        <label for="rate10">Car Parking Area</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate11" name="radios" value="all">
                                                        <label for="rate11">Central Heating</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate12" name="radios" value="all">
                                                        <label for="rate12">Spa & Massage</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate13" name="radios" value="all">
                                                        <label for="rate13">Window Covering</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="radiobx">
                                                        <input type="checkbox" id="rate14" name="radios" value="all">
                                                        <label for="rate14">Air Conditioning</label>
                                                    </div>
                                                </li> --}}

                                            </ul>
                                        </div>
                                        <label id="facilities_amenities[]-error" class="error" for="facilities_amenities[]" style="display: none"></label>
                                    </div>
                                </div>
                                @endif
                                <div class="col-md-12">
                                    <div class="bodar"></div>
                                </div>
                                <div class="col-md-12">
                                    <div class="uplodimg">
                                        <div class="uplodimgfil">
                                            <b>Exterior Photos</b>
                                            <input type="file" name="file-1[]" id="file-1" class="inputfile inputfile-1 @if($propertyDetails->status=='I') required @endif" data-multiple-caption="{count} files selected"  onchange="fun1()" multiple="" accept="image/*">
                                            <label for="file-1">Upload Photos <img src="images/clickhe.png" alt=""></label>
                                            <label id="file-1-error" class="error" for="file-1" style="display: none"></label>
                                        </div>

                                    </div>
                                    <div class="dash-list-agent" id="file-1-upoade">
                                        <ul class="edit-gallery" id="file-1-ul">

                                            <div class="clearfix"></div>
                                        </ul>
                                    </div>

                                    @if($propertyDetails->status!='I')
                                    <div class="dash-list-agent">

                                        <ul class="edit-gallery">
                                            @foreach ($propertyImageExterior as $imageExterior)
                                            <li>
                                                <div class="upimg">
                                                    <img src="{{ URL::to('storage/app/public/property_image')}}/{{$imageExterior->image}}">
                                                    <a href="{{route('agent.add.property.image.remove',['id'=>$imageExterior->id])}}"><img src="{{ URL::to('public/frontend/images/w-cross.png')}}"></a>
                                                </div>
                                            </li>
                                            @endforeach

                                            <div class="clearfix"></div>
                                        </ul>
                                    </div>
                                    @endif
                                </div>
                                <div class="col-md-12">
                                    <div class="bodar"></div>
                                </div>
                                <div class="col-md-12">
                                    <div class="uplodimg">
                                        <div class="uplodimgfil">
                                            <b>Interior photos</b>
                                            <input type="file" name="file-2[]" id="file-2" class="inputfile inputfile-1 @if($propertyDetails->status=='I') required @endif" data-multiple-caption="{count} files selected"  onchange="fun2()" multiple="" accept="image/*">
                                            <label for="file-2">Upload Photos <img src="images/clickhe.png" alt=""></label>
                                            <label id="file-2-error" class="error" for="file-2" style="display: none"></label>
                                        </div>
                                    </div>
                                    <div class="dash-list-agent" id="file-2-upoade">
                                        <ul class="edit-gallery" id="file-2-ul">

                                            <div class="clearfix"></div>
                                        </ul>
                                    </div>

                                    @if($propertyDetails->status!='I')
                                    <div class="dash-list-agent">

                                        <ul class="edit-gallery">
                                            @foreach ($propertyImageInterior as $imageInterior)
                                            <li>
                                                <div class="upimg">
                                                    <img src="{{ URL::to('storage/app/public/property_image')}}/{{$imageInterior->image}}">
                                                    <a href="{{route('agent.add.property.image.remove',['id'=>$imageInterior->id])}}"><img src="{{ URL::to('public/frontend/images/w-cross.png')}}"></a>
                                                </div>
                                            </li>
                                            @endforeach
                                            <div class="clearfix"></div>
                                        </ul>
                                    </div>
                                    @endif

                                </div>
                                <div class="col-md-12">
                                    <div class="bodar"></div>
                                </div>


                                <div class="col-md-12">
                                    <div class="uplodimg">
                                        <div class="uplodimgfil">
                                            <b>Images of floor plan </b>
                                            <input type="file" name="file-3[]" id="file-3" class="inputfile inputfile-1 @if($propertyDetails->status=='I') required @endif" data-multiple-caption="{count} files selected" onchange="fun3()" multiple="" accept="image/*">
                                            <label for="file-3">Upload Photos <img src="images/clickhe.png" alt=""></label>
                                            <label id="file-3-error" class="error" for="file-3" style="display: none"></label>
                                        </div>
                                    </div>
                                    <div class="dash-list-agent" id="file-3-upoade">
                                        <ul class="edit-gallery" id="file-3-ul">

                                            <div class="clearfix"></div>
                                        </ul>
                                    </div>
                                    @if($propertyDetails->status!='I')
                                    <div class="dash-list-agent">

                                        <ul class="edit-gallery">
                                            @foreach ($propertyImageFloor as $imageFloor)
                                            <li>
                                                <div class="upimg">
                                                    <img src="{{ URL::to('storage/app/public/property_image')}}/{{$imageFloor->image}}">
                                                    <a href="{{route('agent.add.property.image.remove',['id'=>$imageFloor->id])}}"><img src="{{ URL::to('public/frontend/images/w-cross.png')}}"></a>
                                                </div>
                                            </li>
                                            @endforeach

                                            <div class="clearfix"></div>
                                        </ul>
                                    </div>
                                    @endif
                                </div>
                                <div class="col-md-12">
                                    <div class="bodar">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="usei_sub text-right">
                                        <input type="submit" value="Save & Complete " class="see_cc">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection


@section('footer')
@include('includes.footer')
@endsection



@section('script')
@include('includes.script')
<script>
    function fun3(){
        console.log(document.getElementById('file-3').files);
        var html='';
        for (i = 0; i < document.getElementById('file-3').files.length; ++i) {
            console.log(i)
            var ii=document.getElementById('file-3').files[i];
            var b=URL.createObjectURL(ii);
            html = html+`<li><div class="upimg"><img src="`+b+`"></div></li>`;
        }
        $('#file-3-ul').html(html);
        console.log(html)
    }
    function fun2(){
        console.log(document.getElementById('file-2').files);
        var html='';
        for (i = 0; i < document.getElementById('file-2').files.length; ++i) {
            console.log(i)
            var ii=document.getElementById('file-2').files[i];
            var b=URL.createObjectURL(ii);
            html = html+`<li><div class="upimg"><img src="`+b+`"></div></li>`;
        }
        $('#file-2-ul').html(html);
        console.log(html)
    }
    function fun1(){
        console.log(document.getElementById('file-1').files);
        var html='';
        for (i = 0; i < document.getElementById('file-1').files.length; ++i) {
            console.log(i)
            var ii=document.getElementById('file-1').files[i];
            var b=URL.createObjectURL(ii);
            html = html+`<li><div class="upimg"><img src="`+b+`"></div></li>`;
        }
        $('#file-1-ul').html(html);
        console.log(html)
    }
</script>
<script>
    $(document).ready(function(){
        $('#addPropertyForm').validate({
            rules: {
               'facilities_amenities[]':{
                    required:true,
                },
                'file-3[]':{
                    extension: "png|jpe?g",
                },
               'file-2[]':{
                   extension: "png|jpe?g",
                },
               'file-1[]':{
                   extension: "png|jpe?g",
                },
            },
            messages: {
                'facilities_amenities[]':{
                    required:'Minimun one facilities or amenities required',
                },
                'file-3[]':{
                    required:'Images of floor plan required',
                    extension: "File must be JPG or PNG",
                },
               'file-2[]':{
                    required:'Interior photos required',
                    extension: "File must be JPG or PNG",
                },
               'file-1[]':{
                    required:'Exterior Photos required',
                    extension: "File must be JPG or PNG",
                },
            },
            ignore: [],
        });
    });
</script>
@endsection

@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Dashbord </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<!----header--->
<div class="haeder-padding"></div>

<div class="dashboard-agent">
	<div class="container">
		<div class="row">
			@include('includes.sidebar')
            <div class="col-lg-9 col-md-12 col-sm-12">
				<div class="cus-dashboard-right">
				   <h2>Visit Availability</h2>
				</div>
                @include('includes.message')
				<div class="agent-dash-right">
                    <div class="agent-right-body">
                        <form action="{{route('agent.manage.availability.save')}}" method="POST" id="mondayform">
                            @csrf
                            <input type="hidden" value="1" name="day">
                            <div class="row new_rows no-gutters align-items-center">
                                <div class="col-md-2 col-sm-12">
                                    <div class="check-box new-av-check">
                                        {{-- <input type="checkbox" id="checkbox1" name="checkbox1" value="all"> --}}
                                        <label for="checkbox1" class="checklabeel">Monday : </label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form_box_area das_input">
                                        <label>From</label>
                                        <div class="dash-d">
                                              <input type="text" placeholder="Enter here.. " class="position-relative" name="fromtime" id="fromtime1"> <span class="over_llp1"><img src="{{ URL::to('public/frontend/images/clocks.png')}}" alt=""></span>
                                         </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form_box_area das_input">
                                        <label>To</label>
                                        <div class="dash-d">
                                              <input type="text" placeholder="Enter here.. " class="position-relative" name="totime"> <span class="over_llp1"><img src="{{ URL::to('public/frontend/images/clocks.png')}}" alt=""></span>
                                         </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-12">
                                    <div class="form_box_area">
                                        <label></label>
                                        {{-- <a href="#" class="adds_btn nesave">Add</a> --}}
                                        <input type="submit"class="adds_btn nesave" value="Add">
                                    </div>
                                </div>
                                <div class="col-md-10 offset-md-2">
                                    <ul class="list-showag">
                                        @foreach ($monday as $item1)
                                        <li>{{date('h:i A',strtotime(@$item1->from_time))}} to {{date('h:i A',strtotime(@$item1->to_time))}}<span><a href="{{route('agent.manage.availability.remove',$item1->id)}}" onclick="return confirm('Are you want to remove this availability ?')"><img src="{{ URL::to('public/frontend/images/cross4.png')}}"></a></span></li>
                                        @endforeach
                                        {{-- <li>12:00 am to 12:30 pm  <span><a href="#"><img src="{{ URL::to('public/frontend/images/cross4.png')}}"></a></span></li>
                                        <li>2:00 am to 2:30 pm  <span><a href="#"><img src="{{ URL::to('public/frontend/images/cross4.png')}}"></a></span></li> --}}
                                    </ul>
                                </div>
                            </div>
                        </form>
                        <form action="{{route('agent.manage.availability.save')}}" method="POST" id="tuesdayform">
                            @csrf
                            <input type="hidden" value="2" name="day">
                            <div class="row new_rows no-gutters align-items-center">
                                <div class="col-md-2 col-sm-12">
                                    <div class="check-box new-av-check">
                                        {{-- <input type="checkbox" id="checkbox2" name="checkbox1" value="all"> --}}
                                        <label for="checkbox2" class="checklabeel">Tuesday : </label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form_box_area das_input">
                                        <label>From</label>
                                        <div class="dash-d">
                                              <input type="text" placeholder="Enter here.. " class="position-relative" name="fromtime" id="fromtime2"> <span class="over_llp1"><img src="{{ URL::to('public/frontend/images/clocks.png')}}" alt=""></span>
                                         </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form_box_area das_input">
                                        <label>To</label>
                                        <div class="dash-d">
                                              <input type="text" placeholder="Enter here.. " class="position-relative" name="totime"> <span class="over_llp1"><img src="{{ URL::to('public/frontend/images/clocks.png')}}" alt=""></span>
                                         </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-12">
                                    <div class="form_box_area">
                                        <label></label>
                                        {{-- <a href="#" class="adds_btn nesave">Add</a> --}}
                                        <input type="submit"class="adds_btn nesave" value="Add">
                                    </div>
                                </div>
                                <div class="col-md-10 offset-md-2">
                                    <ul class="list-showag">
                                        @foreach ($tuesday as $item2)
                                        <li>{{date('h:i A',strtotime(@$item2->from_time))}} to {{date('h:i A',strtotime(@$item2->to_time))}}<span><a href="{{route('agent.manage.availability.remove',$item2->id)}}" onclick="return confirm('Are you want to remove this availability ?')"><img src="{{ URL::to('public/frontend/images/cross4.png')}}"></a></span></li>
                                        @endforeach
                                        {{-- <li>12:00 am to 12:30 pm  <span><a href="#"><img src="{{ URL::to('public/frontend/images/cross4.png')}}"></a></span></li>
                                        <li>2:00 am to 2:30 pm  <span><a href="#"><img src="{{ URL::to('public/frontend/images/cross4.png')}}"></a></span></li> --}}
                                    </ul>
                                </div>
                            </div>
                        </form>
                        <form action="{{route('agent.manage.availability.save')}}" method="POST" id="wednesdayform">
                            @csrf
                            <input type="hidden" value="3" name="day">
                            <div class="row new_rows no-gutters align-items-center">
                                <div class="col-md-2 col-sm-12">
                                    <div class="check-box new-av-check">
                                        {{-- <input type="checkbox" id="checkbox3" name="checkbox1" value="all"> --}}
                                        <label for="checkbox3" class="checklabeel">Wednesday : </label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form_box_area das_input">
                                        <label>From</label>
                                        <div class="dash-d">
                                              <input type="text" placeholder="Enter here.. " class="position-relative" name="fromtime" id="fromtime3"> <span class="over_llp1"><img src="{{ URL::to('public/frontend/images/clocks.png')}}" alt=""></span>
                                         </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form_box_area das_input">
                                        <label>To</label>
                                        <div class="dash-d">
                                              <input type="text" placeholder="Enter here.. " class="position-relative" name="totime"> <span class="over_llp1"><img src="{{ URL::to('public/frontend/images/clocks.png')}}" alt=""></span>
                                         </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-12">
                                    <div class="form_box_area">
                                        <label></label>
                                        {{-- <a href="#" class="adds_btn nesave">Add</a> --}}
                                        <input type="submit"class="adds_btn nesave" value="Add">
                                    </div>
                                </div>
                                <div class="col-md-10 offset-md-2">
                                    <ul class="list-showag">
                                        @foreach ($wednesday as $item3)
                                        <li>{{date('h:i A',strtotime(@$item3->from_time))}} to {{date('h:i A',strtotime(@$item3->to_time))}}<span><a href="{{route('agent.manage.availability.remove',$item3->id)}}" onclick="return confirm('Are you want to remove this availability ?')"><img src="{{ URL::to('public/frontend/images/cross4.png')}}"></a></span></li>
                                        @endforeach
                                    </ul>
                                </div>

                            </div>

                        </form>
                        <form action="{{route('agent.manage.availability.save')}}" method="POST" id="thursdayform">
                            @csrf
                            <input type="hidden" value="4" name="day">
                            <div class="row new_rows no-gutters align-items-center">
                                <div class="col-md-2 col-sm-12">
                                    <div class="check-box new-av-check">
                                        {{-- <input type="checkbox" id="checkbox4" name="checkbox1" value="all"> --}}
                                        <label for="checkbox4" class="checklabeel">Thursday : </label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form_box_area das_input">
                                        <label>From</label>
                                        <div class="dash-d">
                                              <input type="text" placeholder="Enter here.. " class="position-relative" name="fromtime" id="fromtime4"> <span class="over_llp1"><img src="{{ URL::to('public/frontend/images/clocks.png')}}" alt=""></span>
                                         </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form_box_area das_input">
                                        <label>To</label>
                                        <div class="dash-d">
                                              <input type="text" placeholder="Enter here.. " class="position-relative" name="totime"> <span class="over_llp1"><img src="{{ URL::to('public/frontend/images/clocks.png')}}" alt=""></span>
                                         </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-12">
                                    <div class="form_box_area">
                                        <label></label>
                                        {{-- <a href="#" class="adds_btn nesave">Add</a> --}}
                                        <input type="submit"class="adds_btn nesave" value="Add">
                                    </div>
                                </div>
                                <div class="col-md-10 offset-md-2">
                                    <ul class="list-showag">
                                        @foreach ($thursday as $item4)
                                        <li>{{date('h:i A',strtotime(@$item4->from_time))}} to {{date('h:i A',strtotime(@$item4->to_time))}}<span><a href="{{route('agent.manage.availability.remove',$item4->id)}}" onclick="return confirm('Are you want to remove this availability ?')"><img src="{{ URL::to('public/frontend/images/cross4.png')}}"></a></span></li>
                                        @endforeach
                                    </ul>
                                </div>

                            </div>

                        </form>
                        <form action="{{route('agent.manage.availability.save')}}" method="POST" id="fridayform" id="fromtime7">
                            @csrf
                            <input type="hidden" value="5" name="day">
                            <div class="row new_rows no-gutters align-items-center">
                                <div class="col-md-2 col-sm-12">
                                    <div class="check-box new-av-check">
                                        {{-- <input type="checkbox" id="checkbox5" name="checkbox1" value="all"> --}}
                                        <label for="checkbox5" class="checklabeel">Friday : </label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form_box_area das_input">
                                        <label>From</label>
                                        <div class="dash-d">
                                              <input type="text" placeholder="Enter here.. " class="position-relative" name="fromtime" id="fromtime5"> <span class="over_llp1"><img src="{{ URL::to('public/frontend/images/clocks.png')}}" alt=""></span>
                                         </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form_box_area das_input">
                                        <label>To</label>
                                        <div class="dash-d">
                                              <input type="text" placeholder="Enter here.. " class="position-relative" name="totime"> <span class="over_llp1"><img src="{{ URL::to('public/frontend/images/clocks.png')}}" alt=""></span>
                                         </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-12">
                                    <div class="form_box_area">
                                        <label></label>
                                        {{-- <a href="#" class="adds_btn nesave">Add</a> --}}
                                        <input type="submit"class="adds_btn nesave" value="Add">
                                    </div>
                                </div>
                                <div class="col-md-10 offset-md-2">
                                    <ul class="list-showag">
                                        @foreach ($friday as $item5)
                                        <li>{{date('h:i A',strtotime(@$item5->from_time))}} to {{date('h:i A',strtotime(@$item5->to_time))}}<span><a href="{{route('agent.manage.availability.remove',$item5->id)}}" onclick="return confirm('Are you want to remove this availability ?')"><img src="{{ URL::to('public/frontend/images/cross4.png')}}"></a></span></li>
                                        @endforeach
                                    </ul>
                                </div>

                            </div>
                        </form>
                        <form action="{{route('agent.manage.availability.save')}}" method="POST" id="saturdayform">
                            @csrf
                            <input type="hidden" value="6" name="day">
                            <div class="row new_rows no-gutters align-items-center">
                                <div class="col-md-2 col-sm-12">
                                    <div class="check-box new-av-check">
                                        {{-- <input type="checkbox" id="checkbox6" name="checkbox1" value="all"> --}}
                                        <label for="checkbox6" class="checklabeel">Saturday : </label>
                                    </div>

                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form_box_area das_input">
                                        <label>From</label>
                                        <div class="dash-d">
                                              <input type="text" placeholder="Enter here.. " class="position-relative" name="fromtime" id="fromtime6"> <span class="over_llp1"><img src="{{ URL::to('public/frontend/images/clocks.png')}}" alt=""></span>
                                         </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form_box_area das_input">
                                        <label>To</label>
                                        <div class="dash-d">
                                              <input type="text" placeholder="Enter here.. " class="position-relative" name="totime"> <span class="over_llp1"><img src="{{ URL::to('public/frontend/images/clocks.png')}}" alt=""></span>
                                         </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-12">
                                    <div class="form_box_area">
                                        <label></label>
                                        {{-- <a href="#" class="adds_btn nesave">Add</a> --}}
                                        <input type="submit"class="adds_btn nesave" value="Add">
                                    </div>
                                </div>
                                <div class="col-md-10 offset-md-2">
                                    <ul class="list-showag">
                                        @foreach ($saturday as $item6)
                                        <li>{{date('h:i A',strtotime(@$item6->from_time))}} to {{date('h:i A',strtotime(@$item6->to_time))}}<span><a href="{{route('agent.manage.availability.remove',$item6->id)}}" onclick="return confirm('Are you want to remove this availability ?')"><img src="{{ URL::to('public/frontend/images/cross4.png')}}"></a></span></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </form>
                        <form action="{{route('agent.manage.availability.save')}}" method="POST" id="sundayform">
                            @csrf
                            <input type="hidden" value="7" name="day">
                            <div class="row new_rows no-gutters align-items-center">
                                <div class="col-md-2 col-sm-12">
                                    <div class="check-box new-av-check">
                                        {{-- <input type="checkbox" id="checkbox7" name="checkbox7" value="all"> --}}
                                        <label for="checkbox5" class="checklabeel">Sunday : </label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form_box_area das_input">
                                        <label>From</label>
                                        <div class="dash-d">
                                              <input type="text" placeholder="Enter here.. " class="position-relative" name="fromtime" id="fromtime7"> <span class="over_llp1"><img src="{{ URL::to('public/frontend/images/clocks.png')}}" alt=""></span>
                                         </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="form_box_area das_input">
                                        <label>To</label>
                                        <div class="dash-d">
                                              <input type="text" placeholder="Enter here.. " class="position-relative" name="totime"> <span class="over_llp1"><img src="{{ URL::to('public/frontend/images/clocks.png')}}" alt=""></span>
                                         </div>
                                    </div>

                                </div>
                                <div class="col-md-2 col-sm-12">
                                    <div class="form_box_area">
                                        <label></label>
                                        {{-- <a href="#" class="adds_btn nesave">Add</a> --}}
                                        <input type="submit"class="adds_btn nesave" value="Add">
                                    </div>
                                </div>
                                <div class="col-md-10 offset-md-2">
                                    <ul class="list-showag">
                                        @foreach ($sunday as $item7)
                                        <li>{{date('h:i A',strtotime(@$item7->from_time))}} to {{date('h:i A',strtotime(@$item7->to_time))}}<span><a href="{{route('agent.manage.availability.remove',$item7->id)}}" onclick="return confirm('Are you want to remove this availability ?')"><img src="{{ URL::to('public/frontend/images/cross4.png')}}"></a></span></li>
                                        @endforeach
                                    </ul>
                                </div>

                            </div>
                        </form>
					</div>
				</div>

			</div>
		</div>

	</div>
</div>

@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
<!-- Time picek jas -->
<link rel='stylesheet' href='{{ URL::to('public/frontend/css/jquery-clockpicker.min.css')}}'>
<script src='https://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.js'></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
    var choices = ["00","15","30","45"];
    $('input[name=fromtime],input[name=totime]').timepicker({
        timeFormat: 'HH:mm',
        interval: 15,
        minTime: '0',
        startTime: '00:00',
        dynamic: false,
        dropdown: true,
        scrollbar: false
    });
// $("input[name=fromtime],input[name=totime]").clockpicker({
//     placement: 'bottom',
//     align: 'left',
//     autoclose: true,
//     default: 'now',
//     donetext: "Select",
//     init: function() {
//         console.log("colorpicker initiated");
//     },
//     beforeShow: function() {
//         console.log("before show");
//     },
//     afterShow: function() {
//         console.log("after show");
//     },
//     beforeHide: function() {
//         console.log("before hide");
//     },
//     afterHide: function() {
//         console.log("after hide");
//     },
//     beforeHourSelect: function() {
//         console.log("before hour selected");
//     },
//     afterHourSelect: function() {
//         console.log("after hour selected");
//     },
//     beforeDone: function() {
//         console.log("before done");
//     },
//     afterDone: function() {
//         console.log("after done");
//     }
// });

</script>
<script>
    $(document).ready(function(){
        $.validator.addMethod("timemin", function(value, element, min) {
            // var valuestart = $("select[name='timestart']").val();
            // var valuestop = $("select[name='timestop']").val();

            //create date format
            var timeStart = new Date("01/01/2007 " + min);
            var timeEnd = new Date("01/01/2007 " + value);

            var difference = timeEnd - timeStart;
            console.log(value);

            difference = difference / 60 / 60 / 1000;
            if(value>min && difference*60>=30){
                return true;
            }
            else if(value==''){
                return true;
            }
        }, "To Time 30 min geter from time");

        $.validator.addMethod("timeduration", function(value, element, min) {
            // var valuestart = $("select[name='timestart']").val();
            // var valuestop = $("select[name='timestop']").val();

            //create date format
            var timeStart = new Date("01/01/2007 " + min);
            var timeEnd = new Date("01/01/2007 " + value);

            var difference = timeEnd - timeStart;
            difference = difference / 60 / 1000;
            console.log(difference);
            if(difference % 2 === 0 ){
                return true;
            }
        }, "From time and To Time duration difference must be multiplier of 30 minutes");

        $('#mondayform').validate({
            rules: {
                fromtime:{
                    required:true
                },
                totime:{
                    required:true,
                    timemin:function(){
                        return $("#fromtime1").val();
                    },
                    timeduration:function(){
                        return $("#fromtime2").val();
                    }
                },
            },
            messages:{
                fromtime:{
                    required:"Select from time"
                },
                totime:{
                    required:"Select to time",
                },
            },
            ignore: [],
        });
        $('#tuesdayform').validate({
            rules: {
                fromtime:{
                    required:true
                },
                totime:{
                    required:true,
                    timemin:function(){
                        return $("#fromtime2").val();
                    },
                    timeduration:function(){
                        return $("#fromtime2").val();
                    }
                },
            },
            messages:{
                fromtime:{
                    required:"Select from time"
                },
                totime:{
                    required:"Select to time",
                },
            },
            ignore: [],
        });
        $('#wednesdayform').validate({
            rules: {
                fromtime:{
                    required:true
                },
                totime:{
                    required:true,
                    timemin:function(){
                        return $("#fromtime3").val();
                    },
                    timeduration:function(){
                        return $("#fromtime2").val();
                    }
                },
            },
            messages:{
                fromtime:{
                    required:"Select from time"
                },
                totime:{
                    required:"Select to time",
                },
            },
            ignore: [],
        });
        $('#thursdayform').validate({
            rules: {
                fromtime:{
                    required:true
                },
                totime:{
                    required:true,
                    timemin:function(){
                        return $("#fromtime4").val();
                    },
                    timeduration:function(){
                        return $("#fromtime2").val();
                    }
                },
            },
            messages:{
                fromtime:{
                    required:"Select from time"
                },
                totime:{
                    required:"Select to time",
                },
            },
            ignore: [],
        });
        $('#fridayform').validate({
            rules: {
                fromtime:{
                    required:true
                },
                totime:{
                    required:true,
                    timemin:function(){
                        return $("#fromtime5").val();
                    },
                    timeduration:function(){
                        return $("#fromtime2").val();
                    }
                },
            },
            messages:{
                fromtime:{
                    required:"Select from time"
                },
                totime:{
                    required:"Select to time",
                },
            },
            ignore: [],
        });
        $('#saturdayform').validate({
            rules: {
                fromtime:{
                    required:true
                },
                totime:{
                    required:true,
                    timemin:function(){
                        return $("#fromtime6").val();
                    },
                    timeduration:function(){
                        return $("#fromtime2").val();
                    }
                },
            },
            messages:{
                fromtime:{
                    required:"Select from time"
                },
                totime:{
                    required:"Select to time",
                },
            },
            ignore: [],
        });
        $('#sundayform').validate({
            rules: {
                fromtime:{
                    required:true
                },
                totime:{
                    required:true,
                    timemin:function(){
                        return $("#fromtime7").val();
                    },
                    timeduration:function(){
                        return $("#fromtime2").val();
                    }
                },
            },
            messages:{
                fromtime:{
                    required:"Select from time"
                },
                totime:{
                    required:"Select to time",
                },
            },
            ignore: [],
        });
    });
</script>
@endsection

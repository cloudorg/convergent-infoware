@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Contact Us </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<div class="haeder-padding"></div>

    <!-----filter--------->

    <div class="priv-sec">

        <div class="container">

            <div class="row">

                

                    <div class="col-lg-10 col-xl-9 col-md-11 mx-auto">

                        <div class="section-heading2 text-center">

                            <h2>Terms and Condition</h2>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed lorem sapien, auctor in justo id, dignissim dipiscing elit sed lorem sapien</p>

                        </div>

                    </div>

                <div class="col-12">

                    <div class="privacy-content mb-60">

                    <h3>1. Welcome to company Terms and Condition</h3>

                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

                    <h3>2. Lorem Ipsum is simply dummy text</h3>

                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Ipsum.</p>

                    <h3>3. Lorem Ipsum is simply dummy text</h3>

                    <p> It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem </p>

                    <h3>4. Lorem Ipsum is simply dummy text</h3>

                    <ol>

                      <li>Responding to your questions and concerns;</li>

                      <li>Sending you administrative messages and information, including messages from instructors and teaching assistants, notifications about changes to our Service, and updates to our agreements;</li>

                      <li>Sending push notifications to your wireless device to provide updates and other relevant messages (which you can manage from the “options” or “settings” page of the mobile app);</li>

                    </ol>

                    <h3>5. Lorem Ipsum is simply dummy text</h3>

                    <p> It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>

                    <ul>

                      <li> It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem </li>

                      <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Ipsum.</li>

                      <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Ipsum.</li>

                      <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Ipsum.</li>

                    </ul>

                    <h3>6. Lorem Ipsum is simply dummy text</h3>

                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Ipsum.</p>

                  </div>

                </div>

            </div>

        </div>

    </div>

@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
@include('includes.toaster')
<script>
    $(document).ready(function(){
        $('#rent_link').click(function(){
            $('#rent').click();
        })
        $('#buy_link').click(function(){
            $('#buy').click();
        })
    })
</script>
<script>
    function search_result_check(that) {
        var $this = that;
        var name = $($this).val();
        console.log($($this).val());
        $.ajax({
            type: "POST",
            url:"{{route('get.locality.available')}}",
            data: {
                'name': name,
                '_token':'{{@csrf_token()}}'
            },
            success: function(data) {
              if(data){
                console.log(data);
                $('#serch_result').show();
                $('#serch_result').fadeIn();
                $('#serch_result').last().html(data);

              }
              else
              {
                $('#serch_result').children().remove();
              }
            }
        });
    }
    function add_searchbar(that) {
        var $this = that;
        var name = $($this).text();
        console.log(name);
        $('#location').val(name);
        $($this).remove();
        $('#serch_result').children().remove();
        $('#serch_result').hide();
    }
</script>
@endsection

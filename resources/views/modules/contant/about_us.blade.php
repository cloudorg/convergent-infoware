@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Home </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<div class="haeder-padding"></div>

    <!-----filter--------->



    <div class="about-section">

        <div class="container">

            <div class="row">

                <div class="col-lg-12">

                    <div class="breadcrumbs-inner">

                        <h1 class="page-title">About Us</h1>

                        <div class="breadcrumbs-list">

                            <ul>

                                <li><a href="index.html"><span class=""><i class="icofont-home"></i></span> Home</a></li>

                                <li>About Us</li>

                            </ul>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    

    <div class="about-inner">

        <div class="container">

            <div class="row">

                <div class="col-md-6 align-self-center">

                    <div class="about-us-img-wrap about-img-left">

                        <img src="{{ asset('public/frontend/images/about1.png') }}" alt="About Us Image">

                        <div class="about-us-img-info about-us-img-info-2 about-us-img-info-3">

                            

                            <div class="">

                                <img src="{{ asset('public/frontend/images/about2.png') }}" alt="video popup bg image">

                                

                            </div>

                        </div>

                    </div>

                </div>



                <div class="col-md-6 align-self-center">

                    <div class="about-us-info-wrap">

                        

                        <h1 class="section-title">The Leading Real Estate

                                Rental Marketplace<span>.</span></h1>

                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passage.</p>

                        <p> It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passage.</p>



                        <ul class="list-item-half clearfix">

                            <li>

                                <i class="icofont-building-alt"></i>

                                Smart Home Design

                            </li>

                            <li>

                                <i class="icofont-beach"></i>

                                Beautiful Scene Around

                            </li>

                            <li>

                                <i class="icofont-cycling-alt"></i>

                                Exceptional Lifestyle

                            </li>

                            <li>

                                <i class="icofont-cop-badge"></i>

                                Complete 24/7 Security

                            </li>

                        </ul>



                        <a href="javascript:;" class="theme-btn">Contact Us</a>

                    </div>

                </div>

            </div>

        </div>

    </div>



<section class="property-sec py-50">

    <div class="container">

        <div class="row">

            <div class="col-lg-10 col-xl-9 col-md-11 mx-auto">

                <div class="section-heading2 text-center">

                    <h2>Our Vission And Mission</h2>

                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed lorem sapien, auctor in justo id, dignissim dipiscing elit sed lorem sapien</p>

                </div>

            </div>

            <div class="col-sm-6 col-lg-4">

                <div class="misson-box">

                    <img src="{{ asset('public/frontend/images/miss.png') }}">

                    <h2>Our Mission</h2>

                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took.</p>

                </div>

            </div>

            <div class="col-sm-6 col-lg-4">

                <div class="misson-box">

                    <img src="{{ asset('public/frontend/images/visa.png') }}">

                    <h2>Our Vision</h2>

                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took.</p>

                </div>

            </div>

            <div class="col-sm-6 col-lg-4">

                <div class="misson-box">

                    <img src="{{ asset('public/frontend/images/vis.png') }}">

                    <h2>Our Goal</h2>

                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took.</p>

                </div>

            </div>

        </div>

    </div>



    

</section>

@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
@include('includes.toaster')
<script>
    $(document).ready(function(){
        $('#rent_link').click(function(){
            $('#rent').click();
        })
        $('#buy_link').click(function(){
            $('#buy').click();
        })
    })
</script>
<script>
    function search_result_check(that) {
        var $this = that;
        var name = $($this).val();
        console.log($($this).val());
        $.ajax({
            type: "POST",
            url:"{{route('get.locality.available')}}",
            data: {
                'name': name,
                '_token':'{{@csrf_token()}}'
            },
            success: function(data) {
              if(data){
                console.log(data);
                $('#serch_result').show();
                $('#serch_result').fadeIn();
                $('#serch_result').last().html(data);

              }
              else
              {
                $('#serch_result').children().remove();
              }
            }
        });
    }
    function add_searchbar(that) {
        var $this = that;
        var name = $($this).text();
        console.log(name);
        $('#location').val(name);
        $($this).remove();
        $('#serch_result').children().remove();
        $('#serch_result').hide();
    }
</script>
@endsection

@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Contact Us </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<div class="haeder-padding"></div>

    <!-----filter--------->



    <div class="about-section">

        <div class="container">

            <div class="row">

                <div class="col-lg-12">

                    <div class="breadcrumbs-inner">

                        <h1 class="page-title">Frequently Asked Questions</h1>

                        <div class="breadcrumbs-list">

                            <ul>

                                <li><a href="index.html"><span class=""><i class="icofont-home"></i></span> Home</a></li>

                                <li>FAQ</li>

                            </ul>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    

    <div class="faqs-sec ">

        <div class="container">

            <div class="row">

                <div class="col-lg-10 col-xl-9 col-md-11 mx-auto">

                    <div class="section-heading2 text-center">

                        <h2>Frequently Asked Questions</h2>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed lorem sapien, auctor in justo id, dignissim dipiscing elit sed lorem sapien</p>

                    </div>

                </div>



                <div class="col-lg-10 col-xl-9 col-md-11 mx-auto">

                    <div class="faq-atbs">

                            <ul class="nav nav-tabs ">

                                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#today">User </a></li>

                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#home">Agents  </a></li>

                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#menu4">Service Pro</a></li>

                            </ul>

                        </div>

                        <div class="tab-content">

                            <div class="tab-pane active" id="today">

                                <div class="accordian-faq">

                                    <div class="accordion" id="faq">

                                                <div class="card">

                                                    <div class="card-header" id="faqhead1">

                                                        <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq1" aria-expanded="true" aria-controls="faq1">

                                                            <p><span>1. </span> What is Ri Virtual</p>

                                                        </a>

                                                    </div>

                                                    <div id="faq1" class="collapse" aria-labelledby="faqhead1" data-parent="#faq">

                                                        <div class="card-body">

                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                        </div>

                                                    </div>

                                                </div>





                                                <div class="card">

                                                    <div class="card-header" id="faqhead2">

                                                        <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq2" aria-expanded="true" aria-controls="faq2">

                                                            <p><span>2. </span> How to work?</p>

                                                        </a>

                                                    </div>

                                                    <div id="faq2" class="collapse" aria-labelledby="faqhead2" data-parent="#faq">

                                                        <div class="card-body">

                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                        </div>

                                                    </div>

                                                </div>



                                                <div class="card">

                                                    <div class="card-header" id="faqhead3">

                                                        <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq3" aria-expanded="true" aria-controls="faq3">

                                                            <p><span>3. </span> How to hire a pro?</p>

                                                        </a>

                                                    </div>

                                                    <div id="faq3" class="collapse" aria-labelledby="faqhead3" data-parent="#faq">

                                                        <div class="card-body">

                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                        </div>

                                                    </div>

                                                </div>



                                                <div class="card">

                                                    <div class="card-header" id="faqhead4">

                                                        <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq4" aria-expanded="true" aria-controls="faq4">

                                                            <p><span>4. </span> Dummy questions here?</p>

                                                        </a>

                                                    </div>

                                                    <div id="faq4" class="collapse" aria-labelledby="faqhead4" data-parent="#faq">

                                                        <div class="card-body">

                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                        </div>

                                                    </div>

                                                </div>



                                                <div class="card">

                                                    <div class="card-header" id="faqhead5">

                                                        <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq5" aria-expanded="true" aria-controls="faq5">

                                                            <p><span>5. </span>Dummy questions here go?</p>

                                                        </a>

                                                    </div>

                                                    <div id="faq5" class="collapse" aria-labelledby="faqhead5" data-parent="#faq">

                                                        <div class="card-body">

                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                        </div>

                                                    </div>

                                                </div>





                                                <div class="card">

                                                    <div class="card-header" id="faqhead10">

                                                        <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq10" aria-expanded="true" aria-controls="faq10">

                                                            <p><span>6. </span> simple questions here?</p>

                                                        </a>

                                                    </div>

                                                    <div id="faq10" class="collapse" aria-labelledby="faqhead10" data-parent="#faq">

                                                        <div class="card-body">

                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                        </div>

                                                    </div>

                                                </div>



                                                <div class="card">

                                                    <div class="card-header" id="faqhead6">

                                                        <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq6" aria-expanded="true" aria-controls="faq6">

                                                            <p><span>7. </span> How to contact a agent?</p>

                                                        </a>

                                                    </div>

                                                    <div id="faq6" class="collapse" aria-labelledby="faqhead6" data-parent="#faq">

                                                        <div class="card-body">

                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                        </div>

                                                    </div>

                                                </div>



                                                <div class="card">

                                                    <div class="card-header" id="faqhead7">

                                                        <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq7" aria-expanded="true" aria-controls="faq7">

                                                            <p><span>8. </span> Dummy questions here?</p>

                                                        </a>

                                                    </div>

                                                    <div id="faq7" class="collapse" aria-labelledby="faqhead7" data-parent="#faq">

                                                        <div class="card-body">

                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                        </div>

                                                    </div>

                                                </div>



                                                <div class="card">

                                                    <div class="card-header" id="faqhead8">

                                                        <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq8" aria-expanded="true" aria-controls="faq8">

                                                            <p><span>9. </span> how  Rivirtual work?</p>

                                                        </a>

                                                    </div>

                                                    <div id="faq8" class="collapse" aria-labelledby="faqhead8" data-parent="#faq">

                                                        <div class="card-body">

                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                        </div>

                                                    </div>

                                                </div>



                                                <div class="card">

                                                    <div class="card-header" id="faqhead9">

                                                        <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq9" aria-expanded="true" aria-controls="faq9">

                                                            <p><span>10. </span> Dummy questions here?</p>

                                                        </a>

                                                    </div>

                                                    <div id="faq9" class="collapse" aria-labelledby="faqhead9" data-parent="#faq">

                                                        <div class="card-body">

                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                        </div>

                                                    </div>

                                                </div>

                                    </div>

                                </div>

                            </div>



                            <div class="tab-pane " id="home">

                                <div class="accordian-faq">

                                    <div class="accordion" id="accordionExample">

                                                <div class="card">

                                                    <div class="card-header" id="faqhead11">

                                                        <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq11" aria-expanded="true" aria-controls="faq11">

                                                            <p><span>1. </span> What is Ri Virtual</p>

                                                        </a>

                                                    </div>

                                                    <div id="faq11" class="collapse" aria-labelledby="faqhead11" data-parent="#accordionExample">

                                                        <div class="card-body">

                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                        </div>

                                                    </div>

                                                </div>



                                                <div class="card">

                                                    <div class="card-header" id="faqhead12">

                                                        <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq12" aria-expanded="true" aria-controls="faq12">

                                                            <p><span>2. </span> How to work?</p>

                                                        </a>

                                                    </div>

                                                    <div id="faq12" class="collapse" aria-labelledby="faqhead12" data-parent="#accordionExample">

                                                        <div class="card-body">

                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                        </div>

                                                    </div>

                                                </div>



                                                <div class="card">

                                                    <div class="card-header" id="faqhead13">

                                                        <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq13" aria-expanded="true" aria-controls="faq13">

                                                            <p><span>3. </span> How to hire a pro?</p>

                                                        </a>

                                                    </div>

                                                    <div id="faq13" class="collapse" aria-labelledby="faqhead13" data-parent="#accordionExample">

                                                        <div class="card-body">

                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                        </div>

                                                    </div>

                                                </div>



                                                <div class="card">

                                                    <div class="card-header" id="faqhead14">

                                                        <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq14" aria-expanded="true" aria-controls="faq14">

                                                            <p><span>4. </span> Dummy questions here?</p>

                                                        </a>

                                                    </div>

                                                    <div id="faq14" class="collapse" aria-labelledby="faqhead14" data-parent="#accordionExample">

                                                        <div class="card-body">

                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                        </div>

                                                    </div>

                                                </div>



                                                <div class="card">

                                                    <div class="card-header" id="faqhead15">

                                                        <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq15" aria-expanded="true" aria-controls="faq15">

                                                            <p><span>5. </span>Dummy questions here go?</p>

                                                        </a>

                                                    </div>

                                                    <div id="faq15" class="collapse" aria-labelledby="faqhead15" data-parent="#accordionExample">

                                                        <div class="card-body">

                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                        </div>

                                                    </div>

                                                </div>





                                                <div class="card">

                                                    <div class="card-header" id="faqhead16">

                                                        <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq16" aria-expanded="true" aria-controls="faq16">

                                                            <p><span>6. </span> simple questions here?</p>

                                                        </a>

                                                    </div>

                                                    <div id="faq16" class="collapse" aria-labelledby="faqhead16" data-parent="#accordionExample">

                                                        <div class="card-body">

                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                        </div>

                                                    </div>

                                                </div>

                                    </div>

                                </div>

                            </div>



                            <div class="tab-pane " id="menu4">

                                <div class="accordian-faq">

                                    <div class="accordion" id="accordionExampletwo">

                                                <div class="card">

                                                    <div class="card-header" id="faqhead11">

                                                        <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq21" aria-expanded="true" aria-controls="faq21">

                                                            <p><span>1. </span> What is Ri Virtual</p>

                                                        </a>

                                                    </div>

                                                    <div id="faq21" class="collapse" aria-labelledby="faqhead21" data-parent="#accordionExampletwo">

                                                        <div class="card-body">

                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                        </div>

                                                    </div>

                                                </div>



                                                <div class="card">

                                                    <div class="card-header" id="faqhead22">

                                                        <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq22" aria-expanded="true" aria-controls="faq22">

                                                            <p><span>2. </span> How to Signup?</p>

                                                        </a>

                                                    </div>

                                                    <div id="faq22" class="collapse" aria-labelledby="faqhead22" data-parent="#accordionExampletwo">

                                                        <div class="card-body">

                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                        </div>

                                                    </div>

                                                </div>



                                                <div class="card">

                                                    <div class="card-header" id="faqhead23">

                                                        <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq23" aria-expanded="true" aria-controls="faq23">

                                                            <p><span>3. </span> How to hire a pro?</p>

                                                        </a>

                                                    </div>

                                                    <div id="faq23" class="collapse" aria-labelledby="faqhead23" data-parent="#accordionExampletwo">

                                                        <div class="card-body">

                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                        </div>

                                                    </div>

                                                </div>



                                                <div class="card">

                                                    <div class="card-header" id="faqhead24">

                                                        <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq24" aria-expanded="true" aria-controls="faq24">

                                                            <p><span>4. </span> Dummy questions here?</p>

                                                        </a>

                                                    </div>

                                                    <div id="faq24" class="collapse" aria-labelledby="faqhead24" data-parent="#accordionExampletwo">

                                                        <div class="card-body">

                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                        </div>

                                                    </div>

                                                </div>



                                                <div class="card">

                                                    <div class="card-header" id="faqhead15">

                                                        <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq25" aria-expanded="true" aria-controls="faq25">

                                                            <p><span>5. </span>Dummy questions here go?</p>

                                                        </a>

                                                    </div>

                                                    <div id="faq25" class="collapse" aria-labelledby="faqhead25" data-parent="#accordionExampletwo">

                                                        <div class="card-body">

                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                        </div>

                                                    </div>

                                                </div>





                                                <div class="card">

                                                    <div class="card-header" id="faqhead26">

                                                        <a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq26" aria-expanded="true" aria-controls="faq26">

                                                            <p><span>6. </span> simple questions here?</p>

                                                        </a>

                                                    </div>

                                                    <div id="faq26" class="collapse" aria-labelledby="faqhead26" data-parent="#accordionExampletwo">

                                                        <div class="card-body">

                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                        </div>

                                                    </div>

                                                </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                </div>



            </div>

        </div>

@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')
@include('includes.toaster')
<script>
    $(document).ready(function(){
        $('#rent_link').click(function(){
            $('#rent').click();
        })
        $('#buy_link').click(function(){
            $('#buy').click();
        })
    })
</script>
<script>
    function search_result_check(that) {
        var $this = that;
        var name = $($this).val();
        console.log($($this).val());
        $.ajax({
            type: "POST",
            url:"{{route('get.locality.available')}}",
            data: {
                'name': name,
                '_token':'{{@csrf_token()}}'
            },
            success: function(data) {
              if(data){
                console.log(data);
                $('#serch_result').show();
                $('#serch_result').fadeIn();
                $('#serch_result').last().html(data);

              }
              else
              {
                $('#serch_result').children().remove();
              }
            }
        });
    }
    function add_searchbar(that) {
        var $this = that;
        var name = $($this).text();
        console.log(name);
        $('#location').val(name);
        $($this).remove();
        $('#serch_result').children().remove();
        $('#serch_result').hide();
    }
</script>
@endsection

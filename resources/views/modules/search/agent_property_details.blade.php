@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Home </title>
@endsection




@section('header')
@include('includes.header')
@endsection


@section('content')
<!----header--->
<div class="haeder-padding"></div>
<div class="property_details_pg">
	<div class="container con_det_pg">
		<div class="property_details_inr">
			<div class="property_left">
				<div class="property_top">
					<div class="property_top_le">
						<h1>{{@$propertyDetails->name}}</h1>
						<ul>
							<li><b><a href="#url" class="preowned">Preowned</a></b></li>
							<li><img src="{{ URL::asset('public/frontend/images/property_icon1.png')}}" alt="">Avenue De-Luserna 1203</li>
							<li><img src="{{ URL::asset('public/frontend/images/property_icon2.png')}}" alt="">
                                @if(@$propertyDetails->property_type=='F')
                                Flat
                                @elseif(@$propertyDetails->property_type=='H')
                                House
                                @elseif(@$propertyDetails->property_type=='L')
                                Land
                                @elseif(@$propertyDetails->property_type=='R')
                                Residential
                                @elseif(@$propertyDetails->property_type=='O')
                                Office
                                @endif
                            </li>
							<li><img src="{{ URL::asset('public/frontend/images/property_icon3.png')}}" alt="">
                                @if(@$propertyDetails->construction_status=='RM')
                                Ready to move
                                @elseif(@$propertyDetails->construction_status=='UC')
                                Under Construction
                                @elseif(@$propertyDetails->construction_status=='PL')
                                Pre Launch
                                @endif
                            </li>
						</ul>
					</div>
					<div class="property_top_rig">
                        @if(@$propertyDetails->property_for=='B')
						<strong class="for_sale">For Sale</strong>
                        @elseif(@$propertyDetails->property_for=='R')
						<strong class="for_sale">For Rent</strong>
                        @endif
						<span>INR <b>{{ number_format(@$propertyDetails->budget_range_from, 0, '.', ',') }} - {{ number_format(@$propertyDetails->budget_range_to, 0, '.', ',') }}</b></span>
					</div>
				</div>
				<div class="mangi_glla_sec">
					<div class="ninja_trail_remove">
						<div class="left_slider_area">
							<div id="thumbnail-slider" style="float:left;">
								<div class="inner">
									<ul>
                                        @foreach ($propertyImageExterior as $image1)
										<li><a class="thumb" href="{{ URL::asset('storage/app/public/property_image')}}/{{$image1->image}}"></a></li>
                                        @endforeach
                                        @foreach ($propertyImageInterior as $image2)
										<li><a class="thumb" href="{{ URL::asset('storage/app/public/property_image')}}/{{$image2->image}}"></a></li>
                                        @endforeach
                                        @foreach ($propertyImageFloor as $image3)
										<li><a class="thumb" href="{{ URL::asset('storage/app/public/property_image')}}/{{$image3->image}}"></a></li>
                                        @endforeach
										{{-- <li><a class="thumb" href="{{ URL::asset('public/frontend/images/property_img22.jpg')}}"></a></li>
										<li><a class="thumb" href="{{ URL::asset('public/frontend/images/property_img33.jpg')}}"></a></li>
                                        <li><a class="thumb" href="{{ URL::asset('public/frontend/images/property_img44.jpg')}}"></a></li>
										<li><a class="thumb" href="{{ URL::asset('public/frontend/images/property_img55.jpg')}}"></a></li>
										<li><a class="thumb" href="{{ URL::asset('public/frontend/images/property_img66.jpg')}}"></a></li> --}}
									</ul>
								</div>
							</div>
							<div id="ninja-slider" style="float:left;">
								<div class="slider-inner">
									<ul>
                                        @foreach ($propertyImageExterior as $image1)
										<li><a class="ns-img" href="{{ URL::asset('storage/app/public/property_image')}}/{{$image1->image}}"></a></li>
                                        @endforeach
                                        @foreach ($propertyImageInterior as $image2)
										<li><a class="ns-img" href="{{ URL::asset('storage/app/public/property_image')}}/{{$image2->image}}"></a></li>
                                        @endforeach
                                        @foreach ($propertyImageFloor as $image3)
										<li><a class="ns-img" href="{{ URL::asset('storage/app/public/property_image')}}/{{$image3->image}}"></a></li>
                                        @endforeach
										{{-- <li><a class="ns-img" href="{{ URL::asset('public/frontend/images/list3.png')}}"></a></li>
										<li><a class="ns-img" href="{{ URL::asset('public/frontend/images/list4.png')}}"></a></li>
                                        <li><a class="ns-img" href="{{ URL::asset('public/frontend/images/list5.png')}}"></a></li>
										<li><a class="ns-img" href="{{ URL::asset('public/frontend/images/list6.png')}}"></a></li>
										<li><a class="ns-img" href="{{ URL::asset('public/frontend/images/list7.png')}}"></a></li> --}}
									</ul>
                                    <a class="hhert_icon"><i class="fa fa-heart" aria-hidden="true"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="hom_det_sec">
					<div class="hom_det_bx hom_det_bx1">
						<div class="hom_det_it">
                            <h4 class="hom_det_h4">Home details</h4>
                            <ul class="hom_det_ul">
                                <li><em><img src="{{ URL::asset('public/frontend/images/hom_icon1.png')}}" alt=""></em>Carpet area : <span>{{@$propertyDetails->carpet_area}} Sf</span></li>
							 	<li><em><img src="{{ URL::asset('public/frontend/images/hom_icon2.png')}}" alt=""></em>Year Built : <span>2018</span></li>
							 	<li><em><img src="{{ URL::asset('public/frontend/images/hom_icon1.png')}}" alt=""></em>Rooms : <span>{{@$propertyDetails->no_of_bedrooms}}</span></li>
							 	<li><em><img src="{{ URL::asset('public/frontend/images/hom_icon3.png')}}" alt=""></em>Bathrooms : <span>{{@$propertyDetails->bathroom}}</span></li>
							 	<li><em><img src="{{ URL::asset('public/frontend/images/hom_icon4.png')}}" alt=""></em>Sqft : <span>{{@$propertyDetails->area}}</span></li>
							 </ul>
						</div>
						<div class="facil_panel">
                            <h4 class="hom_det_h4">Facilities / Amenities </h4>
                            <ul class="facil_ad">
                                @foreach ($propertyFacilitiesAmenities as $facilitiesAmenities)
                                <li>{{@$facilitiesAmenities->facilitiesAmenitiesName->name}}</li>
                                @endforeach
							 </ul>
						</div>
						<div class="description_panel">
							 <h4 class="hom_det_h4">Description</h4>
							 <p>{{@$propertyDetails->description}}</p>
						</div>
						<div class="locti_panel">
                            <h4 class="hom_det_h4">Location</h4>
                            <span><img src="{{ URL::asset('public/frontend/images/property_icon1.png')}}" alt="">Kolkata, India</span>


                            <iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCRZMuXnvy3FntdZUehn0IHLpjQm55Tz1E&q={{$propertyDetails->address}}&center={{$propertyDetails->address_lat}},{{$propertyDetails->address_long}}&zoom=15" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
							 {{-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2760.87548680098!2d6.123337414939436!3d46.21293189119253!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478c64c6ad5d9af5%3A0x4aeae57b59d5b7d6!2sMedical%20Center%20De%20La%20Servette!5e0!3m2!1sen!2sin!4v1633602437490!5m2!1sen!2sin" style="border:0;" allowfullscreen="" loading="lazy"></iframe> --}}
						</div>
					</div>
					<div class="hom_det_bx revew_sec">
						<h4 class="hom_det_h4">Reviews</h4>
						<div class="revew_panel">
							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										<span><img src="{{ URL::asset('public/frontend/images/revew1.png')}}" alt=""></span>
										<div class="media-body">
											<h4>Rabin Chatterjee</h4>
											<h5>This is a simply dummy review heading</h5>
										</div>
									</div>
									<div class="star_bx">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star gray"></i></li>
										</ul>
										<strong><img src="{{ URL::asset('public/frontend/images/calen.png')}}" alt=""> 20th Sept, 2021</strong>
									</div>
								</div>
								<p>Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiat dui. Lorem ipsum dolor sit ametconsectetur adipiscing elit  iaculis, magna. <span class="moretext moretext_all">Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiat dui. Lorem ipsum dolor sit ametconsectetur adipiscing elit  iaculis, </span> <a href="#url" class="moreless-button more_cc">more +</a> </p>
							</div>
							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										<span><img src="{{ URL::asset('public/frontend/images/revew2.png')}}" alt=""></span>
										<div class="media-body">
											<h4>Saikat Roy</h4>
											<h5>Reviews title text show here</h5>
										</div>
									</div>
									<div class="star_bx">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star gray"></i></li>
										</ul>
										<strong><img src="{{ URL::asset('public/frontend/images/calen.png')}}" alt=""> 20th Sept, 2021</strong>
									</div>
								</div>
								<p>Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiat dui. Lorem ipsum dolor sit ametconsectetur.</p>
							</div>
							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										<span><img src="{{ URL::asset('public/frontend/images/revew3.png')}}" alt=""></span>
										<div class="media-body">
											<h4>Abhijheet Manna</h4>
											<h5>Simply dummy review heading</h5>
										</div>
									</div>
									<div class="star_bx">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star gray"></i></li>
											<li><i class="fa fa-star gray"></i></li>
											<li><i class="fa fa-star gray"></i></li>
										</ul>
										<strong><img src="{{ URL::asset('public/frontend/images/calen.png')}}" alt=""> 20th Sept, 2021</strong>
									</div>
								</div>
								<p>Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus.</p>
							</div>
							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										<span><img src="{{ URL::asset('public/frontend/images/revew4.png')}}" alt=""></span>
										<div class="media-body">
											<h4>Saikat Roy</h4>
											<h5>Reviews title text show here</h5>
										</div>
									</div>
									<div class="star_bx">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star gray"></i></li>
											<li><i class="fa fa-star gray"></i></li>
											<li><i class="fa fa-star gray"></i></li>
										</ul>
										<strong><img src="{{ URL::asset('public/frontend/images/calen.png')}}" alt=""> 20th Sept, 2021</strong>
									</div>
								</div>
								<p>Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiat dui. Lorem ipsum dolor sit ametconsectetur adipiscing elit  iaculis, magna. <span class="moretext1 moretext_all">Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiat dui. Lorem ipsum dolor sit ametconsectetur adipiscing elit  iaculis, </span>  <a href="#url" class="moreless-button1 more_cc">more +</a> </p>
							</div>
							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										<span><img src="{{ URL::asset('public/frontend/images/revew5.png')}}" alt=""></span>
										<div class="media-body">
											<h4>Abhijheet Manna</h4>
											<h5>This is a simply dummy review heading</h5>
										</div>
									</div>
									<div class="star_bx">
										<ul>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
											<li><i class="fa fa-star"></i></li>
										</ul>
										<strong><img src="{{ URL::asset('public/frontend/images/calen.png')}}" alt=""> 20th Sept, 2021</strong>
									</div>
								</div>
								<p>Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiasapien neque sodales lacus auctor the pretium nibhfeugia ametconsectetur.</p>
							</div>
							<div class="moretext_all moretext2">
								<div class="revew_item">
									<div class="revew_top">
										<div class="media">
											<span><img src="{{ URL::asset('public/frontend/images/revew3.png')}}" alt=""></span>
											<div class="media-body">
												<h4>Abhijheet Manna</h4>
												<h5>Simply dummy review heading</h5>
											</div>
										</div>
										<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star gray"></i></li>
												<li><i class="fa fa-star gray"></i></li>
												<li><i class="fa fa-star gray"></i></li>
											</ul>
											<strong><img src="{{ URL::asset('public/frontend/images/calen.png')}}" alt=""> 20th Sept, 2021</strong>
										</div>
									</div>
									<p>Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus.</p>
								</div>
								<div class="revew_item">
									<div class="revew_top">
										<div class="media">
											<span><img src="{{ URL::asset('public/frontend/images/revew4.png')}}" alt=""></span>
											<div class="media-body">
												<h4>Saikat Roy</h4>
												<h5>Reviews title text show here</h5>
											</div>
										</div>
										<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star gray"></i></li>
												<li><i class="fa fa-star gray"></i></li>
												<li><i class="fa fa-star gray"></i></li>
											</ul>
											<strong><img src="{{ URL::asset('public/frontend/images/calen.png')}}" alt=""> 20th Sept, 2021</strong>
										</div>
									</div>
									<p>Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiat dui. Lorem ipsum dolor sit ametconsectetur adipiscing elit  iaculis, magna. <span class="moretext1 moretext_all">Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiat dui. Lorem ipsum dolor sit ametconsectetur adipiscing elit  iaculis, </span>  <a href="#url" class="moreless-button1 more_cc">more +</a> </p>
								</div>
								<div class="revew_item">
									<div class="revew_top">
										<div class="media">
											<span><img src="{{ URL::asset('public/frontend/images/revew5.png')}}" alt=""></span>
											<div class="media-body">
												<h4>Abhijheet Manna</h4>
												<h5>This is a simply dummy review heading</h5>
											</div>
										</div>
										<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
											</ul>
											<strong><img src="{{ URL::asset('public/frontend/images/calen.png')}}" alt=""> 20th Sept, 2021</strong>
										</div>
									</div>
									<p>Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiasapien neque sodales lacus auctor the pretium nibhfeugia ametconsectetur.</p>
								</div>
							</div>
							<a href="#url" class="see_cc moreless-button2">See all review +</a>
						</div>
					</div>
				</div>
			</div>
			<div class="property_right">
				<div class="property_ri_inr">
					<div class="share_bx">
						<span><i class="fa fa-share"></i> Share this property by using these</span>
						<ul>
							<li><a href="#" target="_blank"><img src="{{ URL::asset('public/frontend/images/share1.png')}}" alt=""></a></li>
							<li><a href="#" target="_blank"><img src="{{ URL::asset('public/frontend/images/share2.png')}}" alt=""></a></li>
							<li><a href="#" target="_blank"><img src="{{ URL::asset('public/frontend/images/share3.png')}}" alt=""></a></li>
							<li><a href="#" target="_blank"><img src="{{ URL::asset('public/frontend/images/share4.png')}}" alt=""></a></li>
							<li><a href="#" target="_blank"><img src="{{ URL::asset('public/frontend/images/share5.png')}}" alt=""></a></li>
						</ul>
					</div>
					<div class="property_ri_itm">
						<div class="media pro_media">
							<span>
                                @if(@$propertyDetails->propertyUser->profile_pic != null)
                                <img src="{{ URL::to('storage/app/public/profile_picture')}}/{{@$propertyDetails->propertyUser->profile_pic}}" alt="">
                                @else
                                <img src="{{ URL::to('public/frontend/images/agent1.png')}}" alt="">
                                @endif
                                {{-- <img src="{{ URL::asset('public/frontend/images/pro_img.jpg')}}" alt=""> --}}
                            </span>
							<div class="media-body">
								<h5>{{@$propertyDetails->propertyUser->name}}</h5>
								<b><img src="{{ URL::asset('public/frontend/images/property_icon1.png')}}" alt="">{{@$propertyDetails->propertyUser->state}}, {{@$propertyDetails->propertyUser->userCountry->name}}</b>
							</div>
						</div>
						<span class="bodr_arw"></span>
						<div class="send_mess_bx">
							<ul>
								{{-- <li>
									<span><img src="{{ URL::asset('public/frontend/images/send_img1.png')}}" alt=""><a href="mailto:test-agent12@gmail.com">test-agent12@gmail.com</a></span>
								</li>
								<li class="num_2">
									<span><img src="{{ URL::asset('public/frontend/images/send_img2.png')}}" alt=""><a href="tel:919876542100">+91 9876542100</a></span>
									<span><img src="{{ URL::asset('public/frontend/images/send_img4.png')}}" alt=""><a href="tel:876542130">876542130</a></span>
								</li>
								<li>
									<span><img src="{{ URL::asset('public/frontend/images/send_img3.png')}}" alt=""><a href="#">https://www.bbbbannnnhhfg.com</a></span>
								</li> --}}
                                <li>

									<span><img src="{{ URL::asset('public/frontend/images/send_img1.png')}}" alt="">
                                    	<a href="javascript:void(0);" class="view_email">View Email</a>
                                        <strong class="show_email">{{@$propertyDetails->propertyUser->email}}</strong>
                                    </span>

								</li>
                                @if(@$propertyDetails->propertyUser->whatsapp_no)

								<li class="num_2">

									<span><img src="{{ URL::asset('public/frontend/images/send_img2.png')}}" alt="">
                                    	<a href="javascript:void(0);" class="view_noo">View Phone Number</a>
                                        <strong class="show_noo">+91 {{@$propertyDetails->propertyUser->mobile_number}}</strong>
                                    </span>

									<span><img src="{{ URL::asset('public/frontend/images/send_img4.png')}}" alt="">
                                    	<a href="javascript:void(0);" class="view_whatsapp">View WhatsApp </a>
                                        <strong class="show_whatsapp">+91 {{@$propertyDetails->propertyUser->whatsapp_no}}</strong>
                                    </span>

								</li>
                                @endif
                                @if(@$propertyDetails->propertyUser->website)
								<li>

									<span><img src="{{ URL::asset('public/frontend/images/send_img3.png')}}" alt="">
                                    	<a href="javascript:void(0);" class="view_website">View Website</a>
                                        <strong class="show_website">{{@$propertyDetails->propertyUser->website}}</strong>
                                    </span>

								</li>
                                @endif
							</ul>
							<div class="view_pro_btn">
								<a href="{{route('agent.public.profile',['slug'=>@$propertyDetails->propertyUser->slug])}}" class="see_cc">View Profile</a>
								<button class="see_cc see_cc_gr">Send Message</button>
							</div>
						</div>
					</div>
					<div class="property_ri_itm">
						<h2>Schedule a visit request</h2>
						<span class="bodr_arw"></span>
						<div class="calendar_div">
							<div class="container-calendar">
					          <h3 id="monthAndYear"></h3>
					          <div class="button-container-calendar">
					              <button id="previous" onclick="previous()">&#8249;</button>
					              <button id="next" onclick="next()">&#8250;</button>
					          </div>

					          <table class="table-calendar" id="calendar" data-lang="en">
					              <thead id="thead-month"></thead>
					              <tbody id="calendar-body"></tbody>
					          </table>

					          <div class="footer-container-calendar">
					              <label for="month">Jump To: </label>
					              <select id="month" onchange="jump()">
					                  <option value=0>Jan</option>
					                  <option value=1>Feb</option>
					                  <option value=2>Mar</option>
					                  <option value=3>Apr</option>
					                  <option value=4>May</option>
					                  <option value=5>Jun</option>
					                  <option value=6>Jul</option>
					                  <option value=7>Aug</option>
					                  <option value=8>Sep</option>
					                  <option value=9>Oct</option>
					                  <option value=10>Nov</option>
					                  <option value=11>Dec</option>
					              </select>
					              <select id="year" onchange="jump()"></select>
					          </div>
					      </div>
						</div>
						<div class="input_type_bx">
							<form>
								 <div class="floating-label">
							      <select
							      class="floating-select" onclick="this.setAttribute('value', this.value);" value="">
							        <option value=""></option>
							        <option value="1">10:00 AM</option>
							        <option value="2">12:00 AM</option>
							        <option value="3">01:00 AM</option>
							      </select>
							      <label>Choose a time</label>
							    </div>
							    <div class="input-field">
								    <input type="text" id="name" required="">
								    <label for="name">Phone</label>
								 </div>
								 <div class="input-field">
								    <input type="text" id="name" required="">
								    <label for="name">Full addres</label>
								 </div>
								 <div class="chec_bx">
								 	<div class="radiobx">
										<input type="checkbox" id="rate1" name="radios" value="all" checked="">
										<label for="rate1">I want to talk about financing</label>
									</div>
								 </div>
								 <div class="view_pro_btn">
									<button class="see_cc">Send Request</button>
								</div>
							</form>
							<p>Donec quis felis tincidunt, sollicitudin risus the imperdiet turpis sagittis cursus risus aliquet isurna sagittis aliquam tempus erat ia adummy ahnm ipsum facilisis tincidunt.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="similar_sec">
	<div class="container con_det_pg">
		<div class="similar_inr">
			<div class="section-heading2 text-center">
				<h2>Similar Properties You May Like</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed lorem sapien, auctor in justo id, dignissim<br>dipiscing elit sed lorem sapien</p>
			</div>
			<div class="similar_slaid">
				<div class="owl-carousel">
                    @foreach ($similarProperty as $smProperty)

                    @endforeach
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list1.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical"><p>For Rent</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">Canada</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.5</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹4,500 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent1.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list2.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical Sale"><p>For Sale</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">New York , USA</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.5</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹9,100 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent2.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list3.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical Sale"><p>For Sale</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">Hydrabad</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.8</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹4,300 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent1.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list4.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical Sale"><p>For Sale</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">Canada</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.5</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹9,500 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent2.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list1.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical"><p>For Rent</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">Canada</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.5</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹4,500 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent1.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list2.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical Sale"><p>For Sale</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">New York , USA</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.5</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹9,100 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent2.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list3.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical Sale"><p>For Sale</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">Hydrabad</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.8</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹4,300 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent1.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list4.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical Sale"><p>For Sale</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">Canada</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.5</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹9,500 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent2.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list1.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical"><p>For Rent</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">Canada</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.5</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹4,500 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent1.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list2.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical Sale"><p>For Sale</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">New York , USA</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.5</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹9,100 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent2.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list3.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical Sale"><p>For Sale</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">Hydrabad</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.8</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹4,300 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent1.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="property-item">
							<div class="listing-image">
									<a href="#"><img src="{{ URL::asset('public/frontend/images/list4.png')}}" alt="listing property">
									<div class="propety-info-top">
									</div></a>
									<div class="ribbon-img">
										<div class="ribbon-vertical Sale"><p>For Sale</p></div>
									</div>
									<div class="wish-property">
										<a href="#"><i class="icofont-heart"></i></a>
									</div>
									<div class="location-rating">
										<div class="product-img-location">
											<p><img src="{{ URL::asset('public/frontend/images/loac.png')}}">Canada</p>
										</div>
										<div class="rating-pro">
											<p><img src="{{ URL::asset('public/frontend/images/star.png')}}"> 4.5</p>
										</div>
									</div>
							</div>
							<div class="property-detasils-info">
								<div class="name-price">
									<a href="#"><h5>Blue Reef Properties</h5></a>
									<p>₹9,500 </p>
								</div>
								<ul class="list-item-info ">
									<li class="before">
										<span>3 <img src="{{ URL::asset('public/frontend/images/bed.png')}}"></span> Bedrooms
									</li>
									<li class="before">
										<span>2 <img src="{{ URL::asset('public/frontend/images/bath.png')}}"></span> Bathrooms
									</li>
									<li class="">
										<span>3450 <img src="{{ URL::asset('public/frontend/images/sqft.png')}}"></span> Square Ft
									</li>
								</ul>
							</div>
							<div class="propety-agent">
								<div class="agent-info">
									<em>
										<img src="{{ URL::asset('public/frontend/images/agent2.png')}}">
									</em>
									<div class="agent-name">
										<h6>Gina Mconihon</h6>
										<p>14 Properties</p>
									</div>
								</div>
								<a href="#" class="vi-more">
									View More
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection


@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection



@section('script')
@include('includes.script')

<script>
    // Initialize and add the map
function initMap() {
  // The location of Uluru
  var uluru = {lat: {{$propertyDetails->address_lat}}, lng: {{$propertyDetails->address_long}}};
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 10, center: uluru});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: uluru, map: map});
  var mapKey ='{{env('GOOGLE_MAP_KEY')}}';
}
</script>
<!--Load the API from the specified URL
    * The async attribute allows the browser to render the page while the API loads
    * The key parameter will contain your own API key (which is not needed for this tutorial)
    * The callback parameter executes the initMap() function
    -->
<script defer src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&callback=initMap">
</script>
<script>
    $(document).ready(function(){
        $(".view_noo").click(function(){
            $(".show_noo").show();
        });
        $(".view_noo").click(function(){
            $(".view_noo").hide();
        });
    });
    </script>

    <script>
    $(document).ready(function(){
        $(".view_email").click(function(){
            $(".show_email").show();
        });
        $(".view_email").click(function(){
            $(".view_email").hide();
        });
    });
    </script>

    <script>
    $(document).ready(function(){
        $(".view_whatsapp").click(function(){
            $(".show_whatsapp").show();
        });
        $(".view_whatsapp").click(function(){
            $(".view_whatsapp").hide();
        });
    });
    </script>

    <script>
    $(document).ready(function(){
        $(".view_website").click(function(){
            $(".show_website").show();
        });
        $(".view_website").click(function(){
            $(".view_website").hide();
        });
    });
    </script>
@endsection
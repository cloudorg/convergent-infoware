@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Agent Profile</title>
@endsection




@section('header')
@include('includes.header')
@endsection

@section('content')

<div class="haeder-padding"></div>

<div class="agent_publick_sec">
	<div class="container">
		<div class="agent_publick_inr">
			<div class="agent_publick_left">
				<div class="agent_publick_left_inr">
					<a href="#url" class="book_mark"><i class="fa fa-bookmark-o"></i></a>
					<div class="agent_publick_det">
						<em>
							@if(@$agent->profile_pic != null)
                            <img src="{{ URL::to('storage/app/public/profile_picture') }}/{{ @$agent->profile_pic }}">
                             @else
							<img src="{{ url('public/frontend/images/pro1.png') }}">
                             @endif
							{{--<img src="images/agent_publickimg.png" alt="">--}}
						  </em>
						<span>{{ @$property->count() }} Property</span>
						<h4>{{ $agent->name }}</h4>
						<h5>Private Broker</h5>
						<p><i class="fa fa-star"></i><b>{{ @$agent->avg_review }}</b> ({{ @$agent->tot_review }} reviews)</p>
					</div>
					<div class="agent_publick_link">

						<p>
						<span><img src="{{ url('public/frontend/images/agent_icon1.png') }}" alt=""> Address:</span>
						<strong>{{ @$agent->address }}</strong>
						</p>

						<p>
						<span><img src="{{ url('public/frontend/images/agent_icon2.png') }}" alt=""> 
						<strong class="view_email_02 sstll">View Email :</strong></span>
						<strong class="show_email_02">{{ @$agent->email }}</strong>
						</p>

						<p>
						<span><img src="{{ url('public/frontend/images/agent_icon3.png') }}" alt=""> 
						<strong class="view_contact_02 sstll">View Contact Number : </strong></span>
						<strong class="show_contact_02">+91 {{ @$agent->mobile_number }} </strong>
						</p>
                         @if(@$agent->whatsapp_no != null)
						<p>
						<span><img src="{{ url('public/frontend/images/agent_icon4.png') }}" alt="">
						<strong class="view_whatsapp_02 sstll">View WhatsApp : </strong></span>
						<strong class="show_whatsapp_02">+91 {{ @$agent->whatsapp_no }} </strong>
						</p>
                         @endif
						 @if(@$agent->website != null)
						<p>
						<span><img src="{{ url('public/frontend/images/agent_icon5.png') }}" alt="">
						<strong class="view_website_02 sstll">View Website :</strong></span>
						<strong class="show_website_02"> {{ @$agent->website }}</strong>
						</p>
                       @endif
					</div>
					<div class="agent_publick_share">
						<a href="#url" class="see_cc">Message Me</a>
						<div class="pro-social">
							<p>Share :</p>
							<div class="addthis_inline_share_toolbox"></div>
							<!-- <ul>
								<li>
									<a href="#"><img src="{{ url('public/frontend/images/facebook.png') }}"></a>
								</li>
								<li>
									<a href="#"><img src="{{ url('public/frontend/images/twitter2.png') }}"></a>
								</li>
								<li>
									<a href="#"><img src="{{ url('public/frontend/images/in.png') }}"></a>
								</li>
								<li>
									<a href="#"><img src="{{ url('public/frontend/images/plus.png') }}"></a>
								</li>
							</ul> -->
						</div>
					</div>
				</div>
			</div>
			<div class="agent_publick_right">
				<div class="agent_publick_right_inr">
					<div class="agent_publick_abu">
						<h4>About Me:</h4>
						 
						<p>
							@if(strlen(@$agent->about)>200)
							{!! substr(@$agent->about,0,200) . '...' !!}<br>
							<p> 
							 <span class="moretext1" style="display: none;">{{ @$agent->about }}</span><br>
							{{--<a href="#url" class="moreless-button1 more_cc">View All +</a>--}}
						  </p>
                            <a href="#url" class="moreless-button1 more_cc">Read More +</a>
							@else
							
							{!! @$agent->about !!}
							@endif
						 </p>
						{{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
							 Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
							 when an unknown printer took a galley of type and electronic typesetting, 
							 remaining essentially unchanged.
							  It was popularised in the 1960s with the release of Letraset sheets containing when an unknown printer took a galley of type and electronic typesetting,
							   remaining essentially unchanged. It was popularised in Lorem Ipsum passages</p>--}}
						
					</div>
					<div class="our_properties_sec">
						<div class="our_properties_top">
							<span><em>{{ @$property->count() }}</em>Our Properties </span>
							<!-- <a href="javascript:;" class="see_cc">See All</a> -->
						</div>
					</div>
					<div class="our_properties_panel">
						<div class="row">
							@if(@$property)
							 @foreach($property as $pro)
							<div class="col-sm-4">
								<div class="property-item">
									<div class="listing-image">
										    @if(@$pro->propertyImageMain != null)
											<a href="#"><img src="{{ url('storage/app/public/property_image/'.@$pro->propertyImageMain->image) }}" alt="listing property">
											<div class="propety-info-top">									
											</div></a>
											@endif
											<div class="ribbon-img">
												<div class="ribbon-vertical">
													<p>
												    @if(@$pro->property_for == 'B')		
													For Buy
													@elseif(@$pro->property_for == 'R')
													 For Rent
													 @endif
												  </p></div>
											</div>
											<div class="wish-property">
												<a href="#"><i class="icofont-heart"></i></a>
											</div>	
											<div class="location-rating">
												<div class="product-img-location">
													<p><img src="{{ url('public/frontend/images/loac.png') }}">{{ @$pro->address }}</p>
												</div>
												<div class="rating-pro">
													<p><img src="{{ url('public/frontend/images/star.png') }}">{{ @$pro->avg_review }}</p>
												</div>
											</div>
									</div>
									<div class="property-detasils-info">
										<div class="name-price">
											<a href="{{route('search.property.details',['slug'=>@$pro->slug])}}">
												<h5>
												@if(strlen(@$pro->name) > 40)
                                               {!! substr(@$pro->name, 0, 40 ) . '..' !!}
                                               @else
                                              {!!@$pro->name!!}
                                                @endif
												  </h5></a>
											<p>₹{{ number_format(@$pro->budget_range_from, 0, '.', ',') }}</p>
										</div>
										<ul class="list-item-info ">
											<li class="before">
												<span>{{@$pro->no_of_bedrooms}}<img src="{{ url('public/frontend/images/bed.png') }}"></span> Bedrooms
											</li>
											<li class="before">
												<span>{{@$pro->bathroom}}<img src="{{ url('public/frontend/images/bath.png') }}"></span> Bathrooms
											</li>
											<li class="">
												<span>{{@$pro->area}} <img src="{{ url('public/frontend/images/sqft.png') }}"></span> Square Ft
											</li>
										</ul>
									</div>
								</div>
							</div>
							@endforeach
							@endif
							{{--<div class="col-sm-4">
								<div class="property-item">
									<div class="listing-image">
											<a href="#"><img src="{{ url('public/frontend/images/list2.png') }}" alt="listing property">
											<div class="propety-info-top">									
											</div></a>
											<div class="ribbon-img">
												<div class="ribbon-vertical Sale"><p>For Sale</p></div>
											</div>
											<div class="wish-property">
												<a href="#"><i class="icofont-heart"></i></a>
											</div>	
											<div class="location-rating">
												<div class="product-img-location">
													<p><img src="{{ url('public/frontend/images/loac.png') }}">Karnataka</p>
												</div>
												<div class="rating-pro">
													<p><img src="{{ url('public/frontend/images/star.png') }}"> 4.7</p>
												</div>
											</div>
									</div>
									<div class="property-detasils-info">
										<div class="name-price">
											<a href="#"><h5>Blue Reef Properties</h5></a>
											<p>₹4,300 </p>
										</div>
										<ul class="list-item-info ">
											<li class="before">
												<span>3 <img src="{{ url('public/frontend/images/bed.png') }}"></span> Bedrooms
											</li>
											<li class="before">
												<span>2 <img src="{{ url('public/frontend/images/bath.png') }}"></span> Bathrooms
											</li>
											<li class="">
												<span>3450 <img src="{{ url('public/frontend/images/sqft.png') }}"></span> Square Ft
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="property-item">
									<div class="listing-image">
											<a href="#"><img src="{{ url('public/frontend/images/list3.png') }}" alt="listing property">
											<div class="propety-info-top">									
											</div></a>
											<div class="ribbon-img">
												<div class="ribbon-vertical Sale"><p>For Sale</p></div>
											</div>
											<div class="wish-property">
												<a href="#"><i class="icofont-heart"></i></a>
											</div>	
											<div class="location-rating">
												<div class="product-img-location">
													<p><img src="{{ url('public/frontend/images/loac.png') }}">Hyderabad, Andhra Pradesh</p>
												</div>
												<div class="rating-pro">
													<p><img src="{{ url('public/frontend/images/star.png') }}"> 4.5</p>
												</div>
											</div>
									</div>
									<div class="property-detasils-info">
										<div class="name-price">
											<a href="#"><h5>Blue Reef Properties</h5></a>
											<p>₹7,500 </p>
										</div>
										<ul class="list-item-info ">
											<li class="before">
												<span>3 <img src="{{ url('public/frontend/images/bed.png') }}"></span> Bedrooms
											</li>
											<li class="before">
												<span>2 <img src="{{ url('public/frontend/images/bath.png') }}"></span> Bathrooms
											</li>
											<li class="">
												<span>3450 <img src="{{ url('public/frontend/images/sqft.png') }}"></span> Square Ft
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="property-item">
									<div class="listing-image">
											<a href="#"><img src="{{ url('public/frontend/images/list6.png') }}" alt="listing property">
											<div class="propety-info-top">									
											</div></a>
											<div class="ribbon-img">
												<div class="ribbon-vertical Sale"><p>For Sale</p></div>
											</div>
											<div class="wish-property">
												<a href="#"><i class="icofont-heart"></i></a>
											</div>	
											<div class="location-rating">
												<div class="product-img-location">
													<p><img src="{{ url('public/frontend/images/loac.png') }}">Bangalore, Karnataka</p>
												</div>
												<div class="rating-pro">
													<p><img src="{{ url('public/frontend/images/star.png') }}"> 4.6</p>
												</div>
											</div>
									</div>
									<div class="property-detasils-info">
										<div class="name-price">
											<a href="#"><h5>Blue Reef Properties</h5></a>
											<p>₹4,800 </p>
										</div>
										<ul class="list-item-info ">
											<li class="before">
												<span>3 <img src="{{ url('public/frontend/images/bed.png') }}"></span> Bedrooms
											</li>
											<li class="before">
												<span>2 <img src="{{ url('public/frontend/images/bath.png') }}"></span> Bathrooms
											</li>
											<li class="">
												<span>3450 <img src="{{ url('public/frontend/images/sqft.png') }}"></span> Square Ft
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="property-item">
									<div class="listing-image">
											<a href="#"><img src="{{ url('public/frontend/images/list1.png') }}" alt="listing property">
											<div class="propety-info-top">									
											</div></a>
											<div class="ribbon-img">
												<div class="ribbon-vertical"><p>For Rent</p></div>
											</div>
											<div class="wish-property">
												<a href="#"><i class="icofont-heart"></i></a>
											</div>	
											<div class="location-rating">
												<div class="product-img-location">
													<p><img src="{{ url('public/frontend/images/loac.png') }}">USA</p>
												</div>
												<div class="rating-pro">
													<p><img src="{{ url('public/frontend/images/star.png') }}"> 4.9</p>
												</div>
											</div>
									</div>
									<div class="property-detasils-info">
										<div class="name-price">
											<a href="#"><h5>Blue Reef Properties</h5></a>
											<p>₹7,500 </p>
										</div>
										<ul class="list-item-info ">
											<li class="before">
												<span>3 <img src="{{ url('public/frontend/images/bed.png') }}"></span> Bedrooms
											</li>
											<li class="before">
												<span>2 <img src="{{ url('public/frontend/images/bath.png') }}"></span> Bathrooms
											</li>
											<li class="">
												<span>3450 <img src="{{ url('public/frontend/images/sqft.png') }}"></span> Square Ft
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="property-item">
									<div class="listing-image">
											<a href="#"><img src="{{ url('public/frontend/images/list7.png') }}" alt="listing property">
											<div class="propety-info-top">									
											</div></a>
											<div class="ribbon-img">
												<div class="ribbon-vertical"><p>For Rent</p></div>
											</div>
											<div class="wish-property">
												<a href="#"><i class="icofont-heart"></i></a>
											</div>	
											<div class="location-rating">
												<div class="product-img-location">
													<p><img src="{{ url('public/frontend/images/loac.png') }}">New York</p>
												</div>
												<div class="rating-pro">
													<p><img src="{{ url('public/frontend/images/star.png') }}"> 4.7</p>
												</div>
											</div>
									</div>
									<div class="property-detasils-info">
										<div class="name-price">
											<a href="#"><h5>Blue Reef Properties</h5></a>
											<p>₹10,500 </p>
										</div>
										<ul class="list-item-info ">
											<li class="before">
												<span>3 <img src="{{ url('public/frontend/images/bed.png') }}"></span> Bedrooms
											</li>
											<li class="before">
												<span>2 <img src="{{ url('public/frontend/images/bath.png') }}"></span> Bathrooms
											</li>
											<li class="">
												<span>3450 <img src="{{ url('public/frontend/images/sqft.png') }}"></span> Square Ft
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="property-item">
									<div class="listing-image">
											<a href="#"><img src="{{ url('public/frontend/images/list4.png') }}" alt="listing property">
											<div class="propety-info-top">									
											</div></a>
											<div class="ribbon-img">
												<div class="ribbon-vertical Sale"><p>For Sale</p></div>
											</div>
											<div class="wish-property">
												<a href="#"><i class="icofont-heart"></i></a>
											</div>	
											<div class="location-rating">
												<div class="product-img-location">
													<p><img src="{{ url('public/frontend/images/loac.png') }}">USA</p>
												</div>
												<div class="rating-pro">
													<p><img src="{{ url('public/frontend/images/star.png') }}"> 4.6</p>
												</div>
											</div>
									</div>
									<div class="property-detasils-info">
										<div class="name-price">
											<a href="#"><h5>Blue Reef Properties</h5></a>
											<p>₹4,800 </p>
										</div>
										<ul class="list-item-info ">
											<li class="before">
												<span>3 <img src="{{ url('public/frontend/images/bed.png') }}"></span> Bedrooms
											</li>
											<li class="before">
												<span>2 <img src="{{ url('public/frontend/images/bath.png') }}"></span> Bathrooms
											</li>
											<li class="">
												<span>3450 <img src="{{ url('public/frontend/images/sqft.png') }}"></span> Square Ft
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="property-item">
									<div class="listing-image">
											<a href="#"><img src="{{ url('public/frontend/images/list8.png') }}" alt="listing property">
											<div class="propety-info-top">									
											</div></a>
											<div class="ribbon-img">
												<div class="ribbon-vertical Sale"><p>For Sale</p></div>
											</div>
											<div class="wish-property">
												<a href="#"><i class="icofont-heart"></i></a>
											</div>	
											<div class="location-rating">
												<div class="product-img-location">
													<p><img src="{{ url('public/frontend/images/loac.png') }}">New York</p>
												</div>
												<div class="rating-pro">
													<p><img src="{{ url('public/frontend/images/star.png') }}"> 4.7</p>
												</div>
											</div>
									</div>
									<div class="property-detasils-info">
										<div class="name-price">
											<a href="#"><h5>Blue Reef Properties</h5></a>
											<p>₹4,300 </p>
										</div>
										<ul class="list-item-info ">
											<li class="before">
												<span>3 <img src="{{ url('public/frontend/images/bed.png') }}"></span> Bedrooms
											</li>
											<li class="before">
												<span>2 <img src="{{ url('public/frontend/images/bath.png') }}"></span> Bathrooms
											</li>
											<li class="">
												<span>3450 <img src="{{ url('public/frontend/images/sqft.png') }}"></span> Square Ft
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="property-item">
									<div class="listing-image">
											<a href="#"><img src="{{ url('public/frontend/images/list9.png') }}" alt="listing property">
											<div class="propety-info-top">									
											</div></a>
											<div class="ribbon-img">
												<div class="ribbon-vertical"><p>For Rent</p></div>
											</div>
											<div class="wish-property">
												<a href="#"><i class="icofont-heart"></i></a>
											</div>	
											<div class="location-rating">
												<div class="product-img-location">
													<p><img src="{{ url('public/frontend/images/loac.png') }}">New York</p>
												</div>
												<div class="rating-pro">
													<p><img src="{{ url('public/frontend/images/star.png') }}"> 4.7</p>
												</div>
											</div>
									</div>
									<div class="property-detasils-info">
										<div class="name-price">
											<a href="#"><h5>Blue Reef Properties</h5></a>
											<p>₹4,500 </p>
										</div>
										<ul class="list-item-info ">
											<li class="before">
												<span>3 <img src="{{ url('public/frontend/images/bed.png') }}"></span> Bedrooms
											</li>
											<li class="before">
												<span>2 <img src="{{ url('public/frontend/images/bath.png') }}"></span> Bathrooms
											</li>
											<li class="">
												<span>3450 <img src="{{ url('public/frontend/images/sqft.png') }}"></span> Square Ft
											</li>
										</ul>
									</div>
								</div>
							</div>--}}
						</div>							
					</div>
					<div class="hom_det_bx revew_sec revew_sec_agent">
						<h4 class="hom_det_h4">Reviews</h4>
						<div class="revew_panel">
							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										<span><img src="{{ url('public/frontend/images/revew6.png') }}" alt=""></span>
										<div class="media-body">
											<h4>Akashbev Roy</h4>
											<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star-half-o"></i></li>
											</ul>
											<strong><img src="{{ url('public/frontend/images/calendar2.png') }}" alt=""> 20th Sept, 2021</strong>
										</div>
										</div>
									</div>
								</div>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
									Lorem Ipsum has been the of Letraset sheets containing  
									It was popularised in the 1960s with the release of Letraset 
									sheets containing Lorem Ipsum passages, Lorem Ipsum has been the 
									of Letraset sheets containing  
									Lorem Ipsum passages..
									<span class="moretext" style="display: none;">
									Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis
									 magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque
									  ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiat dui.
									   Lorem ipsum dolor sit ametconsectetur adipiscing elit  iaculis, </span> 
									   <a href="#url" class="moreless-button more_cc"> Read More+</a> </p>
							</div>
							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										<span><img src="{{ url('public/frontend/images/revew7.png') }}" alt=""></span>
										<div class="media-body">
											<h4>Akashbev Roy</h4>
											<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star gray"></i></li>
											</ul>
											<strong><img src="{{ url('public/frontend/images/calendar2.png') }}" alt=""> 20th Sept, 2021</strong>
										</div>
										</div>
									</div>									
								</div>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the of Letraset sheets containing  It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, Lorem Ipsum.</p>
							</div>
							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										<span><img src="{{ url('public/frontend/images/revew8.png') }}" alt=""></span>
										<div class="media-body">
											<h4>Akashbev Roy</h4>
											<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star gray"></i></li>
												<li><i class="fa fa-star gray"></i></li>
												<li><i class="fa fa-star gray"></i></li>
											</ul>
											<strong><img src="{{ url('public/frontend/images/calendar2.png') }}" alt=""> 20th Sept, 2021</strong>
										</div>
										</div>
									</div>
								</div>									
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the of Letraset sheets containing  It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, Lorem Ipsum.</p>
							</div>
							<div class="moretext_all moretext14">
								<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										<span><img src="{{ url('public/frontend/images/revew7.png') }}" alt=""></span>
										<div class="media-body">
											<h4>Akashbev Roy</h4>
											<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star gray"></i></li>
											</ul>
											<strong><img src="{{ url('public/frontend/images/calendar2.png') }}" alt=""> 20th Sept, 2021</strong>
										</div>
										</div>
									</div>									
								</div>
								<p>Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus auctor the pretium nibhfeugiat dui. Lorem ipsum dolor sit ametconsectetur.</p>
							</div>
							<div class="revew_item">
								<div class="revew_top">
									<div class="media">
										<span><img src="{{ url('public/frontend/images/revew8.png') }}" alt=""></span>
										<div class="media-body">
											<h4>Akashbev Roy</h4>
											<div class="star_bx">
											<ul>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
											</ul>
											<strong><img src="{{ url('public/frontend/images/calendar2.png') }}" alt=""> 20th Sept, 2021</strong>
										</div>
										</div>
									</div>
								</div>									
								<p>Lorem ipsum dolor sit amet consectetur the adipiscing elit iaculis magna dignissim facilisis felis nisl pellentesque libero, imperdiet neque ante ut nisi sapien neque sodales lacus.</p>
							</div>
							</div>
							<a href="#url" class="see_cc moreless-button14">See More reviews +</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection

@section('script')
@include('includes.script')

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-613782201f0abe3d"></script>

<script>
$(document).ready(function(){
    $(".view_email_02").click(function(){
        $(".show_email_02").show();
    });
	$(".view_email_02").click(function(){
		$(".view_email_02").hide();
	});
});
</script> 


<script>
$(document).ready(function(){
    $(".view_noo").click(function(){
        $(".show_noo").show();
    });
	$(".view_noo").click(function(){
		$(".view_noo").hide();
	});
});
</script> 

<script>
$(document).ready(function(){
    $(".view_contact_02").click(function(){
        $(".show_contact_02").show();
    });
	$(".view_contact_02 ").click(function(){
		$(".view_contact_02 ").hide();
	});
});
</script> 

<script>
$(document).ready(function(){
    $(".view_whatsapp_02").click(function(){
        $(".show_whatsapp_02").show();
    });
	$(".view_whatsapp_02").click(function(){
		$(".view_whatsapp_02").hide();
	});
});
</script> 

<script>
$(document).ready(function(){
    $(".view_website_02").click(function(){
        $(".show_website_02").show();
    });
	$(".view_website_02").click(function(){
		$(".view_website_02").hide();
	});
});
</script> 


@endsection
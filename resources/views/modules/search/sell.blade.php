@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Sell </title>
@endsection




@section('header')
@include('includes.header')
@endsection

@section('content')

<div class="haeder-padding"></div>

<div class="sell_hed_sec">
	<div class="container">
		<div class="sell_hed_inr">
			<p>RIvirtual Offers is winding down, which means we are not making any new offers on homes. We're focused on helping existing customers and<br> selling our remaining inventory. If you're looking to sell, we can connect you with an expert agent in your market. If you have<br> questions about a current contract with RIvirtual Offers, get answers at <a href="#url" class="offer_cc">Offers & Closings.</a></p>
		</div>
	</div>
</div>
<div class="sell_ban_sec">
	<img src="{{ url('public/frontend/images/sel_ban.jpg') }}" alt="">
	<div class="sell_ban_bx">
		<div class="container">
			<div class="sell_ban_bx_inr">
				<h1>Sell your home with confidence</h1>
				<p>RIvirtual is making it simpler to sell your home and move forward.</p>
			</div>
		</div>
	</div>
</div>
<div class="traditionally_sec">
	<div class="container">
		<div class="traditionally_ir">
			<div class="traditionally_img">
				<img src="{{ url('public/frontend/images/selimg3.jpg') }}" alt="">
			</div>
			<div class="traditionally_text">
				<h2>Sell traditionally with an agent</h2>
				<p>When you work with a real estate agent, you'll get selling support at every step, from prepping and listing your home to marketing that gets buyers in the door.</p>
				<a href="#" class="see_cc">Find an agent</a>
				<p><a href="#" class="choose_agent">Learn how to choose an agent</a></p>
				<div class="row">
					<div class="col-lg-6">
						<div class="traditionally_text_bx">
							<h4>Why sell traditionally</h4>
							<ul>
								<li><i class="fa fa-check" aria-hidden="true"></i>Potential for bidding wars</li>
								<li><i class="fa fa-check" aria-hidden="true"></i>Access to local market expertise</li>
								<li><i class="fa fa-check" aria-hidden="true"></i>Get help negotiating and reviewing offers</li>
								<li><i class="fa fa-check" aria-hidden="true"></i>Navigate a stressful process with a dedicated guide</li>
							</ul>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="traditionally_text_bx">
							<h4>How to sell traditionally</h4>
							<p>Learn more about the process of <a href="#">selling your house with a listing agent.</a> If this is the best route for you, interview agents and select a professional who will meet your expectations. Your agent will then guide you through the steps of <a href="#">selling your home.</a></p>
						</div>
					</div>
				</div>
				<p><small>Agents listed in the directory under ‘Find an agent’ are not licensed with RIvirtual, Inc. or any of our affiliated entities.</small></p>
			</div>
		</div>
	</div>
</div>
<div class="traditionally_sec">
	<div class="container">
		<div class="traditionally_ir">
			<div class="traditionally_img">
				<img src="{{ url('public/frontend/images/selimg4.jpg') }}" alt="">
			</div>
			<div class="traditionally_text">
				<h2>Sell your home with a partner agent</h2>
				<p>It’s easy to sell your home when we connect you with a trusted local agent*. Start by seeing if partner agents are available in your area. They’ll be in more places soon!</p>
				<!-- <div class="traditionally_frm">
					<form class="banner-form" action="">
						  <div class="tab-content">
						      <div class="from-sec-banner">
						      	<div class="input-from">
						      		<div class="form-group ">
						      			<div class="input-with-icon">
											<input type="text" class="form-control" placeholder="Enter Your Address">
										</div>
						      		</div>
						      	</div>
						      	<div class="search-box">
						      		<input type="submit" name="" value="Get started">
						      	</div>
						      </div>
						  </div>
					</form>
				</div> -->
				<a href="#" class="see_cc">Get started</a>
				<div class="row">
					<div class="col-lg-6">
						<div class="traditionally_text_bx">
							<h4>Why sell with a partner agent</h4>
							<ul>
								<li><i class="fa fa-check" aria-hidden="true"></i>Local market expertise</li>
								<li><i class="fa fa-check" aria-hidden="true"></i>Reduced listing fees in some markets**</li>
								<li><i class="fa fa-check" aria-hidden="true"></i>Help negotiating and reviewing offers</li>
								<li><i class="fa fa-check" aria-hidden="true"></i>Priority placement on RIvirtual from tools like 3D Home tours</li>
							</ul>
						</div>
					</div>
				</div>
				<p><small>*Listing compensation and terms are determined by local partner brokerages and not by RIvirtual. Subject to individual brokerage eligibility criteria. Restrictions may apply.</small></p>
			</div>
		</div>
	</div>
</div>
<div class="home_lone_sec">
	<div class="container">
		<div class="home_lone_inr">
			<div class="home_lone_logo">
				<img src="{{ url('public/frontend/images/logo2.png') }}" alt="">
				<span>HomeLoane</span>
			</div>
			<div class="row">
				<div class="col-lg-5">
					<div class="homeLoane_bx">
						<h5>Refinance with RIvirtual Home Loans</h5>
						<p>If you're not ready to sell, see if you can save money every month by refinancing with RIvirtual Home Loans. You get competitive rates from a lender you can trust, and loan options suited to your unique needs.</p>
						<a href="#" class="choose_agent">Start saving</a>
					</div>
				</div>
				<div class="col-lg-5">
					<div class="homeLoane_bx">
						<h5>Why refinance with RIvirtual Home Loans</h5>
						<ul>
							<li><i class="fa fa-check" aria-hidden="true"></i>Competitive rates and fees</li>
							<li><i class="fa fa-check" aria-hidden="true"></i>Transparent process</li>
							<li><i class="fa fa-check" aria-hidden="true"></i>Help negotiating and reviewing offers</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-2">
					<div class="homeLoane_img">
						<h5>ADVERTISEMENT</h5>
						<img src="{{ url('public/frontend/images/selimg5.png') }}" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="traditionally_sec">
	<div class="container">
		<div class="traditionally_ir">
			<div class="traditionally_img">
				<img src="{{ url('public/frontend/images/selimg6.jpg') }}" alt="">
			</div>
			<div class="traditionally_text">
				<h2>Sell your home yourself</h2>
				<p>Deciding to sell your home yourself is referred to as for-sale-by-owner (FSBO). The FSBO process is <a href="#"> similar to traditional selling,</a> but without the help of a real estate agent. In this case, you’re responsible for the home prep, marketing, showings, and negotiations.</p>
				<a href="#" class="see_cc">Post your home for sale</a>
				<p><a href="#" class="choose_agent">Learn more about FSBO</a></p>
				<div class="row">
					<div class="col-lg-6">
						<div class="traditionally_text_bx">
							<h4>How to sell FSBO</h4>
							<p>When selling yourself, start with home prep, staging, and hiring a professional photographer. Once your marketing materials are ready, research comparable homes to help price your home. Then, create a listing on Zillow. You’ll likely host home showings or open houses. Then select an offer, negotiate, accept and close.</p>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="traditionally_text_bx">
							<h4>Why sell FSBO</h4>
							<ul>
								<li><i class="fa fa-check" aria-hidden="true"></i>Avoid paying a listing agent commission</li>
								<li><i class="fa fa-check" aria-hidden="true"></i>Advertise your listing for free on RIvirtual </li>
								<li><i class="fa fa-check" aria-hidden="true"></i>Flexibility and control from start to finish</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="successful_sale_sec">
	<div class="container">
		<div class="successful_sale_inr">
			<div class="section-heading text-center">
				<h2>Explore your home's value</h2>
				<p>Enter your address to get your free Zestimate.</p>
			</div>
			<a href="#" class="see_cc">Contact Us</a>
			<!-- <div class="traditionally_frm">
				<form class="banner-form" action="">
					  <div class="tab-content">
					      <div class="from-sec-banner">
					      	<div class="input-from">
					      		<div class="form-group ">
					      			<div class="input-with-icon">
										<input type="text" class="form-control" placeholder="Enter Your Address">
									</div>
					      		</div>
					      	</div>
					      	<div class="search-box">
					      		<input type="submit" name="" value="Submit">
					      	</div>
					      </div>
					  </div>
				</form>
			</div> -->
		</div>
	</div>
</div>

<!-- <div class="acquainted_sec">
	<div class="container">
		<div class="section-heading2 text-center">
			<h2>Get acquainted with the process</h2>
			<p>As you begin the steps to selling, learn what to expect with our <a href="#url">Sellers Guide.</a> </p>
		</div>
		<div class="acquainted_sec_inr">
			<div class="faq-atbs">
				<ul class="nav nav-tabs ">
					<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#acqu1">Preparing to sell </a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#acqu2">Timing your sale  </a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#acqu3">Pricing your home</a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#acqu4">Getting noticed</a></li>
					<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#acqu5">Offers, closing & moving</a></li>
				</ul>
			</div>
			<div class="tab-content">
				<div class="tab-pane active" id="acqu1">
				 	<div class="acquainted_tab_panel">
				 		<div class="row">
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list1.png" alt="">
				 					<h4><a href="#">How to Price Your Home to Sell</a></h4>
				 					<p>When selling, you definitely want to get top dollar.  Follow these nine steps to price your home competitively  for your market, sell quickly and earn maximum profit.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list2.png" alt="">
				 					<h4><a href="#">Best Home Improvements to Increase Value</a></h4>
				 					<p>If you're thinking about selling your home sometime in the next  few years, make sure any renovations you complete add value.  Learn more about home improvements with the best ROI, and a few to avoid.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list3.png" alt="">
				 					<h4><a href="#">Tips for Negotiating With Real Estate Agents</a></h4>
				 					<p>When interviewing listing agents, the majority  of sellers don't negotiate. Learn how to  successfully negotiate terms of your listing agreement with this guide.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list4.png" alt="">
				 					<h4><a href="#">How to Calculate Home Equity</a></h4>
				 					<p>Learn everything you need to know about your home equity:  how to calculate it, how it increases, how  much equity you need to sell and more in this helpful guide.</p>
				 				</div>
				 			</div>
				 		</div>
				 	</div>
				</div>
				<div class="tab-pane" id="acqu2">
				 	<div class="acquainted_tab_panel">
				 		<div class="row">
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list5.png" alt="">
				 					<h4><a href="#">How Soon Can I Sell My House After Purchase?</a></h4>
				 					<p>Need to sell your home sooner than expected?  Learn more about your break even timeframe, amortization , fees and possible consequences for selling soon after purchasing.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list6.png" alt="">
				 					<h4><a href="#">Selling a Property With Tenants</a></h4>
				 					<p>It's time to sell your rental property. But how do you  approach the topic with your tenants? Learn more  about sale timing, strategies and tenant considerations.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list7.png" alt="">
				 					<h4><a href="#">Can You Sell Your House Before Paying Off the Mortgage?</a></h4>
				 					<p>Selling your home before it's paid off is a common occurrence. Learn about who pays the mortgage when selling,  pricing to repay your balance and options for selling underwater homes.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list8.png" alt="">
				 					<h4><a href="#">What Happens When You Inherit a House?</a></h4>
				 					<p>It’s possible to inherit a property at fair market value and only pay  capital gains tax from the time of inheritance to sale. But,  existing mortgages and other stakeholders can complicate the process. Learn about your options.</p>
				 				</div>
				 			</div>
				 		</div>
				 	</div>
				</div>
				<div class="tab-pane" id="acqu3">
				 	<div class="acquainted_tab_panel">
				 		<div class="row">
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list1.png" alt="">
				 					<h4><a href="#">How to Price Your Home to Sell</a></h4>
				 					<p>When selling, you definitely want to get top dollar.  Follow these nine steps to price your home competitively  for your market, sell quickly and earn maximum profit.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list2.png" alt="">
				 					<h4><a href="#">Best Home Improvements to Increase Value</a></h4>
				 					<p>If you're thinking about selling your home sometime in the next  few years, make sure any renovations you complete add value.  Learn more about home improvements with the best ROI, and a few to avoid.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list3.png" alt="">
				 					<h4><a href="#">Tips for Negotiating With Real Estate Agents</a></h4>
				 					<p>When interviewing listing agents, the majority  of sellers don't negotiate. Learn how to  successfully negotiate terms of your listing agreement with this guide.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list4.png" alt="">
				 					<h4><a href="#">How to Calculate Home Equity</a></h4>
				 					<p>Learn everything you need to know about your home equity:  how to calculate it, how it increases, how  much equity you need to sell and more in this helpful guide.</p>
				 				</div>
				 			</div>
				 		</div>
				 	</div>
				</div>
				<div class="tab-pane" id="acqu4">
				 	<div class="acquainted_tab_panel">
				 		<div class="row">
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list5.png" alt="">
				 					<h4><a href="#">How Soon Can I Sell My House After Purchase?</a></h4>
				 					<p>Need to sell your home sooner than expected?  Learn more about your break even timeframe, amortization , fees and possible consequences for selling soon after purchasing.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list6.png" alt="">
				 					<h4><a href="#">Selling a Property With Tenants</a></h4>
				 					<p>It's time to sell your rental property. But how do you  approach the topic with your tenants? Learn more  about sale timing, strategies and tenant considerations.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list7.png" alt="">
				 					<h4><a href="#">Can You Sell Your House Before Paying Off the Mortgage?</a></h4>
				 					<p>Selling your home before it's paid off is a common occurrence. Learn about who pays the mortgage when selling,  pricing to repay your balance and options for selling underwater homes.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list8.png" alt="">
				 					<h4><a href="#">What Happens When You Inherit a House?</a></h4>
				 					<p>It’s possible to inherit a property at fair market value and only pay  capital gains tax from the time of inheritance to sale. But,  existing mortgages and other stakeholders can complicate the process. Learn about your options.</p>
				 				</div>
				 			</div>
				 		</div>
				 	</div>
				</div>
				<div class="tab-pane" id="acqu5">
				 	<div class="acquainted_tab_panel">
				 		<div class="row">
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list5.png" alt="">
				 					<h4><a href="#">How Soon Can I Sell My House After Purchase?</a></h4>
				 					<p>Need to sell your home sooner than expected?  Learn more about your break even timeframe, amortization , fees and possible consequences for selling soon after purchasing.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list6.png" alt="">
				 					<h4><a href="#">Selling a Property With Tenants</a></h4>
				 					<p>It's time to sell your rental property. But how do you  approach the topic with your tenants? Learn more  about sale timing, strategies and tenant considerations.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list7.png" alt="">
				 					<h4><a href="#">Can You Sell Your House Before Paying Off the Mortgage?</a></h4>
				 					<p>Selling your home before it's paid off is a common occurrence. Learn about who pays the mortgage when selling,  pricing to repay your balance and options for selling underwater homes.</p>
				 				</div>
				 			</div>
				 			<div class="col-md-3">
				 				<div class="acquainted_bx">
				 					<img src="images/list8.png" alt="">
				 					<h4><a href="#">What Happens When You Inherit a House?</a></h4>
				 					<p>It’s possible to inherit a property at fair market value and only pay  capital gains tax from the time of inheritance to sale. But,  existing mortgages and other stakeholders can complicate the process. Learn about your options.</p>
				 				</div>
				 			</div>
				 		</div>
				 	</div>
				</div>
			</div>
		</div>
	</div>
</div> -->


<div class="faqs-sec faq_sec">

		<div class="container">

			<div class="row">

				<div class="col-lg-10 col-xl-9 col-md-11 mx-auto">

					<div class="section-heading2 text-center">

						<h2>Frequently Asked Questions</h2>

						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed lorem sapien, auctor in justo id, dignissim dipiscing elit sed lorem sapien</p>

					</div>

				</div>



				<div class="col-lg-10 col-xl-9 col-md-11 mx-auto">

					<div class="faq-atbs">

							<ul class="nav nav-tabs ">

								<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#today">User </a></li>

								<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#home">Agents  </a></li>

								<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#menu4">Service Pro</a></li>

							</ul>

						</div>

						<div class="tab-content">

							<div class="tab-pane active" id="today">

								<div class="accordian-faq">

									<div class="accordion" id="faq">

												<div class="card">

													<div class="card-header" id="faqhead1">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq1" aria-expanded="true" aria-controls="faq1">

															<p><span>1. </span> What is Ri Virtual</p>

														</a>

													</div>

													<div id="faq1" class="collapse" aria-labelledby="faqhead1" data-parent="#faq">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>





												<div class="card">

													<div class="card-header" id="faqhead2">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq2" aria-expanded="true" aria-controls="faq2">

															<p><span>2. </span> How to work?</p>

														</a>

													</div>

													<div id="faq2" class="collapse" aria-labelledby="faqhead2" data-parent="#faq">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead3">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq3" aria-expanded="true" aria-controls="faq3">

															<p><span>3. </span> How to hire a pro?</p>

														</a>

													</div>

													<div id="faq3" class="collapse" aria-labelledby="faqhead3" data-parent="#faq">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead4">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq4" aria-expanded="true" aria-controls="faq4">

															<p><span>4. </span> Dummy questions here?</p>

														</a>

													</div>

													<div id="faq4" class="collapse" aria-labelledby="faqhead4" data-parent="#faq">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead5">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq5" aria-expanded="true" aria-controls="faq5">

															<p><span>5. </span>Dummy questions here go?</p>

														</a>

													</div>

													<div id="faq5" class="collapse" aria-labelledby="faqhead5" data-parent="#faq">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>





												<div class="card">

													<div class="card-header" id="faqhead10">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq10" aria-expanded="true" aria-controls="faq10">

															<p><span>6. </span> simple questions here?</p>

														</a>

													</div>

													<div id="faq10" class="collapse" aria-labelledby="faqhead10" data-parent="#faq">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead6">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq6" aria-expanded="true" aria-controls="faq6">

															<p><span>7. </span> How to contact a agent?</p>

														</a>

													</div>

													<div id="faq6" class="collapse" aria-labelledby="faqhead6" data-parent="#faq">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead7">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq7" aria-expanded="true" aria-controls="faq7">

															<p><span>8. </span> Dummy questions here?</p>

														</a>

													</div>

													<div id="faq7" class="collapse" aria-labelledby="faqhead7" data-parent="#faq">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead8">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq8" aria-expanded="true" aria-controls="faq8">

															<p><span>9. </span> how  Rivirtual work?</p>

														</a>

													</div>

													<div id="faq8" class="collapse" aria-labelledby="faqhead8" data-parent="#faq">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead9">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq9" aria-expanded="true" aria-controls="faq9">

															<p><span>10. </span> Dummy questions here?</p>

														</a>

													</div>

													<div id="faq9" class="collapse" aria-labelledby="faqhead9" data-parent="#faq">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>

									</div>

								</div>

							</div>



							<div class="tab-pane " id="home">

								<div class="accordian-faq">

									<div class="accordion" id="accordionExample">

												<div class="card">

													<div class="card-header" id="faqhead11">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq11" aria-expanded="true" aria-controls="faq11">

															<p><span>1. </span> What is Ri Virtual</p>

														</a>

													</div>

													<div id="faq11" class="collapse" aria-labelledby="faqhead11" data-parent="#accordionExample">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead12">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq12" aria-expanded="true" aria-controls="faq12">

															<p><span>2. </span> How to work?</p>

														</a>

													</div>

													<div id="faq12" class="collapse" aria-labelledby="faqhead12" data-parent="#accordionExample">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead13">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq13" aria-expanded="true" aria-controls="faq13">

															<p><span>3. </span> How to hire a pro?</p>

														</a>

													</div>

													<div id="faq13" class="collapse" aria-labelledby="faqhead13" data-parent="#accordionExample">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead14">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq14" aria-expanded="true" aria-controls="faq14">

															<p><span>4. </span> Dummy questions here?</p>

														</a>

													</div>

													<div id="faq14" class="collapse" aria-labelledby="faqhead14" data-parent="#accordionExample">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead15">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq15" aria-expanded="true" aria-controls="faq15">

															<p><span>5. </span>Dummy questions here go?</p>

														</a>

													</div>

													<div id="faq15" class="collapse" aria-labelledby="faqhead15" data-parent="#accordionExample">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>





												<div class="card">

													<div class="card-header" id="faqhead16">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq16" aria-expanded="true" aria-controls="faq16">

															<p><span>6. </span> simple questions here?</p>

														</a>

													</div>

													<div id="faq16" class="collapse" aria-labelledby="faqhead16" data-parent="#accordionExample">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>

									</div>

								</div>

							</div>



							<div class="tab-pane " id="menu4">

								<div class="accordian-faq">

									<div class="accordion" id="accordionExampletwo">

												<div class="card">

													<div class="card-header" id="faqhead11">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq21" aria-expanded="true" aria-controls="faq21">

															<p><span>1. </span> What is Ri Virtual</p>

														</a>

													</div>

													<div id="faq21" class="collapse" aria-labelledby="faqhead21" data-parent="#accordionExampletwo">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead22">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq22" aria-expanded="true" aria-controls="faq22">

															<p><span>2. </span> How to Signup?</p>

														</a>

													</div>

													<div id="faq22" class="collapse" aria-labelledby="faqhead22" data-parent="#accordionExampletwo">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead23">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq23" aria-expanded="true" aria-controls="faq23">

															<p><span>3. </span> How to hire a pro?</p>

														</a>

													</div>

													<div id="faq23" class="collapse" aria-labelledby="faqhead23" data-parent="#accordionExampletwo">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead24">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq24" aria-expanded="true" aria-controls="faq24">

															<p><span>4. </span> Dummy questions here?</p>

														</a>

													</div>

													<div id="faq24" class="collapse" aria-labelledby="faqhead24" data-parent="#accordionExampletwo">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>



												<div class="card">

													<div class="card-header" id="faqhead15">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq25" aria-expanded="true" aria-controls="faq25">

															<p><span>5. </span>Dummy questions here go?</p>

														</a>

													</div>

													<div id="faq25" class="collapse" aria-labelledby="faqhead25" data-parent="#accordionExampletwo">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>





												<div class="card">

													<div class="card-header" id="faqhead26">

														<a href="#" class="btn btn-header-link acco-chap collapsed" data-toggle="collapse" data-target="#faq26" aria-expanded="true" aria-controls="faq26">

															<p><span>6. </span> simple questions here?</p>

														</a>

													</div>

													<div id="faq26" class="collapse" aria-labelledby="faqhead26" data-parent="#accordionExampletwo">

														<div class="card-body">

															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

														</div>

													</div>

												</div>

									</div>

								</div>

							</div>

						</div>

				</div>



			</div>

		</div>

	</div>
    @endsection

    @section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection

@section('script')
@include('includes.script')
@include('includes.toaster')

@endsection
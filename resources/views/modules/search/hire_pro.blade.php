@extends('layouts.app')

@section('style')
@include('includes.style')
<style>
    .error{
        color: red !important;
    }
</style>
<style>
    input[type=number] {
        /* height: 45px;
        width: 45px;
        font-size: 25px;
        text-align: center;
        border: 1px solid #000000; */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
    -moz-appearance:textfield;
    }
</style>
@endsection

@section('title')
<title> RiVirtual | Hire a Pro </title>
@endsection


@section('header')
@include('includes.header')
@endsection


@section('content')

<div class="haeder-padding"></div>


<!----banner--->

<section class="banner-section requirements_sec">
	<div class="banner-image">
		<img src="{{ url('public/frontend/images/list2.png') }}" alt="">
	</div>
	<div class="banner-text">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="text-center">
						<h1>Hire a Pro</h1>
						<form class="banner-form" action="{{ route('search.service.pro') }}" method="post" id="searchForm">
                            @csrf
							<input type="hidden" name="sub_category" id="sub_category" value="">
							  <div class="tab-content">
							      <div class="from-sec-banner">
							      	<div class="input-from">
							      		<div class="form-group borders">
							      			<div class="input-with-icon">
												<select name="category" id="category" class="input_sel">
													<option value="">Service category</option>
                                                    @foreach(@$category as $cat)
													<option value="{{ $cat->id }}">{{ $cat->category_name }}</option>
													@endforeach
												</select>
											</div>
							      		</div>
							      		<div class="form-group ">
							      			<div class="input-with-icon input-banner-flex">
											  <div class="div_flexs borders">
												<select name="state" id="states" class="input_sel">
													<option value="">Select state</option>
                                                    
													@foreach(@$states as $state)
													<option value="{{ $state->id }}">{{ $state->name }}</option>
													@endforeach
												
												</select>
											</div>
											<div class="div_flexs">
												<select name="city" id="city" class="input_sel">
													<option value="">Select city</option></option>
                                                    @foreach(@$cites as $city)
													<option value="{{ $city->id }}">{{ $city->name }}</option>
													@endforeach
												</select>
											</div>
											</div>
							      		</div>
							      	</div>
							      	<div class="search-box">
							      		<div class="sub2-lap">
							      			<img src="{{ url('public/frontend/images/search.png') }}" alt="search">
							      		</div>
							      		<input type="submit" name="" value="Search">
							      	</div>
							      </div>
							  </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="requirements_salidar_sec">
	<div class="container">
		<div class="requir_salidar_inr">
			@if(!@$categories->isEmpty())
			 @foreach(@$categories as $category)
			 @if(@$category->subCategoryDetails->isNotEmpty())
			<div class="requir_salidar_box" id="construction_services">
				<h4 class="heding_top">{{ @$category->category_name }}</h4>
				<div class="owl-carousel">
					
					 @foreach($category->subCategoryDetails as $subcat)
					<div class="item">
						<div class="requir_salidar_item">
							<a href="{{ route('search.service.pro',['slug'=>@$subcat->slug]) }}">
							    
								@if(@$subcat->image)
								<img src="{{ URL::to('storage/app/public/category_image/'.@$subcat->image) }}" alt="">
								@else
								<img src="{{ asset('public/frontend/images/logo_icon.png') }}" alt="">
								@endif
								<h4>{{ @$subcat->name }}</h4>
							</a>
							
						</div>
					</div>
					@endforeach
				
					{{--<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/propertiespic1.png') }}" alt="">
								<h4>General contractors</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/propertiespic2.png') }}" alt="">
								<h4>Earth Works </h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/propertiespic3.png') }}" alt="">
								<h4>Masons</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/propertiespic4.png') }}" alt="">
								<h4>Construction workers</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/propertiespic5.png') }}" alt="">
								<h4>Building Materials</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/propertiespic6.png') }}" alt="">
								<h4>Carpenters</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/propertiespic7.png') }}" alt="">
								<h4>Welders</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/propertiespic8.png') }}" alt="">
								<h4>Electricity connection</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/propertiespic9.png') }}" alt="">
								<h4>Water Connection</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/propertiespic10.png') }}" alt="">
								<h4>Plumbers</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/propertiespic7.png') }}" alt="">
								<h4>Vaastu Consultant </h4>
							</a>
						</div>
					</div>--}}

				</div>
			</div>
			@endif
			@endforeach
             @endif
			{{--<div class="requir_salidar_box" id="interiors_services">
				<h4 class="heding_top">Interiors Services</h4>
				<div class="owl-carousel">
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/w1.png') }}" alt="">
								<h4>Flooring and slabbing </h4>
							</a>
							
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/w2.png') }}" alt="">
								<h4>Tiles and Marbles</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/w3.png') }}" alt="">
								<h4>Painters</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/w4.png') }}" alt="">
								<h4>Interior Designer</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/w1.png') }}" alt="">
								<h4>Bathroom Remodeling </h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/w2.png') }}" alt="">
								<h4>Kitchen Remodeling</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/w1.png') }}" alt="">
								<h4>Air Condition Services </h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/w3.png') }}" alt="">
								<h4>Electricians & Lightings </h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/w4.png') }}" alt="">
								<h4>Home Automation</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/w5.png') }}" alt="">
								<h4>Carpenters for Furnitures & interiors</h4>
							</a>
						</div>
					</div>

				</div>
			</div>

			<div class="requir_salidar_box" id="outdoor_services">
				<h4 class="heding_top">Outdoor Services </h4>
				<div class="owl-carousel">
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/list1.png') }}" alt="">
								<h4>Landscape</h4>
							</a>
							
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/list2.png') }}" alt="">
								<h4>Gardener</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/list3.png') }}" alt="">
								<h4>Artistic Architects </h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/list4.png') }}" alt="">
								<h4>Swimming pool</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/list5.png') }}" alt="">
								<h4>Pavements</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/list6.png') }}" alt="">
								<h4>Fencing</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/list7.png') }}" alt="">
								<h4>Roof Repair</h4>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="requir_salidar_box" id="cleaning_services">
				<h4 class="heding_top">Cleaning Services</h4>
				<div class="owl-carousel">
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/propertiespic4.png') }}" alt="">
								<h4>House Cleaners </h4>
							</a>
							
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/propertiespic5.png') }}" alt="">
								<h4>Bathroom Cleaners</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/propertiespic1.png') }}" alt="">
								<h4>Pest Control</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/propertiespic6.png') }}" alt="">
								<h4>Carpet Cleaners </h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/propertiespic4.png') }}" alt="">
								<h4>Furniture Restoration</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/propertiespic7.png') }}" alt="">
								<h4>Windows & glass cleaners </h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/propertiespic4.png') }}" alt="">
								<h4>Exterior Cleaners </h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/propertiespic1.png') }}" alt="">
								<h4>Stone Cleaners</h4>
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="requir_salidar_box" id="popular_services">
				<h4 class="heding_top">Popular Services</h4>
				<div class="owl-carousel">
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/ga3.png') }}" alt="">
								<h4>House Cleaners</h4>
							</a>
							
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/ga2.png') }}" alt="">
								<h4>Bathroom Cleaners</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/ga3.png') }}" alt="">
								<h4>Air Condition Services</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/ga1.png') }}" alt="">
								<h4>Interior Designer </h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/ga5.png') }}" alt="">
								<h4>Kitchen Remodeling</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/ga6.png') }}" alt="">
								<h4>Electricians & Lightings </h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/ga2.png') }}" alt="">
								<h4>Vaastu Consultant 	</h4>
							</a>
						</div>
					</div>
					<div class="item">
						<div class="requir_salidar_item">
							<a href="#url">
								<img src="{{ url('public/frontend/images/ga3.png') }}" alt="">
								<h4>Carpenters for Furnitures & interiors</h4>
							</a>
						</div>
					</div>
				</div>
			</div>--}}
		</div>
	</div>
</section>

<section class="browse_professionals">
	<div class="container">
		<div class="browse_professionals_inr">
			<h6 class="heding_top">Browse All Professionals</h6>

			@if(!@$categories->isEmpty())
			 @foreach($categories as $category)
			 @if(@$category->subCategoryDetails->isNotEmpty())
			<div class="browse_professionals_bx">
				<h5>{{ @$category->category_name }}</h5>
				<div class="row">
					
					  @foreach(@$category->subCategoryDetails as $subcat)
					<div class="col-md-3">
						<div class="browse_professionals_pan">
							<a href="{{ route('search.service.pro',['slug'=>@$subcat->slug]) }}">{{ @$subcat->name }}</a>
							
						</div>
					</div>
					@endforeach
					 
					{{--<div class="col-md-3">
						<div class="browse_professionals_pan">							
							<a href="#url">Masons</a>
							<a href="#url">Construction workers</a>
							<a href="#url">Building Materials </a>
						</div>
					</div>
					<div class="col-md-3">
						<div class="browse_professionals_pan">
							<a href="#url">Carpenters</a>
							<a href="#url">Welders</a>
							<a href="#url">Electricity connection</a>
						</div>
					</div>
					<div class="col-md-3">
						<div class="browse_professionals_pan">							
							<a href="#url">Water Connection</a>
							<a href="#url">Plumbers</a>
							<a href="#url">Vaastu Consultant </a>
						</div>
					</div>--}}
				</div>
			</div>
			 @endif
			@endforeach
			@endif
			{{--<div class="browse_professionals_bx">
				<h5>Interiors Services </h5>
				<div class="row">
					<div class="col-md-3">
						<div class="browse_professionals_pan">
							<a href="#url">Flooring and slabbing </a>
							<a href="#url">Tiles and Marbles</a>
							<a href="#url">Painters</a>
						</div>
					</div>
					<div class="col-md-3">
						<div class="browse_professionals_pan">							
							<a href="#url">Plumbers</a>
							<a href="#url">Interior Designer</a>
							<a href="#url">Bathroom Remodeling </a>
						</div>
					</div>
					<div class="col-md-3">
						<div class="browse_professionals_pan">
							<a href="#url">Kitchen Remodeling</a>
							<a href="#url">Air Condition Services</a>
							<a href="#url">Electricians & Lightings</a>
						</div>
					</div>
					<div class="col-md-3">
						<div class="browse_professionals_pan">							
							<a href="#url">Home Automation</a>
							<a href="#url">Carpenters for Furnitures & interiors</a>
						</div>
					</div>
				</div>
			</div>
			<div class="browse_professionals_bx">
				<h5>Outdoor Services </h5>
				<div class="row">
					<div class="col-md-3">
						<div class="browse_professionals_pan">
							<a href="#url">Landscape</a>
							<a href="#url">Gardener</a>
							
						</div>
					</div>
					<div class="col-md-3">
						<div class="browse_professionals_pan">
							<a href="#url">Artistic Architects</a>							
							<a href="#url">Swimming pool</a>							
							
						</div>
					</div>
					<div class="col-md-3">
						<div class="browse_professionals_pan">
							<a href="#url">Pavements</a>
							<a href="#url">Fencing</a>
						</div>
					</div>
					<div class="col-md-3">
						<div class="browse_professionals_pan">	

							<a href="#url">Roof Repair</a>				
						</div>
					</div>
				</div>
			</div>
			<div class="browse_professionals_bx">
				<h5>Cleaning Services </h5>
				<div class="row">
					<div class="col-md-3">
						<div class="browse_professionals_pan">
							<a href="#url">House Cleaners </a>
							<a href="#url">Bathroom Cleaners</a>
							
						</div>
					</div>
					<div class="col-md-3">
						<div class="browse_professionals_pan">
							<a href="#url">Pest Control</a>							
							<a href="#url">Carpet Cleaners</a>							
							
						</div>
					</div>
					<div class="col-md-3">
						<div class="browse_professionals_pan">
							<a href="#url">Furniture Restoration </a>
							<a href="#url">Windows & glass cleaners </a>
						</div>
					</div>
					<div class="col-md-3">
						<div class="browse_professionals_pan">
							<a href="#url">Exterior Cleaners</a>				
							<a href="#url">Stone Cleaners</a>				
						</div>
					</div>
				</div>
			</div>
			<div class="browse_professionals_bx">
				<h5>Popular Services </h5>
				<div class="row">
					<div class="col-md-3">
						<div class="browse_professionals_pan">
							<a href="#url">House Cleaners </a>
							<a href="#url">Bathroom Cleaners</a>
							
						</div>
					</div>
					<div class="col-md-3">
						<div class="browse_professionals_pan">
							<a href="#url">Air Condition Services</a>							
							<a href="#url">Interior Designer </a>							
							
						</div>
					</div>
					<div class="col-md-3">
						<div class="browse_professionals_pan">
							<a href="#url">Interior Designer </a>
							<a href="#url">Electricians & Lightings </a>
						</div>
					</div>
					<div class="col-md-3">
						<div class="browse_professionals_pan">
							<a href="#url">Vaastu Consultant</a>	
							<a href="#url">Carpenters for Furnitures & interiors </a>				
						</div>
					</div>
				</div>
			</div>--}}
		</div>
	</div>
</section>
@endsection

@section('footer')
@include('includes.footer')
@include('includes.login_modal')
@endsection

@section('script')
@include('includes.script')
@include('includes.toaster')
<script type="text/javascript">
    $(document).ready(function(){

      $('#states').on('change',function(e){
        e.preventDefault();
        var id = $(this).val();
        $.ajax({
          url:'{{route('get.city')}}',
          type:'GET',
          data:{state:id},
          success:function(data){
            console.log(data);
            $('#city').html(data.city);
          }
        })
      });


     });
  </script>
  {{--<script>
	  function fun(id){

		  var id = $('#subcat'+id).data('id');
		  $('#sub_category').val(id);
		  $('#searchForm').submit();
	  }
  </script>--}}
@endsection

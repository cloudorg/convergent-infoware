@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin | View Availability
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>

@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection

<div class="content-page"> 
    <!-- Start content -->
    <div class="content">
      <div class="container"> 
        
        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title"> Manage Agent <i class="fa fa-chevron-right" aria-hidden="true"></i> Rabin Manna [A12345678]</h4>
            <a href="{{ route('admin.manage.agent') }}" class="rm_new04"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
            <!--<a href="add_class_links.html" class="rm_new04"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Class Links</a>-->
            <!--<ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        
        <div class="row">
          <div class="col-md-12">
          
          
          <div class="tab_stylee">
            <a href="javascript:;" class="tab_btnn01">Profile</a>
          	<a href="javascript:;" class="">View Property</a>
            <a href="javascript:;" class="tab_active">View Availability</a>
            <a href="javascript:;">View Visit Requests</a>
          </div>
          
            <div class="panel panel-default panel-fill">
            
                    <div class="panel-body"> 
                        
                        <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Day</th>
                            <th>Availability Time</th>
                          </tr>
                        </thead>
                        <tbody>
                        
<tr>
<td>Monday</td>
<td><span class="ttm">10:00 am to 11:30 am</span> <span class="ttm">12:00 pm to 1:30 pm</span> <span class="ttm">6:30 pm to 10:00 pm</span></td>
</tr>
              
<tr>
<td>Tuesday</td>
<td><span class="ttm">10:00 am to 11:30 am</span> <span class="ttm">12:00 pm to 1:30 pm</span> <span class="ttm">6:30 pm to 10:00 pm</span></td>
</tr>              
              

<tr>
<td>Wednesday</td>
<td><span class="ttm">10:00 am to 11:30 am</span>  <span class="ttm">6:30 pm to 10:00 pm</span></td>
</tr>
              
<tr>
<td>Thursday</td>
<td><span class="ttm">10:00 am to 11:30 am</span> <span class="ttm">12:00 pm to 1:30 pm</span> <span class="ttm">6:30 pm to 10:00 pm</span></td>
</tr>          
              
              
<tr>
<td>Friday</td>
<td><span class="ttm">10:00 am to 11:30 am</span> <span class="ttm">12:00 pm to 1:30 pm</span>  <span class="ttm">2:00 pm to 2:30 pm</span> <span class="ttm">6:30 pm to 10:00 pm</span></td>
</tr>
              
<tr>
<td>Saturday</td>
<td><span class="ttm">10:00 am to 11:30 am</span> <span class="ttm">12:00 pm to 1:30 pm</span> <span class="ttm">6:30 pm to 10:00 pm</span></td>
</tr>             
                            
<tr>
<td>Sunday</td>
<td><span class="ttm">6:30 pm to 10:00 pm</span></td>
</tr>                           
                          
                         
                      
                        </tbody>
                      </table>
                    </div>
                    
                    
                    
                        
                    </div> 
                </div>
          
            
            
          </div>
        </div>
        <!-- End row --> 
        
      </div>
      <!-- container --> 
      
    </div>
    <!-- content -->
    
  @include('admin.includes.footer')
  </div>

  @section('scripts')

  @include('admin.includes.scripts')

  @endsection
  @endsection
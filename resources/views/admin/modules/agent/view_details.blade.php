@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin | Agent Details
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<style>

  #profileimage img{

       width:200px;
  }

</style>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>

@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection

<div class="content-page">
    <!-- Start content -->
    <div class="content">
      <div class="container">

        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title"> Manage Agents <i class="fa fa-chevron-right" aria-hidden="true"></i> {{ $agent->name }} [{{ $agent->user_id }}]</h4>
            <a href="{{ route('admin.manage.agent') }}" class="rm_new04"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
            <!--<a href="add_class_links.html" class="rm_new04"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Class Links</a>-->
            <!--<ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        @include('admin.includes.message')
        <div class="row">
          <div class="col-md-12">


          <div class="tab_stylee">
          	<a href="{{ route('admin.agent.details',['id'=>@$agent->id]) }}" class="tab_btnn01 tab_active">Profile</a>
            {{-- <a href="{{ route('admin.agent.view.property',['id'=>@$agent->id]) }}">View Property</a>
            <a href="{{ route('admin.agent.availability',['id'=>@$agent->id]) }}">View Availability</a>
            <a href="{{ route('admin.agent.visit.request',['id'=>@$agent->id]) }}">View Visit Requests</a> --}}
            <a href="javascript:;">View Property</a>
            <a href="javascript:;">View Availability</a>
            <a href="javascript:;">View Visit Requests</a>
          </div>

            <div class="panel panel-default panel-fill">

                    <div class="panel-body">

                        <!--<div class="about-info-p rm_new10">
                            <strong>Full Name :</strong>
                            <p class="text-muted">Mark Dicus</p>
                        </div>
                        <div class="about-info-p rm_new10">
                            <strong>User Name :</strong>
                            <p class="text-muted">Abc Username</p>
                        </div>
                        <div class="about-info-p rm_new10">
                            <strong>Email Address :</strong>
                            <p class="text-muted">testemail123@gmail.com</p>
                        </div>-->

                        <div class="agent_ppcc">
                            @if($agent->profile_pic)
                            <img src="{{ url('storage/app/public/profile_picture/'.$agent->profile_pic) }}" alt="">
                            @else
                            <img src="{{ url('public/admin/assets/images/avatar.png') }}" alt="">
                            @endif
                        </div>

                         <div class="about-info-p rm_new08">

                            <p class="text-muted">
                              @if(@$agent->email_mob_change != null)
                              <a href="{{ route('user.email.mob.aproval',['id'=>@$agent->id]) }}" class="btn btn-primary">Approved @if(@$agent->email_mob_change == 'E') Email @elseif(@$agent->email_mob_change == 'M') Mobile @elseif(@$agent->email_mob_change == 'B') Email and Mobile @endif</a>
                               @endif
                            </p>
                        </div>
                        <div class="boxx_002">

                        <div class="about-info-p rm_new08">
                            <strong>Full Name</strong>
                            <p class="text-muted"> : {{ $agent->name }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Email Address</strong>
                            <p class="text-muted"> : {{ $agent->email }}</p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Modified Email</strong>
                            <p class="text-muted"> :

                              @if(@$agent->temp_email != null)
                                {{ @$agent->temp_email }}
                                @endif
                            </p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Phone</strong>
                            <p class="text-muted"> : +91 {{ $agent->mobile_number }}</p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Modified Phone</strong>
                            <p class="text-muted"> :

                              @if(@$agent->temp_mobile != null)
                                {{ @$agent->temp_mobile }}
                                @endif
                            </p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>WhatsApp </strong>
                            <p class="text-muted"> :
                              @if($agent->whatsapp_no != null)
                            +91 {{ $agent->whatsapp_no }}
                             @else

                             @endif
                          </p>
                        </div>


                        <div class="about-info-p rm_new08">
                            <strong>Status</strong>
                             @if($agent->status == 'A')
                            <p class="text-muted"> : Active</p>
                            @elseif($agent->status == 'I')
                            <p class="text-muted"> : Inactive</p>
                            @elseif($agent->status == 'U')
                            <p class="text-muted"> : Unverified</p>
                            @endif
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Sign Up Date</strong>
                            <p class="text-muted"> : {{ date('m.d.Y',strtotime($agent->singup_date)) }}</p>
                        </div>


                        </div>

                        <div class="boxx_002">

                        <div class="about-info-p rm_new08">
                            <strong>Property For</strong>
                              @if($agent->agent_for == 'C')
                            <p class="text-muted"> : Company</p>
                            @elseif($agent->agent_for == 'I')
                            <p class="text-muted"> : Individual</p>
                            @endif
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Company Name</strong>
                            <p class="text-muted"> : {{ $agent->company_name }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Country</strong>
                            <p class="text-muted"> : {{ $country }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>State</strong>
                            <p class="text-muted"> : {{ @$agent->userState->name?$agent->userState->name:'--' }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>City</strong>
                            <p class="text-muted"> : {{@$agent->userCity->name? $agent->userCity->name:'--' }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Address</strong>
                            <p class="text-muted"> : {{ $agent->address }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Website </strong>
                            <p class="text-muted"> : {{ $agent->website }}</p>
                        </div>

                        </div>


                        <div class="about-info-p rm_new08">
                            <strong>About Me</strong>
                            <p class="text-muted">:{{ $agent->about }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Gob Id</strong>
                            <p class="text-muted"> :

                             @if($agent->gov_id_image)

                             <a href="{{ url('storage/app/public/gov_id_image/'.$agent->gov_id_image) }}"
                             style="color: #95bb95; font-size: 17px;" target="_blank" title="view gov ID">
                             <i class="fa fa-eye "></i>
                            </a>

                             @else
                             <span style="color:red;">No gov ID uploaded here</span>
                             @endif

                            </p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>License</strong>
                            <p class="text-muted"> :

                              @if($agent->license_image)

                              <a href="{{ url('storage/app/public/license_image/'.$agent->license_image) }}"
                             style="color: #95bb95; font-size: 17px;" target="_blank" title="view license">
                             <i class="fa fa-eye "></i>
                            </a>

                             @else
                             <span style="color:red;">No License file uploaded here</span>
                             @endif

                            </p>
                        </div>

                    </div>
                </div>



          </div>
        </div>
        <!-- End row -->

      </div>
      <!-- container -->

    </div>
    <!-- content -->

    @include('admin.includes.footer')
  </div>

  @section('scripts')

  @include('admin.includes.scripts')

  @endsection

  @endsection

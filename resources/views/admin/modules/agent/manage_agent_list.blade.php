@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin | Manage Agent
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>

@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection

<div class="content-page">
    <!-- Start content -->
    <div class="content">
      <div class="container">

        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title">Manage Agents</h4>
            <!--<a href="add_category.html" class="rm_new04"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Category </a>
            <ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        @include('admin.includes.message')
        <div class="row">
          <div class="col-md-12">

          <div class="panel panel-default">
          <div class="panel-heading rm02 rm04 rm_new01">
                <form role="form" action="{{ route('admin.manage.agent') }}" method="get">

					<div class="form-group">
                    <label for="">Keyword</label>
                    <input type="text" name="keyword" value="{{ Request::get('keyword') }}" id="" class="form-control" placeholder="Name/Email/Phone/User Id">
					</div>

                   {{-- <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" name="name" value="{{ Request::get('name') }}" id="" class="form-control" placeholder="Enter here">
					</div>

                    <div class="form-group">
                    <label for="">Email</label>
                    <input type="text" name="email" value="{{ Request::get('email') }}" id="" class="form-control" placeholder="Enter here">
					</div>

					<div class="form-group">
                    <label for="">Phone</label>
                    <input type="text" name="phone" value="{{ Request::get('phone') }}" id="" class="form-control" placeholder="Enter here">
					</div>--}}
          {{-- <div class="form-group">
                    <label for="">Location</label>
                    <input type="text" name="location" value="{{ Request::get('location') }}" id="" class="form-control" placeholder="City/State">
					</div> --}}

                    <div class="form-group">
                        <label for="">Country</label>
                        <select class="form-control rm06" name="country" id="country">
                            <option value="">Select country</option>
                            @foreach($country as $cnt)
                            <option value="{{ $cnt->id }}" @if(@$cnt->id == Request::get('country') ) selected @endif>{{ $cnt->name }}</option>
                            @endforeach
                        </select>
					</div>
                    <div class="form-group">
                        <label for="state">State</label>
                        <select class="form-control rm06" name="state" id="state">
                            <option value="">Select state</option>
                            @foreach($states as $state)
                            <option value="{{ $state->id }}" @if(@$state->id == Request::get('state') ) selected @endif>{{ $state->name }}</option>
                            @endforeach
                        </select>
					</div>
                    <div class="form-group">
                        <label for="city">City</label>
                        <select class="form-control rm06" name="city" id="city">
                            <option value="">Select city</option>
                            @foreach($cites as $city)
                            <option value="{{ $city->id }}" @if(@$city->id == Request::get('city') ) selected @endif>{{ $city->name }}</option>
                            @endforeach
                        </select>
					</div>

                    <div class="form-group">
                    <label for="">Status</label>
                    <select class="form-control rm06" name="status">
                        <option value="">Select</option>
                        <option value="U" @if(Request::get('status') == 'U') selected @endif>Unverified</option>
                        <option value="A" @if(Request::get('status') == 'A') selected @endif>Active</option>
                        <option value="I" @if(Request::get('status') == 'I') selected @endif>Inactive</option>
                    </select>
                   </div>

                 <div class="form-group rm_new14">
                    <label for="">Sign Up From Date </label>
                    <input type="text" name="from_date" value="{{ Request::get('from_date') }}" id="datepicker1" class=" form-control calander_icn " placeholder="Select" autocomplete="off">
                  </div>
                  <div class="form-group rm_new14">
                    <label for="">Sign Up To Date</label>
                    <input type="text" name="to_date" value="{{ Request::get('to_date') }}"  id="datepicker"  class=" form-control calander_icn" placeholder="Select" autocomplete="off">
                  </div>

                  <div class="form-group">
                    <label for="">Sign Up Using</label>
                    <select class="form-control rm06" name="signup_from">
                        <option value="">Select</option>
                        <option value="E" @if(Request::get('signup_from') == 'E') selected @endif>Email</option>
                        <option value="G" @if(Request::get('signup_from') == 'G') selected @endif>Google</option>
                        <option value="F" @if(Request::get('signup_from') == 'F') selected @endif>Facebook</option>
                    </select>
                   </div>
                   <div class="form-group">
                    <label for="">Approval Required</label>
                    <select class="form-control rm06" name="approval_status">
                        <option value="">Select</option>
                        <option value="E" @if(Request::get('approval_status') == 'E') selected @endif>Email Change</option>
                        <option value="M" @if(Request::get('approval_status') == 'M') selected @endif>Mobile Change</option>
                        <option value="B" @if(Request::get('approval_status') == 'B') selected @endif>Both</option>
                    </select>
                   </div>
                  <!--
                <div class="form-group">
                    <label for="">Subject</label>
                    <select class="form-control rm06">
                        <option>Select</option>
                        <option>History</option>
                        <option>Geography</option>
                        <option>Mathematics</option>
                        <option>Life Science</option>
                        <option>Physical Science</option>
                        <option>English</option>
                    </select>
                </div>-->

                  <div class="rm05">
                    <button class="btn btn-primary waves-effect waves-light w-md" type="submit">Search</button>
                    <a href="{{ route('admin.manage.agent') }}" class="btn btn-success waves-effect waves-light w-md">Reset</a>
                  </div>
                </form>
              </div>
          </div>


            <div class="panel panel-default">


              <div class="panel-body">
                <div class="row">
                  <form action="{{ route('admin.select.agent.delete') }}" method="post" id="deleteForm">
                    @csrf
                    @if(count(@$agents)>0)
                    <input type="submit" name="" id="" class="btn btn-sm btn-success" value="Delete Selected Agent"><br>
                    <label id="allid[]-error" class="error" for="allid[]" style="display: none"></label>
                    @endif
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th></th>
                            <th>Name</th>
                            <th>User ID</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Sign Up Date</th>
                            <th>Sign Up Using</th>
                            <th>Status</th>
                            <th>Approval Status</th>
                            <th>Last Login</th>
                            <th class="rm07">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                         @if(count($agents)>0)
                           @foreach($agents as $agent)
                          <tr>
                            <td>
                            <div class="">
							             <input type="checkbox" id="deleteId{{ @$agent->id }}"  value="{{@$agent->id}}" name="allid[]">
					                	</div>
                            </td>
                            <td>{{ $agent->name }}</td>
                            <td>{{ @$agent->user_id }}</td>
                            <td>{{ $agent->email }}</td>
                            <td>{{ $agent->mobile_number }}</td>
                            <td>{{  $agent->created_at->format('m/d/Y') }}</td>
                            <td>
                            @if($agent->signup_from == 'G')
                               Google
                               @elseif($agent->signup_from == 'F')
                                 Facebook
                                 @elseif($agent->signup_from == 'E')
                                 Email

                                 @endif
                            </td>
                            <td class="status{{ @$agent->id }}">
                                @if($agent->status == 'A')

                                Active

                                @elseif($agent->status == 'I')

                                 Inactive

                                 @elseif($agent->status == 'U')

                                 Unverified
                                @endif
                            </td>
                            <td class="approval_status{{ @$agent->id }}">
                               @if($agent->approval_status == 'Y')

                                Yes

                                @elseif($agent->approval_status == 'N')

                                 No

                                 @else

                                 None
                                @endif
                            </td>
                            <td>{{ @$agent->last_login?date('m/d/Y',strtotime(@$agent->last_login)):'--' }}</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action1" onclick="fun({{ @$agent->id }})"><img src="{{ url('public/admin/assets/images/action-dots.png') }}" alt=""></a>
                            <div class="show-actions" id="show-action{{ @$agent->id }}" style="display: none;">
                                <span class="angle"><img src="{{ url('public/admin/assets//images/angle.png') }}" alt=""></span>
                                <ul>
                                    <li><a href="{{ route('admin.agent.details',@$agent->id) }}">View</a></li>
                                    <li><a href="{{ route('admin.edit.agent.profile',@$agent->id) }}">Edit</a></li>
                                    <li>
                                        @if($agent->status == 'A')
                                        <a href="javascript:void(0)" class="status_change" data-id="{{ $agent->id }}"><span class="text_change{{ $agent->id }}">Make Inactive</span></a>
                                        @elseif($agent->status == 'I')
                                        <a href="javascript:void(0)" class="status_change" data-id="{{ $agent->id }}"><span class="text_change{{ $agent->id }}">Make Active</span></a>
                                        @endif
                                    </li>
                                    <li>
                                    @if($agent->approval_status == 'N')
                                        <a href="javascript:void(0)" class="approval_status_change" data-id="{{ $agent->id }}" onclick="return confirm('Are you want to approve this user?')"><span class="approve_text_change{{ @$agent->id }}">Make Approval</span></a>
                                       @endif
                                    </li>
                                    <li><a href="{{ route('admin.delete.agent.profile',@$agent->id) }}" onclick="return confirm('Are you want to delete this user?')">Delete</a></li>
                                </ul>
                              </div>
                            </td>
                          </tr>
                          @endforeach
                           @else
                           <tr role="row" class="odd">
                                        <td colspan="10" style="text-align:center;">
                                            No Data Found
                                        </td>
                                    </tr>
                            @endif
                          {{--<tr>
                            <td>Abhijeet Roy</td>
                            <td>sample-test123@gmail.com</td>
                            <td>+91 9876543210</td>
                            <td>10.11.2021</td>
                            <td>Unblock</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action2"><img src="assets/images/action-dots.png" alt=""></a>
                            <div class="show-actions" id="show-action2" style="display: none;">
                                <span class="angle"><img src="assets//images/angle.png" alt=""></span>
                                <ul>
                                    <li><a href="#">View</a></li>
                                    <li><a href="#">Edit</a></li>
                                    <li><a href="#">Block</a></li>
                                </ul>
                              </div>
                            </td>
                          </tr>

                          <tr>
                            <td>Rabin Das</td>
                            <td>test123@gmail.com</td>
                            <td>+91 1234567890</td>
                            <td>14.10.2021</td>
                            <td>Block</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action3"><img src="assets/images/action-dots.png" alt=""></a>
                            <div class="show-actions" id="show-action3" style="display: none;">
                                <span class="angle"><img src="assets//images/angle.png" alt=""></span>
                                <ul>
                                    <li><a href="#">View</a></li>
                                    <li><a href="#">Edit</a></li>
                                    <li>

                                        <a href="#">Unblock</a>
                                     </li>
                                </ul>
                              </div>
                            </td>
                          </tr>

                          <tr>
                            <td>Abhijeet Roy</td>
                            <td>sample-test123@gmail.com</td>
                            <td>+91 9876543210</td>
                            <td>10.11.2021</td>
                            <td>Unblock</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action4"><img src="assets/images/action-dots.png" alt=""></a>
                            <div class="show-actions" id="show-action4" style="display: none;">
                                <span class="angle"><img src="assets//images/angle.png" alt=""></span>
                                <ul>
                                    <li><a href="#">View</a></li>
                                    <li><a href="#">Edit</a></li>
                                    <li><a href="#">Block</a></li>
                                </ul>
                              </div>
                            </td>
                          </tr>--}}


                        </tbody>
                      </table>
                    </div>
                    <div style="float: right;">{{@$agents->appends(request()->except(['page', '_token']))->links()}}</div>

                    <!-- <ul class="pagination">
                      <li class="paginate_button previous disabled"><a href="#">Previous</a></li>
                      <li class="paginate_button active"><a href="#">1</a></li>
                      <li class="paginate_button"><a href="#">2</a></li>
                      <li class="paginate_button"><a href="#">3</a></li>
                      <li class="paginate_button"><a href="#">4</a></li>
                      <li class="paginate_button"><a href="#">5</a></li>
                      <li class="paginate_button"><a href="#">6</a></li>
                      <li class="paginate_button next"><a href="#">Next</a></li>
                    </ul> -->


                  </div>
                  </form>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- End row -->

      </div>
      <!-- container -->

    </div>
    <!-- content -->

    @include('admin.includes.footer')
  </div>

  @section('scripts')

  @include('admin.includes.scripts')
  
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script> -->
  <script>
    $("#datepicker1").datepicker({
      numberOfMonths: 1,
      onSelect: function (selected) {
        var dt = new Date(selected);
        dt.setDate(dt.getDate() + 1);
        $("#datepicker").datepicker("option", "minDate", dt);
      },
      maxDate: new Date("{{ date('m/d/Y') }}")
    });
    $("#datepicker").datepicker({
      numberOfMonths: 1,
      onSelect: function (selected) {
        var dt = new Date(selected);
        dt.setDate(dt.getDate() - 1);
        $("#datepicker1").datepicker("option", "maxDate", dt);
      },
    });

  </script>
  <script>
      function fun(id){

             $('.show-actions').slideUp();
              $("#show-action"+id).show();
      }
      
       $(document).on('click', function () {
      var $target = $(event.target);
      if (!$target.closest('.action-dots').length && $('.show-actions').is(":visible")) {
          $('.show-actions').slideUp();
        }
    });

 $(document).on('click','.status_change', function(e){

var id = $(e.currentTarget).attr('data-id');

  if(confirm('Are you want to change this status?')){

       $.ajax({

            url: '{{ route('agent.status.update') }}',
            method: 'GET',
            data: 'id='+id,

            success: function(resp){
                 $('.text_change'+id).html('');
                  if(resp != 0){

                      var str = "";
                     if(resp.status == 'A'){
                         str = "Active"
                        $(".success_msg").html("Success! Status is changed to Active!");
                        $('.text_change'+id).html('Make Inactive');

                     }else if(resp.status == 'I'){
                        str = "Inactive";
                      $(".success_msg").html("Success! Status is changed to Inactive!");
                      $('.text_change'+id).html('Make Active');

                     }
                     $('.status'+id).html(str);
                      $(".success_msg_div").show();

                     $(".error_msg_div").hide();


                  }else{

                    $(".error_msg_div").show();

                      $(".success_msg_div").hide();

                      $(".error_msg").html("Something went wrong!");
                  }


            },
            error: function(error){

               consolse.log(error);
            }
       });
  }


});

$(document).on('click','.approval_status_change', function(e){

var id = $(e.currentTarget).attr('data-id');

  if(confirm('Are you want to change this status?')){

       $.ajax({

            url: '{{ route('agent.approval.status.update') }}',
            method: 'GET',
            data: 'id='+id,

            success: function(resp){
                 $('.approve_text_change'+id).html('');
                  if(resp != 0){

                      var str = "";
                     if(resp.approval_status == 'Y'){
                         str = "Yes"
                        $(".success_msg").html("Success! Status is changed to Approved!");
                        $('.approve_text_change'+id).css('display','none');

                     }
                     $('.approval_status'+id).html(str);
                      $(".success_msg_div").show();

                     $(".error_msg_div").hide();


                  }else{

                    $(".error_msg_div").show();

                      $(".success_msg_div").hide();

                      $(".error_msg").html("Something went wrong!");
                  }


            },
            error: function(error){

               consolse.log(error);
            }
       });
  }


});

 </script>

<script>


$(document).ready(function(){

     $('#deleteForm').validate({

           rules: {

                'allid[]': {

                    required: true,
                },
           },
           messages: {

               'allid[]': {

                   required: "Please select at least one Agent",
               },
           }
     });
});




</script>
<script type="text/javascript">
    $(document).ready(function(){
      $('#country').on('change',function(e){
        e.preventDefault();
        var id = $(this).val();

        $.ajax({
          url:'{{route('get.state')}}',
          type:'GET',
          data:{country:id,id:'{{@$user->state}}'},
          success:function(data){
            console.log(data);
            $('#state').html(data.state);
          }
        })
      });

      $('#state').on('change',function(e){
        e.preventDefault();
        var id = $(this).val();
        $.ajax({
          url:'{{route('get.city')}}',
          type:'GET',
          data:{state:id,id:'{{@$user->city}}'},
          success:function(data){
            console.log(data);
            $('#city').html(data.city);
          }
        })
      });

    });
</script>
@endsection

  @endsection

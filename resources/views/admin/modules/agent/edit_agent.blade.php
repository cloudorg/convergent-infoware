@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin | Edit Agent Profile
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<style>
    label.error{

         color:red;
    }

    .uplodimg_pick img {
    width: 50px;
    height: 50px;
    object-fit: cover;
    border-radius: 100%;
}
</style>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>
<link href="{{ URL::asset('public/frontend/croppie/croppie.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('public/frontend/croppie/croppie.min.css') }}" rel="stylesheet" />
@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection

<div class="content-page">
    <!-- Start content -->
    <div class="content">
      <div class="container">

        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title">Edit Agent Profile</h4>
            <a href="{{ route('admin.manage.agent') }}" class="rm_new04"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
            <!--<ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        @include('admin.includes.message')
        <div class="row">
          <div class="col-md-12">




            <div class="panel panel-default">


              <div class="panel-body rm02 rm04">
            <form role="form" action="{{ route('admin.edit.agent.profile.save') }}" method="post" enctype="multipart/form-data" id="addForm">
                @csrf
                <input type="hidden" name="agentId" id="" value="{{ $agent->id }}">
                <div class="col-md-6 m-b-15">
                    @php
                     $split_name = explode(' ',$agent->name);
                     $fname = $split_name[0];
                     $lname = $split_name[1];
                    @endphp
                    <label for="">First Name</label>
                    <input type="text" name="first_name" value="{{ $fname }}"  placeholder="Enter here" class="form-control">
                </div>
                <div class="col-md-6 m-b-15">
                    <label for="">Last Name</label>
                    <input type="text" name="last_name" value="{{ $lname }}"  placeholder="Enter here" class="form-control">
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6 m-b-15">
                    <label for="">Email</label>
                    <input type="text" name="email" value="{{ $agent->email }}"  placeholder="Enter here" class="form-control" disabled>
                </div>
                <div class="col-md-6 m-b-15">
                    <label for="">Mobile Number</label>
                    <input type="text" name="phone" value="{{ $agent->mobile_number }}"  placeholder="Enter here" class="form-control" disabled>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6 m-b-15">
                    <label for="">Website</label>
                    <input type="url" name="website" value="{{ @$agent->website }}" placeholder="Enter here" class="form-control">
                </div>
                <div class="col-md-6 m-b-15">
                    <label for="">WhatsApp</label>
                    <input type="text" name="whatsapp_no" value="{{ @$agent->whatsapp_no }}" placeholder="Enter here" class="form-control" maxlength="10">
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6 m-b-15">
                    <div class="das_input malti_select language_kn">
                       <label>Languages Knows</label>
                       <span class="autocomplete-select"></span>
                       <label id="language-error" class="error" for="language" style="display: none;"></label>
                    </div>
                 </div>
                <div class="col-md-6 m-b-15">
                <label for="">Country</label>
                    <select class="form-control rm06" name="country" id="country">

                        <option value="">Select Country</option>
                        @foreach($allCountry as $cnt)
                        <option value="{{ $cnt->id }}" @if($agent->country == $cnt->id) selected @endif>{{ $cnt->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="clearfix"></div>
                {{-- <div class="col-md-6 m-b-15">
                    <label for="">State</label>
                    <input type="text" name="state" value="{{ @$agent->state }}"  placeholder="Enter here" class="form-control">
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6 m-b-15">
                    <label for="">City</label>
                    <input type="text" name="city" value="{{ @$agent->city }}"  placeholder="Enter here" class="form-control">
                </div> --}}

                <div class="col-md-6 m-b-15">
                    <label>State</label>
                    <select name="state" id="states" class="form-control rm06 required">
                        <option value="">Select State</option>
                        @foreach(@$states as $state)
                        <option value="{{@$state->id}}" @if(@$agent->state==@$state->id)selected @endif>{{@$state->name}}</option>
                        @endforeach
                    </select>
                </div>
                {{-- <div class="clearfix"></div> --}}
                <div class="col-md-6 m-b-15">
                    <label>City</label>
                    <select name="city" id="city" class="form-control rm06 required">
                        <option value="">Select City</option>
                        @foreach(@$cites as $city)
                        <option value="{{@$city->id}}" @if(@$agent->city==@$city->id)selected @endif>{{@$city->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6 m-b-15">
                    <label for="">Address</label>
                    <input type="text" name="address" value="{{ @$agent->address }}"  placeholder="Enter here" class="form-control">
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 m-b-15">
					<div class="das_input">
					<label>About Me </label>
					<textarea placeholder="Enter here.." name="about" rows="5" class="required form-control" >{!! @$agent->about !!}</textarea>

					</div>
				</div>

                <div class="col-md-6 m-b-15">
                <div class="uplodimg">
                    <div class="uplodimgfil">
                        <b>License (Allowed files - images, pdf, docx )</b>
                        <input type="file" name="license" id="license" class="inputfile inputfile-1" />
                        <label for="license">Upload License <img src="{{ url('public/admin/assets/images/clickhe.png') }}" alt=""></label>
                        <label id="license-error" class="error" for="license" style="display: none"></label>
                    </div>

                </div>
                <span class="filename"></span>
            </div>

            <div class="clearfix"></div>
            <div class="col-md-6 m-b-15">
                <div class="uplodimg">
                    <div class="uplodimgfil">
                        <b>Gov ID (Allowed files - images, pdf, docx )</b>
                        <input type="file" name="gov_id" id="gov_id" class="inputfile inputfile-1" />
                        <label for="gov_id">Upload Id <img src="{{ url('public/admin/assets/images/clickhe.png')}}" alt=""></label>
                        <label id="gov_id-error" class="error" for="gov_id" style="display: none"></label>
                    </div>

                </div>
                <span class="filename1"></span>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6 m-b-15">
                <div class="uplodimg">

               <div class="uplodimgfil">
                    <b>Upload Photo</b>
                    <input type="hidden" name="profile_picture" id="profile_picture">
                    <input type="file" name="file" id="file-11" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />

                    <label for="file-11">Upload Picture <img src="{{ url('public/admin/assets/images/clickhe.png') }}" alt=""></label>

                </div>

                <div class="uplodimg_pick uplodimgfilimg">
                    @if(@$agent->profile_pic != null)
                    <img src="{{ URL::to('storage/app/public/profile_picture')}}/{{@$agent->profile_pic}}" alt="" id="img2">
                    @else
                    <img src="{{ URL::to('public/frontend/images/avatar.png')}}" alt="" id="img2">
                    @endif
                </div>

                   </div>
                   <label id="file-error" class="error" for="file-11"></label>
                 </div>

                <!--<div class="form-group">
                    <label for="">Class</label>
                    <select class="form-control rm06">
                        <option>Select</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                        <option>8</option>
                        <option>9</option>
                        <option>10</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Subject</label>
                    <select class="form-control rm06">
                        <option>Select</option>
                        <option>History</option>
                        <option>Geography</option>
                        <option>Mathematics</option>
                        <option>Life Science</option>
                        <option>Physical Science</option>
                        <option>English</option>
                    </select>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <label for="">Deadline</label>
                    <input type="text" placeholder="Select" id="datepicker" class="form-control calander_icn">
                </div>

                <div class="clearfix"></div>
               <div class="form-group">
                   <label for="">Assignments File Upload (pdf)</label>
                    <div class="fileUpload btn btn-primary cust_file clearfix">
                        <span class="upld_txt"><i class="fa fa-upload upld-icon" aria-hidden="true"></i>Click here to Upload File</span>
                        <input type="file" class="upload">
                    </div>
                </div>
                -->
                <input type="hidden" name="language" id="language">


                <div class="col-lg-12"> <button class="btn btn-primary waves-effect waves-light w-md rm_new15" type="submit">Save Change</button></div>
            </form>
			</div>
            </div>

          </div>
        </div>
        <!-- End row -->

      </div>
      <!-- container -->

    </div>
    <!-- content -->

    <div class="modal" tabindex="-1" role="dialog" id="croppie-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Crop Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="croppie-div" style="width: 100%;"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="crop-img">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

    @include('admin.includes.footer')
</div>
@section('scripts')

@include('admin.includes.scripts')
<script src="{{ URL::asset('public/frontend/croppie/croppie.js') }}"></script>

<script>
$(document).ready(function(){
    jQuery.validator.addMethod("validate_word", function(value, element) {
        if (/^([a-zA-Z ])+$/.test(value)) {
            return true;
        } else {
            return false;
        }
    }, "Not allow special characters or numbers");
    jQuery.validator.addMethod("validate_address", function(value, element) {
        if (/^([a-zA-Z0-9./_ ,-])+$/.test(value)) {
            return true;
        } else {
            return false;
        }
    }, "Allow only (a-z, A-Z, 0-9, ./_ ,-)");
    jQuery.validator.addMethod("validate_about", function(value, element) {
        if (/^([a-zA-Z0-9./_ ,-])+$/.test(value)) {
            return true;
        } else {
            return false;
        }
    }, "Allow only (a-z, A-Z, 0-9, ./_ ,-)");
    $('#addForm').validate({
        rules: {
            first_name: {
                required: true,
                validate_word:true,
            },last_name: {
                required: true,
                validate_word:true,
            },
            country: {
                required: true,
            },
            state: {
                required: true,
            },
            whatsapp_no: {
                required:true,
                digits: true ,
                minlength: 10,
                maxlength: 10,
            },
            state:{
                required:true,
            },
            city:{
                required:true,
            },
            address: {
                validate_address:true,
            },
            about:{
                validate_about:true,
            },
            file: {
                accept: "image/jpg,image/jpeg,image/png"
            },
            gov_id:{
                extension: "docx|doc|pdf|jpg|png|jpeg|doc|gif",
            },
            license:{
                extension: "docx|doc|pdf|jpg|png|jpeg|doc|gif",
            },
        },
        messages: {
            file: {
                accept: 'You must be select jpg,jpeg and png file'
            },
            country:{
                required:"Select a country",
            },
            state:{
                required:"Select a state",
            },
            city:{
                required:"Select a city",
            },
            whatsapp_no:{
                required: 'Enter whatsApp Number',
                digits: 'Please enter a valid number ',
                minlength: 'Exactly only 10 digits without country code',
                maxlength: 'Exactly only 10 digits without country code',
            },
            gov_id:{
                extension: "Only allow  pdf, doc, docx, jpeg, gif, png",
            },
            license:{
                extension: "Only allow pdf, doc, docx, jpeg, gif, png",
            },
        },
        ignore: [],
        submitHandler:function(form){
               var language = $('#language').val();
                if(language == ''){
                   $('#language-error').html('Minimum one language required');
                   $('#language-error').css('display', 'block');
                   return false;
               }else{
                   form.submit();
               }
           }
    });
});

    //   var reader = new FileReader();
    //     reader.onload = function (e) {
	// 		$(".img-preview").html('<img src='+e.target.result+' width="100"/>');
    //     }

    //     function readURL(input) {
    //         if (input.files && input.files[0]) {
    //             reader.readAsDataURL(input.files[0]);
    //         }
    //     }

    //     $("#file-11").change(function(){
    //         readURL(this);
    //     });

    $(document).ready(function(){


           $('#license').change(function(event){

                 var x = event.target.files[0].name;
                 $('.filename').text(x);
           });

            $('#gov_id').change(function(event){

                 var x = event.target.files[0].name;
                 $('.filename1').text(x);
           });
    });
  </script>

<script>
    function dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]),
        n = bstr.length,
        u8arr = new Uint8Array(n);
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, {type:mime});
    }
    var uploadCrop;
    $(document).ready(function(){
        $('#croppie-modal').on('hidden.bs.modal', function() {
            uploadCrop.croppie('destroy');
        });

        $('#crop-img').click(function() {
            uploadCrop.croppie('result', {
                type: 'base64',
                format: 'png'
            }).then(function(base64Str) {
                $("#croppie-modal").modal("hide");
               //  $('.lds-spinner').show();
               let file = dataURLtoFile('data:text/plain;'+base64Str+',aGVsbG8gd29ybGQ=','hello.png');
                  console.log(file.mozFullPath);
                  console.log(base64Str);
                  $('#profile_picture').val(base64Str);
               // $.each(file, function(i, f) {
                    var reader = new FileReader();
                    reader.onload = function(e){
                        $('.uplodimgfilimg').append('<em><img  src="' + e.target.result + '"><em>');
                    };
                    reader.readAsDataURL(file);

               //  });
                $('.uplodimgfilimg').show();

            });
        });
    });
    $("#file-11").change(function () {
            $('.uplodimgfilimg').html('');
            let files = this.files;
            console.log(files);
            let img  = new Image();
            if (files.length > 0) {
                let exts = ['image/jpeg', 'image/png', 'image/gif'];
                let valid = true;
                $.each(files, function(i, f) {
                    if (exts.indexOf(f.type) <= -1) {
                        valid = false;
                        return false;
                    }
                });
                if (! valid) {
                    alert('Please choose valid image files (jpeg, png, gif) only.');
                    $("#file-11").val('');
                    return false;
                }
                // img.src = window.URL.createObjectURL(event.target.files[0])
                // img.onload = function () {
                //     if(this.width > 250 || this.height >160) {
                //         flag=0;
                //         alert('Please upload proper image size less then : 250px x 160px');
                //         $("#file").val('');
                //         $('.uploadImg').hide();
                //         return false;
                //     }
                // };
                $("#croppie-modal").modal("show");
                uploadCrop = $('.croppie-div').croppie({
                    viewport: { width: 256, height: 256, type: 'square' },
                    boundary: { width: $(".croppie-div").width(), height: 400 }
                });
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.upload-demo').addClass('ready');
                    // console.log(e.target.result)
                    uploadCrop.croppie('bind', {
                        url: e.target.result
                    }).then(function(){
                        console.log('jQuery bind complete');
                    });
                }
                reader.readAsDataURL(this.files[0]);
               //  $('.uploadImg').append('<img width="100"  src="' + reader.readAsDataURL(this.files[0]) + '">');
               //  $.each(files, function(i, f) {
               //      var reader = new FileReader();
               //      reader.onload = function(e){
               //          $('.uploadImg').append('<img width="100"  src="' + e.target.result + '">');
               //      };
               //      reader.readAsDataURL(f);
               //  });
               //  $('.uploadImg').show();
            }

        });
</script>
<script type="text/javascript">
$(document).ready(function() {
    /*all language data push in array*/
    var language = [];
    @foreach($allLanguage as $language )
    language.push({ label : '{{ $language->name }}', value : '{{ $language->id }}'});
    @endforeach
    console.log(language);
    /*end*/

    /*user language data push in array*/
    var languagevalue= [];
    @foreach(@$allUserLanguage as $languages )
    languagevalue.push('{{ $languages->language_id }}');
    @endforeach
    console.log(languagevalue);
    /*end*/
    $('#language').val(languagevalue);




    var autocomplete = new SelectPure(".autocomplete-select", {
        options: language,
        value: languagevalue,
        multiple: true,
        autocomplete: true,
        icon: "icofont-close-line",
        onChange: value => {
            console.log(value);
            $('#language').val(value);
            if(value==''){
                $('#language-error').html('Minimum one language required');
                $('#language-error').css('display', 'block');
            }else{
                $('#language-error').css('display', 'none');
            }
        },
        classNames: {
            select: "select-pure__select",
            dropdownShown: "select-pure__select--opened",
            multiselect: "select-pure__select--multiple",
            label: "select-pure__label",
            placeholder: "select-pure__placeholder",
            dropdown: "select-pure__options",
            option: "select-pure__option",
            autocompleteInput: "select-pure__autocomplete",
            selectedLabel: "select-pure__selected-label",
            selectedOption: "select-pure__option--selected",
            placeholderHidden: "select-pure__placeholder--hidden",
            optionHidden: "select-pure__option--hidden",
        }
    });
    var resetAutocomplete = function() {
        autocomplete.reset();
    };
});
    $(document).ready(function(){
      $('#country').on('change',function(e){
        e.preventDefault();
        var id = $(this).val();

        $.ajax({
          url:'{{route('get.state')}}',
          type:'GET',
          data:{country:id,id:'{{@$user->state}}'},
          success:function(data){
            console.log(data);
            $('#states').html(data.state);
          }
        })
      });

      $('#states').on('change',function(e){
        e.preventDefault();
        var id = $(this).val();
        $.ajax({
          url:'{{route('get.city')}}',
          type:'GET',
          data:{state:id,id:'{{@$user->city}}'},
          success:function(data){
            console.log(data);
            $('#city').html(data.city);
          }
        })
      });

    });
</script>
@endsection
@endsection

@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin | Add sub Category
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<style>
    label.error{

         color:red;
    }
</style>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>
<link href="{{ URL::asset('public/frontend/croppie/croppie.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('public/frontend/croppie/croppie.min.css') }}" rel="stylesheet" />
@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection

<div class="content-page"> 
    <!-- Start content -->
    <div class="content">
      <div class="container"> 
        
        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title">Pro Add Sub Category</h4>
            <a href="{{ route('admin.manage.sub.category') }}" class="rm_new04"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
            <!--<ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        @include('admin.includes.message')
        <div class="row">
          <div class="col-md-12">
          
          
          
          
            <div class="panel panel-default">
              
              
              <div class="panel-body rm02 rm04"> 
            <form role="form" action="{{ route('admin.add.sub.category') }}" method="post" id="addForm" enctype="multipart/form-data">
                @csrf
                <div class="col-md-6 m-b-15">
                    <label for="">Category</label>
                    <select name="category" id="category" class="form-control rm06 required">
                        <option value="">Select Category</option>
                        @foreach(@$category as $cat)
                        <option value="{{@$cat->id}}" >{{@$cat->category_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6 m-b-15">
                    <label for="">Sub Category</label>
                    <input type="text" name="sub_category" value="" id="sub_category" placeholder="Enter here" class="form-control">
                </div>
                
                <div class="clearfix"></div>
                <div class="col-md-6 m-b-15">
                <div class="uplodimg">

                    <div class="uplodimgfil">
                        <b>Upload Photo</b>
                        <input type="hidden" name="category_image" id="category_image">
                        <input type="file" name="file" id="file-11" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />

                        <label for="file-11">Upload Picture <img src="{{ url('public/admin/assets/images/clickhe.png') }}" alt=""></label>

                    </div>

                    <div class="uplodimg_pick uplodimgfilimg">
                       
                    </div>

                    </div>
                    <label id="file-error" class="error" for="file-11"></label>
                </div>

                <!--<div class="form-group">
                    <label for="">Class</label>
                    <select class="form-control rm06">
                        <option>Select</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                        <option>8</option>
                        <option>9</option>
                        <option>10</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Subject</label>
                    <select class="form-control rm06">
                        <option>Select</option>
                        <option>History</option>
                        <option>Geography</option>
                        <option>Mathematics</option>
                        <option>Life Science</option>
                        <option>Physical Science</option>
                        <option>English</option>
                    </select>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <label for="">Deadline</label>
                    <input type="text" placeholder="Select" id="datepicker" class="form-control calander_icn">
                </div>
                
                <div class="clearfix"></div>
               <div class="form-group">
                   <label for="">Assignments File Upload (pdf)</label>
                    <div class="fileUpload btn btn-primary cust_file clearfix">
                        <span class="upld_txt"><i class="fa fa-upload upld-icon" aria-hidden="true"></i>Click here to Upload File</span>
                        <input type="file" class="upload">
                    </div> 
                </div>
                -->
                
                
                
                
                <div class="clearfix"></div>
                <div class="col-lg-12"> <button class="btn btn-primary waves-effect waves-light w-md rm_new15" type="submit">Save Sub Category</button></div>
            </form>
			</div>
            </div>
            
          </div>
        </div>
        <!-- End row --> 
        
      </div>
      <!-- container --> 
      
    </div>
    <!-- content -->

    <div class="modal" tabindex="-1" role="dialog" id="croppie-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Crop Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="croppie-div" style="width: 100%;"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="crop-img">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
    
    @include('admin.includes.footer')
  </div>

  @section('scripts')

  @include('admin.includes.scripts')
  <script src="{{ URL::asset('public/frontend/croppie/croppie.js') }}"></script>

<script>
    function dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]),
        n = bstr.length,
        u8arr = new Uint8Array(n);
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, {type:mime});
    }
    var uploadCrop;
    $(document).ready(function(){
        $('#croppie-modal').on('hidden.bs.modal', function() {
            uploadCrop.croppie('destroy');
        });

        $('#crop-img').click(function() {
            uploadCrop.croppie('result', {
                type: 'base64',
                format: 'png'
            }).then(function(base64Str) {
                $("#croppie-modal").modal("hide");
               //  $('.lds-spinner').show();
               let file = dataURLtoFile('data:text/plain;'+base64Str+',aGVsbG8gd29ybGQ=','hello.png');
                  console.log(file.mozFullPath);
                  console.log(base64Str);
                  $('#category_image').val(base64Str);
               // $.each(file, function(i, f) {
                    var reader = new FileReader();
                    reader.onload = function(e){
                        $('.uplodimgfilimg').append('<em><img  src="' + e.target.result + '"><em>');
                    };
                    reader.readAsDataURL(file);

               //  });
                $('.uplodimgfilimg').show();

            });
        });
    });
    $("#file-11").change(function () {
            $('.uplodimgfilimg').html('');
            let files = this.files;
            console.log(files);
            let img  = new Image();
            if (files.length > 0) {
                let exts = ['image/jpeg', 'image/png', 'image/gif'];
                let valid = true;
                $.each(files, function(i, f) {
                    if (exts.indexOf(f.type) <= -1) {
                        valid = false;
                        return false;
                    }
                });
                if (! valid) {
                    alert('Please choose valid image files (jpeg, png, gif) only.');
                    $("#file-11").val('');
                    return false;
                }
                // img.src = window.URL.createObjectURL(event.target.files[0])
                // img.onload = function () {
                //     if(this.width > 250 || this.height >160) {
                //         flag=0;
                //         alert('Please upload proper image size less then : 250px x 160px');
                //         $("#file").val('');
                //         $('.uploadImg').hide();
                //         return false;
                //     }
                // };
                $("#croppie-modal").modal("show");
                uploadCrop = $('.croppie-div').croppie({
                    viewport: { width: 279, height: 195, type: 'rectangle' },
                    boundary: { width: $(".croppie-div").width(), height: 400 }
                });
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.upload-demo').addClass('ready');
                    // console.log(e.target.result)
                    uploadCrop.croppie('bind', {
                        url: e.target.result
                    }).then(function(){
                        console.log('jQuery bind complete');
                    });
                }
                reader.readAsDataURL(this.files[0]);
               //  $('.uploadImg').append('<img width="100"  src="' + reader.readAsDataURL(this.files[0]) + '">');
               //  $.each(files, function(i, f) {
               //      var reader = new FileReader();
               //      reader.onload = function(e){
               //          $('.uploadImg').append('<img width="100"  src="' + e.target.result + '">');
               //      };
               //      reader.readAsDataURL(f);
               //  });
               //  $('.uploadImg').show();
            }

        });
</script>
<script>
      $(document).ready(function(){

           $('#addForm').validate({

                 rules: {

                     category: {

                          required:true,
                     },

                      sub_category: {

                            required: true,

                             remote: {

                                 url: '{{ route("check.sub.category") }}',
                                 dataType: 'json',
                                 type: 'post',
                                 data: {

                                      category: function(){

                                          return $('#sub_category').val();
                                      },

                                      _token: '{{ csrf_token() }}'
                                 }
                            },

                      },
                      file: {

                        accept: "image/jpg,image/jpeg,image/png",
                      }
                 },

                 messages: {

                     category: {

                        required:"Select a category"
                     },

                     sub_category: {

                          remote: 'Unique sub category name required'
                     },
                     file: {

                        accept: 'You must be select jpg,jpeg and png file'
                     },
                 },
                 ignore: [],
           });  
      });
  </script>
  @endsection
  @endsection
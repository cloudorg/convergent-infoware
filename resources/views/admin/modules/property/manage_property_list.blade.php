@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin | Manage Property
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>
@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection

<div class="content-page">
    <!-- Start content -->
    <div class="content">
      <div class="container">

        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title">Manage Property</h4>
            <!--<a href="add_category.html" class="rm_new04"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Category </a>
            <ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        @include('admin.includes.message')
        <div class="row">
          <div class="col-md-12">
          <div class="panel panel-default">
          <div class="admin_frm_cc">
                <form role="form" action="{{ route('admin.manage.property') }}" method="get">
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-12">
                    <div class="form-group">
                              <label for="">Keyword</label>
                              <input type="text" name="keyword" value="{{ Request::get('keyword') }}" id="" class="form-control" placeholder="Enter keyword...">
                    </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-12">
                    <div class="form-group">
                    <label for="">Status</label>
                    <select class="form-control rm06" name="status">
                        <option value="">Select</option>
                        <option value="A" @if(Request::get('status') == 'A') selected @endif>Active</option>
                        <option value="B" @if(Request::get('status') == 'B') selected @endif>Block</option>
                        <option value="I" @if(Request::get('status') == 'I') selected @endif>Incomplete</option>
                        <option value="S" @if(Request::get('status') == 'S') selected @endif>Sold</option>
                    </select>
                   </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-12">
                    <div class="form-group">
                    <label for="">Property Type</label>
                    <select class="form-control rm06" name="property_type">
                        <option value="">Select</option>
                        <option value="F" @if(Request::get('property_type') == 'F') selected @endif>Flat</option>
                        <option value="H" @if(Request::get('property_type') == 'H') selected @endif>House</option>
                        <option value="L" @if(Request::get('property_type') == 'L') selected @endif>Land</option>
                        <option value="R" @if(Request::get('property_type') == 'R') selected @endif>Residential</option>
                        <option value="O" @if(Request::get('property_type') == 'O') selected @endif>Office</option>
                    </select>
                   </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-12">
                    <div class="form-group">
                    <label for="">Property For</label>
                    <select class="form-control rm06" name="property_for">
                        <option value="">Select</option>
                        <option value="B" @if(Request::get('property_for') == 'B') selected @endif>Buy</option>
                        <option value="R" @if(Request::get('property_for') == 'R') selected @endif>Rent</option>

                    </select>
                   </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-12">
                    <div class="form-group">
                    <label for="">Post Date From</label>
                    <input type="text" name="from_date" value="{{ Request::get('from_date') }}" id="datepicker1" class=" form-control calander_icn " placeholder="Select" autocomplete="off">
                  </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-12">
                    <div class="form-group">
                    <label for="">Post Date To</label>
                    <input type="text" name="to_date" value="{{ Request::get('to_date') }}"  id="datepicker"  class=" form-control calander_icn" placeholder="Select" autocomplete="off">
                  </div>
                    </div>

                    <div class="col-lg-3 col-md-4 col-sm-12">
                    <div class="form-group">
                    <label for="">Area From</label>
                    <input type="text" name="from_area" value="{{ Request::get('from_area') }}" class=" form-control"   placeholder="From sqft" autocomplete="off">
                  </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-12">
                    <div class="form-group">
                    <label for="">Area To</label>
                    <input type="text" name="to_area" value="{{ Request::get('to_area') }}" class="form-control"   placeholder="To sqft" autocomplete="off">
                  </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="form-group">
                    <label for="">Construction Status</label>
                    <select class="form-control rm06" name="construction_status">
                        <option value="">Select</option>
                        <option value="RM" @if(Request::get('construction_status') == 'RM') selected @endif>Ready to Move</option>
                        <option value="UC" @if(Request::get('construction_status') == 'UC') selected @endif>Under Construction</option>
                        <option value="PL" @if(Request::get('construction_status') == 'PL') selected @endif>Pre-Launch</option>

                    </select>
                   </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="form-group">
                    <label for="">Furnishing</label>
                    <select class="form-control rm06" name="furnishing">
                        <option value="">Select</option>
                        <option value="SF" @if(Request::get('furnishing') == 'SF') selected @endif>Semi Furnished</option>
                        <option value="FF" @if(Request::get('furnishing') == 'FF') selected @endif>Full Furnished</option>
                        <option value="NF" @if(Request::get('furnishing') == 'NF') selected @endif>Not Furnished</option>

                    </select>
                   </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="form-group">
                    <label for="">Location</label>
                    <input type="text" name="location" id="location" placeholder="Enter Location" class="form-control" value="{{@$key['location']}}" onkeyup="search_result_check(this);" autocomplete="off">
                    <ul id="serch_result" class="search_sajasation" style="display: none"></ul>
                    {{-- <div class="diiferent-sec">
                      <div class="locatiob-search">

                        <input type="text" name="location" id="location" placeholder="Enter Location" class="form-control" value="{{@$key['location']}}">
                        <input type="hidden" name="lat" id="lat" value="{{@$key['lat']}}">
                         <input type="hidden" name="long" id="long" value="{{@$key['long']}}">
                         <select name="distance">
                          <option value="10" @if(@$key['distance']=='10')  selected @endif>10 km</option>
                          <option value="30" @if(@$key['distance']=='30')  selected @endif>30 km</option>
                          <option value="40" @if(@$key['distance']=='40')  selected @endif>40 km</option>
                          <option value="50" @if(@$key['distance']=='50')  selected @endif>50 km</option>
                      </select>

                      </div>

                      </div> --}}
					         </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-12">
                    <div class="form-group">
      
                    <label for="state">State</label>
                        <select class="form-control rm06" name="state" id="state">
                            <option value="">Select State</option>
                            @foreach($states as $state)
                            <option value="{{ $state->id }}" @if(@$state->id == Request::get('state') ) selected @endif>{{ $state->name }}</option>
                            @endforeach
                        </select>
                  </div>
                    </div>
                  <div class="col-lg-3 col-md-4 col-sm-12">
                    <div class="form-group">
      
                    <label for="state">City</label>
                        <select class="form-control rm06" name="city" id="city">
                            <option value="">Select City</option>
                            @foreach($cites as $city)
                            <option value="{{ $city->id }}" @if(@$city->id == Request::get('city') ) selected @endif>{{ $city->name }}</option>
                            @endforeach
                        </select>
                  </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="form-group">
                    <label for="">Bedroom</label>
                    <select class="form-control rm06" name="no_of_bedrooms">
                        <option value="">Select Bedroom</option>
                         @for($i = 1; $i<=10; $i++)
                        <option value="{{ $i }}" @if(Request::get('no_of_bedrooms') == $i) selected @endif>{{ $i }} Bedroom</option>

                        @endfor
                    </select>
                   </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="form-group">
                    <label for="">Bathroom</label>
                    <select class="form-control rm06" name="bathroom">
                        <option value="">Select Bathroom</option>
                         @for($i = 1; $i<=10; $i++)
                        <option value="{{ $i }}" @if(Request::get('bathroom') == $i) selected @endif>{{ $i }} Bathroom</option>

                        @endfor
                    </select>
                   </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="form-group">
                   <div class="bedroom-derp">

													<label>Budget</label>

													<div class="slider_rnge">

									                  <div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">

									                    <div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 100%;"></div> <span tabindex="0" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 0%;"></span> <span tabindex="0" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 100%;"></span> </div> <span class="range-text">

									                           <input type="text" class="price_numb" readonly id="amount">
                                             <input type="hidden" class="price_numb" id="amount1" name="amount1" value="{{@$key['amount1']?@$key['amount1']:0}}">
                                              <input type="hidden" class="price_numb" id="amount2" name="amount2" value="{{@$key['amount2']?@$key['amount2']:(int)@$maxPrice->budget_range_from}}">
									                           </span>

									                </div>
                     </div>
                    </div>
                    </div>
                    <div class="col-sm-12">
                    <div class="ser_btn">
                    <button class="btn btn-primary waves-effect waves-light w-md" type="submit">Search</button>
                    <a href="{{ route('admin.manage.property') }}" class="btn btn-success waves-effect waves-light w-md">Reset</a>
                  </div>
                    </div>



                   {{-- <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" name="name" value="{{ Request::get('name') }}" id="" class="form-control" placeholder="Enter here">
					</div>

                    <div class="form-group">
                    <label for="">Email</label>
                    <input type="text" name="email" value="{{ Request::get('email') }}" id="" class="form-control" placeholder="Enter here">
					         </div>

					<div class="form-group">
                    <label for="">Phone</label>
                    <input type="text" name="phone" value="{{ Request::get('phone') }}" id="" class="form-control" placeholder="Enter here">
					</div>--}}














                  <!--
                <div class="form-group">
                    <label for="">Subject</label>
                    <select class="form-control rm06">
                        <option>Select</option>
                        <option>History</option>
                        <option>Geography</option>
                        <option>Mathematics</option>
                        <option>Life Science</option>
                        <option>Physical Science</option>
                        <option>English</option>
                    </select>
                </div>-->

                   </div>
                </form>
              </div>
          </div>


            <div class="panel panel-default">


              <div class="panel-body">
                <div class="row">
                <form action="{{ route('admin.select.property.delete') }}" method="post" id="deleteForm">
                    @csrf
                    <input type="hidden" name="status" id="select_block">
                    <input type="hidden" name="status_home" id="select_home">
                    @if(count(@$properties)>0)
                    <input type="submit" name="" id="" class="btn btn-sm btn-success" value="Delete Selected Property">&nbsp;
                    <input type="submit" name="" id="" class="btn btn-sm btn-success" onclick="selectedfun()" value="Block Selected Property">&nbsp;
                    <input type="submit" name="" id="" class="btn btn-sm btn-success" onclick="selectedfun1()" value="Show home Selected Property">
                    <br>
                    <label id="allid[]-error" class="error" for="allid[]" style="display: none"></label>
                    @endif
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th></th>
                            <th>Property Name</th>
                            <th>Property ID</th>
                            <th>User Name</th>
                            <th>Property Type</th>
                            <th>Property For</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Show in Home</th>
                            <th>Approval Status</th>
                            <th class="rm07">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                         @if(count($properties)>0)
                           @foreach($properties as $property)
                          <tr>
                          <td>
                            <div class="">
							             <input type="checkbox" id="deleteId{{ @$property->id }}"  value="{{@$property->id}}" name="allid[]">
					                	</div>
                            </td>
                            <td>
                                @if(strlen(@$property->name)>20)
                               {!! substr(@$property->name,0,20) . '...'  !!}
                               @else
                              {!! $property->name !!}
                              @endif
                              </td>
                            <td>{{ @$property->property_id }}</td>
                            <td>{{ $property->propertyUser->name }}</td>
                            <td>
                                @if(@$property->property_type == 'F')
                                 Flat
                                 @elseif(@$property->property_type == 'H')

                                  House
                                  @elseif(@$property->property_type == 'L')

                                   Land
                                   @elseif(@$property->property_type == 'R')

                                   Residential
                                   @elseif(@$property->property_type == 'O')

                                    Office

                                    @endif
                            </td>
                            <td>
                                @if(@$property->property_for == 'B')
                                 Buy

                                 @elseif(@$property->property_for == 'R')
                                  Rent
                                  @endif
                            </td>
                            <td>{{  $property->created_at->format('m/d/Y') }}</td>
                            <td class="status{{ @$property->id }}">
                                @if($property->status == 'A')

                                Active

                                @elseif($property->status == 'B')

                                 Block

                                 @elseif($property->status == 'I')

                                 Incomplete
                                  @elseif($property->status == 'S')

                                 Sold
                                @endif
                            </td>
                            <td class="home_status{{ @$property->id }}">
                              @if(@$property->show_in_home == 'Y')
                               Yes
                               @elseif(@$property->show_in_home == 'N')
                               No
                               @endif
                            </td>
                            <td class="approval_status{{ @$property->id }}">
                               @if($property->aprove_by_admin == 'Y')

                                Yes

                                @elseif($property->aprove_by_admin == 'N')

                                 No

                                 @else

                                 None
                                @endif
                            </td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action1" onclick="fun({{ @$property->id }})"><img src="{{ url('public/admin/assets/images/action-dots.png') }}" alt=""></a>
                            <div class="show-actions" id="show-action{{ @$property->id }}" style="display: none;">
                                <span class="angle"><img src="{{ url('public/admin/assets//images/angle.png') }}" alt=""></span>
                                <ul>
                                    @if(@$property->status == 'I')
                                    <li><a href="{{ route('admin.property.details',@$property->id) }}">View</a></li>
                                    <li><a href="{{ route('admin.edit.property',@$property->id) }}">Edit</a></li>
                                    @else
                                    <li><a href="{{ route('admin.property.details',@$property->id) }}">View</a></li>
                                    <li><a href="{{ route('admin.edit.property',@$property->id) }}">Edit</a></li>
                                    @if($property->aprove_by_admin == 'Y')
                                    <li>
                                      
                                        @if($property->status == 'A')
                                        <a href="javascript:void(0)" class="status_change" data-id="{{ $property->id }}"><span class="text_change{{ $property->id }}">Make Block</span></a>
                                        @elseif($property->status == 'B')
                                        <a href="javascript:void(0)" class="status_change" data-id="{{ $property->id }}"><span class="text_change{{ $property->id }}">Make Active</span></a>
                                        @endif
                                    </li>
                                    @endif
                                    <li>
                                    @if($property->aprove_by_admin == 'N')
                                        <a href="javascript:void(0)" class="approval_status_change" data-id="{{ $property->id }}" onclick="return confirm('Are you want to approve this property?')"><span class="approve_text_change{{ @$property->id }}">Approve Property</span></a>
                                       @endif
                                    </li>
                                    @if($property->aprove_by_admin == 'Y')
                                    <li>
                                        @if($property->show_in_home == 'Y')
                                        <a href="javascript:void(0)" class="home_status_change" data-id="{{ $property->id }}"><span class="text_change_home{{ $property->id }}">Hide in Home</span></a>
                                        @elseif($property->show_in_home == 'N')
                                        <a href="javascript:void(0)" class="home_status_change" data-id="{{ $property->id }}"><span class="text_change_home{{ $property->id }}">Show in Home</span></a>
                                        @endif
                                    </li>
                                    @endif
                                    @endif
                                    <li><a href="{{ route('admin.delete.property',@$property->id) }}" onclick="return confirm('Are you want to delete this property?')">Delete</a></li>
                                </ul>
                              </div>
                            </td>
                          </tr>
                          @endforeach
                           @else
                           <tr role="row" class="odd">
                                        <td colspan="8" style="text-align:center;">
                                            No Data Found
                                        </td>
                                    </tr>
                            @endif
                          {{--<tr>
                            <td>Abhijeet Roy</td>
                            <td>sample-test123@gmail.com</td>
                            <td>+91 9876543210</td>
                            <td>10.11.2021</td>
                            <td>Unblock</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action2"><img src="assets/images/action-dots.png" alt=""></a>
                            <div class="show-actions" id="show-action2" style="display: none;">
                                <span class="angle"><img src="assets//images/angle.png" alt=""></span>
                                <ul>
                                    <li><a href="#">View</a></li>
                                    <li><a href="#">Edit</a></li>
                                    <li><a href="#">Block</a></li>
                                </ul>
                              </div>
                            </td>
                          </tr>

                          <tr>
                            <td>Rabin Das</td>
                            <td>test123@gmail.com</td>
                            <td>+91 1234567890</td>
                            <td>14.10.2021</td>
                            <td>Block</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action3"><img src="assets/images/action-dots.png" alt=""></a>
                            <div class="show-actions" id="show-action3" style="display: none;">
                                <span class="angle"><img src="assets//images/angle.png" alt=""></span>
                                <ul>
                                    <li><a href="#">View</a></li>
                                    <li><a href="#">Edit</a></li>
                                    <li>

                                        <a href="#">Unblock</a>
                                     </li>
                                </ul>
                              </div>
                            </td>
                          </tr>

                          <tr>
                            <td>Abhijeet Roy</td>
                            <td>sample-test123@gmail.com</td>
                            <td>+91 9876543210</td>
                            <td>10.11.2021</td>
                            <td>Unblock</td>
                            <td class="rm07">
                            <a href="javascript:void(0);" class="action-dots" id="action4"><img src="assets/images/action-dots.png" alt=""></a>
                            <div class="show-actions" id="show-action4" style="display: none;">
                                <span class="angle"><img src="assets//images/angle.png" alt=""></span>
                                <ul>
                                    <li><a href="#">View</a></li>
                                    <li><a href="#">Edit</a></li>
                                    <li><a href="#">Block</a></li>
                                </ul>
                              </div>
                            </td>
                          </tr>--}}


                        </tbody>
                      </table>
                    </div>
                    <div style="float: right;">{{@$properties->appends(request()->except(['page', '_token']))->links()}}</div>

                    <!-- <ul class="pagination">
                      <li class="paginate_button previous disabled"><a href="#">Previous</a></li>
                      <li class="paginate_button active"><a href="#">1</a></li>
                      <li class="paginate_button"><a href="#">2</a></li>
                      <li class="paginate_button"><a href="#">3</a></li>
                      <li class="paginate_button"><a href="#">4</a></li>
                      <li class="paginate_button"><a href="#">5</a></li>
                      <li class="paginate_button"><a href="#">6</a></li>
                      <li class="paginate_button next"><a href="#">Next</a></li>
                    </ul> -->


                  </div>
                  </form>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- End row -->

      </div>
      <!-- container -->

    </div>
    <!-- content -->

    @include('admin.includes.footer')
  </div>

  @section('scripts')

  @include('admin.includes.scripts')
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script> -->
  {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRZMuXnvy3FntdZUehn0IHLpjQm55Tz1E&libraries=places&callback=initAutocomplete" async defer></script> --}}
  <script>
    $("#datepicker1").datepicker({
      numberOfMonths: 1,
      onSelect: function (selected) {
        var dt = new Date(selected);
        dt.setDate(dt.getDate() + 1);
        $("#datepicker").datepicker("option", "minDate", dt);
      },
      maxDate: new Date("{{ date('m/d/Y') }}")
    });
    $("#datepicker").datepicker({
      numberOfMonths: 1,
      onSelect: function (selected) {
        var dt = new Date(selected);
        dt.setDate(dt.getDate() - 1);
        $("#datepicker1").datepicker("option", "maxDate", dt);
      },
    });

  </script>
  {{-- <script>
    function initAutocomplete() {
        // Create the search box and link it to the UI element.
        var input = document.getElementById('location');

        var options = {
          types: ['establishment']
        };

        var input = document.getElementById('location');
        var autocomplete = new google.maps.places.Autocomplete(input, options);

        autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);

        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            console.log(place)
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            $('#lat').val(place.geometry.location.lat());
            $('#long').val(place.geometry.location.lng());
            lat = place.geometry.location.lat();
            lng = place.geometry.location.lng();
            $('.exct_btn').show();

            initMap();
        });
        initMap();
    }
</script>

<script>

    function initMap() {
        geocoder = new google.maps.Geocoder();
        var lat = $('#lat').val();
        var lng = $('#lng').val();
        var myLatLng = new google.maps.LatLng(lat, lng);
        // console.log(myLatLng);
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Choose hotel location',
          draggable: true
        });

        google.maps.event.addListener(marker, 'dragend', function(evt,status){
        $('#lat').val(evt.latLng.lat());
        $('#long').val(evt.latLng.lng());
        var lat_1 = evt.latLng.lat();
        var lng_1 = evt.latLng.lng();
        var latlng = new google.maps.LatLng(lat_1, lng_1);
            geocoder.geocode({'latLng': latlng}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    $('#location').val(results[0].formatted_address);
                }
            });


        });
    }
    </script> --}}

  <script>
      function fun(id){

             $('.show-actions').slideUp();
              $("#show-action"+id).show();
      }
      $(document).on('click', function () {
      var $target = $(event.target);
      if (!$target.closest('.action-dots').length && $('.show-actions').is(":visible")) {
          $('.show-actions').slideUp();
        }
    });


 $(document).on('click','.status_change', function(e){

var id = $(e.currentTarget).attr('data-id');

  if(confirm('Are you want to change this status?')){

       $.ajax({

            url: '{{ route('property.status.update') }}',
            method: 'GET',
            data: 'id='+id,

            success: function(resp){
                 $('.text_change'+id).html('');
                  if(resp != 0){

                      var str = "";
                     if(resp.status == 'A'){
                         str = "Active"
                        $(".success_msg").html("Success! Status is changed to Active!");
                        $('.text_change'+id).html('Make Inactive');

                     }else if(resp.status == 'B'){
                        str = "Block";
                      $(".success_msg").html("Success! Status is changed to Block!");
                      $('.text_change'+id).html('Make Active');

                     }
                     $('.status'+id).html(str);
                      $(".success_msg_div").show();

                     $(".error_msg_div").hide();


                  }else{

                    $(".error_msg_div").show();

                      $(".success_msg_div").hide();

                      $(".error_msg").html("Something went wrong!");
                  }


            },
            error: function(error){

               consolse.log(error);
            }
       });
  }


});

$(document).on('click','.approval_status_change', function(e){

var id = $(e.currentTarget).attr('data-id');

       $.ajax({

            url: '{{ route('property.approval.status.update') }}',
            method: 'GET',
            data: 'id='+id,

            success: function(resp){
              console.log(resp);
                 $('.approve_text_change'+id).html('');
                  if(resp != 0){

                      var str = "";
                     if(resp.aprove_by_admin == 'Y'){
                         str = "Yes"
                        $(".success_msg").html("Success! Status is changed to Approved!");
                        $('.approve_text_change'+id).css('display','none');

                        window.location.href = window.location.href;

                     }
                     $('.approval_status'+id).html(str);
                      $(".success_msg_div").show();

                     $(".error_msg_div").hide();


                  }else{

                    $(".error_msg_div").show();

                      $(".success_msg_div").hide();

                      $(".error_msg").html("Something went wrong!");
                  }


            },
            error: function(error){

               consolse.log(error);
            }
       });

});

$(document).on('click','.home_status_change', function(e){

var id = $(e.currentTarget).attr('data-id');
  if(confirm('Are you want to change this status?')){

       $.ajax({

            url: '{{ route('home.property.status.update') }}',
            method: 'GET',
            data: 'id='+id,

            success: function(resp){
                 $('.text_change_home'+id).html('');
                  if(resp != 0){

                      var str = "";
                     if(resp.show_in_home == 'Y'){
                         str = "Show"
                        $(".success_msg").html("Success! Status is changed to Show!");
                        $('.text_change_home'+id).html('Hide in Home');

                     }else if(resp.show_in_home == 'N'){
                        str = "Hide";
                      $(".success_msg").html("Success! Status is changed to Hide!");
                      $('.text_change_home'+id).html('Show in Home');

                     }
                     $('.home_status'+id).html(str);
                      $(".success_msg_div").show();

                     $(".error_msg_div").hide();


                  }else{

                    $(".error_msg_div").show();

                      $(".success_msg_div").hide();

                      $(".error_msg").html("Something went wrong!");
                  }


            },
            error: function(error){

               console.log(error);
            }
       });
  }


});

 </script>

 
<script>

$(document).ready(function(){

     $('#deleteForm').validate({

           rules: {

                'allid[]': {

                    required: true,
                },
           },
           messages: {

               'allid[]': {

                   required: "Please select at least one Property",
               },
           }
     });
});


// $(function () {
// $(".datepicker").datepicker({
// autoclose: true,
// todayHighlight: true
// });
// });

function selectedfun(){

     $('#select_block').val('B');
     $('#deleteForm').submit();
}

function selectedfun1(){

$('#select_home').val('H');
$('#deleteForm').submit();
}
</script>

<script>
 var allSliderAmount;
        @if(@$key['amount1']!=null)
        allSliderAmount=[];
        allSliderAmount=['{{$key['amount1']}}','{{$key['amount2']}}'];
        @else
         var minprice = 10;
         
        allSliderAmount=[minprice,'{{(int)@$maxPrice->budget_range_from}}'];
        @endif
        console.log(allSliderAmount);
        var x=allSliderAmount[ 0 ];
        var y=allSliderAmount[ 1 ];
                x=x.toString();
                y=y.toString();
                var lastThree = x.substring(x.length-3);
                var otherNumbers = x.substring(0,x.length-3);
                var lastThreey = y.substring(y.length-3);
                var otherNumbersy = y.substring(0,y.length-3);
                if(otherNumbers != '')
                lastThree = ',' + lastThree;
                if(otherNumbersy != '')
                lastThreey = ',' + lastThreey;
                var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
                var resy = otherNumbersy.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThreey;
        $( "#slider-range" ).slider({
            range: true,
            min:10,
            max: '{{(int)@$maxPrice->budget_range_from}}',
            values: allSliderAmount,
            step: 20000,
            slide: function( event, ui ) {
              var x=ui.values[ 1 ];
                var y=ui.values[ 0 ];
                x=x.toString();
                y=y.toString();
                var lastThree = x.substring(x.length-3);
                var otherNumbers = x.substring(0,x.length-3);
                var lastThreey = y.substring(y.length-3);
                var otherNumbersy = y.substring(0,y.length-3);
                if(otherNumbers != '')
                lastThree = ',' + lastThree;
                if(otherNumbersy != '')
                lastThreey = ',' + lastThreey;
                var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
                var resy = otherNumbersy.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThreey;
                $( "#amount" ).val( "₹" + resy + " - ₹" + res );
                $( "#amount1" ).val( ui.values[ 0 ]);
                $( "#amount2" ).val( ui.values[ 1 ] );
            }
        });

// $( "#amount" ).val( "₹" + $( "#slider-range" ).slider( "values", 0 ) +

//     " - ₹" + $( "#slider-range" ).slider( "values", 1 ) );


    
        $( "#amount" ).val( "₹" + res +" - ₹" + resy );



$(function () {
$(".datepicker").datepicker({
autoclose: true,
todayHighlight: true
});
});


</script>

<script>
    function search_result_check(that) {
        var $this = that;
        var name = $($this).val();
        console.log($($this).val());
        $.ajax({
            type: "POST",
            url:"{{route('get.locality.available')}}",
            data: {
                'name': name,
                '_token':'{{@csrf_token()}}'
            },
            success: function(data) {
              if(data){
                console.log(data);
                $('#serch_result').show();
                $('#serch_result').fadeIn();
                $('#serch_result').last().html(data);

              }
              else
              {
                $('#serch_result').children().remove();
              }
            }
        });
    }
    function add_searchbar(that) {
        var $this = that;
        var name = $($this).text();
        console.log(name);
        $('#location').val(name);
        $($this).remove();
        $('#serch_result').children().remove();
        $('#serch_result').hide();
    }
</script>
<script type="text/javascript">
    $(document).ready(function(){
      $('#country').on('change',function(e){
        e.preventDefault();
        var id = $(this).val();

        $.ajax({
          url:'{{route('get.state')}}',
          type:'GET',
          data:{country:id,id:'{{@$user->state}}'},
          success:function(data){
            console.log(data);
            $('#state').html(data.state);
          }
        })
      });

      $('#state').on('change',function(e){
        e.preventDefault();
        var id = $(this).val();
        $.ajax({
          url:'{{route('get.city')}}',
          type:'GET',
          data:{state:id,id:'{{@$user->city}}'},
          success:function(data){
            console.log(data);
            $('#city').html(data.city);
          }
        })
      });

    });
</script>
@endsection

@endsection

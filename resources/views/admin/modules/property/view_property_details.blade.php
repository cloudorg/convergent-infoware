@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin | Property Details
@endsection

@section('content')

@section('links')

@include('admin.includes.links')

<style>

  #profileimage img{

       width:200px;
  }

</style>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>

@endsection

@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection

<div class="content-page">
    <!-- Start content -->
    <div class="content">
      <div class="container">

        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <h4 class="pull-left page-title"> Property Details <i class="fa fa-chevron-right" aria-hidden="true"></i> </h4>
            <a href="{{ route('admin.manage.property') }}" class="rm_new04"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
            <!--<a href="add_class_links.html" class="rm_new04"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Class Links</a>-->
            <!--<ol class="breadcrumb pull-right">
              <li><a href="#">Aariv School</a></li>
              <li><a href="#">Data Tables</a></li>
              <li class="active">Basic Tables</li>
            </ol>-->
          </div>
        </div>
        @include('admin.includes.message')
        <div class="row">
          <div class="col-md-12">


         <div class="tab_stylee">
         <a href="{{ route('admin.property.details',['id'=>@$property->id]) }}" class="tab_active">Property Details</a>
          	{{--<a href="{{ route('admin.property.visit.request',['id'=>@$property->id]) }}" >View Visit Request</a>--}}
            {{--<a href="#">View Property</a>
            <a href="javascript:;">View Availability</a>
            <a href="javascript:;">View Visit Requests</a>--}}
          </div>

            <div class="panel panel-default panel-fill">

                    <div class="panel-body">

                        <!--<div class="about-info-p rm_new10">
                            <strong>Full Name :</strong>
                            <p class="text-muted">Mark Dicus</p>
                        </div>
                        <div class="about-info-p rm_new10">
                            <strong>User Name :</strong>
                            <p class="text-muted">Abc Username</p>
                        </div>
                        <div class="about-info-p rm_new10">
                            <strong>Email Address :</strong>
                            <p class="text-muted">testemail123@gmail.com</p>
                        </div>-->

                        <div class="agent_ppcc">
                            @if($property->propertyImageMain != null)
                            <img src="{{ url('storage/app/public/property_image/'.$property->propertyImageMain->image) }}" alt="">
                            @else
                            <img src="{{ url('public/admin/assets/images/avatar-1.jpg') }}" alt="">
                            @endif
                        </div>

                        <div class="boxx_002">

                        <div class="about-info-p rm_new08">
                            <strong>Property Name</strong>
                            <p class="text-muted"> : {{ $property->name }}</p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Property Type</strong>
                             @if($property->property_type == 'F')
                            <p class="text-muted"> : Flat</p>
                            @elseif($property->property_type == 'H')
                            <p class="text-muted"> : House</p>
                            @elseif($property->property_type == 'L')
                            <p class="text-muted"> : Land</p>
                            @elseif($property->property_type == 'R')
                            <p class="text-muted"> : Residential</p>
                            @elseif($property->property_type == 'O')
                            <p class="text-muted"> : Office</p>
                            @endif
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Property For</strong>
                             @if($property->property_for == 'B')
                            <p class="text-muted"> : Buy</p>
                            @elseif($property->property_for == 'R')
                            <p class="text-muted"> : Rent</p>
                            @endif
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Construction Status</strong>
                             @if($property->construction_status == 'RM')
                            <p class="text-muted"> : Ready to Move</p>
                            @elseif($property->construction_status == 'UC')
                            <p class="text-muted"> : Under Construction</p>
                            @elseif($property->construction_status == 'PL')
                            <p class="text-muted"> : Pre-Launch</p>
                            @endif
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Furnishing</strong>
                             @if($property->furnishing == 'SF')
                            <p class="text-muted"> : Semi Furnished</p>
                            @elseif($property->furnishing == 'FF')
                            <p class="text-muted"> : Full Furnished</p>
                            @elseif($property->furnishing == 'NF')
                            <p class="text-muted"> : Not Furnished</p>
                            @endif
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Property For</strong>
                             @if($property->property_for == 'B')
                            <p class="text-muted"> : Buy</p>
                            @elseif($property->property_for == 'R')
                            <p class="text-muted"> : Rent</p>
                            @endif
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Status</strong>
                             @if($property->status == 'A')
                            <p class="text-muted"> : Active</p>
                            @elseif($property->status == 'B')
                            <p class="text-muted"> : Block</p>
                            @elseif($property->status == 'I')
                            <p class="text-muted"> : Incomplete</p>
                            @elseif($property->status == 'S')
                            <p class="text-muted"> : Sold</p>
                            @endif
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Date</strong>
                            <p class="text-muted"> : {{ date('m.d.Y',strtotime($property->created_at)) }}</p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Total Bedrooms</strong>
                            <p class="text-muted"> : {{ @$property->no_of_bedrooms }}</p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Total Bathrooms</strong>
                            <p class="text-muted"> : {{ @$property->bathroom }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Facilities/Amenities</strong>
                             @if(@$facilities)
                                @php
                                 $i = count($facilities)-1;
                                @endphp
                            <p class="text-muted"> :
                            @foreach($facilities as $key=>$lan)

                            <span>{{ $lan->propertyFacilities->name }}</span>@if($i == $key) @else , @endif
                              @endforeach
                            </p>
                            @endif
                        </div>

                        </div>

                        <div class="boxx_002">

                        <div class="about-info-p rm_new08">
                            <strong>Budget Range</strong>
                            <p class="text-muted"> : ₹{{ $property->budget_range_from }} - ₹{{ $property->budget_range_to  }}</p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Area</strong>
                            <p class="text-muted"> : {{ @$property->area }}</p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Super Area</strong>
                            <p class="text-muted"> : {{ @$property->super_area }}</p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Carpet Area</strong>
                            <p class="text-muted"> : {{ @$property->carpet_area }}</p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Country</strong>
                            <p class="text-muted"> : {{ $country }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>State</strong>
                            <p class="text-muted"> : {{ @$property->stateName->name }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>City</strong>
                            <p class="text-muted"> : {{ @$property->cityName->name }}</p>
                        </div>

                        <div class="about-info-p rm_new08">
                            <strong>Address</strong>
                            <p class="text-muted"> : {{ $property->address }}</p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Locality</strong>
                            <p class="text-muted"> : {{ @$property->localityName->locality_name }}</p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Post Code</strong>
                            <p class="text-muted"> : {{ @$property->post_code }}</p>
                        </div>





                  </div>
                   <div class="">
                  <div class="about-info-p rm_new08">
                            <strong>Description</strong>
                            <p class="text-muted">:{!! $property->description !!}</p>
                        </div>
                    </div>
                  </div>

          </div>
        </div>
        <!-- End row -->
        <div class="row">
         <div class="col-md-12">
          <div class="panel panel-default panel-fill">
              <div class="panel-body">
              <div class="boxx_002">
                      <h3>User Details</h3>
                      <div class="about-info-p rm_new08">
                            <strong>User Name</strong>
                            <p class="text-muted"> : {{ @$property->propertyUser->name }}</p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Email</strong>
                            <p class="text-muted"> : {{ @$property->propertyUser->email }}</p>
                        </div>
                        <div class="about-info-p rm_new08">
                            <strong>Phone</strong>
                            <p class="text-muted"> : {{ @$property->propertyUser->mobile_number }}</p>
                        </div>
                     </div>
              </div>
          </div>
         </div>
        </div>
        <!-- End row -->
        <div class="row">
            <div class="col-md-12">
            <div class="ext-image">
                      <h3>Exterior Images</h3>
                      <div class="dash-listnew">
                                    <ul class="edit-gallerynew">
                                    @foreach ($exteriorimages as $image)
                                     <li>
                                    <div class="upimg">

                                        <img src="{{ URL::to('storage/app/public/property_image')}}/{{$image->image}}" alt="">

                                    </div>
                                    </li>
                                @endforeach

                                       <div class="clearfix"></div>
                                    </ul>
                                 </div>
                    </div>
            </div>
        </div>
        <!-- End row -->
        <div class="row">
            <div class="col-md-12">
            <div class="ext-image">
                      <h3>Interior Images</h3>
                      <div class="dash-listnew">
                                    <ul class="edit-gallerynew">
                                    @foreach ($interiorimages as $image)
                                     <li>
                                    <div class="upimg">

                                        <img src="{{ URL::to('storage/app/public/property_image')}}/{{$image->image}}" alt="">

                                    </div>
                                    </li>
                                @endforeach

                                       <div class="clearfix"></div>
                                    </ul>
                                 </div>
                    </div>
            </div>
        </div>
        <!-- End row -->
        <div class="row">
            <div class="col-md-12">
            <div class="ext-image">
                      <h3>Floor Plan Images</h3>
                      <div class="dash-listnew">
                                    <ul class="edit-gallerynew">
                                    @foreach ($floorplanimages as $image)
                                     <li>
                                    <div class="upimg">

                                        <img src="{{ URL::to('storage/app/public/property_image')}}/{{$image->image}}" alt="">

                                    </div>
                                    </li>
                                @endforeach

                                       <div class="clearfix"></div>
                                    </ul>
                                 </div>
                    </div>
            </div>
        </div>
        <!-- End row -->
      </div>
      <!-- container -->

    </div>
    <!-- content -->

    @include('admin.includes.footer')
  </div>

  @section('scripts')

  @include('admin.includes.scripts')

  @endsection

  @endsection

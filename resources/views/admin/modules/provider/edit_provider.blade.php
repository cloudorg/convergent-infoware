@extends('admin.layouts.app')
@section('title')
RiVirtual | Admin | Edit Pro Profile
@endsection
@section('content')
@section('links')
@include('admin.includes.links')
<style>
   label.error{
   color:red;
   }

   .uplodimg_pick img {
    width: 50px;
    height: 50px;
    object-fit: cover;
    border-radius: 100%;
}

</style>
<script>
   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
   m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
   })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

   ga('create', 'UA-65046120-1', 'auto');
   ga('send', 'pageview');
</script>
<link href="{{ URL::asset('public/frontend/croppie/croppie.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('public/frontend/croppie/croppie.min.css') }}" rel="stylesheet" />
@endsection
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
<div class="content-page">
   <!-- Start content -->
   <div class="content">
      <div class="container">
         <!-- Page-Title -->
         <div class="row">
            <div class="col-sm-12">
               <h4 class="pull-left page-title">Edit Pro Profile</h4>
               <a href="{{ route('admin.manage.provider') }}" class="rm_new04"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
               <!--<ol class="breadcrumb pull-right">
                  <li><a href="#">Aariv School</a></li>
                  <li><a href="#">Data Tables</a></li>
                  <li class="active">Basic Tables</li>
                  </ol>-->
            </div>
         </div>
         @include('admin.includes.message')
         <div class="row">
            <div class="col-md-12">
               <div class="panel panel-default">
                  <div class="panel-body rm02 rm04">
                     <form role="form" action="{{ route('admin.edit.provider.profile.save') }}" method="post" enctype="multipart/form-data" id="editForm">
                        @csrf
                        <input type="hidden" name="proId" id="" value="{{ $provider->id }}">
                        <div class="col-md-6 m-b-15">
                          @php
                          $split_name = explode(' ',$provider->name);
                           $fname = $split_name[0];
                            $lname = $split_name[1];
                           @endphp
                           <label for="">First Name</label>
                           <input type="text" name="first_name" value="{{ $fname }}" placeholder="Enter here" class="form-control">
                        </div>
                        <div class="col-md-6 m-b-15">
                           <label for="">Last Name</label>
                           <input type="text" name="last_name" value="{{ $lname }}" placeholder="Enter here" class="form-control">
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 m-b-15">
                           <label for="">Email</label>
                           <input type="text" name="email" value="{{ $provider->email }}"  placeholder="Enter here" class="form-control" disabled>
                        </div>
                        <div class="col-md-6 m-b-15">
                           <label for="">Mobile Number</label>
                           <input type="text" name="phone" value="{{ $provider->mobile_number }}"  placeholder="Enter here" class="form-control" disabled>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 m-b-15">
                           <label for="">Website</label>
                           <input type="url" name="website" value="{{ @$provider->website }}" placeholder="Enter here" class="form-control">
                        </div>
                        <div class="col-md-6 m-b-15">
                           <label for="">WhatsApp</label>
                           <input type="text" name="whatsapp_no" value="{{ @$provider->whatsapp_no }}" placeholder="Enter here" class="form-control" maxlength="10">
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 m-b-15">
                           <label for="">Category</label>
                           <select class="form-control rm06" name="category" id="category">
                              <option value="">Select Category</option>
                              @foreach($allCategory as $cat)
                              <option value="{{ $cat->id }}" @if(@$myCategory->category_id == $cat->id) selected @endif>{{ $cat->category_name}}</option>
                              @endforeach
                           </select>
                        </div>
                        <div class="col-md-6 m-b-15">
                        <div class="das_input malti_select language_kn">
							<label>Sub Category</label>
							<span class="autocomplete-select2"></span>
                             <label id="sub_category-error" class="error" for="sub_category" style="display: none;"></label>
						 </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 m-b-15">
                           <label for="">Experience (Year)</label>
                           <input type="text" name="experience" value="{{ @$provider->experience }}" id="" placeholder="Enter here" class="form-control">
                        </div>
                        
                        <div class="col-md-6 m-b-15">
                           <label for="">Budget</label>
                           <div class="budget-box budget_iconn">
                              <input type="text" name="budget" placeholder="Enter here.." class="required form-control ad_pp" value="{{(int)$provider->budget}}">
                              <span class="budget_iconn02"><i class="fa fa-inr" aria-hidden="true"></i></span>
                              <select name="budget_type" class="form-control">
                              <option value="H" @if(@$provider->budget_type=='H') Selected @endif>Hourly</option>
                              <option value="F" @if(@$provider->budget_type=='F') Selected @endif>Fixed</option>
                              </select>
                           </div>
                           <label id="budget-error" class="error" for="budget" style="display: none"></label>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 m-b-15">
                           <div class="das_input malti_select language_kn">
                              <label>Languages Knows</label>
                              <span class="autocomplete-select"></span>
                              <label id="language-error" class="error" for="language" style="display: none;"></label>
                           </div>
                        </div>
                        
                        <div class="col-md-6 m-b-15">
                           <div class="das_input malti_select language_kn">
                              <label>Skills</label>
                              <span class="autocomplete-select1"></span>
                              <label id="skill-error" class="error" for="skill" style="display: none;"></label>
                           </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 m-b-15">
                           <label for="">Country</label>
                           <select class="form-control rm06" name="country" id="country">
                              <option value="">Select Country</option>
                              @foreach($allCountry as $cnt)
                              <option value="{{ $cnt->id }}" @if(@$provider->country == $cnt->id) selected @endif>{{ $cnt->name }}</option>
                              @endforeach
                           </select>
                        </div>
                        
                        {{-- <div class="col-md-6 m-b-15">
                           <label for="">State</label>
                           <input type="text" name="state" value="{{ @$provider->state }}"  placeholder="Enter here" class="form-control">
                        </div>
                          
                        <div class="col-md-6 m-b-15">
                           <label for="">City</label>
                           <input type="text" name="city" value="{{ @$provider->city }}"  placeholder="Enter here" class="form-control">
                        </div> --}}
                        <div class="col-md-6 m-b-15">
                            <label>State</label>
                            <select name="state" id="states" class="form-control rm06 required">
                                <option value="">Select State</option>
                                @foreach(@$states as $state)
                                <option value="{{@$state->id}}" @if($provider->state==@$state->id)selected @endif>{{@$state->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 m-b-15">
                            <label>City</label>
                            <select name="city" id="city" class="form-control rm06 required">
                                <option value="">Select City</option>
                                @foreach(@$cites as $city)
                                <option value="{{@$city->id}}" @if(@$provider->city==@$city->id)selected @endif>{{@$city->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        
                        <div class="col-md-6 m-b-15">
                           <label for="">Address</label>
                           <input type="text" name="address" value="{{ @$provider->address }}"  placeholder="Enter here" class="form-control">
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 m-b-15">
                           <div class="das_input">
                              <label>About Me </label>
                              <textarea placeholder="Enter here.." name="about" rows="8" class="required form-control">{!! @$provider->about !!}</textarea>
                           </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 m-b-15">
                           <div class="uplodimg">
                              <div class="uplodimgfil">
                                 <b>Certifications</b>
                                 <input type="file" name="certificate" id="certificate" class="inputfile inputfile-1" />
                                 <label for="certificate">Upload Certificate <img src="{{URL::to('public/frontend/images/clickhe.png')}}" alt=""></label>
                                 <label id="certificate-error" class="error" for="certificate" style="display: none"></label>
                              </div>
                           </div>
                           <span class="filename"></span>
                        </div>

                        <div class="col-md-6 m-b-15">
                           <div class="uplodimg">
                              <div class="uplodimgfil">
                                 <b>Gov ID</b>
                                 <input type="file" name="gov_id" id="gov_id" class="inputfile inputfile-1" />
                                 <label for="gov_id">Upload Id <img src="{{ URL::to('public/frontend/images/clickhe.png')}}" alt=""></label>
                                 <label id="gov_id-error" class="error" for="gov_id" style="display: none"></label>
                              </div>
                           </div>
                           <span class="filename1"></span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 m-b-15">
                           <div class="uplodimg">
                              <div class="uplodimgfil">
                                 <b>Upload Photo</b>
                                 <input type="hidden" name="profile_picture" id="profile_picture">
                                 <input type="file" name="file" id="file-11" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                                 <label for="file-11">Upload Picture <img src="{{ url('public/admin/assets/images/clickhe.png') }}" alt=""></label>


                              </div>
                              <div class="uplodimg_pick uplodimgfilimg">
                                 @if(@$provider->profile_pic != null)
                                 <img src="{{ URL::to('storage/app/public/profile_picture')}}/{{$provider->profile_pic}}" alt="" id="img2">
                                 @else
                                 <img src="{{ URL::to('public/frontend/images/avatar.png')}}" alt="" id="img2">
                                 @endif
											</div>
                           </div>
                           <label id="file-error" class="error" for="file-11"></label>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 m-b-15">
                           <div class="work-image-panel">
                              <div class="agent-right-body">

                                  <div class="row">
                                  <div class="col-md-6 m-b-15">
                                    <div class="uplodimg">
                                        <div class="uplodimgfil">
                                            <b>Work Images</b>
                                            <input type="file" name="file" id="file-15" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                                            <label for="file-15">Upload Images <img src="{{ url('public/admin/assets/images/clickhe.png') }}" alt=""></label>
                                            <label id="file-error" class="error" for="file-15"></label>
                                        </div>
                                    </div>
                                    </div>
                                  </div>




                                 <div class="dash-listnew">
                                    <ul class="edit-gallerynew">
                                    @foreach ($allWorkImage as $image)
                                     <li>
                                    <div class="upimg">
                                        {{-- <img src="{{ {{ url('public/admin/assets/images/w5.png') }}}}"> --}}
                                        <img src="{{ URL::to('storage/app/public/work_image')}}/{{$image->image}}" alt="">
                                        <a href="{{route('admin.provider.remove.work.image',['id'=>@$image->id,'proid'=>$provider->id])}}" onclick="return confirm('Are you sure remove image?')"><img src="{{ url('public/admin/assets/images/w-cross.png')}}"></a>
                                    </div>
                                    </li>
                                @endforeach
                                      {{--<li>
                                          <div class="upimg">
                                             <img src="{{ url('public/admin/assets/images/w5.png') }}">
                                             <a href="#"><img src="{{ url('public/admin/assets/images/w-cross.png') }}"></a>
                                          </div>
                                       </li>
                                       <li>
                                          <div class="upimg">
                                             <img src="{{ url('public/admin/assets/images/w5.png') }}">
                                             <a href="#"><img src="{{ url('public/admin/assets/images/w-cross.png') }}"></a>
                                          </div>
                                       </li>
                                       <li>
                                          <div class="upimg">
                                             <img src="{{ url('public/admin/assets/images/w5.png') }}">
                                             <a href="#"><img src="{{ url('public/admin/assets/images/w-cross.png') }}"></a>
                                          </div>
                                       </li>
                                       <li>
                                          <div class="upimg">
                                             <img src="{{ url('public/admin/assets/images/w5.png') }}">
                                             <a href="#"><img src="{{ url('public/admin/assets/images/w-cross.png') }}"></a>
                                          </div>
                                       </li>--}}
                                       <div class="clearfix"></div>
                                    </ul>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="bodar"></div>
                                    </div>
                                 </div>
                                 {{--
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="usei_sub uppro">
                                          <input type="submit" value="Save &amp; Complete " class="see_cc">
                                       </div>
                                    </div>
                                 </div>
                                 --}}
                              </div>
                           </div>
                           <div class="clearfix"></div>
                        </div>
                        <!--<div class="form-group">
                           <label for="">Class</label>
                           <select class="form-control rm06">
                               <option>Select</option>
                               <option>5</option>
                               <option>6</option>
                               <option>7</option>
                               <option>8</option>
                               <option>9</option>
                               <option>10</option>
                           </select>
                           </div>
                           <div class="form-group">
                           <label for="">Subject</label>
                           <select class="form-control rm06">
                               <option>Select</option>
                               <option>History</option>
                               <option>Geography</option>
                               <option>Mathematics</option>
                               <option>Life Science</option>
                               <option>Physical Science</option>
                               <option>English</option>
                           </select>
                           </div>
                           <div class="clearfix"></div>
                           <div class="form-group">
                           <label for="">Deadline</label>
                           <input type="text" placeholder="Select" id="datepicker" class="form-control calander_icn">
                           </div>

                           <div class="clearfix"></div>
                           <div class="form-group">
                           <label for="">Assignments File Upload (pdf)</label>
                           <div class="fileUpload btn btn-primary cust_file clearfix">
                               <span class="upld_txt"><i class="fa fa-upload upld-icon" aria-hidden="true"></i>Click here to Upload File</span>
                               <input type="file" class="upload">
                           </div>
                           </div>
                           -->
                        <input type="hidden" name="skill" id="skill">
                        <input type="hidden" name="language" id="language">
                        <input type="hidden" name="sub_category" id="sub_category">
                        <div class="clearfix"></div>
                        <div class="col-lg-12"> <button class="btn btn-primary waves-effect waves-light w-md rm_new15" type="submit">Save Change</button></div>
                     </form>
                     <form action="{{route('admin.provider.work.image.save',$provider->id)}}" method="post" id="work_image_galary">
                                  @csrf
                                  <input type="hidden" id="work_image" name="work_image">
</form>
                  </div>
               </div>
            </div>
         </div>
         <!-- End row -->
      </div>
      <!-- container -->
   </div>
   <!-- content -->

   <div class="modal" tabindex="-1" role="dialog" id="croppie-modal-11">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Crop Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="croppie-div-11" style="width: 100%;"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="crop-img-11">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



   <div class="modal" tabindex="-1" role="dialog" id="croppie-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Crop Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="croppie-div" style="width: 100%;"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="crop-img">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

   @include('admin.includes.footer')
</div>
@section('scripts')
@include('admin.includes.scripts')
<script src="{{ URL::asset('public/frontend/croppie/croppie.js') }}"></script>

<script>
    function dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]),
        n = bstr.length,
        u8arr = new Uint8Array(n);
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, {type:mime});
    }
    var uploadCrop;
    $(document).ready(function(){
        $('#croppie-modal-11').on('hidden.bs.modal', function() {
            uploadCrop.croppie('destroy');
        });

        $('#crop-img-11').click(function() {
            uploadCrop.croppie('result', {
                type: 'base64',
                format: 'png'
            }).then(function(base64Str) {
                $("#croppie-modal-11").modal("hide");
               //  $('.lds-spinner').show();
               let file = dataURLtoFile('data:text/plain;'+base64Str+',aGVsbG8gd29ybGQ=','hello.png');
                  console.log(file.mozFullPath);
                  console.log(base64Str);
                  $('#profile_picture').val(base64Str);
               // $.each(file, function(i, f) {
                    var reader = new FileReader();
                    reader.onload = function(e){
                        $('.uplodimgfilimg').append('<em><img  src="' + e.target.result + '"><em>');
                    };
                    reader.readAsDataURL(file);

               //  });
                $('.uplodimgfilimg').show();

            });
        });
    });
    $("#file-11").change(function () {
            $('.uplodimgfilimg').html('');
            let files = this.files;
            console.log(files);
            let img  = new Image();
            if (files.length > 0) {
                let exts = ['image/jpeg', 'image/png', 'image/gif'];
                let valid = true;
                $.each(files, function(i, f) {
                    if (exts.indexOf(f.type) <= -1) {
                        valid = false;
                        return false;
                    }
                });
                if (! valid) {
                    alert('Please choose valid image files (jpeg, png, gif) only.');
                    $("#file-11").val('');
                    return false;
                }
                // img.src = window.URL.createObjectURL(event.target.files[0])
                // img.onload = function () {
                //     if(this.width > 250 || this.height >160) {
                //         flag=0;
                //         alert('Please upload proper image size less then : 250px x 160px');
                //         $("#file").val('');
                //         $('.uploadImg').hide();
                //         return false;
                //     }
                // };
                $("#croppie-modal-11").modal("show");
                uploadCrop = $('.croppie-div-11').croppie({
                    viewport: { width: 256, height: 256, type: 'square' },
                    boundary: { width: $(".croppie-div-11").width(), height: 400 }
                });
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.upload-demo').addClass('ready');
                    // console.log(e.target.result)
                    uploadCrop.croppie('bind', {
                        url: e.target.result
                    }).then(function(){
                        console.log('jQuery bind complete');
                    });
                }
                reader.readAsDataURL(this.files[0]);
               //  $('.uploadImg').append('<img width="100"  src="' + reader.readAsDataURL(this.files[0]) + '">');
               //  $.each(files, function(i, f) {
               //      var reader = new FileReader();
               //      reader.onload = function(e){
               //          $('.uploadImg').append('<img width="100"  src="' + e.target.result + '">');
               //      };
               //      reader.readAsDataURL(f);
               //  });
               //  $('.uploadImg').show();
            }

        });
</script>


 <script>
//    $(document).ready(function(){

//         $('#addForm').validate({

//               rules: {

//                  name: {

//                       required: true,
//                  },
//                   country: {

//                       required: true,
//                  },
//                   state: {

//                       required: true,
//                  },
//                   city: {

//                       required: true,
//                  },
//                  address: {

//                       required: true,
//                  },

//                  file: {

//                      accept: "image/jpg,image/jpeg,image/png"
//                  }

//               },

//               messages: {

//                     file: {

//                      accept: 'You must be select jpg,jpeg and png file'
//                     },
//               }


//         });
//    });

   // var reader = new FileReader();
   //   reader.onload = function (e) {
   // $(".img-preview").html('<img src='+e.target.result+' width="100"/>');
   //   }

   //   function readURL(input) {
   //       if (input.files && input.files[0]) {
   //           reader.readAsDataURL(input.files[0]);
   //       }
   //   }

   //   $("#file-11").change(function(){
   //       readURL(this);
   //   });

    $(document).ready(function(){


           $('#certificate').change(function(event){

                 var x = event.target.files[0].name;
                 $('.filename').text(x);
           });

            $('#gov_id').change(function(event){

                 var x = event.target.files[0].name;
                 $('.filename1').text(x);
           });
    });

     function dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]),
        n = bstr.length,
        u8arr = new Uint8Array(n);
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, {type:mime});
    }
    var uploadCrop;
    $(document).ready(function(){
        $('#croppie-modal').on('hidden.bs.modal', function() {
            uploadCrop.croppie('destroy');
        });

        $('#crop-img').click(function() {
            uploadCrop.croppie('result', {
                type: 'base64',
                format: 'png'
            }).then(function(base64Str) {
                $("#croppie-modal").modal("hide");
               //  $('.lds-spinner').show();
               let file = dataURLtoFile('data:text/plain;'+base64Str+',aGVsbG8gd29ybGQ=','hello.png');
                  console.log(file.mozFullPath);
                  console.log(base64Str);
                  $('#work_image').val(base64Str);
                  $('#work_image_galary').submit();
               // $.each(file, function(i, f) {
            //         var reader = new FileReader();
            //         reader.onload = function(e){
            //             $('.uplodimgfilimg').append('<em><img  src="' + e.target.result + '"><em>');
            //         };
            //         reader.readAsDataURL(file);

            //    //  });
            //     $('.uplodimgfilimg').show();

            });
        });
    });
    $("#file-15").change(function () {
            $('.uplodimgfilimg').html('');
            let files = this.files;
            console.log(files);
            let img  = new Image();
            if (files.length > 0) {
                let exts = ['image/jpeg', 'image/png', 'image/gif'];
                let valid = true;
                $.each(files, function(i, f) {
                    if (exts.indexOf(f.type) <= -1) {
                        valid = false;
                        return false;
                    }
                });
                if (! valid) {
                    alert('Please choose valid image files (jpeg, png, gif) only.');
                    $("#file").val('');
                    return false;
                }
                // img.src = window.URL.createObjectURL(event.target.files[0])
                // img.onload = function () {
                //     if(this.width > 250 || this.height >160) {
                //         flag=0;
                //         alert('Please upload proper image size less then : 250px x 160px');
                //         $("#file").val('');
                //         $('.uploadImg').hide();
                //         return false;
                //     }
                // };
                $("#croppie-modal").modal("show");
                uploadCrop = $('.croppie-div').croppie({
                    viewport: { width: 398, height: 222, type: 'square' },
                    boundary: { width: $(".croppie-div").width(), height: 400 }
                });
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.upload-demo').addClass('ready');
                    // console.log(e.target.result)
                    uploadCrop.croppie('bind', {
                        url: e.target.result
                    }).then(function(){
                        console.log('jQuery bind complete');
                    });
                }
                reader.readAsDataURL(this.files[0]);
               //  $('.uploadImg').append('<img width="100"  src="' + reader.readAsDataURL(this.files[0]) + '">');
               //  $.each(files, function(i, f) {
               //      var reader = new FileReader();
               //      reader.onload = function(e){
               //          $('.uploadImg').append('<img width="100"  src="' + e.target.result + '">');
               //      };
               //      reader.readAsDataURL(f);
               //  });
               //  $('.uploadImg').show();
            }

        });


   </script>
<script>
$(document).ready(function(){
    jQuery.validator.addMethod("validate_word", function(value, element) {
        if (/^([a-zA-Z ])+$/.test(value)) {
            return true;
        } else {
            return false;
        }
    }, "Not allow special characters or numbers");
    jQuery.validator.addMethod("validate_address", function(value, element) {
        if (/^([a-zA-Z0-9./_ ,-])+$/.test(value)) {
            return true;
        } else {
            return false;
        }
    }, "Allow only (a-z, A-Z, 0-9, ./_ ,-)");
    jQuery.validator.addMethod("validate_about", function(value, element) {
        if (/^([a-zA-Z0-9./_ ,-])+$/.test(value)) {
            return true;
        } else {
            return false;
        }
    }, "Allow only (a-z, A-Z, 0-9, ./_ ,-)");
    $('#editForm').validate({
           rules: {

               first_name: {

                     required: true,
                     validate_word:true,
               },
               last_name: {

                     required: true,
                     validate_word:true,
               },
               state: {

                    required: true,
               },
               city: {

                    required: true
               },
               address: {

                    required: true,
                    validate_address:true,
               },
               about: {

                    required: true,
                    validate_about:true,
               },
               whatsapp_no:{
                   required: true,
                   digits: true ,
                   minlength: 10,
                   maxlength: 10,
               },
               budget:{
                   required:true,
                   number: true ,
                   min:1
               },
               experience:{
                   number: true ,
                   min:0
               },
               gov_id:{

                    extension: "docx|doc|pdf|jpg|png|jpeg",
                },
                certificate: {

                    extension: "docx|doc|pdf|jpg|png|jpeg",
                },
           },
           messages: {
               budget:{
                   min:'Minimum amount 1 '
               },
               experienc:{
                   min:'Minimum experience 0 '
               },
               country:{
                required:"Select a country",
            },
            state:{
                required:"Select a state",
            },
            city:{
                required:"Select a city",
            },
            whatsapp_no:{
                required: 'Enter whatsApp Number',
                digits: 'Please enter a valid number ',
                minlength: 'Exactly only 10 digits without country code',
                maxlength: 'Exactly only 10 digits without country code',
            },
            gov_id:{
                extension: "Only allow  pdf, doc, docx, jpeg, gif, png",
            },
            certificate:{
                extension: "Only allow pdf, doc, docx, jpeg, gif, png",
            },
           },
           ignore: [],
           submitHandler:function(form){
               var skill= $('#skill').val();
               var language = $('#language').val();
               var sub_category = $('#sub_category').val();
               if(skill== '' && language == '' && sub_category==''){
                   $('#skill-error').html('Minimum one skill is required');
                   $('#skill-error').css('display', 'block');
                   $('#language-error').html('Minimum one language required');
                   $('#language-error').css('display', 'block');
                   $('#sub_category-error').html('Minimum one sub category required');
                    $('#sub_category-error').css('display', 'block');
                   return false;
               } else if(skill == ''){
                   $('#skill-error').html('Minimum one skill is required');
                   $('#skill-error').css('display', 'block');
                   return false;
               }else if(language == ''){
                   $('#language-error').html('Minimum one language required');
                   $('#language-error').css('display', 'block');
                   return false;
               }else if(sub_category == ''){
                    $('#sub_category-error').html('Minimum one sub category required');
                    $('#sub_category-error').css('display', 'block');
                    return false;
                }
               else{
                   form.submit();
               }
           }
       });
   });
</script>
<script>
   $(document).ready(function() {
   /*all language data push in array*/
   var language = [];
   @foreach($allLanguage as $language )
   language.push({ label : '{{ $language->name }}', value : '{{ $language->id }}'});
   @endforeach
   console.log(language);
   /*end*/

   /*user language data push in array*/
   var languagevalue= [];
   @foreach(@$allUserLanguage as $languages )
   languagevalue.push('{{ $languages->language_id }}');
   @endforeach
   console.log(languagevalue);
   /*end*/
   $('#language').val(languagevalue);


   /*all skill data push in array*/
   var skill = [];
   @foreach($allSkill as $skill )
   skill.push({ label : '{{ $skill->skill_name }}', value : '{{ $skill->id }}'});
   @endforeach
   console.log(skill);
   /*end*/
   /*user skill data push in array*/

   var skillvalue = [];
   @foreach(@$allUserSkill as $skills )
   skillvalue .push('{{ $skills->skills_id }}');
   @endforeach
   console.log(skillvalue);
   /*end*/
   $('#skill').val(skillvalue);

 /*all skill data push in array*/
 var subCategory = [];
    @foreach($allSubCategory as $sub )
    subCategory.push({ label : '{{ $sub->name }}', value : '{{ $sub->id }}'});
    @endforeach
    console.log(subCategory);
    /*end*/
    /*user skill data push in array*/

    var subCategoryvalue = [];
    @foreach(@$mySubCategory as $mySub )
    subCategoryvalue.push('{{ $mySub->category_id }}');
    @endforeach
    console.log(subCategoryvalue);
    /*end*/
    $('#sub_category').val(subCategoryvalue);




   var autocomplete = new SelectPure(".autocomplete-select", {
   options: language,
   value: languagevalue,
   multiple: true,
   autocomplete: true,
   icon: "fa fa-times",
   onChange: value => {
       console.log(value);
       $('#language').val(value);
       if(value==''){
           $('#language-error').html('Minimum one language required');
           $('#language-error').css('display', 'block');
       }else{
           $('#language-error').css('display', 'none');
       }
   },
   classNames: {
     select: "select-pure__select",
     dropdownShown: "select-pure__select--opened",
     multiselect: "select-pure__select--multiple",
     label: "select-pure__label",
     placeholder: "select-pure__placeholder",
     dropdown: "select-pure__options",
     option: "select-pure__option",
     autocompleteInput: "select-pure__autocomplete",
     selectedLabel: "select-pure__selected-label",
     selectedOption: "select-pure__option--selected",
     placeholderHidden: "select-pure__placeholder--hidden",
     optionHidden: "select-pure__option--hidden",
   }
   });
   var resetAutocomplete = function() {
   autocomplete.reset();
   };

   var autocomplete = new SelectPure(".autocomplete-select1", {
   options: skill,
   value: skillvalue,
   multiple: true,
   autocomplete: true,
   icon: "fa fa-times",
   onChange: value => {
       console.log(value);
       $('#skill').val(value);
       if(value==''){
           $('#skill-error').html('Minimum one skill is required');
           $('#skill-error').css('display', 'block');
       }else{
           $('#skill-error').css('display', 'none');
       }
    },
   classNames: {
     select: "select-pure__select",
     dropdownShown: "select-pure__select--opened",
     multiselect: "select-pure__select--multiple",
     label: "select-pure__label",
     placeholder: "select-pure__placeholder",
     dropdown: "select-pure__options",
     option: "select-pure__option",
     autocompleteInput: "select-pure__autocomplete",
     selectedLabel: "select-pure__selected-label",
     selectedOption: "select-pure__option--selected",
     placeholderHidden: "select-pure__placeholder--hidden",
     optionHidden: "select-pure__option--hidden",
   }
   });
   var resetAutocomplete = function() {
   autocomplete.reset();
   };
  
   var autocomplete2 = new SelectPure(".autocomplete-select2", {
        options: subCategory,
        value: subCategoryvalue,
        multiple: true,
        autocomplete: true,
        icon: "fa fa-times",
        onChange: value => {
            console.log(value);
            $('#sub_category').val(value);
            if(value==''){
                $('#sub_category-error').html('Minimum one sub category is required');
                $('#sub_category-error').css('display', 'block');
            }else{
                $('#sub_category-error').css('display', 'none');
            }
         },
        classNames: {
            select: "select-pure__select",
            dropdownShown: "select-pure__select--opened",
            multiselect: "select-pure__select--multiple",
            label: "select-pure__label",
            placeholder: "select-pure__placeholder",
            dropdown: "select-pure__options",
            option: "select-pure__option",
            autocompleteInput: "select-pure__autocomplete",
            selectedLabel: "select-pure__selected-label",
            selectedOption: "select-pure__option--selected",
            placeholderHidden: "select-pure__placeholder--hidden",
            optionHidden: "select-pure__option--hidden",
        }
    });
    var resetAutocomplete = function() {
        autocomplete2.reset();
    };

   });
   $(document).ready(function(){
      $('#country').on('change',function(e){
        e.preventDefault();
        var id = $(this).val();

        $.ajax({
          url:'{{route('get.state')}}',
          type:'GET',
          data:{country:id,id:'{{@$user->state}}'},
          success:function(data){
            console.log(data);
            $('#states').html(data.state);
          }
        })
      });

      $('#states').on('change',function(e){
        e.preventDefault();
        var id = $(this).val();
        $.ajax({
          url:'{{route('get.city')}}',
          type:'GET',
          data:{state:id,id:'{{@$user->city}}'},
          success:function(data){
            console.log(data);
            $('#city').html(data.city);
          }
        })
      });

      $('#category').on('change',function(e){
        e.preventDefault();
        var id = $(this).val();
        $.ajax({
          url:'{{route('get.sub.category')}}',
          type:'GET',
          data:{category:id},
          success:function(data){
            const res = data.sub_category;
            var subCategory = [];
            $.each(res, function (i, v) {
                subCategory.push({ label : v.name, value : ""+v.id+""});
            })
            console.log(subCategory);
            $('#sub_category').val('');
            $('.autocomplete-select2').html('');
            var subCategoryvalue = [];
            var autocomplete2 = new SelectPure(".autocomplete-select2", {
                options: subCategory,
                value: subCategoryvalue,
                multiple: true,
                autocomplete: true,
                icon: "fa fa-times",
                onChange: value => {
                    console.log(value);
                    $('#sub_category').val(value);
                    if(value==''){
                        $('#sub_category-error').html('Minimum one sub category is required');
                        $('#sub_category-error').css('display', 'block');
                    }else{
                        $('#sub_category-error').css('display', 'none');
                    }
                },
                classNames: {
                    select: "select-pure__select",
                    dropdownShown: "select-pure__select--opened",
                    multiselect: "select-pure__select--multiple",
                    label: "select-pure__label",
                    placeholder: "select-pure__placeholder",
                    dropdown: "select-pure__options",
                    option: "select-pure__option",
                    autocompleteInput: "select-pure__autocomplete",
                    selectedLabel: "select-pure__selected-label",
                    selectedOption: "select-pure__option--selected",
                    placeholderHidden: "select-pure__placeholder--hidden",
                    optionHidden: "select-pure__option--hidden",
                }
            });
            var resetAutocomplete = function() {
                autocomplete2.reset();
            };
            // $('#city').html(data.city);
          }
        })
      });

    });

</script>
@endsection
@endsection

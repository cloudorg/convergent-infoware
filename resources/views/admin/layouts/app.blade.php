
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ url('public/admin/assets/images/favicon.ico')}}">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <title>@yield('title')</title>
        
        @yield('links')
        <script src="{{ url('public/admin/assets/js/modernizr.min.js') }}"></script> 
    </head>
    <body class="fixed-left">
        <div id="wrapper" class="enlarged">
            @yield('header')
            @yield('sidebar')
            @yield('content')
        </div>
        @yield('scripts')
    </body>
</html>

@extends('admin.layouts.app')

@section('title')
RiVirtual | Admin | Dashboard
@endsection


@section('links')

@include('admin.includes.links')

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');
</script>

@endsection


@section('header')

@include('admin.includes.header')

@endsection

@section('sidebar')

@include('admin.includes.sidebar')

@endsection
@section('content')
<div class="content-page">
<div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="pull-left page-title">Welcome!</h4>
                                <!--<ol class="breadcrumb pull-right">
                                    <li><a href="#">Aariv School</a></li>
                                    <li class="active">Dashboard</li>
                                </ol>-->
                            </div>
                        </div>

                        <!-- Start Widget -->
                        <div class="row">
                        <div class="col-md-6 col-sm-6 col-lg-3">
                                <div class="mini-stat clearfix bx-shadow bg-white">
                                    <span class="mini-stat-icon bg-info"><i class="fa fa-users" aria-hidden="true"></i></span>
                                    <div class="mini-stat-info text-right text-dark">
                                        <span class="counter text-dark">{{ @$totalAgent }}</span>
                                        Total Agents
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-lg-3">
                                <div class="mini-stat clearfix bx-shadow bg-white">
                                    <span class="mini-stat-icon bg-warning"><i class="fa fa-users" aria-hidden="true"></i></span>
                                    <div class="mini-stat-info text-right text-dark">
                                        <span class="counter text-dark">{{ @$totalUser }}</span>
                                        Total Users
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-lg-3">
                                <div class="mini-stat clearfix bx-shadow bg-white">
                                    <span class="mini-stat-icon bg-pink"><i class="fa fa-users" aria-hidden="true"></i></span>
                                    <div class="mini-stat-info text-right text-dark">
                                        <span class="counter text-dark">{{ @$totalProvider }}</span>
                                        Total Providers
                                    </div>
                                </div>
                            </div>
                           <div class="col-md-6 col-sm-6 col-lg-3">
                                <div class="mini-stat clearfix bx-shadow bg-white">
                                    <span class="mini-stat-icon captio1"><i class="fa fa-text-width" aria-hidden="true"></i></span>
                                    <div class="mini-stat-info text-right text-dark">
                                        <span class="counter text-dark">20544</span>
                                        Captio text
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-lg-3">
                                <div class="mini-stat clearfix bx-shadow bg-white">
                                    <span class="mini-stat-icon captio2"><i class="fa fa-sign-in" aria-hidden="true"></i></span>
                                    <div class="mini-stat-info text-right text-dark">
                                        <span class="counter text-dark">{{ @$totalNewAgent }}</span>
                                        Total New Sign-up Agents 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-lg-3">
                                <div class="mini-stat clearfix bx-shadow bg-white">
                                    <span class="mini-stat-icon captio3"><i class="fa fa-sign-in" aria-hidden="true"></i></span>
                                    <div class="mini-stat-info text-right text-dark">
                                        <span class="counter text-dark">{{ @$totalNewUser }}</span>
                                        Total New Sign-up Users
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-lg-3">
                                <div class="mini-stat clearfix bx-shadow bg-white">
                                    <span class="mini-stat-icon captio4"><i class="fa fa-sign-in" aria-hidden="true"></i></span>
                                    <div class="mini-stat-info text-right text-dark">
                                        <span class="counter text-dark">{{ @$totalNewProvider }}</span>
                                        Total New Sign-up Providers
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-lg-3">
                                <div class="mini-stat clearfix bx-shadow bg-white">
                                    <span class="mini-stat-icon captio5"><i class="fa fa-briefcase" aria-hidden="true"></i></span>
                                    <div class="mini-stat-info text-right text-dark">
                                        <span class="counter text-dark">20544</span>
                                        Total Job Post
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-lg-3">
                                <div class="mini-stat clearfix bx-shadow bg-white">
                                    <span class="mini-stat-icon bg-success"><i class="fa fa-building" aria-hidden="true"></i></span>
                                    <div class="mini-stat-info text-right text-dark">
                                        <span class="counter text-dark">20544</span>
                                        Total Properties
                                    </div>
                                </div>
                            </div>
							<div class="clearfix"></div>
                        
                    </div> <!-- container -->
                               
                </div> <!-- content -->

            </div>
            @include('admin.includes.footer')
      </div>
      @endsection

      @section('scripts')

      @include('admin.includes.scripts')

      <script type="text/javascript">
            /* ==============================================
            Counter Up
            =============================================== */
            jQuery(document).ready(function($) {
                $('.counter').counterUp({
                    delay: 100,
                    time: 1200
                });
            });
        </script>

      @endsection

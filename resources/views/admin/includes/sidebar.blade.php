<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
            <div class="rmn_new_logo_area">
  	<a href="{{ route('admin.dashboard') }}" class="logo"><img src="{{ url('public/admin/assets/images/logo.png') }}" alt=""></a>
  </div>
    <div class="sidebar-inner slimscrollleft">
       
<div id="sidebar-menu">
<ul>
<li>
<a href="{{ route('admin.manage.category') }}" class="waves-effect {{ Route::is('admin.manage.category','admin.add.category','admin.edit.category') ? 'active' : '' }}"><i class="fa fa-tag ri" aria-hidden="true"></i><span> Pro Category</span></a></li>
<li><a href="{{ route('admin.manage.sub.category') }}" class="waves-effect {{ Route::is('admin.manage.sub.category','admin.add.sub.category','admin.edit.sub.category') ? 'active' : '' }}"><i class="fa fa-tag ri" aria-hidden="true"></i><span> Pro Sub Category</span></a></li>
<li><a href="{{ route('admin.manage.language') }}" class="waves-effect {{ Route::is('admin.manage.language','admin.add.language','admin.edit.language') ? 'active' : '' }}"><i class="fa fa-globe ri" aria-hidden="true"></i><span> Languages</span></a></li>
<li><a href="{{ route('admin.manage.state') }}" class="waves-effect {{ Route::is('admin.manage.state','admin.add.state','admin.edit.state') ? 'active' : '' }}"><i class="fa fa-globe ri" aria-hidden="true"></i><span>Manage State</span></a></li>
<li><a href="{{ route('admin.manage.city') }}" class="waves-effect {{ Route::is('admin.manage.city','admin.add.city','admin.edit.city') ? 'active' : '' }}"><i class="fa fa-globe ri" aria-hidden="true"></i><span>Manage City</span></a></li>
<li><a href="{{ route('admin.manage.skill') }}" class="waves-effect {{ Route::is('admin.manage.skill','admin.add.skill','admin.edit.skill') ? 'active' : '' }}"><i class="fa fa-lightbulb-o ri" aria-hidden="true"></i><span> Skills</span></a></li>

<li><a href="{{ route('admin.manage.agent') }}" class="waves-effect {{ Route::is('admin.manage.agent','admin.agent.details','admin.edit.agent.profile','admin.agent.visit.request','admin.agent.availability') ? 'active' : '' }}"><i class="fa fa-cog ri" aria-hidden="true"></i><span> Manage Agents</span></a></li>
<li><a href="{{ route('admin.manage.provider') }}" class="waves-effect {{ Route::is('admin.manage.provider','admin.provider.details','admin.edit.provider.profile') ? 'active' : '' }}"><i class="fa fa-id-card ri" aria-hidden="true"></i><span> Manage Pro</span></a></li>
<li><a href="{{ route('admin.manage.user') }}" class="waves-effect {{ Route::is('admin.manage.user','admin.user.details','admin.edit.user') ? 'active' : '' }}"><i class="fa fa-user ri" aria-hidden="true"></i><span> Manage Users</span></a></li>
<li><a href="{{ route('admin.manage.facilities') }}" class="waves-effect {{ Route::is('admin.manage.facilities','admin.add.facilities','admin.edit.facilities') ? 'active' : '' }}"><i class="fa fa-lightbulb-o ri" aria-hidden="true"></i><span>Facilities/Amenities</span></a></li>
<!--<li><a href="#" class="waves-effect"><i class="fab fa-product-hunt"></i><span> Manage Property</span></a></li>-->
<li><a href="{{ route('admin.manage.property') }}" class="waves-effect {{ Route::is('admin.manage.property','admin.property.details','admin.edit.property','admin.edit.property.image','admin.property.visit.request') ? 'active' : '' }}"><i class="fab fa-product-hunt"></i><span> Manage Property</span></a></li>
<li><a href="{{ route('admin.manage.visit.request') }}" class="waves-effect {{ Route::is('admin.manage.visit.request') ? 'active' : '' }}"><i class="fa fa-user ri" aria-hidden="true"></i><span>Visit Request</span></a></li>
<li>
    <a href="{{ url('/admin/logout') }}" class="waves-effect" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
    <i class="fa fa-sign-out ri" aria-hidden="true"></i><span> Logout </span></a></li>
    <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">

           {{ csrf_field() }}

        </form>
</ul>
<div class="clearfix"></div>
</div>
        


        <div class="clearfix"></div>
    </div>
</div>
            <!-- Left Sidebar End --> 

 @if($errors->any())
        @foreach($errors->all() as $err)
            <p class="alert alert-danger">{{ $err }}</p>
        @endforeach
    @endif

  @if(session()->has('success'))

<div class="alert alert-success alert-dismissible">
<button type="button" class="close" data-dismiss="alert">&times;</button>
<strong>Success!</strong> {{ session('success') }}
</div>
@elseif(session()->has('error'))

<div class="alert alert-danger alert-dismissible">
<button type="button" class="close" data-dismiss="alert">&times;</button>
<strong>Error!</strong> {{ session('error') }}
</div>
@endif 

<div class="alert alert-success vd_hidden success_msg_div" style="display: none;">
<a class="close" data-dismiss="alert" aria-hidden="true">
&times;
</a>
<span class="vd_alert-icon"><i class="fa fa-check-circle"></i></span>
<strong class="success_msg">Success!</strong>
</div>
<div class="alert alert-danger vd_hidden error_msg_div" style="display: none;">
<a class="close" data-dismiss="alert" aria-hidden="true">
&times;
</a>
<span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span>
<strong class="error_msg">Error!</strong>
</div>